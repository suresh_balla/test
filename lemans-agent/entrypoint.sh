#!/bin/sh -e

function inject_variables() {
  for ARGUMENT in "$@"
  do
     KEY=$(echo $ARGUMENT | cut -f1 -d=)
     KEY_LENGTH=${#KEY}
     VALUE="${ARGUMENT:$KEY_LENGTH+1}"
     if [[ ! -z "$KEY" && ! -z "$VALUE" && "$VALUE" != " " && ! $KEY == --* ]]; then
       command="$KEY=$VALUE"
       # echo $command
       export "$command"
     fi
  done
}

function populate_default_variables() {
  export IAC_TYPE=IDEM
  # TODO Check if there is better way
  IAC_VERSION=`idem --version`
  IAC_VERSION=${IAC_VERSION#* }
  echo $IAC_VERSION
  export IAC_VERSION
  export BACKEND_CONFIG_FILE=/run/config/envBackend.yaml
  export ROUTER_CONFIG_FILE=/run/config/router.yaml

  MOUNT_PATH="/config/settings.xml"
  if [ -f $MOUNT_PATH ]; then
    echo "settings.xml is present"
    export MVN_SETTINGS_PATH= "-s "$MOUNT_PATH
  fi
}

function create_fernet_files() {
  echo "Generating Fernet files.."
  create_backend_fernet_file
  create_router_fernet_file
}

function create_router_fernet_file() {
  if [ "$ROUTER_CONFIG_FILE" == "" ]
  then
    echo "current run is not a helm deployment and ROUTER_CONFIG_FILE variable not set. skipping the encryption of router.yaml"
  else
    #get router config file and encrypt it
    OUTPUT_FERNET_FILE="/tmp/router.yaml.fernet"
    echo "current working directory:$(pwd)"
    echo "router.yaml location ""$ROUTER_CONFIG_FILE"
    echo "router.yaml.fernet location "$OUTPUT_FERNET_FILE
    router_key=$(idem encrypt "$ROUTER_CONFIG_FILE" --output-file=$OUTPUT_FERNET_FILE)

    export ROUTER_KEY=$router_key
    export ROUTER_FILE=$OUTPUT_FERNET_FILE
  fi
}

function create_backend_fernet_file() {
  if [ "$BACKEND_CONFIG_FILE" == "" ]
  then
    echo "current run is not a helm deployment and BACKEND_CONFIG_FILE variable not set. skipping the encryption of envBackend.yaml"
  else
    #get acct backend config file and encrypt it
    OUTPUT_FERNET_FILE="/tmp/backend.yaml.fernet"
    echo "current working directory:$(pwd)"
    echo "backend.yaml location ""$BACKEND_CONFIG_FILE"
    echo "backend.yaml.fernet location "$OUTPUT_FERNET_FILE
    acct_key=$(acct encrypt "$BACKEND_CONFIG_FILE" --output-file=$OUTPUT_FERNET_FILE)

    export BACKEND_ACCT_KEY=$acct_key
    export BACKEND_ACCT_FILE=$OUTPUT_FERNET_FILE
  fi
}

function validate_credentials() {
    validate_aws_keys
    validate_azure_keys
    validate_git_keys
}

function validate_aws_keys() {
    #echo "AWS values: $AWS_ACCESS_KEY_ID, $AWS_SECRET_ACCESS_KEY, $AWS_IAM_ASSUME_ROLE_NAME"

    if [[ -z "${AWS_ACCESS_KEY_ID}" && -z "${AWS_SECRET_ACCESS_KEY}" && -z "${AWS_IAM_ASSUME_ROLE_NAME}" ]]; then
        echo "AWS variables are not set. This is OK"
    elif [[ -n "${AWS_ACCESS_KEY_ID}" && -n "${AWS_SECRET_ACCESS_KEY}" && -n "${AWS_IAM_ASSUME_ROLE_NAME}" ]]; then
        echo "AWS variables are supplied !!"
    else
        echo "Not all required AWS parameters are unavailable. Exiting.."
        # TODO
        exit 1
    fi
}

function validate_azure_keys() {
    #echo "Azure values: $AZURE_CLIENT_ID, $AZURE_CLIENT_SECRET, $AZURE_TENANT_ID"

    if [[ -z "${AZURE_CLIENT_ID}" && -z "${AZURE_CLIENT_SECRET}" && -z "${AZURE_TENANT_ID}" ]]; then
        echo "Azure variables are not set. This is OK"
    elif [[ -n "${AZURE_CLIENT_ID}" && -n "${AZURE_CLIENT_SECRET}" && -n "${AZURE_TENANT_ID}" ]]; then
        echo "Azure variables are supplied !!"
    else
        echo "Not all required Azure parameters are unavailable. Exiting.."
        # TODO
        exit 1
    fi
}

function validate_git_keys() {
    #echo "GIT values: $GIT_PERSONAL_TOKEN"

    if [[ -z "${GIT_PERSONAL_TOKEN}" ]]; then
        echo "Git variables are not set. This is OK"
    else
        echo "Git variables are supplied !!"
    fi
}

function setup_java_options() {
    if [ "${DEFAULT_OPTS:-}" == "" ]; then
    DEFAULT_OPTS="\
    -server \
    -showversion \
    -XshowSettings \
    -XX:+UseStringDeduplication \
    -XX:+PrintCommandLineFlags \
    -XX:+PrintFlagsFinal \
    -XX:ErrorFile=$IDEM_WORKER_AGENT_HOME/hprof/error_PIPELINE_%p.log \
    -XX:+HeapDumpOnOutOfMemoryError \
    -XX:HeapDumpPath=$HEAP_FILE_PATH \
    -XX:+ExitOnOutOfMemoryError \
    -Dsun.io.useCanonCaches=false \
    -Djava.awt.headless=true \
    -Dfile.encoding=UTF-8 \
    "
    fi

    if [ -n "$JMX_PORT" ]
    then
      echo "Enabling JMX connection on port: $JMX_PORT"
      JAVA_OPTS="${JAVA_OPTS:-} -Dcom.sun.management.jmxremote \
                                -Dcom.sun.management.jmxremote.port=$JMX_PORT \
                                -Dcom.sun.management.jmxremote.rmi.port=$JMX_PORT \
                                -Djava.rmi.server.hostname=127.0.0.1 \
                                -Dcom.sun.management.jmxremote.ssl=false \
                                -Dcom.sun.management.jmxremote.local.only=false \
                                -Dcom.sun.management.jmxremote.authenticate=false"
    fi

    JVM_HEAP="${JVM_HEAP:-1536m}"

    if [ -n "$JVM_HEAP" ]
    then
      echo "Setting heap to $JVM_HEAP"
      JAVA_OPTS="${JAVA_OPTS:-} -Xms$JVM_HEAP -Xmx$JVM_HEAP"
    fi

    JVM_METASPACE="${JVM_METASPACE:-256m}"

    if [ -n "$JVM_METASPACE" ]
    then
      echo "Setting metaspace to $JVM_METASPACE"
      JAVA_OPTS="${JAVA_OPTS:-} -XX:MetaspaceSize=$JVM_METASPACE -XX:MaxMetaspaceSize=$JVM_METASPACE"
    fi

    JAVA_OPTS="${JAVA_OPTS:-} -XX:+UseParallelGC -XX:ParallelGCThreads=4 -XX:MaxGCPauseMillis=100 -XX:-UseGCOverheadLimit"


    if [ -n "$DEBUG_PORT" ]
    then
      echo "Enabling debugging on port: $DEBUG_PORT"
      JAVA_OPTS="${JAVA_OPTS:-} -Xdebug -Xrunjdwp:transport=dt_socket,suspend=n,server=y,address=$DEBUG_PORT"
    fi

    JAVA_OPTS="${JAVA_OPTS:-} -Dhttp.proxyHost=$http_proxy_server -Dhttp.proxyPort=$http_proxy_port -Dhttp.proxyUser=$http_proxy_username -Dhttp.proxyPassword=$http_proxy_password "

    JAVA_OPTS="${JAVA_OPTS:-} -Dhttps.proxyHost=$http_proxy_server -Dhttps.proxyPort=$http_proxy_port -Dhttps.proxyUser=$http_proxy_username -Dhttps.proxyPassword=$http_proxy_password "

    # Uncomment the below options if the application run fails with jdk17. Below options were suggested as a workaround in https://jira.eng.vmware.com/browse/VRLEM-1508
    #JAVA_OPTS+=" --add-exports=java.base/sun.security.tools.keytool=ALL-UNNAMED "
    #JAVA_OPTS+=" --add-exports=java.base/sun.security.x509=ALL-UNNAMED "

    # List of non-proxy hosts.
    if [ -n "$NO_PROXY" ]
    then
      echo "Setting No Proxy list: $NO_PROXY"
      JAVA_NON_PROXY_HOSTS=`echo $NO_PROXY | sed -r 's/[,]+/|/g'`
      JAVA_OPTS="${JAVA_OPTS:-} -Dhttp.nonProxyHosts=${JAVA_NON_PROXY_HOSTS}"
    fi

    echo "$(date): starting jvm"

    status=0
    set +e

    cd "$IDEM_WORKER_AGENT_HOME"
    # Temporary workaround for LeMans on 17 https://jira.eng.vmware.com/browse/VRAE-51321
    JAVA_OPTS="${DEFAULT_OPTS} ${JAVA_OPTS} --add-exports java.base/sun.security.tools.keytool=ALL-UNNAMED --add-exports java.base/sun.security.x509=ALL-UNNAMED"
}

function heap_dump_collection() {
  HEAP_FILE_PATH=/var/log/hprof

  # copy existing heap dump file to temp location and delete this folder.
  # Check if a new heap dump got created or not. If not, no need to copy the file.
  if [ -d $HEAP_FILE_PATH ]; then
     if [ "$(ls -A $HEAP_FILE_PATH)" ]; then
          if [ -d /var/log/hprof_temp/hprof ]; then
               rm -rf /var/log/hprof_temp/hprof
          fi
          mv $HEAP_FILE_PATH /var/log/hprof_temp/
     fi
  fi

  # Delete the directory with previous heap dump files (which gets taken auto
  # whenever process exits with out of memory error.
  echo "${HEAP_FILE_PATH}"
  rm -rf "${HEAP_FILE_PATH}"

  mkdir -p $HEAP_FILE_PATH $IDEM_WORKER_AGENT_HOME/sandbox || true
}

IDEM_WORKER_AGENT_HOME=/opt/vmware/idem-worker

heap_dump_collection

# Be receptive to core dumps
ulimit -c unlimited

# Allow high connection count per process (raise file descriptor limit)
ulimit -n 65536

setup_java_options

inject_variables $@
populate_default_variables
create_fernet_files
validate_credentials

java ${JAVA_OPTS} \
                  -cp ./ext/*:* \
                  com.vmware.ensemble.idem.worker.WorkerApplication \
                  -Dspring.tracing.web.enabled=true \
                  -Xlog:gc*:/var/log/gc_idem_worker_agent.log:time,level,tags:filecount=1,filesize=50M \
                  "$@"

status=$?
set -e

if [ $status -gt 0 ]
then
  cat /var/log/vmware/idem-service-worker/idem-service-remoteworker.log
fi

echo "$(date): jvm exited with status code ${status}"
exit ${status}

