/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.manager.service;

import static com.vmware.ensemble.idem.manager.service.constants.IacServiceTestConstants.IDEM_TASK_ID1;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.StatusRuntimeException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.vmware.ensemble.idem.manager.service.constants.IacServiceTestConstants;
import com.vmware.ensemble.idem.model.ClaimTaskRequest;
import com.vmware.ensemble.idem.model.ClaimTaskResponse;
import com.vmware.ensemble.idem.model.IAC_PROVIDER;
import com.vmware.ensemble.idem.model.Identity;
import com.vmware.ensemble.idem.model.TaskIdentity;
import com.vmware.ensemble.idem.model.TaskNotificationResponse;
import com.vmware.ensemble.idem.model.WorkerIdentity;
import com.vmware.ensemble.idem.worker.config.WorkerProperties;
import com.vmware.ensemble.idem.worker.util.TaskAvailabilityStreamObserver;
import com.vmware.lemans.client.gateway.GatewayClient;
import com.vmware.lemans.client.gateway.GatewayRequest;
import com.vmware.lemans.client.gateway.GatewayResponse;
import com.vmware.lemans.client.gateway.LemansClient;

import reactor.core.publisher.Sinks;


public class TaskManagerServiceImplTest {

    private Server server;

    @Mock
    GatewayClient gatewayClient;

    @Mock
    LemansClient lemansClient;

    @Mock
    WorkerProperties workerProperties;

    Sinks.Many<TaskNotificationResponse> directSinks;

    TaskManagerServiceGrpc.TaskManagerServiceBlockingStub blockingStub;

    TaskManagerServiceGrpc.TaskManagerServiceStub stub;

    protected final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void init() throws IOException {

        MockitoAnnotations.openMocks(this);
        directSinks = Sinks.many().multicast().onBackpressureBuffer();
        TaskManagerServiceImpl taskManagerService = new TaskManagerServiceImpl(lemansClient, workerProperties, directSinks);
        server = ServerBuilder
                .forPort(4001)
                .addService(taskManagerService.bindService())
                .build();
        server.start();
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", server.getPort())
                .usePlaintext()
                .directExecutor()
                .build();
        blockingStub = TaskManagerServiceGrpc.newBlockingStub(channel);
        stub = TaskManagerServiceGrpc.newStub(channel);
    }

    @AfterEach
    public void after() {
        server.shutdown();
    }

    @Test
    @Timeout(value = 5)
    public void pendingTasksTestSuccess() throws InterruptedException {

        //Creating required inputs
        Identity identity = Identity.newBuilder()
                .setOrgId(IacServiceTestConstants.TEST_ORG_ID)
                .setProjectId(IacServiceTestConstants.TEST_PROJECT_ID)
                .build();
        WorkerIdentity workerIdentity = WorkerIdentity.newBuilder().setIdentity(identity).setWorkerType(WorkerIdentity.WorkerType.REMOTE).build();

        int numberOftasks = 2;
        directSinks.tryEmitNext(TaskNotificationResponse.newBuilder().build());
        directSinks.tryEmitNext(TaskNotificationResponse.newBuilder().build());

        //Creating a Countdown latch
        CountDownLatch latch = new CountDownLatch(numberOftasks);
        TaskAvailabilityStreamObserver streamObserver = new TaskAvailabilityStreamObserver(latch, numberOftasks);

        //Calling the function
        stub.pendingTasks(workerIdentity, streamObserver);
        latch.await();

        //Assertions
        Assertions.assertEquals(2, streamObserver.list.size());
    }

    @Test
    public void claimTaskSuccess() throws IOException {

        //Creating required inputs
        Identity identity = Identity.newBuilder()
                .setOrgId(IacServiceTestConstants.TEST_ORG_ID)
                .setProjectId(IacServiceTestConstants.TEST_PROJECT_ID)
                .build();
        WorkerIdentity workerIdentity = WorkerIdentity.newBuilder().setIdentity(identity).setWorkerType(WorkerIdentity.WorkerType.REMOTE).build();
        ClaimTaskRequest request = ClaimTaskRequest.newBuilder()
                .setWorkerIdentity(workerIdentity)
                .build();
        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("claim-task-responses/claim-task-response-success.json"), GatewayResponse.class);
        CompletableFuture<GatewayResponse> responseFuture = new CompletableFuture<>();
        responseFuture.complete(gatewayResponse);

        //Mocking the function calls
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);
        when(gatewayClient.sendRequest(any(GatewayRequest.class))).thenReturn(responseFuture);

        //Calling the function
        ClaimTaskResponse claimTaskResponse = blockingStub.claimTask(request);

        //Assertions
        Assertions.assertNotNull(claimTaskResponse);
        Assertions.assertTrue(claimTaskResponse.getClaimed());

    }

    @Test
    public void claimTaskFailure() throws IOException {

        //Creating required inputs
        Identity identity = Identity.newBuilder()
                .setOrgId(IacServiceTestConstants.TEST_ORG_ID)
                .setProjectId(IacServiceTestConstants.TEST_PROJECT_ID)
                .build();
        WorkerIdentity workerIdentity = WorkerIdentity.newBuilder().setIdentity(identity).setWorkerType(WorkerIdentity.WorkerType.REMOTE).build();
        ClaimTaskRequest request = ClaimTaskRequest.newBuilder()
                .setWorkerIdentity(workerIdentity)
                .build();
        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("claim-task-responses/claim-task-response-failure.json"), GatewayResponse.class);
        CompletableFuture<GatewayResponse> responseFuture = new CompletableFuture<>();
        responseFuture.complete(gatewayResponse);

        //Mocking the function calls
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);
        when(gatewayClient.sendRequest(any(GatewayRequest.class))).thenReturn(responseFuture);

        //Assertions
        Assertions.assertThrows(StatusRuntimeException.class,() -> blockingStub.claimTask(request));
    }

    @Test
    public void finaliseTaskFailure() {
        Identity identity = Identity.newBuilder()
                .setOrgId(IacServiceTestConstants.TEST_ORG_ID)
                .setProjectId(IacServiceTestConstants.TEST_PROJECT_ID)
                .build();
        TaskIdentity taskIdentity = TaskIdentity.newBuilder().setIdentity(identity).setTaskId(IDEM_TASK_ID1).setIacProvider(IAC_PROVIDER.IDEM).build();
        Assertions.assertThrows(RuntimeException.class, () -> blockingStub.finaliseTask(taskIdentity));
    }
}
