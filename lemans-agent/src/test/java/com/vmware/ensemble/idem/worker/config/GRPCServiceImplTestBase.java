/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.config;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.boot.test.mock.mockito.MockBean;

import com.vmware.lemans.client.gateway.GatewayClient;
import com.vmware.lemans.client.gateway.LemansClient;

public class GRPCServiceImplTestBase {

    @MockBean
    protected GatewayClient gatewayClient;

    @MockBean
    protected LemansClient lemansClient;

    @MockBean
    protected WorkerProperties workerProperties;

    protected final ObjectMapper objectMapper = new ObjectMapper();
}
