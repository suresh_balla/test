/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.server;

import static org.mockito.Mockito.when;

import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.vmware.ensemble.idem.worker.config.GrpcProperties;
import com.vmware.ensemble.idem.worker.config.WorkerProperties;
import com.vmware.ensemble.idem.worker.dto.WorkerRegistrationDetails;

public class IacWorkerBootStrapperTest {
    @Mock
    WorkerProperties workerProperties;

    @Mock
    GrpcServerBootStrapper grpcServerRunner;

    @Mock
    GrpcProperties grpcProperties;

    @Mock
    WorkerRegistrationDetails workerRegistrationDetails;

    @BeforeEach
    public void init()  {
        MockitoAnnotations.openMocks(this);
        when(grpcServerRunner.getServerProperties()).thenReturn(grpcProperties);
        when(grpcProperties.getServerPort()).thenReturn(8081);
        when(grpcProperties.getMaxMessageLength()).thenReturn(4194304);

        when(workerRegistrationDetails.getLemansServiceUri()).thenReturn("https://dummy-uri/");
        when(workerRegistrationDetails.getKey()).thenReturn(UUID.randomUUID().toString());
    }

    @Test
    public void testIsServerAlreadyRunning() {
        IacWorkerBootStrapper iacWorkerBootStrapper = new IacWorkerBootStrapper(workerProperties,grpcServerRunner, workerRegistrationDetails);
        iacWorkerBootStrapper.isRunning.set(true);
        iacWorkerBootStrapper.start();
        Assertions.assertTrue(iacWorkerBootStrapper.isRunning());
        iacWorkerBootStrapper.stop();
    }

    @Test
    public void testServerStart() {

        IacWorkerBootStrapper iacWorkerBootStrapper = new IacWorkerBootStrapper(workerProperties,grpcServerRunner, workerRegistrationDetails);
        iacWorkerBootStrapper.start();
        Assertions.assertTrue(iacWorkerBootStrapper.isRunning());
        iacWorkerBootStrapper.stop();

    }

}
