/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.vmware.ensemble.idem.manager.service.IacServiceImpl;
import com.vmware.ensemble.idem.manager.service.LemansAgentServiceImpl;
import com.vmware.ensemble.idem.manager.service.TaskManagerServiceImpl;
import com.vmware.ensemble.idem.model.TaskNotificationResponse;
import com.vmware.lemans.client.gateway.LemansClient;

import reactor.core.publisher.Sinks;

@ExtendWith(MockitoExtension.class)
public class GrpcAutoConfigurationTest {

    GrpcAutoConfiguration grpcAutoConfiguration;

    @Mock
    LemansClient lemansClient;

    @Mock
    WorkerProperties workerProperties;

    @Mock
    Sinks.Many<TaskNotificationResponse> taskNotificationResponseSinks;

    @Mock
    GrpcProperties grpcProperties;

    @Mock
    TaskManagerServiceImpl taskManagerService;

    @Mock
    IacServiceImpl idemService;

    @Mock
    LemansAgentServiceImpl lemansAgentService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);

        grpcAutoConfiguration = new GrpcAutoConfiguration();
    }

    @Test
    public void taskManagerServiceTest() {
        Assertions.assertNotNull(grpcAutoConfiguration.taskManagerService(lemansClient,workerProperties, taskNotificationResponseSinks));
    }

    @Test
    public void idemServiceTest() {
        Assertions.assertNotNull(grpcAutoConfiguration.idemService(lemansClient));
    }

    @Test
    public void lemansAgentServiceTest() {
        Assertions.assertNotNull(grpcAutoConfiguration.lemansAgentService(lemansClient, workerProperties));
    }

    @Test
    public void grpcServerRunner() {
        Assertions.assertNotNull(grpcAutoConfiguration.grpcServerRunner(grpcProperties,taskManagerService,idemService,lemansAgentService));
    }
}
