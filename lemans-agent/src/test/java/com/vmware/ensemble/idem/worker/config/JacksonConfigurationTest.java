/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class JacksonConfigurationTest {

    JacksonConfiguration jacksonConfiguration;

    @BeforeEach
    public void init() {
        jacksonConfiguration = new JacksonConfiguration();
    }

    @Test
    public void objectMapperTest() {
        Assertions.assertNotNull(jacksonConfiguration.objectMapper());
    }
}
