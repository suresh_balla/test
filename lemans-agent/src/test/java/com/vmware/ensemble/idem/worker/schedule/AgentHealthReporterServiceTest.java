/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.schedule;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.vmware.ensemble.idem.worker.config.GRPCServiceImplTestBase;
import com.vmware.ensemble.idem.worker.config.JacksonConfiguration;
import com.vmware.lemans.client.gateway.GatewayRequest;
import com.vmware.lemans.client.gateway.GatewayResponse;



@SpringBootTest(classes = {AgentHealthReporterService.class, JacksonConfiguration.class})
public class AgentHealthReporterServiceTest extends GRPCServiceImplTestBase {

    @Autowired
    AgentHealthReporterService agentHealthReporterService;

    @Autowired
    ObjectMapper objectMapper;


    @Test
    public void sendHealthInfoTest() throws IOException {

        //Building expected response
        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("event-responses/event-response-success.json"), GatewayResponse.class);
        CompletableFuture<GatewayResponse> responseFuture = new CompletableFuture<>();
        responseFuture.complete(gatewayResponse);

        //Mocking return values
        when(workerProperties.getWorkerId()).thenReturn("worker_idem_1");
        when(workerProperties.getWorkerGroupId()).thenReturn("worker_group_1");
        when(lemansClient.getAgentId()).thenReturn("f2a8ee60-3aa0-42ad-8ae0-6da45c75cd5a");
        when(gatewayClient.sendRequest(any(GatewayRequest.class))).thenReturn(responseFuture);
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);

        //Doing function call
        agentHealthReporterService.sendHealthInfo();

        //Assertions
        verify(lemansClient).getGatewayClient();
        verify(workerProperties).getWorkerId();
        verify(workerProperties).getWorkerGroupId();
        verify(lemansClient).getAgentId();
        verify(gatewayClient).sendRequest(any(GatewayRequest.class));

    }
}
