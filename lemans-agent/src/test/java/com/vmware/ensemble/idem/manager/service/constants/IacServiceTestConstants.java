/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.manager.service.constants;

public interface IacServiceTestConstants {

    String TEST_ORG_ID = "3fd39d28-a8e4-5c0f-9856-c3083f63b35d";

    String TEST_PROJECT_ID = "7e327780-6acd-4e47-93d7-b0a89ea5799a";

    String TEST_WORKER_ID = "24ca8948-d3e4-44d0-a1b2-06a4fdd17e46f";

    String IDEM_TASK_ID1 = "6d9e8ef4-de3b-41a2-9443-f04cf5de8633";

}
