/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ThreadPoolConfigTest {

    ThreadPoolConfig threadPoolConfig;

    @BeforeEach
    public void init() {
        threadPoolConfig = new ThreadPoolConfig();
    }

    @Test
    public void objectMapperTest() {
        Assertions.assertNotNull(threadPoolConfig.taskExecutor());
    }
}
