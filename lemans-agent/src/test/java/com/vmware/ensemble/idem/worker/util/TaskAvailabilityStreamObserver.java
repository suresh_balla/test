/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import io.grpc.stub.ServerCallStreamObserver;

import com.vmware.ensemble.idem.model.TaskNotificationResponse;



public class TaskAvailabilityStreamObserver extends ServerCallStreamObserver<TaskNotificationResponse> {

    CountDownLatch latch;
    Throwable error ;
    public List<TaskNotificationResponse> list = new ArrayList<>();
    Integer numberOftasks;



    public TaskAvailabilityStreamObserver(CountDownLatch latch, Integer numberOftasks) {
        this.latch = latch;
        this.numberOftasks = numberOftasks;
    }

    @Override
    public void onNext(TaskNotificationResponse value) {
        latch.countDown();
        list.add(value);
        if (latch.getCount() == numberOftasks) {
            onCompleted();
        }
    }

    @Override
    public void onError(Throwable t) {
        error = t;
        latch.countDown();
    }

    @Override
    public void onCompleted() {
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public void setOnCancelHandler(Runnable onCancelHandler) {

    }

    @Override
    public void setCompression(String compression) {

    }

    @Override
    public boolean isReady() {
        return false;
    }

    @Override
    public void setOnReadyHandler(Runnable onReadyHandler) {

    }

    @Override
    public void disableAutoInboundFlowControl() {

    }

    @Override
    public void request(int count) {

    }

    @Override
    public void setMessageCompression(boolean enable) {

    }
}