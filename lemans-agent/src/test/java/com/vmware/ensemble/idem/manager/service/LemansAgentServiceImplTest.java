/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.manager.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.vmware.ensemble.idem.model.EventRequest;
import com.vmware.ensemble.idem.model.EventResponse;
import com.vmware.ensemble.idem.worker.config.GRPCServiceImplTestBase;
import com.vmware.lemans.client.gateway.GatewayException;
import com.vmware.lemans.client.gateway.GatewayRequest;
import com.vmware.lemans.client.gateway.GatewayResponse;

@SpringBootTest(classes = LemansAgentServiceImpl.class)
@TestPropertySource(properties = {"LOG_FOLDER=$HOME"})
public class LemansAgentServiceImplTest extends GRPCServiceImplTestBase {

    private ManagedChannel channel;

    private Server server;

    @BeforeEach
    public void init() throws IOException {
        MockitoAnnotations.openMocks(this);
        LemansAgentServiceImpl lemansAgentServiceGrpc = new LemansAgentServiceImpl(lemansClient, workerProperties);
        server = ServerBuilder
                .forPort(8083)
                .addService(lemansAgentServiceGrpc.bindService())
                .build();
        server.start();
        channel = ManagedChannelBuilder
                .forAddress("localhost",
                        8083)
                .usePlaintext()
                .build();
    }

    @AfterEach
    public void after() {
        server.shutdown();
    }

    @Test
    public void postToLemansStreamTestSuccess() throws IOException {

        //Building expected response
        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("event-responses/event-response-success.json"), GatewayResponse.class);
        CompletableFuture<GatewayResponse> responseFuture = new CompletableFuture<>();
        responseFuture.complete(gatewayResponse);

        //Mocking return value of function calls
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);
        when(gatewayClient.sendRequest(any(GatewayRequest.class))).thenReturn(responseFuture);
        when(workerProperties.getStreamName()).thenReturn("test-stream");

        EventRequest eventRequest = EventRequest.newBuilder().setEvent("Test_Event_Success").setEventIdentity("0001354a-b865-4f42-84b0-377eb39b6080").setTaskEventKey("test_run_name").build();

        //Making the postToLemansStream function call
        LemansAgentServiceGrpc.LemansAgentServiceBlockingStub stub = LemansAgentServiceGrpc.newBlockingStub(channel);
        EventResponse response = stub.postToLemansStream(eventRequest);

        //Assertions
        Assertions.assertTrue(response.getSuccess());
        verify(lemansClient).getGatewayClient();
        verify(workerProperties).getStreamName();

        channel.shutdown();
    }

    @Test
    public void postToLemansStreamTestFailure() throws IOException {

        //Building expected response
        CompletableFuture<GatewayResponse> responseFuture = new CompletableFuture<>();
        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("event-responses/event-response-failure.json"), GatewayResponse.class);
        responseFuture.completeExceptionally(new GatewayException(gatewayResponse, "Error sending gateway request", new RuntimeException()));

        //Mocking return value of function calls
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);
        when(gatewayClient.sendRequest(any(GatewayRequest.class))).thenReturn(responseFuture);
        when(workerProperties.getStreamName()).thenReturn("test-stream");

        EventRequest eventRequest = EventRequest.newBuilder().setEvent("Test_Event_Failure").setEventIdentity("344e9994-2a07-4245-a46b-2ffea56be0bd").setTaskEventKey("test_run_name").build();

        LemansAgentServiceGrpc.LemansAgentServiceBlockingStub stub = LemansAgentServiceGrpc.newBlockingStub(channel);
        assertThrows(RuntimeException.class,
                () -> stub.postToLemansStream(eventRequest));

        //Assertions
        verify(lemansClient).getGatewayClient();
        verify(workerProperties).getStreamName();
        channel.shutdown();
    }

    @Test
    public void postToLemansStreamWithInvalidInputTest()  {

        EventRequest eventRequest = EventRequest.newBuilder().setEvent("").setEventIdentity("144e9994-2a07-4245-a46b-2ffea56be0bd").setTaskEventKey("").build();

        LemansAgentServiceGrpc.LemansAgentServiceBlockingStub stub = LemansAgentServiceGrpc.newBlockingStub(channel);
        EventResponse response = stub.postToLemansStream(eventRequest);

        //Assertions
        Assertions.assertFalse(response.getSuccess());
        Assertions.assertEquals("Event Request should contain a valid event data and task event key for LeMans Stream", response.getErrorMessage());
        channel.shutdown();
    }


}
