/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.server;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.concurrent.CountDownLatch;

import io.grpc.Server;
import io.grpc.ServerServiceDefinition;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.vmware.ensemble.idem.manager.service.IacServiceImpl;
import com.vmware.ensemble.idem.manager.service.LemansAgentServiceImpl;
import com.vmware.ensemble.idem.manager.service.TaskManagerServiceImpl;
import com.vmware.ensemble.idem.worker.config.GrpcProperties;


public class GrpcServerBootStrapperTest {

    GrpcProperties grpcProperties;

    @Mock
    TaskManagerServiceImpl taskManagerService;

    @Mock
    IacServiceImpl iacService;

    @Mock
    LemansAgentServiceImpl lemansAgentService;

    @BeforeEach
    public void init()  {
        MockitoAnnotations.openMocks(this);
        grpcProperties = new GrpcProperties();
        grpcProperties.setServerPort(4002);
        grpcProperties.setMaxMessageLength(4194304);

    }

    @Test
    public void testStopServerFailure() throws InterruptedException {
        Server server1 = Mockito.mock(Server.class);
        CountDownLatch latch = Mockito.mock(CountDownLatch.class);
        GrpcServerBootStrapper grpcServerBootStrapper = new GrpcServerBootStrapper(grpcProperties,taskManagerService, iacService,lemansAgentService);
        grpcServerBootStrapper.server = server1;
        grpcServerBootStrapper.latch = latch;
        doThrow(new InterruptedException()).when(server1).awaitTermination();
        grpcServerBootStrapper.stop();
    }

    @Test
    public void testIsServerAlreadyRunning() {
        GrpcServerBootStrapper grpcServerBootStrapper = new GrpcServerBootStrapper(grpcProperties,taskManagerService, iacService,lemansAgentService);
        grpcServerBootStrapper.isRunning.set(true);
        grpcServerBootStrapper.start();
        Assertions.assertTrue(grpcServerBootStrapper.isRunning());
        grpcServerBootStrapper.stop();
    }

    @Test
    public void testServerStart() {
        GrpcServerBootStrapper grpcServerBootStrapper = new GrpcServerBootStrapper(grpcProperties,taskManagerService, iacService,lemansAgentService);
        when(iacService.bindService())
                .thenReturn(ServerServiceDefinition.builder("iacService")
                        .build());
        when(taskManagerService.bindService())
                .thenReturn(ServerServiceDefinition.builder("taskManagerService")
                        .build());
        when(lemansAgentService.bindService())
                .thenReturn(ServerServiceDefinition.builder("lemansAgentService")
                        .build());

        grpcServerBootStrapper.start();
        grpcServerBootStrapper.stop();

    }


}
