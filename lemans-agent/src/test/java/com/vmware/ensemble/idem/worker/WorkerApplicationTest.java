/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import org.mockito.Mockito;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

class WorkerApplicationTest {

    @Test
    void main_success() {

        try (MockedConstruction<SpringApplicationBuilder> mockAppBuilder = Mockito.mockConstruction(SpringApplicationBuilder.class,
                (mock, context) -> {
                    when(mock.sources(any())).thenReturn(mock);
                    when(mock.resourceLoader(any())).thenReturn(mock);
                    when(mock.web(any())).thenReturn(mock);
                    when(mock.run(any())).thenReturn(Mockito.mock(ConfigurableApplicationContext.class));
                })) {
            WorkerApplication.main(new String[]{"test"});
        }
    }

}


