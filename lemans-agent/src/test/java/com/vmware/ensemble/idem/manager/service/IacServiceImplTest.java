/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.manager.service;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.StatusRuntimeException;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.vmware.ensemble.idem.manager.service.constants.IacServiceTestConstants;
import com.vmware.ensemble.idem.model.BundleChunk;
import com.vmware.ensemble.idem.model.Identity;
import com.vmware.ensemble.idem.model.OldState;
import com.vmware.ensemble.idem.model.TaskIdentity;
import com.vmware.ensemble.idem.model.TaskUpdateRequest;
import com.vmware.ensemble.idem.model.TaskUpdateResponse;
import com.vmware.ensemble.idem.model.ValidateTaskUpdateRequest;
import com.vmware.ensemble.idem.model.ValidateTaskUpdateResponse;
import com.vmware.ensemble.idem.service.IdemServiceGrpc;
import com.vmware.ensemble.idem.worker.config.GRPCServiceImplTestBase;
import com.vmware.lemans.client.gateway.GatewayRequest;
import com.vmware.lemans.client.gateway.GatewayResponse;

@SpringBootTest(classes = IacServiceImpl.class)
@TestPropertySource(properties = {"LOG_FOLDER=$HOME"})
public class IacServiceImplTest extends GRPCServiceImplTestBase {

    private ManagedChannel channel;

    private Server server;

    @BeforeEach
    public void init() throws IOException {
        MockitoAnnotations.openMocks(this);
        IacServiceImpl iacServiceGrpc = new IacServiceImpl(lemansClient);
        server = ServerBuilder
                .forPort(8083)
                .addService(iacServiceGrpc.bindService())
                .build();
        server.start();
        channel = ManagedChannelBuilder
                .forAddress("localhost",
                        8083)
                .usePlaintext()
                .build();
    }

    @AfterEach
    public void after() {
        server.shutdown();
    }

    @Test
    public void downloadBundleTestSuccess() throws IOException {

        //Creating required inputs
        Identity identity = Identity.newBuilder()
                .setOrgId(IacServiceTestConstants.TEST_ORG_ID)
                .setProjectId(IacServiceTestConstants.TEST_PROJECT_ID)
                .build();
        TaskIdentity taskIdentity = TaskIdentity.newBuilder()
                .setIdentity(identity)
                .setTaskId(IacServiceTestConstants.IDEM_TASK_ID1)
                .build();
        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("sample-download-bundle/sample-bundle.json"), GatewayResponse.class);
        CompletableFuture<GatewayResponse> responseFuture = new CompletableFuture<>();
        responseFuture.complete(gatewayResponse);

        //Mocking the function calls
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);
        when(gatewayClient.sendRequest(any(GatewayRequest.class))).thenReturn(responseFuture);

        //Calling the function
        IdemServiceGrpc.IdemServiceBlockingStub stub = IdemServiceGrpc.newBlockingStub(channel);
        Iterator<BundleChunk> response = stub.downloadBundle(taskIdentity);

        //Assertions
        assertTrue(response.hasNext());
        BundleChunk bundleChunk = response.next();
        Assertions.assertEquals("application/yaml", bundleChunk.getContentType());
        Assertions.assertEquals("mockContent.sls", bundleChunk.getFileName());
        Assertions.assertNotNull(bundleChunk.getData());
    }

    @Test
    public void downloadBundleTestFailure() throws IOException {

        //Creating required inputs
        Identity identity = Identity.newBuilder()
                .setOrgId(IacServiceTestConstants.TEST_ORG_ID)
                .setProjectId(IacServiceTestConstants.TEST_PROJECT_ID)
                .build();
        TaskIdentity taskIdentity = TaskIdentity.newBuilder()
                .setIdentity(identity)
                .setTaskId(IacServiceTestConstants.IDEM_TASK_ID1)
                .build();
        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("sample-download-bundle/sample-bundle-failure.json"), GatewayResponse.class);
        CompletableFuture<GatewayResponse> responseFuture = new CompletableFuture<>();
        responseFuture.complete(gatewayResponse);

        //Mocking the function calls
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);
        when(gatewayClient.sendRequest(any(GatewayRequest.class))).thenReturn(responseFuture);

        IdemServiceGrpc.IdemServiceBlockingStub stub = IdemServiceGrpc.newBlockingStub(channel);

        Assert.assertThrows(RuntimeException.class, () -> stub.downloadBundle(taskIdentity).hasNext());

    }

    @Test
    public void taskUpdateTestSuccess() throws IOException {

        //Creating required inputs
        Identity identity = Identity.newBuilder()
                .setOrgId(IacServiceTestConstants.TEST_ORG_ID)
                .setProjectId(IacServiceTestConstants.TEST_PROJECT_ID)
                .build();
        TaskIdentity taskIdentity = TaskIdentity.newBuilder()
                .setIdentity(identity)
                .setTaskId(IacServiceTestConstants.IDEM_TASK_ID1)
                .build();
        TaskUpdateRequest request = TaskUpdateRequest.newBuilder()
                .setTaskIdentity(taskIdentity)
                .setStatus("RUNNING")
                .setWorkerId(IacServiceTestConstants.TEST_WORKER_ID).build();
        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("task-update-responses/task-update-response-success.json"), GatewayResponse.class);
        CompletableFuture<GatewayResponse> responseFuture = new CompletableFuture<>();
        responseFuture.complete(gatewayResponse);

        //Mocking the function calls
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);
        when(gatewayClient.sendRequest(any(GatewayRequest.class))).thenReturn(responseFuture);

        IdemServiceGrpc.IdemServiceBlockingStub stub = IdemServiceGrpc.newBlockingStub(channel);
        TaskUpdateResponse taskUpdateResponse = stub.taskUpdate(request);
        Assertions.assertTrue(taskUpdateResponse.getIsUpdated());
    }

    @Test
    public void taskUpdateTestFailure() throws IOException {

        //Creating required inputs
        Identity identity = Identity.newBuilder()
                .setOrgId(IacServiceTestConstants.TEST_ORG_ID)
                .setProjectId(IacServiceTestConstants.TEST_PROJECT_ID)
                .build();
        TaskIdentity taskIdentity = TaskIdentity.newBuilder()
                .setIdentity(identity)
                .setTaskId(IacServiceTestConstants.IDEM_TASK_ID1)
                .build();
        TaskUpdateRequest request = TaskUpdateRequest.newBuilder()
                .setTaskIdentity(taskIdentity)
                .setStatus("RUNNING")
                .setWorkerId(IacServiceTestConstants.TEST_WORKER_ID).build();
        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("task-update-responses/task-update-response-failure.json"), GatewayResponse.class);
        CompletableFuture<GatewayResponse> responseFuture = new CompletableFuture<>();
        responseFuture.complete(gatewayResponse);

        //Mocking the function calls
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);
        when(gatewayClient.sendRequest(any(GatewayRequest.class))).thenReturn(responseFuture);

        IdemServiceGrpc.IdemServiceBlockingStub stub = IdemServiceGrpc.newBlockingStub(channel);
        Assert.assertThrows(RuntimeException.class, () -> stub.taskUpdate(request));
    }

    @Test
    public void downloadOldStateTestSuccess() throws IOException {

        //Creating required inputs
        Identity identity = Identity.newBuilder()
                .setOrgId(IacServiceTestConstants.TEST_ORG_ID)
                .setProjectId(IacServiceTestConstants.TEST_PROJECT_ID)
                .build();
        TaskIdentity taskIdentity = TaskIdentity.newBuilder()
                .setIdentity(identity)
                .setTaskId(IacServiceTestConstants.IDEM_TASK_ID1)
                .build();
        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("old-state-responses/old-state.json"), GatewayResponse.class);
        CompletableFuture<GatewayResponse> responseFuture = new CompletableFuture<>();
        responseFuture.complete(gatewayResponse);

        //Mocking the function calls
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);
        when(gatewayClient.sendRequest(any(GatewayRequest.class))).thenReturn(responseFuture);

        IdemServiceGrpc.IdemServiceBlockingStub stub = IdemServiceGrpc.newBlockingStub(channel);
        Iterator<OldState> response = stub.downloadOldState(taskIdentity);

        //Assertions
        assertTrue(response.hasNext());
        OldState oldState = response.next();
        Assertions.assertEquals("application/yaml", oldState.getContentType());
        Assertions.assertNotNull(oldState.getData());
    }

    @Test
    public void downloadOldStateTestBlankResponseSuccess() throws IOException {

        //Creating required inputs
        Identity identity = Identity.newBuilder()
                .setOrgId(IacServiceTestConstants.TEST_ORG_ID)
                .setProjectId(IacServiceTestConstants.TEST_PROJECT_ID)
                .build();
        TaskIdentity taskIdentity = TaskIdentity.newBuilder()
                .setIdentity(identity)
                .setTaskId(IacServiceTestConstants.IDEM_TASK_ID1)
                .build();
        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("old-state-responses/old-state-blank.json"), GatewayResponse.class);
        CompletableFuture<GatewayResponse> responseFuture = new CompletableFuture<>();
        responseFuture.complete(gatewayResponse);

        //Mocking the function calls
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);
        when(gatewayClient.sendRequest(any(GatewayRequest.class))).thenReturn(responseFuture);

        IdemServiceGrpc.IdemServiceBlockingStub stub = IdemServiceGrpc.newBlockingStub(channel);
        Iterator<OldState> response = stub.downloadOldState(taskIdentity);

        //Assertions
        assertFalse(response.hasNext());
    }

    @Test
    public void downloadOldStateTestMultipleStatesSuccess() throws IOException {

        //Creating required inputs
        Identity identity = Identity.newBuilder()
                .setOrgId(IacServiceTestConstants.TEST_ORG_ID)
                .setProjectId(IacServiceTestConstants.TEST_PROJECT_ID)
                .build();
        TaskIdentity taskIdentity = TaskIdentity.newBuilder()
                .setIdentity(identity)
                .setTaskId(IacServiceTestConstants.IDEM_TASK_ID1)
                .build();
        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("old-state-responses/old-state-multiple.json"), GatewayResponse.class);
        CompletableFuture<GatewayResponse> responseFuture = new CompletableFuture<>();
        responseFuture.complete(gatewayResponse);

        //Mocking the function calls
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);
        when(gatewayClient.sendRequest(any(GatewayRequest.class))).thenReturn(responseFuture);

        IdemServiceGrpc.IdemServiceBlockingStub stub = IdemServiceGrpc.newBlockingStub(channel);
        Iterator<OldState> response = stub.downloadOldState(taskIdentity);

        //Assertions
        int count = 0;
        while (count < 6) {
            assertTrue(response.hasNext());
            OldState oldState = response.next();
            Assertions.assertEquals("application/json", oldState.getContentType());
            Assertions.assertNotNull(oldState.getData());
            count++;
        }
        assertFalse(response.hasNext());
    }

    @Test
    public void downloadOldStateTestFailure() throws IOException {

        //Creating required inputs
        Identity identity = Identity.newBuilder()
                .setOrgId(IacServiceTestConstants.TEST_ORG_ID)
                .setProjectId(IacServiceTestConstants.TEST_PROJECT_ID)
                .build();
        TaskIdentity taskIdentity = TaskIdentity.newBuilder()
                .setIdentity(identity)
                .setTaskId(IacServiceTestConstants.IDEM_TASK_ID1)
                .build();
        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("old-state-responses/old-state-failure.json"), GatewayResponse.class);
        CompletableFuture<GatewayResponse> responseFuture = new CompletableFuture<>();
        responseFuture.complete(gatewayResponse);

        //Mocking the function calls
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);
        when(gatewayClient.sendRequest(any(GatewayRequest.class))).thenReturn(responseFuture);

        IdemServiceGrpc.IdemServiceBlockingStub stub = IdemServiceGrpc.newBlockingStub(channel);
        Assert.assertThrows(StatusRuntimeException.class, () -> stub.downloadOldState(taskIdentity).hasNext());
    }

    @Test
    public void updateValidateTaskTestSuccess() throws IOException {

        //Creating required inputs
        Identity identity = Identity.newBuilder()
                .setOrgId(IacServiceTestConstants.TEST_ORG_ID)
                .setProjectId(IacServiceTestConstants.TEST_PROJECT_ID)
                .build();
        TaskIdentity taskIdentity = TaskIdentity.newBuilder()
                .setIdentity(identity)
                .setTaskId(IacServiceTestConstants.IDEM_TASK_ID1)
                .build();
        ValidateTaskUpdateRequest validateTaskUpdateRequest = ValidateTaskUpdateRequest.newBuilder()
                .setTaskIdentity(taskIdentity)
                .setStatus("COMPLETED")
                .setResponse(Map.of("high", " test values", "low", "test values", "parameters", "params").toString())
                .build();

        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("task-update-responses/task-update-response-success.json"), GatewayResponse.class);
        CompletableFuture<GatewayResponse> responseFuture = new CompletableFuture<>();
        responseFuture.complete(gatewayResponse);

        //Mocking the function calls
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);
        when(gatewayClient.sendRequest(any(GatewayRequest.class))).thenReturn(responseFuture);

        IdemServiceGrpc.IdemServiceBlockingStub stub = IdemServiceGrpc.newBlockingStub(channel);
        ValidateTaskUpdateResponse validateTaskUpdateResponse = stub.updateValidateTask(validateTaskUpdateRequest);
        Assertions.assertTrue(validateTaskUpdateResponse.getIsUpdated());
    }

    @Test
    public void updateValidateTaskTestFailure() throws IOException {

        //Creating required inputs
        Identity identity = Identity.newBuilder()
                .setOrgId(IacServiceTestConstants.TEST_ORG_ID)
                .setProjectId(IacServiceTestConstants.TEST_PROJECT_ID)
                .build();
        TaskIdentity taskIdentity = TaskIdentity.newBuilder()
                .setIdentity(identity)
                .setTaskId(IacServiceTestConstants.IDEM_TASK_ID1)
                .build();
        ValidateTaskUpdateRequest validateTaskUpdateRequest = ValidateTaskUpdateRequest.newBuilder()
                .setTaskIdentity(taskIdentity)
                .setStatus("COMPLETED")
                .setResponse(Map.of("high", " test values", "low", "test values", "parameters", "params").toString())
                .build();

        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("task-update-responses/task-update-response-failure.json"), GatewayResponse.class);
        CompletableFuture<GatewayResponse> responseFuture = new CompletableFuture<>();
        responseFuture.complete(gatewayResponse);

        //Mocking the function calls
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);
        when(gatewayClient.sendRequest(any(GatewayRequest.class))).thenReturn(responseFuture);

        IdemServiceGrpc.IdemServiceBlockingStub stub = IdemServiceGrpc.newBlockingStub(channel);
        Assertions.assertThrows(StatusRuntimeException.class, () -> stub.updateValidateTask(validateTaskUpdateRequest));
    }
}
