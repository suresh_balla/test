/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.util;

import static com.vmware.ensemble.idem.manager.service.constants.IacServiceLeMansConstants.LEMANS_STREAM_CUSTOM_KEY_HEADER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.any;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.vmware.ensemble.idem.worker.config.GRPCServiceImplTestBase;
import com.vmware.lemans.client.gateway.GatewayOperation;
import com.vmware.lemans.client.gateway.GatewayRequest;
import com.vmware.lemans.client.gateway.GatewayResponse;

@SpringBootTest(classes = LeMansGatewayUtil.class)
@TestPropertySource(properties = {"LOG_FOLDER=$HOME"})
public class LemansGatewayUtilTest extends GRPCServiceImplTestBase {

    @Captor
    private ArgumentCaptor<GatewayRequest> gatewayRequestCaptor;

    @Test
    public void testPerformAction() throws IOException, ExecutionException, InterruptedException, URISyntaxException {

        // Building required Inputs and expected Outputs
        String apiSelector = "streams";
        String apiPath = "test-stream";
        String payload = Files.readString(Path.of(this.getClass().getClassLoader().getResource("event-responses/event-payload.json").toURI()));
        Map<String, String> headersMap = new HashMap<>();
        headersMap.put(LEMANS_STREAM_CUSTOM_KEY_HEADER,"test_run_name");
        GatewayResponse gatewayResponse = objectMapper.readValue(this.getClass().getClassLoader().getResource("event-responses/event-response-success.json"), GatewayResponse.class);
        String expectedResponse = "{\"success\":true}";

        CompletableFuture<String> responseFuture = new CompletableFuture<>();

        final Consumer<String> successConsumer = (response) -> responseFuture.complete(response);
        final Consumer<Throwable> errorHandler = Throwable::printStackTrace;

        //Mocking return value of function calls
        when(lemansClient.getGatewayClient()).thenReturn(gatewayClient);
        when(gatewayClient.sendRequest(any(GatewayRequest.class)))
                .thenReturn(CompletableFuture.completedFuture(gatewayResponse));

        LeMansGatewayUtil.performAction(lemansClient, apiSelector, apiPath, payload, headersMap,successConsumer,
                errorHandler);

        //Assertions
        Assertions.assertTrue(responseFuture.isDone());
        assertEquals(expectedResponse, responseFuture.get());

        verify(lemansClient).getGatewayClient();
        verify(gatewayClient).sendRequest(gatewayRequestCaptor.capture());

        GatewayRequest capturedGatewayRequest = gatewayRequestCaptor.getValue();
        assertEquals(GatewayOperation.Action.POST, capturedGatewayRequest.getAction());
        assertEquals(URI.create("/" + apiSelector + "/" + apiPath), capturedGatewayRequest.getUri());
        assertEquals(payload, capturedGatewayRequest.getBody());
        assertEquals(headersMap, capturedGatewayRequest.getHeaders());

    }
}









