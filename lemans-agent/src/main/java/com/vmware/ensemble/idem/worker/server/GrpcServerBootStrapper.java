/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.server;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.netty.NettyServerBuilder;

import org.springframework.context.SmartLifecycle;

import com.vmware.ensemble.idem.manager.service.IacServiceImpl;
import com.vmware.ensemble.idem.manager.service.LemansAgentServiceImpl;
import com.vmware.ensemble.idem.manager.service.TaskManagerServiceImpl;
import com.vmware.ensemble.idem.worker.config.GrpcProperties;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * GRPC Implementation for server
 */
//TODO: check if application comes down if server is not up
@Slf4j
public class GrpcServerBootStrapper implements SmartLifecycle {

    final AtomicBoolean isRunning = new AtomicBoolean(false);

    @Getter
    GrpcProperties serverProperties;

    TaskManagerServiceImpl taskManagerService;

    IacServiceImpl idemService;

    LemansAgentServiceImpl lemansAgentService;

    Server server;

    CountDownLatch latch;

    public GrpcServerBootStrapper(GrpcProperties grpcProperties, TaskManagerServiceImpl taskManagerService, IacServiceImpl idemService, LemansAgentServiceImpl lemansAgentService) {
        this.serverProperties = grpcProperties;
        this.taskManagerService = taskManagerService;
        this.idemService = idemService;
        this.lemansAgentService = lemansAgentService;
    }


    @Override
    public void start() {
        if (isRunning()) {
            return;
        }
        log.info("Starting gRPC service on port {} ....", serverProperties.getServerPort());

        try {
            latch = new CountDownLatch(1);
            ServerBuilder<NettyServerBuilder> serverBuilder = NettyServerBuilder
                    .forPort(serverProperties.getServerPort())
                    .maxConnectionAge(10, TimeUnit.MINUTES)
                    // Added the max message size for gRPC to accommodate larger payloads
                    // by picking its value from config file. Can be reviewed and adjusted as necessary in the future.
                    .maxInboundMessageSize(serverProperties.getMaxMessageLength())
                    .addService(taskManagerService.bindService())
                    .addService(idemService.bindService())
                    .addService(lemansAgentService.bindService())
                    .permitKeepAliveWithoutCalls(true)
                    .executor(Executors.newFixedThreadPool(8,
                            new ThreadFactoryBuilder()
                                    .setNameFormat("grpc-service-%d")
                                    .setDaemon(true)
                                    .build()));

            server = serverBuilder.build();
            server.start();
            isRunning.set(true);
            startDaemonAwaitThread();
            log.info("gRPC Server started, listening on port {}.", server.getPort());
        } catch (IOException e) {
            log.error("Error occurred while starting the gRPC server {}", e);
            //TODO: shutdown agent too
            throw new RuntimeException(e);
        }

    }

    @Override
    public void stop() {
        log.info("Shutting down gRPC server ...");
        Optional.ofNullable(server).ifPresent(s -> {
            s.shutdown();
            try {
                s.awaitTermination();
            } catch (InterruptedException e) {
                log.error("Error occurred while stoping gRPC server %s", e);

            } finally {
                latch.countDown();
            }
        });
    }

    @Override
    public boolean isRunning() {
        return isRunning.get();
    }

    private void startDaemonAwaitThread() {
        Thread awaitThread = new Thread(() -> {
            try {

                latch.await();
            } catch (InterruptedException e) {
                log.error("gRPC server awaiter interrupted.", e);
            } finally {
                isRunning.set(false);
            }
        });
        awaitThread.setName("grpc-server-awaiter");
        awaitThread.setDaemon(false);
        awaitThread.start();
    }

    @Override
    public int getPhase() {
        return serverProperties.getStartUpPhase();
    }
}
