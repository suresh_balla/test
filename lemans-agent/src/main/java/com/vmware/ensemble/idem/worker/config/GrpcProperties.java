/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.SmartLifecycle;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Configuration
public class GrpcProperties {

    @Value("${grpc.server.port:${GRPC_PROXY_SERVER_PORT}}")
    Integer serverPort;

    @Value("${grpc.server.maxMessageLength}")
    Integer maxMessageLength;

    private int startUpPhase = SmartLifecycle.DEFAULT_PHASE;
}
