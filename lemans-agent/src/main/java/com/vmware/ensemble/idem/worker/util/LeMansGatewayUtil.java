/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.util;

import java.net.URI;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import com.vmware.lemans.client.gateway.GatewayClient;
import com.vmware.lemans.client.gateway.GatewayOperation;
import com.vmware.lemans.client.gateway.GatewayRequest;
import com.vmware.lemans.client.gateway.GatewayResponse;
import com.vmware.lemans.client.gateway.LemansClient;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LeMansGatewayUtil {

    public static void performAction(LemansClient lemansClient, String apiSelector, String apiPath,
                                     String payload, Map<String, String> headersMap,
                                     Consumer<String> responseHandler,
                                     Consumer<Throwable> errorHandler) {

        URI uri = URI.create(String.format("/%s/%s", apiSelector, apiPath));
        performAction(lemansClient, uri, payload, headersMap, responseHandler, errorHandler);
    }

    public static void performAction(LemansClient lemansClient, URI uri,
                                     String payload, Map<String, String> headersMap,
                                     Consumer<String> responseHandler,
                                     Consumer<Throwable> errorHandler) {

        callLeMansGateway(lemansClient, uri, payload, headersMap, (gatewayResponse, throwable) -> {
            if (throwable != null) {
                log.error("Exception occurred while performing gateway request, payload {}" +
                          "selector & path {}, cause : {}" , payload, uri, throwable);
                errorHandler.accept(throwable);
            } else {
                try {
                    if (gatewayResponse.getBody() != null) {
                        responseHandler.accept(gatewayResponse.getBody(String.class));
                    } else {
                        log.debug("gatewayResponse.getBody() is null");
                        responseHandler.accept(null);
                    }
                } catch (Exception e) {
                    log.error("Exception occurred while parsing response, payload {}" +
                              "selector & path {},  cause : {}" , payload, uri, e);
                    errorHandler.accept(e);
                }
            }
        });
    }

    private static void callLeMansGateway(LemansClient lemansClient, URI uri, String payload, Map<String, String> headersMap,
                                          BiConsumer<GatewayResponse, Throwable> handler) {
        log.debug("Sending request to gateway with uri {}, payload {}", uri.toString(), payload);
        GatewayClient gatewayClient = lemansClient.getGatewayClient();
        GatewayRequest gatewayRequest = GatewayRequest.createRequest(GatewayOperation.Action.POST, uri)
                .setBody(payload)
                .addHeaders(headersMap);
        gatewayClient.sendRequest(gatewayRequest)
                .whenComplete(handler);

    }
}
