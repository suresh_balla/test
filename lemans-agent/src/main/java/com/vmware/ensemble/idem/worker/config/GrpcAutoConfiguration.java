/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.vmware.ensemble.idem.manager.service.IacServiceImpl;
import com.vmware.ensemble.idem.manager.service.LemansAgentServiceImpl;
import com.vmware.ensemble.idem.manager.service.TaskManagerServiceImpl;
import com.vmware.ensemble.idem.model.TaskNotificationResponse;
import com.vmware.ensemble.idem.worker.server.GrpcServerBootStrapper;
import com.vmware.lemans.client.gateway.LemansClient;

import reactor.core.publisher.Sinks;

/**
 * Configuration related to grpc spring integration
 */
@Configuration
public class GrpcAutoConfiguration {

    @Bean
    public TaskManagerServiceImpl taskManagerService(LemansClient lemansClient, WorkerProperties workerProperties,
                                                     Sinks.Many<TaskNotificationResponse> taskNotificationResponseSinks) {
        return new TaskManagerServiceImpl(lemansClient,workerProperties, taskNotificationResponseSinks);
    }

    @Bean
    public IacServiceImpl idemService(@Autowired LemansClient lemansClient) {
        return new IacServiceImpl(lemansClient);
    }

    @Bean
    public LemansAgentServiceImpl lemansAgentService(LemansClient lemansClient, WorkerProperties workerProperties) {
        return new LemansAgentServiceImpl(lemansClient, workerProperties);
    }

    @Bean
    public GrpcServerBootStrapper grpcServerRunner(@Autowired GrpcProperties grpcProperties,
                                                   @Autowired TaskManagerServiceImpl taskManagerService,
                                                   @Autowired IacServiceImpl idemService,
                                                   @Autowired LemansAgentServiceImpl lemansAgentService) {
        return new GrpcServerBootStrapper(grpcProperties, taskManagerService, idemService, lemansAgentService);
    }
}
