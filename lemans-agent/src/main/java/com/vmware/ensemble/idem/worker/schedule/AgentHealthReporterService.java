/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.schedule;

import static com.vmware.ensemble.idem.manager.service.constants.IacServiceLeMansConstants.WORKER_HEALTH_STREAM_URI;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.vmware.ensemble.idem.worker.config.WorkerProperties;
import com.vmware.ensemble.idem.worker.dto.AgentHealthInfo;
import com.vmware.ensemble.idem.worker.util.LeMansGatewayUtil;
import com.vmware.lemans.client.gateway.LemansClient;

import jakarta.annotation.PostConstruct;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AgentHealthReporterService {

    private WorkerProperties workerProperties;

    private LemansClient lemansClient;

    private ObjectMapper objectMapper;

    private String workerVersion;

    private Map<String, Object> properties;

    @Autowired
    public AgentHealthReporterService(LemansClient lemansClient, WorkerProperties workerProperties,
                                      ObjectMapper objectMapper) {
        this.lemansClient = lemansClient;
        this.workerProperties = workerProperties;
        this.objectMapper = objectMapper;
    }

    @PostConstruct
    public void initialise() {
        properties = new HashMap<>();
        properties.put("lemansAgentId", lemansClient.getAgentId());
    }

    @SneakyThrows
    @Scheduled(cron = "${worker.health.schedule.cron-expression:0 * * * * *}")
    public void sendHealthInfo() {

        log.info("Sending health info..");
        AgentHealthInfo agentHealthInfo = AgentHealthInfo.builder()
                .workerGroupId(workerProperties.getWorkerGroupId())
                .workerId(workerProperties.getWorkerId())
                .workerProperties(properties)
                .build();

        String healthPayload = healthPayload = objectMapper.writeValueAsString(agentHealthInfo);

        LeMansGatewayUtil.performAction(lemansClient, URI.create(WORKER_HEALTH_STREAM_URI), healthPayload,
                                        Collections.emptyMap(), s -> {
                    //Do nothing on success
                    log.debug("Health data sent successfully");
                }, throwable -> {
                    log.warn("Exception occurred in sending health info", throwable);
                });
    }

}
