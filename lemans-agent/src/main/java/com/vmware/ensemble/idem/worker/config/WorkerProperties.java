/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.SmartLifecycle;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Configuration
public class WorkerProperties {

    @Value("${worker.data.volume}")
    private String dataVolumePath;

    @Value("${worker.name}")
    private String workerName;

    private String workerId;

    private String workerVersion;

    @Value("${worker.group.id}")
    private String workerGroupId;

    @Value("${worker.iac.type}")
    private String iacType;

    @Value("${worker.iac.version}")
    private String iacVersion;

    @Value("${csp.api-key}")
    private String cspAPIToken;

    @Value("${tanzu.hub.baseurl}")
    private String tanzuHubBaseUrl;

    @Value("${csp.url:https://console.cloud.vmware.com}")
    private String cspURI;

    @Value("${lemans.stream.idem.name}")
    private String streamName;

    @Value("${worker.registration.graphql.enable:true}")
    private boolean enableWorkerRegistrationGraphql;

    private String orgId;

    private int startUpPhase = SmartLifecycle.DEFAULT_PHASE;

}
