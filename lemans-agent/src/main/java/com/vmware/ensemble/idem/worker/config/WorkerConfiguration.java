/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.config;

import static com.vmware.ensemble.idem.worker.util.WorkerUtil.convertBytesToEntity;
import static com.vmware.ensemble.idem.worker.util.WorkerUtil.resourceAsString;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.task.TaskExecutor;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import com.vmware.ensemble.idem.model.TaskNotificationResponse;
import com.vmware.ensemble.idem.model.ValidateInput;
import com.vmware.ensemble.idem.worker.dto.AgentDetails;
import com.vmware.ensemble.idem.worker.dto.CspAuthToken;
import com.vmware.ensemble.idem.worker.dto.ErrorDetails;
import com.vmware.ensemble.idem.worker.dto.GuardrailsWorkerBootstrapResult;
import com.vmware.ensemble.idem.worker.dto.GuardrailsWorkerProperties;
import com.vmware.ensemble.idem.worker.dto.WorkerRegistrationDetails;
import com.vmware.ensemble.idem.worker.server.GrpcServerBootStrapper;
import com.vmware.ensemble.idem.worker.server.IacWorkerBootStrapper;
import com.vmware.ensemble.idem.worker.util.HttpUtil;
import com.vmware.ensemble.idem.worker.util.ValidateTaskUtils;
import com.vmware.ensemble.idem.worker.util.WorkerUtil;
import com.vmware.lemans.client.gateway.CommandExecutor;
import com.vmware.lemans.client.gateway.LemansClient;
import com.vmware.lemans.commons.base.model.Command;
import com.vmware.lemans.commons.base.model.CommandResponse;
import com.vmware.symphony.csp.auth.jwt.CmfJwtDecoderAdaptor;
import com.vmware.symphony.csp.auth.jwt.CspJwt;
import com.vmware.symphony.csp.auth.service.CspClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Sinks;
import reactor.util.concurrent.Queues;

@Configuration
@Slf4j
public class WorkerConfiguration {

    private static final String WORKER_ID = "workerID";
    private static final String WORKER_VERSION = "workerVersion";
    private static final String WORKER_INFO_DIR = "worker_info";
    private static final String WORKER_IDENTITY_FILE = "identity.json";
    private static final String WORKER_HOME = "IDEMD_HOME";
    private static final String WORKER_VERSION_FILE = "version.txt";
    private static final String WORKER_AGENT_HOME = "IDEM_WORKER_AGENT_HOME";
    private static final String WORKER_GROUP_ID = "workerGroupId";
    private static final String IAC_VERSION = "iacVersion";
    private static final String IAC_TYPE = "iacType";
    private static final String IAC_REMOTE_WORKER_AGENT = "iac-remote-worker-agent";
    private static final String AUTHORIZE_REFRESH_TOKEN_URL = "/csp/gateway/am/api/auth/api-tokens/authorize";
    private static final String IAC_SERVICE_GENERATE_AGENT_ACCESS_KEY_URL = "/iac-service/api/lemans/generate-access-key";
    private static final String WORKER_REGISTRATION_GRAPHQL_RESPONSE_PATH = "data.guardrailsMutations.bootstrapWorker";

    private static final String NOTIFY_PATH = "/notify-tasks";

    private static final String VALIDATE_PATH = "/validate-tasks";

    private final JsonFormat.Parser parser = JsonFormat.parser();

    private final TaskExecutor taskExecutor;
    private final ResourceLoader resourceLoader;

    private final CmfJwtDecoderAdaptor cmfJwtDecoder;
    private final WorkerProperties workerProperties;
    private final ValidateTaskUtils validateTaskUtils;
    private LemansClient lemansClient;

    @Autowired
    public WorkerConfiguration(WorkerProperties workerProperties, @Qualifier("validateTaskExecutor") TaskExecutor taskExecutor,
                               ValidateTaskUtils validateTaskUtils, ResourceLoader resourceLoader, JwtDecoder jwtDecoder,
                               CspClient cspClient) {
        this.workerProperties = workerProperties;
        this.taskExecutor = taskExecutor;
        this.validateTaskUtils = validateTaskUtils;
        this.resourceLoader = resourceLoader;
        this.cmfJwtDecoder = new CmfJwtDecoderAdaptor(jwtDecoder, false,
                false, true, false, cspClient);
    }

    @Bean("populateWorkerId")
    public void populateWorkerId() {
        String workerPath = System.getenv(WORKER_HOME);
        if (ObjectUtils.isEmpty(workerPath)) {
            throw new IllegalStateException("Worker home path is not set!!");
        }
        Path workerInfoDir = Paths.get(workerPath + "/" + WORKER_INFO_DIR);
        Path workerIdentityFilePath = Paths.get(workerInfoDir + "/" + WORKER_IDENTITY_FILE);

        String workerId = retrieveExistingWorkerIdentity(workerIdentityFilePath);

        if (null == workerId) {
            workerId = generateWorkerIdentity();
            log.info("Worker identity not found. Assigning a new identity: {}", workerId);
        }

        workerProperties.setWorkerId(workerId);
        saveWorkerIdentity(workerInfoDir, workerIdentityFilePath);

        // Populate worker version
        String workerAgentPath = System.getenv(WORKER_AGENT_HOME);
        if (ObjectUtils.isEmpty(workerAgentPath)) {
            throw new IllegalStateException("Worker agent path is not set!!");
        }

        Path workerVersionFilePath = Paths.get(workerAgentPath + "/" + WORKER_VERSION_FILE);
        workerProperties.setWorkerVersion(retrieveWorkerVersion(workerVersionFilePath));
    }

    private String retrieveExistingWorkerIdentity(Path workerIdentityFilePath) {
        try {
            log.info("Attempting to retrieve worker identity.");
            if (Files.exists(workerIdentityFilePath)) {
                log.info("Worker Identity exists. Re-using the same");
                byte[] fileContent = Files.readAllBytes(workerIdentityFilePath);
                HashMap<String, String> workerIdentityMap = convertBytesToEntity(fileContent, HashMap.class);
                return ObjectUtils.isEmpty(workerIdentityMap) ? null : workerIdentityMap.get("id");
            }
        } catch (Exception ex) {
            log.error("Error occurred while retrieving worker identity", ex);
        }
        return null;
    }

    private void saveWorkerIdentity(Path workerInfoDir, Path workerIdentityFilePath) {

        try {

            // Create the directory if it doesn't exist
            if (!Files.exists(workerInfoDir)) {
                Files.createDirectories(workerInfoDir);
            }

            HashMap<String, String> workerIdentityMap = new HashMap<>();
            workerIdentityMap.put("id", workerProperties.getWorkerId());

            File workerIdentityFile = new File(workerIdentityFilePath.toAbsolutePath().toString()) ;
            WorkerUtil.writeEntityToFile(workerIdentityFile, workerIdentityMap);

            log.info("New worker identity: {} has been written to file.", workerProperties.getWorkerId());

        } catch (Exception ex) {
            throw new RuntimeException("Unable to create worker identity file", ex);
        }
    }

    private String retrieveWorkerVersion(Path workerVersionFilePath) {

        try {

            log.info("Retrieving Worker Version.");
            if (Files.exists(workerVersionFilePath)) {
                log.info("Worker Version File exists.");
                String workerVersion = Files.readString(workerVersionFilePath).trim().replaceAll("^\"|\"$", "");
                return ObjectUtils.isEmpty(workerVersion) ? "latest" : workerVersion;
            }
        } catch (Exception ex) {
            log.error("Error occurred while retrieving worker version file", ex);
        }
        return "latest";
    }

    /**
     * Worker Bootstrapper instance
     * @param idemdProperties
     * @param grpcServerRunner
     * @return
     */
    @Bean
    @DependsOn("lemansClient")
    public IacWorkerBootStrapper idemd(@Autowired WorkerProperties idemdProperties,
                                       @Autowired GrpcServerBootStrapper grpcServerRunner,
                                       @Autowired WorkerRegistrationDetails workerRegistrationDetails) {
        // Setup data volume path
        return new IacWorkerBootStrapper(idemdProperties, grpcServerRunner, workerRegistrationDetails);
    }

    /**
     * This flux is used to streaming the notified tasks to gRPCProxy->TaskManager->PendingTasks
     * @return
     */
    @Bean("taskNotificationResponseSinks")
    public Sinks.Many<TaskNotificationResponse> taskNotificationResponseSinks() {
        return Sinks.many().multicast()
                .onBackpressureBuffer(Queues.SMALL_BUFFER_SIZE, false);
    }

    @DependsOn("populateWorkerId")
    @Bean("workerRegistrationDetails")
    public WorkerRegistrationDetails workerRegistrationDetails() throws Exception {
        WorkerRegistrationDetails workerRegistrationDetails;

        if (this.workerProperties.isEnableWorkerRegistrationGraphql()) {
            workerRegistrationDetails = registerWorkerUsingGraphqlMutation();
        } else {
            workerRegistrationDetails = registrationDetails();
        }

        return workerRegistrationDetails;
    }

    @Bean("lemansClient")
    @DependsOn("populateWorkerId")
    public LemansClient lemansClient(Sinks.Many<TaskNotificationResponse> taskNotificationResponseSinks, @Autowired WorkerRegistrationDetails workerRegistrationDetails) {
        try {
            //TODO: Need to validate each of these workers properties?
            Assert.notNull(workerProperties.getDataVolumePath(), "Data volume path should not be null");
            Assert.notNull(workerProperties.getWorkerId(), "Worker Id should not be null");
            Assert.notNull(workerProperties.getWorkerVersion(), "Worker version should not be null");
            Assert.notNull(workerProperties.getWorkerGroupId(), "Worker Group Id should not be null");
            Assert.notNull(workerProperties.getIacVersion(), "Iac version should not be null");
            Assert.notNull(WorkerUtil.getIACProvider(workerProperties.getIacType()), "Iac type should not be null");
            String workerDataVolume = String.format("%s/%s", workerProperties.getDataVolumePath(), workerProperties.getWorkerId());
            HashMap<String, String> agentCustomProperties = new HashMap<>();
            agentCustomProperties.put(WORKER_ID, workerProperties.getWorkerId());
            agentCustomProperties.put(WORKER_VERSION, workerProperties.getWorkerVersion());
            agentCustomProperties.put(WORKER_GROUP_ID, workerProperties.getWorkerGroupId());
            agentCustomProperties.put(IAC_TYPE, workerProperties.getIacType());
            agentCustomProperties.put(IAC_VERSION, workerProperties.getIacVersion());

            workerProperties.setOrgId(workerRegistrationDetails.getOrgId());

            log.debug("agent key {}, lemans uri {}", workerRegistrationDetails.getKey(),
                    workerRegistrationDetails.getLemansServiceUri());
            log.info("Waiting for 10 seconds before establishing the connection");
            Thread.sleep(10 * 1000);
            LemansClient.Builder builder = LemansClient.Builder.forConfig(workerRegistrationDetails.getKey(),
                            new URI(workerRegistrationDetails.getLemansServiceUri()), workerDataVolume)
                    .withAgentType(IAC_REMOTE_WORKER_AGENT)
                    .withAgentVersion(2.0)
                    .withAgentProperties(agentCustomProperties)
                    .shouldEnableWebSocketSessionHealthCheck(false)
                    .shouldEnableWebsocketSessions(false)
                    .shouldEnableTelemetry(false)    // Set to true to enable telemetry defaults to true
                    .withMetricsLoggingIntervalMinutes(10) // Set the console metrics logging interval, default 5 mins
                    .shouldEnableCommandChannel(true) // Set to true for enabling commands
                    .shouldRegisterAgent(true)
                    // Set to true for enabling registering agents for commands
                    .withHostServiceUri(new URI(workerProperties.getTanzuHubBaseUrl()));
            // invoke an API hosted in the application. This assumes the application is running on port 8080
            addCommandExecutor(workerRegistrationDetails, builder, taskNotificationResponseSinks);
            lemansClient = builder.build();
            // Start LeMansClient
            lemansClient.start();
            return lemansClient;
        } catch (Throwable throwable) {
            throw new RuntimeException("Unable to build lemans client. Error observed: ", throwable);
        }
    }

    /**
     * Adds a command executor for handling incoming commands based on their paths.
     *
     * @param registrationDetails The registration details of the agent.
     * @param builder             The LemansClient.Builder used for configuring the client.
     * @param taskNotificationSinks   The Sinks.Many instance for emitting NotificationResponse to subscribers.
     */
    private void addCommandExecutor(WorkerRegistrationDetails registrationDetails,
                                    LemansClient.Builder builder,
                                    Sinks.Many<TaskNotificationResponse> taskNotificationSinks) {
        CommandExecutor commandExecutor = command -> {
            if (command.request == null || StringUtils.isEmpty(command.request.path)) {
                return failCommand(command, new IllegalArgumentException("Empty command"));
            } else {
                log.info("Command received agent with path {} for org {}, action {}",
                        command.request.path, registrationDetails.getOrgId(), command.request.action);
                if (NOTIFY_PATH.equals(command.request.path)) {
                    return notifyPathExecutor(command, taskNotificationSinks);
                } else if (VALIDATE_PATH.equals(command.request.path)) {
                    return validatePathExecutor(command);
                } else {
                    log.error("unknown command {}, path {}", command.request.body, command.request.path);
                    return failCommand( command, new IllegalStateException(" unknown command " + command.request.path));
                }
            }
        };

        builder.withCommandExecutor(commandExecutor);
    }

    /**
     * Creates a CompletableFuture that completes exceptionally with the given exception.
     *
     * @param exc The exception with which the CompletableFuture should complete exceptionally.
     * @return A CompletableFuture that completes exceptionally with the provided exception.
     */
    private CompletableFuture<Command> failCommand(Command command,  Exception exc) {
        CompletableFuture<Command> completableFuture = new CompletableFuture<>();
        log.error("Error occurred while accepting the command  cause {}", exc.getMessage() );
        command.response.statusCode = 500;
        completableFuture.complete(command);
        return completableFuture;
    }

    /**
     * Asynchronously validates a Command by executing a validation task on a separate thread using a task executor.
     *
     * @param command The Command to be validated.
     * @return A CompletableFuture that completes with the validated Command.
     */
    private CompletableFuture<Command> validatePathExecutor(Command command) {
        CompletableFuture<Command> completableFuture = new CompletableFuture<>();
        ValidateInput.Builder builderInstance = ValidateInput.getDefaultInstance().toBuilder();

        try {
            parser.merge(command.request.body, builderInstance);
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
        command.response = new CommandResponse();
        this.taskExecutor.execute(() -> {
            try {
                validateTaskUtils.processValidation(builderInstance.build());
            } catch (IOException | InterruptedException e) {
                log.error(e.getMessage());
            }
        });
        command.response.statusCode = 200;
        command.response.body = "SUCCESS";
        completableFuture.complete(command);

        return completableFuture;
    }

    /**
     * Asynchronously notifies a path executor about a command and emits a notification response to subscribers.
     *
     * @param command           The command to be processed and notified.
     * @param taskNotificationSinks The Sinks.Many instance for emitting NotificationResponse to subscribers.
     * @return A CompletableFuture that represents the asynchronous operation and holds the processed command result.
     *         The CompletableFuture can complete normally with the command result or exceptionally with an error.
     */
    private CompletableFuture<Command> notifyPathExecutor(Command command, Sinks.Many<TaskNotificationResponse> taskNotificationSinks) {
        CompletableFuture<Command> completableFuture = new CompletableFuture<>();
        try {
            TaskNotificationResponse.Builder builderInstance = TaskNotificationResponse.getDefaultInstance().newBuilderForType();
            parser.merge(command.request.body, builderInstance);
            TaskNotificationResponse taskNotificationResponse = builderInstance.build();
            command.response = new CommandResponse();

            if (taskNotificationSinks.currentSubscriberCount() < 1) {
                String errorMessage = "number of subscribers to notification flux found to be zero";
                completableFuture = failCommand(command, new IllegalStateException(errorMessage));

            } else {
                Sinks.EmitResult emitResult = taskNotificationSinks.tryEmitNext(taskNotificationResponse);
                if (emitResult.isSuccess()) {
                    command.response.statusCode = 200;
                    command.response.body = "SUCCESS";
                    completableFuture.complete(command);
                } else {
                    String errorMessage = String.format("emitting to notification flux failed, cause: %s",
                            emitResult.name());
                    completableFuture = failCommand(command, new IllegalStateException(errorMessage));
                    //TODO: validate when emit error happens next notification can be emitted or not
                }
            }

        } catch (Exception e) {
            log.error("Exception occurred while processing command with path {}, payload {} cause {}",
                    command.request.path, command.request.body, e);
            completableFuture = failCommand(command, e);
        }
        return completableFuture;
    }

    private WorkerRegistrationDetails registrationDetails() throws Exception {

        String cspAccessToken = getCSPAccessToken();
        Assert.notNull(cspAccessToken, "Could not get csp access token from the api token ");
        AgentDetails agentDetails = new AgentDetails(workerProperties.getWorkerId());
        String requestBody = WorkerUtil.objectToString(agentDetails);
        String generateAccessKeyAPI = workerProperties.getTanzuHubBaseUrl() + IAC_SERVICE_GENERATE_AGENT_ACCESS_KEY_URL;
        log.debug("Agent Details for accessKey generation {}, API  {}", requestBody, generateAccessKeyAPI);
        HttpRequest request = HttpRequest.newBuilder()
                .uri(HttpUtil.getUri(generateAccessKeyAPI))
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + cspAccessToken)
                .build();
        HttpClient httpClient = HttpClient.newBuilder().connectTimeout(Duration.ofMinutes(2)).build();
        try {
            HttpResponse<InputStream> response = httpClient.send(request, HttpResponse.BodyHandlers.ofInputStream());
            return HttpUtil.fromResponse(response, WorkerRegistrationDetails.class);
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("Unable to generate agent access token", e);
        }

    }

    private WorkerRegistrationDetails registerWorkerUsingGraphqlMutation() throws Exception {

        // Generate csp access token using api token
        String cspAccessToken = getCSPAccessToken();
        Assert.notNull(cspAccessToken, "Could not get csp access token from the api token ");
        // Build the final request payload
        String mutationMapString = WorkerUtil.objectToString(buildGraphqlRequestPayloadForWorkerRegistration());

        HttpRequest bootstrapWorkerGraphqlRequest = HttpRequest.newBuilder()
                .uri(HttpUtil.getUri(this.workerProperties.getTanzuHubBaseUrl()))
                .POST(HttpRequest.BodyPublishers.ofString(mutationMapString))
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + cspAccessToken)
                .build();

        HttpClient httpClient = HttpClient.newBuilder().connectTimeout(Duration.ofMinutes(2)).build();
        try {
            // TODO: Need a retry to handle certain types of failure
            HttpResponse<InputStream> response = httpClient.send(bootstrapWorkerGraphqlRequest,
                    HttpResponse.BodyHandlers.ofInputStream());
            GuardrailsWorkerBootstrapResult result = parseAndProcessWorkerRegistrationGraphqlResponse(response);
            return transformGRWBootstrapToWorkerRegistrationDetails(result, fetchOrgIdFromToken(cspAccessToken));
        } catch (Exception e) {
            throw new RuntimeException("Unable to generate agent access token", e);
        }
    }

    private GuardrailsWorkerBootstrapResult parseAndProcessWorkerRegistrationGraphqlResponse(HttpResponse<InputStream> response)
            throws JsonProcessingException {

        Object resultObject = HttpUtil.parseResponse(response.body(), Object.class);
        JsonNode resultNode = WorkerUtil.toJsonNode(resultObject);

        // Check if any errors were thrown by the api
        JsonNode errorsNode = WorkerUtil.fetchElementFromGraphqlResponse(resultNode, "errors");
        if (!ObjectUtils.isEmpty(errorsNode)) {

            ErrorDetails errorDetails = WorkerUtil.convertJsonNodeToEntity(errorsNode.get(0), ErrorDetails.class);
            log.error("Worker registration failed with error message: {}", errorDetails.getMessage());
            throw new RuntimeException(errorDetails.getMessage());
        }

        // Fetch data from 'data.guardrailsMutations.bootstrapWorker' to get worker registration details
        JsonNode dataNode = WorkerUtil.fetchElementFromGraphqlResponse(resultNode,
                WORKER_REGISTRATION_GRAPHQL_RESPONSE_PATH);

        if (ObjectUtils.isEmpty(dataNode)) {
            throw new RuntimeException("Empty graphql response received for worker registration!!");
        }

        return WorkerUtil.convertJsonNodeToEntity(dataNode, GuardrailsWorkerBootstrapResult.class);
    }

    private Map buildGraphqlRequestPayloadForWorkerRegistration() throws IOException {

        // Read worker registration mutation from file
        Resource bootstrapWorkerMutationResource = resourceLoader
                .getResource("classpath:graphql/guardrailsService/bootstrapWorkerMutation.graphql");
        String bootstrapWorkerMutation = resourceAsString(bootstrapWorkerMutationResource);

        // Set values for variables defined in the mutation
        Map<String, Object> workerInfo = new HashMap<>();
        workerInfo.put("groupId", workerProperties.getWorkerGroupId());
        // Worker id should be same as agent_name in properties(as per the defined graphql schema doc)
        workerInfo.put("workerId", workerProperties.getWorkerId());
        workerInfo.put(WORKER_VERSION, workerProperties.getWorkerVersion());
        workerInfo.put("properties", List.of(Map.of("name", "agent_name",
                "value", workerProperties.getWorkerId())));

        Map<String, Object> mutationVariables = new HashMap<>();
        mutationVariables.put("workerInfo", workerInfo);

        return Map.of("query", bootstrapWorkerMutation,
                "variables", mutationVariables);
    }

    private String fetchOrgIdFromToken(String cspToken) {

        CspJwt cspJwt = this.cmfJwtDecoder.decode(cspToken);
        return cspJwt.getOrgId();
    }

    private WorkerRegistrationDetails transformGRWBootstrapToWorkerRegistrationDetails(GuardrailsWorkerBootstrapResult gwbr,
                                                                                      String orgId) {

        List<GuardrailsWorkerProperties> guardrailsWorkerProperties = gwbr.getProperties();

        WorkerRegistrationDetails workerRegistrationDetails = new WorkerRegistrationDetails();

        for (GuardrailsWorkerProperties gwProp : guardrailsWorkerProperties) {
            if (gwProp.getName().equals("agent_access_key")) {
                workerRegistrationDetails.setKey(gwProp.getValue());
            }
            if (gwProp.getName().equals("lemans_url")) {
                workerRegistrationDetails.setLemansServiceUri(gwProp.getValue());
            }
        }
        workerRegistrationDetails.setOrgId(orgId);
        return workerRegistrationDetails;
    }

    private String getCSPAccessToken() {
        Assert.notNull(workerProperties.getCspAPIToken(), "api token cannot be null");
        Map<Object, Object> formData = new HashMap<>();
        formData.put("api_token", workerProperties.getCspAPIToken());
        HttpRequest request = HttpRequest.newBuilder()
                .uri(HttpUtil.getUri(workerProperties.getCspURI() + AUTHORIZE_REFRESH_TOKEN_URL))
                .headers("Content-Type", "application/x-www-form-urlencoded")
                .POST(HttpRequest.BodyPublishers.fromPublisher(HttpUtil.ofFormData(formData)))
                .header("Accept", "application/json")
                .build();
        HttpClient httpClient = HttpClient.newBuilder().connectTimeout(Duration.ofMinutes(2)).build();
        try {
            HttpResponse<InputStream> response = httpClient.send(request, HttpResponse.BodyHandlers.ofInputStream());
            return HttpUtil.fromResponse(response, CspAuthToken.class).getAccessToken();
        } catch (IOException | InterruptedException e) {
            log.error("Error occurred while getting csp token %s", e);
        }
        return null;
    }

    private String generateWorkerIdFromHostName() {
        log.info("Generating worker ID from the hostname");
        try {
            String hostName = InetAddress.getLocalHost().getHostName();
            log.info("Generating worker ID for the worker name {} and worker group Id {}",
                    hostName, workerProperties.getWorkerGroupId());
            return String.format("%s_%s", hostName, workerProperties.getWorkerGroupId());
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    private String generateWorkerIdentity() {

        // User provided name will be always the first candidate to generate the worker ID
        if (StringUtils.isNotBlank(workerProperties.getWorkerName())
                && StringUtils.isNotBlank(workerProperties.getWorkerGroupId())) {
            return generateWorkerIdFromWorkerName();
        } else {
        // Fallback option for generating the worker ID. This will guarantee a worker ID.
        // Or else the worker startup will fail
            return generateWorkerIdFromHostName();
        }
    }

    private String generateWorkerIdFromWorkerName() {
        log.info("Generating worker ID for the worker name {} and worker group Id {}",
                workerProperties.getWorkerName(), workerProperties.getWorkerGroupId());
        return String.format("%s_%s", workerProperties.getWorkerName(), workerProperties.getWorkerGroupId());
    }
}