/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.manager.service;

import static com.vmware.ensemble.idem.manager.service.constants.IacServiceLeMansConstants.LEMANS_STREAMS_SELECTOR;
import static com.vmware.ensemble.idem.manager.service.constants.IacServiceLeMansConstants.LEMANS_STREAM_CUSTOM_KEY_HEADER;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;

import io.grpc.stub.ServerCallStreamObserver;
import io.grpc.stub.StreamObserver;

import org.apache.commons.lang3.StringUtils;
import org.reactivestreams.Subscription;
import org.springframework.stereotype.Service;

import com.vmware.ensemble.idem.manager.service.LemansAgentServiceGrpc.LemansAgentServiceImplBase;
import com.vmware.ensemble.idem.model.EventRequest;
import com.vmware.ensemble.idem.model.EventResponse;
import com.vmware.ensemble.idem.worker.config.WorkerProperties;
import com.vmware.ensemble.idem.worker.util.LeMansGatewayUtil;
import com.vmware.lemans.client.gateway.LemansClient;

import lombok.extern.slf4j.Slf4j;

/***
 * Receives request of idem task events and performs various actions via lemans agent like posting event data to stream spi of gateway.
 */
@Slf4j
@Service
public class LemansAgentServiceImpl extends LemansAgentServiceImplBase {

    private final LemansClient lemansClient;

    private final WorkerProperties workerProperties;

    private static final JsonFormat.Parser parser = JsonFormat.parser();

    public LemansAgentServiceImpl(LemansClient lemansClient, WorkerProperties workerProperties) {
        this.lemansClient = lemansClient;
        this.workerProperties = workerProperties;
    }

    @Override
    public void postToLemansStream(EventRequest request, StreamObserver<EventResponse> responseObserver) {

        AtomicReference<Subscription> subscriptionRef = new AtomicReference<>();
        Map<String, String> headersMap = new HashMap<>();
        setClientCancelledHandler(responseObserver, "Posting to LeMans stream ", subscriptionRef);
        log.info("Initiating post to LeMans stream for task id: {} and event identity: {}",
                request.getTaskEventKey(), request.getEventIdentity());
        EventResponse.Builder builderInstance = EventResponse.getDefaultInstance().newBuilderForType();
        if (StringUtils.isNotEmpty(request.getEvent()) && StringUtils.isNotEmpty(request.getTaskEventKey())) {
            final Consumer<String> successConsumer = (response) -> {
                try {
                    parser.merge(response, builderInstance);
                    EventResponse eventResponse = builderInstance.setSuccess(true).build();
                    responseObserver.onNext(eventResponse);
                    responseObserver.onCompleted();
                    log.info("Successfully posted to LeMans stream with task id: {} and event identity: {}",
                            request.getTaskEventKey(), request.getEventIdentity());
                } catch (InvalidProtocolBufferException e) {
                    throw new RuntimeException(e);
                }
            };
            headersMap.put(LEMANS_STREAM_CUSTOM_KEY_HEADER, request.getTaskEventKey());
            //Passing only the event in string format to leMans steam api.
            LeMansGatewayUtil.performAction(lemansClient, LEMANS_STREAMS_SELECTOR, workerProperties.getStreamName(),
                    request.getEvent(), headersMap, successConsumer, responseObserver::onError);
        } else {
            builderInstance.setSuccess(false);
            EventResponse eventResponse = builderInstance.setErrorMessage("Event Request should contain a valid event data and task event key for LeMans Stream").build();
            responseObserver.onNext(eventResponse);
            responseObserver.onCompleted();
        }

    }

    private void setClientCancelledHandler(StreamObserver<?> responseObserver,
                                          String cancelDetails,
                                          AtomicReference<Subscription> subscription) {
        ServerCallStreamObserver<?> serverCallStreamObserver =
                (ServerCallStreamObserver<?>) responseObserver;
        serverCallStreamObserver.setOnCancelHandler( () -> {
            log.info("[{}] client has disconnected...",cancelDetails);
            if (subscription != null && subscription.get() != null) {
                subscription.get().cancel();
                log.info("Cancelled Subscription for client details {}", cancelDetails);
            }
        });
    }
}
