/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.manager.service;

import static com.vmware.ensemble.idem.manager.service.constants.IacServiceLeMansConstants.CLAIM_TASK_PATH;
import static com.vmware.ensemble.idem.manager.service.constants.IacServiceLeMansConstants.TASK_MANAGER_CLAIM_TASK_SELECTOR;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;

import io.grpc.Status;
import io.grpc.stub.ServerCallStreamObserver;
import io.grpc.stub.StreamObserver;

import org.reactivestreams.Subscription;
import org.springframework.stereotype.Service;

import com.vmware.ensemble.idem.manager.service.TaskManagerServiceGrpc.TaskManagerServiceImplBase;
import com.vmware.ensemble.idem.model.ClaimTaskRequest;
import com.vmware.ensemble.idem.model.ClaimTaskResponse;
import com.vmware.ensemble.idem.model.Empty;
import com.vmware.ensemble.idem.model.Identity;
import com.vmware.ensemble.idem.model.TaskIdentity;
import com.vmware.ensemble.idem.model.TaskNotificationResponse;
import com.vmware.ensemble.idem.model.WorkerIdentity;
import com.vmware.ensemble.idem.worker.config.WorkerProperties;
import com.vmware.ensemble.idem.worker.util.LeMansGatewayUtil;
import com.vmware.lemans.client.gateway.LemansClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

/***
 * Receives request for queuing idem tasks and performs queue operation
 */
@Slf4j
@Service
public class TaskManagerServiceImpl extends TaskManagerServiceImplBase {
    private final LemansClient lemansClient;

    private final Flux<TaskNotificationResponse> pendingTasksFlux;

    private final WorkerProperties workerProperties;

    private final JsonFormat.Printer printer = JsonFormat.printer().preservingProtoFieldNames();

    private static final JsonFormat.Parser parser = JsonFormat.parser();

    public TaskManagerServiceImpl(LemansClient lemansClient, WorkerProperties workerProperties, Sinks.Many<TaskNotificationResponse> taskNotificationSinks) {
        this.lemansClient = lemansClient;
        this.workerProperties = workerProperties;
        this.pendingTasksFlux = taskNotificationSinks.asFlux();
    }

    @Override
    public void pendingTasks(WorkerIdentity request, StreamObserver<TaskNotificationResponse> responseObserver) {
        log.info("Long poll request from worker id {} of worker type {} and IAC type {}",
                request.getWorkerId(), request.getWorkerType(), request.getIacProvider());
        AtomicReference<Subscription> subscriptionRef = new AtomicReference<>();
        setClientCancelledHandler(responseObserver, "PendingTasks ", subscriptionRef);
        pendingTasksFlux
                .doOnSubscribe(subscriptionRef::set)
                .doOnNext(taskAvailability -> {
                    log.info("task available {} message obtained for notification to worker id {}",
                            taskAvailability.getTaskAvailable(), request.getWorkerId());
                    if (isClientCancelled(responseObserver)) {
                        log.info("client cancelled for task available message {} and worker id {}",
                                taskAvailability, request.getWorkerId());
                        Status status = Status.CANCELLED.withDescription("client cancelled for worker id " + request.getWorkerId());
                        throw new IllegalStateException(status.asException());
                        //TODO: Evaluate the impact of subscription cancellation event
                    } else {
                        TaskNotificationResponse taskNotificationResponse = TaskNotificationResponse.newBuilder()
                                        .setTaskAvailable(true)
                                                .build();
                        responseObserver.onNext(taskNotificationResponse);
                        log.info("Sent notification for task availability {} for worker id {} ",
                                taskAvailability.getTaskAvailable(), request.getWorkerId());
                    }

                })
                .doOnError(throwable ->  {
                    log.error("Error occurred {}", throwable);
                })
                .subscribe();
    }

    @Override
    public void claimTask(ClaimTaskRequest request, StreamObserver<ClaimTaskResponse> responseObserver) {

        AtomicReference<Subscription> subscriptionRef = new AtomicReference<>();
        setClientCancelledHandler(responseObserver, "ClaimTask ", subscriptionRef);
        WorkerIdentity workerIdentity = request.getWorkerIdentity();
        Identity identity = workerIdentity.getIdentity();
        String requestString;
        try {
            log.info("claim task requested for org id {} worker group id {}, worker id {}, iac provider {}",
                    identity.getOrgId(), workerIdentity.getWorkerGroupId(),
                    workerIdentity.getWorkerId(), workerIdentity.getIacProvider());
            requestString = printer.print(request);
        } catch (InvalidProtocolBufferException e) {
            responseObserver.onError(e);
            return;
        }

        //TODO:: Need to handle timeout scenario from worker when claim takes longer time to respond from saas service
        final Consumer<String> successConsumer = (response) -> {
            ClaimTaskResponse.Builder builderInstance = ClaimTaskResponse.getDefaultInstance().newBuilderForType();
            try {
                parser.merge(response, builderInstance);
                ClaimTaskResponse claimTaskResponse = builderInstance.build();
                responseObserver.onNext(claimTaskResponse);
                responseObserver.onCompleted();
                log.info("claim task responded for org id {} worker group id {}, worker id {}, iac provider {}",
                        claimTaskResponse.getTaskSummary().getTaskIdentity().getIdentity().getOrgId(),
                        workerIdentity.getWorkerGroupId(),
                        workerIdentity.getWorkerId(), workerIdentity.getIacProvider());
            } catch (InvalidProtocolBufferException e) {
                throw new RuntimeException(e);
            }
        };

        LeMansGatewayUtil.performAction(lemansClient, TASK_MANAGER_CLAIM_TASK_SELECTOR, CLAIM_TASK_PATH,
                requestString, Collections.emptyMap(), successConsumer, responseObserver::onError );
    }

    @Override
    public void finaliseTask(TaskIdentity request, StreamObserver<Empty> responseObserver) {
        responseObserver.onError(new IllegalStateException("Not implemented"));
    }


    private boolean isClientCancelled(StreamObserver<?> responseObserver) {
        ServerCallStreamObserver<?> serverCallStreamObserver =
                (ServerCallStreamObserver<?>) responseObserver;
        return serverCallStreamObserver.isCancelled();

    }

    private void setClientCancelledHandler(StreamObserver<?> responseObserver,
                                          String cancelDetails,
                                          AtomicReference<Subscription> subscription) {
        ServerCallStreamObserver<?> serverCallStreamObserver =
                (ServerCallStreamObserver<?>) responseObserver;
        serverCallStreamObserver.setOnCancelHandler( () -> {
            log.info("[{}] client has disconnected...",cancelDetails);
            if (subscription != null && subscription.get() != null) {
                subscription.get().cancel();
                log.info("Cancelled Subscription for client details {}", cancelDetails);
            }
        });
    }
}
