/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.json.JsonMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HttpUtil {
    private static final ObjectMapper mapper;

    static {
        mapper = JsonMapper.builder()
                .configure(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME, true)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .serializationInclusion(Include.NON_NULL)
                .annotationIntrospector(new JacksonAnnotationIntrospector())
                .build();
    }

    public static URI getUri(String serverUrl) {
        try {
            return new URL(serverUrl).toURI();
        } catch (MalformedURLException | URISyntaxException e) {
            throw new RuntimeException("Must never reach here.");
        }
    }

    public static HttpRequest.BodyPublisher ofFormData(Map<Object, Object> data) {
        var builder = new StringBuilder();
        for (Map.Entry<Object, Object> entry : data.entrySet()) {
            if (builder.length() > 0) {
                builder.append("&");
            }
            builder.append(URLEncoder.encode(entry.getKey().toString(), StandardCharsets.UTF_8));
            builder.append("=");
            builder.append(URLEncoder.encode(entry.getValue().toString(), StandardCharsets.UTF_8));
        }
        return BodyPublishers.ofString(builder.toString());
    }

    public static  <T> T fromResponse(HttpResponse<InputStream> httpResponse, Class<T> returnType) {
        final int statusCode = httpResponse.statusCode();
        log.debug("[{} {}]: Status Code: [{}]",
                httpResponse.request().method(), httpResponse.request().uri(), statusCode);

        InputStream inputStream = httpResponse.body();

        if (statusCode < 300) {
            return parseResponse(inputStream, returnType);
        } else if (statusCode < 400) {
            throw new UnsupportedOperationException("Redirect not supported for (" + statusCode + ") ");
        } else {
            // >= 400
            String errorMessage = parseResponse(inputStream, String.class);
            throw new RuntimeException("API responded with error: " + errorMessage);
        }
    }

    public static <T> T parseResponse(InputStream inputStream, Class<T> returnType) {
        try {
            if (inputStream != null) {
                if (returnType == Void.TYPE) {
                    return null;
                } else if (returnType == byte[].class) {
                    return (T) inputStream.readAllBytes();
                } else {
                    return mapper.readValue(inputStream, returnType);
                }
            }
        } catch (IOException exception) {
            throw new IllegalStateException("Error parsing response: " + exception.getMessage());
        }
        return null;
    }
}
