/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.manager.service;

import static com.vmware.ensemble.idem.manager.service.constants.IacServiceLeMansConstants.IDEM_SERVICE_DOWNLOAD_BUNDLE_PATH;
import static com.vmware.ensemble.idem.manager.service.constants.IacServiceLeMansConstants.IDEM_SERVICE_DOWNLOAD_BUNDLE_SELECTOR;
import static com.vmware.ensemble.idem.manager.service.constants.IacServiceLeMansConstants.IDEM_SERVICE_DOWNLOAD_OLD_STATE_PATH;
import static com.vmware.ensemble.idem.manager.service.constants.IacServiceLeMansConstants.IDEM_SERVICE_DOWNLOAD_OLD_STATE_SELECTOR;
import static com.vmware.ensemble.idem.manager.service.constants.IacServiceLeMansConstants.IDEM_SERVICE_TASK_UPDATE_PATH;
import static com.vmware.ensemble.idem.manager.service.constants.IacServiceLeMansConstants.IDEM_SERVICE_TASK_UPDATE_SELECTOR;
import static com.vmware.ensemble.idem.manager.service.constants.IacServiceLeMansConstants.IDEM_SERVICE_UPDATE_VALIDATE_TASK_PATH;
import static com.vmware.ensemble.idem.manager.service.constants.IacServiceLeMansConstants.IDEM_SERVICE_UPDATE_VALIDATE_TASK_SELECTOR;

import java.util.Collections;
import java.util.function.Consumer;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;

import io.grpc.stub.StreamObserver;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.vmware.ensemble.idem.model.BundleChunk;
import com.vmware.ensemble.idem.model.IdemDocUpdateRequest;
import com.vmware.ensemble.idem.model.IdemDocUpdateResponse;
import com.vmware.ensemble.idem.model.OldState;
import com.vmware.ensemble.idem.model.TaskClaimRequest;
import com.vmware.ensemble.idem.model.TaskClaimResponse;
import com.vmware.ensemble.idem.model.TaskIdentity;
import com.vmware.ensemble.idem.model.TaskUpdateRequest;
import com.vmware.ensemble.idem.model.TaskUpdateResponse;
import com.vmware.ensemble.idem.model.ValidateTaskUpdateRequest;
import com.vmware.ensemble.idem.model.ValidateTaskUpdateResponse;
import com.vmware.ensemble.idem.service.IdemServiceGrpc;
import com.vmware.ensemble.idem.worker.util.LeMansGatewayUtil;
import com.vmware.lemans.client.gateway.LemansClient;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IacServiceImpl extends IdemServiceGrpc.IdemServiceImplBase {

    private static final JsonFormat.Parser parser = JsonFormat.parser();

    private final LemansClient lemansClient;

    private final JsonFormat.Printer printer = JsonFormat.printer().preservingProtoFieldNames();

    public IacServiceImpl(LemansClient lemansClient) {
        this.lemansClient = lemansClient;
    }

    @Override
    public void downloadBundle(TaskIdentity request, StreamObserver<BundleChunk> responseObserver) {
        String requestString = null;
        try {
            log.info("Download bundle requested for task Id {}, workerGroup Id {}",
                    request.getTaskId(), request.getIdentity().getOrgId());
            requestString = printer.print(request);
        } catch (InvalidProtocolBufferException e) {
            log.error("Error occurred while parsing request {}, cause {}", request.toString(), e);
            responseObserver.onError(e);
        }

        log.info("download bundle request payload {}", requestString);
        final Consumer<String> successConsumer = (response) -> {
            BundleChunk.Builder builderInstance = BundleChunk.getDefaultInstance().newBuilderForType();
            try {
                log.info("download response {}", response);
                JsonObject jsonObject = JsonParser.parseString(response).getAsJsonObject();
                parser.merge(jsonObject.get("result").toString(), builderInstance);
                //TODO: stream back data in chunks may be since gRPC has a limit of 4MB data size
                //TODO: check with lemans team if there is a limit on payload message size to be responded back
                responseObserver.onNext(builderInstance.build());
                responseObserver.onCompleted();
            } catch (InvalidProtocolBufferException e) {
                throw new RuntimeException(e);
            }
        };

        LeMansGatewayUtil.performAction(lemansClient,
                IDEM_SERVICE_DOWNLOAD_BUNDLE_SELECTOR,
                IDEM_SERVICE_DOWNLOAD_BUNDLE_PATH,
                requestString, Collections.emptyMap(), successConsumer, responseObserver::onError);

    }

    @Override
    public void taskUpdate(TaskUpdateRequest request, StreamObserver<TaskUpdateResponse> responseObserver) {
        String requestString = null;
        try {
            log.info("task update request for task Id {}", request.getTaskIdentity().getTaskId());
            requestString = printer.print(request);
        } catch (InvalidProtocolBufferException e) {
            log.error("Error occurred while parsing request {}, cause {}", request.toString(), e);
            responseObserver.onError(e);
        }

        final Consumer<String> successConsumer = (response) -> {
            TaskUpdateResponse.Builder builderInstance = TaskUpdateResponse.getDefaultInstance().newBuilderForType();
            try {
                parser.merge(response, builderInstance);
                responseObserver.onNext(builderInstance.build());
                log.info("Task Update Completed");
                responseObserver.onCompleted();
            } catch (InvalidProtocolBufferException e) {
                throw new RuntimeException(e);
            }
        };
        LeMansGatewayUtil.performAction(lemansClient,
                IDEM_SERVICE_TASK_UPDATE_SELECTOR,
                IDEM_SERVICE_TASK_UPDATE_PATH,
                requestString, Collections.emptyMap(), successConsumer, responseObserver::onError);

    }

    @Override
    public void downloadOldState(TaskIdentity request, StreamObserver<OldState> responseObserver) {
        String requestString = null;
        try {
            requestString = printer.print(request);
        } catch (InvalidProtocolBufferException e) {
            log.error("Error occurred while parsing request {}, cause {}", request.toString(), e);
            responseObserver.onError(e);
        }
        log.info("Download old state requested for taskId: {}, enforcementStateId: {}",
                request.getTaskId(), request.getEnforcedStateId());
        log.debug("Download oldState bundle request {}", requestString);

        final Consumer<String> successConsumer = (response) -> {
            try {
                log.info("Download old state responded for taskId: {}, enforcementStateId: {}",
                        request.getTaskId(), request.getEnforcedStateId());
                log.debug("Download old state bundle response {}", response);
                if (StringUtils.isNotEmpty(response)) {
                    response.lines()
                            .filter(StringUtils::isNotEmpty)
                            .forEach(stateResponse -> {
                                OldState.Builder builderInstance = OldState.getDefaultInstance().newBuilderForType();
                                JsonObject jsonObject = JsonParser.parseString(stateResponse).getAsJsonObject();
                                try {
                                    parser.merge(jsonObject.get("result").toString(), builderInstance);
                                    responseObserver.onNext(builderInstance.build());
                                } catch (InvalidProtocolBufferException e) {
                                    throw new RuntimeException(e);
                                }
                            });
                }
                responseObserver.onCompleted();
            } catch (Exception e) {
                log.error("Error occurred while parsing Download old bundle request {}, cause {}", request, e.getMessage());
                throw new RuntimeException(e);
            }
        };

        LeMansGatewayUtil.performAction(lemansClient,
                IDEM_SERVICE_DOWNLOAD_OLD_STATE_SELECTOR,
                IDEM_SERVICE_DOWNLOAD_OLD_STATE_PATH,
                requestString, Collections.emptyMap(), successConsumer, responseObserver::onError);
    }

    @Override
    public StreamObserver<IdemDocUpdateRequest> updateIdemDocsStream(StreamObserver<IdemDocUpdateResponse> responseObserver) {
        responseObserver.onError(new IllegalStateException("Not implemented"));
        return null;
    }

    @Override
    public void claimTask(TaskClaimRequest request, StreamObserver<TaskClaimResponse> responseObserver) {
        responseObserver.onError(new IllegalStateException("Not implemented"));
    }

    @Override
    public void updateValidateTask(ValidateTaskUpdateRequest request, StreamObserver<ValidateTaskUpdateResponse> responseObserver) {
        String requestString = null;
        try {
            requestString = printer.print(request);
        } catch (InvalidProtocolBufferException e) {
            log.error("Error occurred while parsing request {}, cause {}", request.toString(), e.getMessage());
            responseObserver.onError(e);
        }

        final Consumer<String> successConsumer = (response) -> {
            ValidateTaskUpdateResponse.Builder builderInstance = ValidateTaskUpdateResponse.getDefaultInstance().newBuilderForType();
            try {
                parser.merge(response, builderInstance);
                responseObserver.onNext(builderInstance.build());
                responseObserver.onCompleted();
            } catch (InvalidProtocolBufferException e) {
                throw new RuntimeException(e);
            }
        };

        LeMansGatewayUtil.performAction(lemansClient,
                IDEM_SERVICE_UPDATE_VALIDATE_TASK_SELECTOR,
                IDEM_SERVICE_UPDATE_VALIDATE_TASK_PATH,
                requestString, Collections.emptyMap(), successConsumer, responseObserver::onError);
    }
}
