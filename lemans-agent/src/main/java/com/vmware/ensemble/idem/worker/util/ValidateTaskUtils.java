/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Base64;

import org.springframework.stereotype.Component;

import com.vmware.ensemble.idem.model.ValidateInput;
import com.vmware.ensemble.idem.worker.config.GrpcProperties;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ValidateTaskUtils {

    Integer grpcPort;

    public ValidateTaskUtils(GrpcProperties properties) {
        this.grpcPort = properties.getServerPort();
    }

    public void processValidation(ValidateInput input) throws IOException, InterruptedException {
        log.info("Validation request received for {}", input.getTaskIdentity().getTaskId());

        String pythonPath = System.getenv("WORKER_PYTHON_PATH");
        pythonPath = (pythonPath != null) ? pythonPath : "python";
        String validatePath = System.getenv("VALIDATE_FILE_PATH");

        byte[] serializedRequest = input.toByteArray();
        String serializedRequestBase64 = Base64.getEncoder().encodeToString(serializedRequest);

        String[] cmd = {pythonPath, validatePath, serializedRequestBase64};

        ProcessBuilder processBuilder = new ProcessBuilder(cmd);
        log.info("Invoking validate on worker request {}", input.getTaskIdentity().getTaskId());
        Process process = processBuilder.start();

        BufferedReader stdoutReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        BufferedReader stderrReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        String line;

        // Read and print standard output
        StringBuilder stdOut = new StringBuilder();
        while ((line = stdoutReader.readLine()) != null) {
            stdOut.append(line).append('\n');
        }

        log.info(stdOut.toString());

        StringBuilder stderr = new StringBuilder();
        while ((line = stderrReader.readLine()) != null) {
            stderr.append(line).append('\n');
        }
        log.info(stderr.toString());

        // Wait for the Python script to finish
        int exitCode = process.waitFor();
        log.info("Validate worker request {} completed with exit code {}",
                input.getTaskIdentity().getTaskId(), exitCode);
    }
}


