/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CspExpriyToken {

    @JsonProperty(value = "expires_in")
    /*
     * In the response to fetch overflow claims, the expiry time is given as value of
     * 'exp' claim. Hence using exp as alias here.
     */
    @JsonAlias("exp")
    protected long expiresIn;

}
