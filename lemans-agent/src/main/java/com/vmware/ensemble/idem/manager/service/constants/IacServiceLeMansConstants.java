/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.manager.service.constants;

public class IacServiceLeMansConstants {
    public static final String IDEM_SERVICE_DOWNLOAD_BUNDLE_SELECTOR = "idem-service-download-bundle";

    public static final String IDEM_SERVICE_DOWNLOAD_OLD_STATE_SELECTOR = "idem-service-download-old-state";

    public static final String IDEM_SERVICE_TASK_UPDATE_SELECTOR = "idem-service-task-update";

    public static final String IDEM_SERVICE_UPDATE_VALIDATE_TASK_SELECTOR = "idem-service-update-validate-task";

    public static final String WORKER_HEALTH_STREAM_URI = "/streams/aria-guardrails-wm-heartbeat";

    public static final String IDEM_SERVICE_TASK_UPDATE_PATH = "taskUpdate";

    public static final String IDEM_SERVICE_DOWNLOAD_BUNDLE_PATH = "download/bundle";

    public static final String IDEM_SERVICE_DOWNLOAD_OLD_STATE_PATH = "download/oldstate";

    public static final String IDEM_SERVICE_UPDATE_VALIDATE_TASK_PATH = "updateValidateTask";

    public static final String TASK_MANAGER_CLAIM_TASK_SELECTOR = "idem-task-manager";

    public static final String CLAIM_TASK_PATH = "claim-task";

    public static final String LEMANS_STREAMS_SELECTOR = "streams";

    public static final String LEMANS_STREAM_CUSTOM_KEY_HEADER = "custom-key-header";

}
