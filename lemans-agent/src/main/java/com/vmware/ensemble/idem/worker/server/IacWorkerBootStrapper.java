/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.SmartLifecycle;

import com.vmware.ensemble.idem.worker.config.WorkerProperties;
import com.vmware.ensemble.idem.worker.dto.WorkerRegistrationDetails;

import lombok.extern.slf4j.Slf4j;

/**
 * Idemd Implementation for server
 */
@Slf4j
public class IacWorkerBootStrapper implements SmartLifecycle {

    private static final String PYTHON_VENV_PATH = "PYTHON_VENV_PATH";
    private static final String WORKER_STARTUP_SCRIPT = "/idemd/run.py";
    private static final String WORKER_HOME = "IDEMD_HOME";
    private static final String SYSTEM_ACCT_FILE_PARAM = "--system-acct-file";
    private static final String SYSTEM_ACCT_KEY_PARAM = "--system-acct-key";
    //Router variables have to align with exported values from create_router_fernet_file function of entrypoint.sh
    private static final String ROUTER_FILE_ENV_NAME = "ROUTER_FILE";
    private static final String ROUTER_KEY_ENV_NAME = "ROUTER_KEY";
    private static final String TASK_MANAGER_HOST_PARAM = "--task-manager-host";
    private static final String TASK_MANAGER_PORT_PARAM = "--task-manager-port";
    private static final String IDEM_SERVICE_HOST_PARAM = "--idem-service-host";
    private static final String IDEM_SERVICE_PORT_PARAM = "--idem-service-port";
    private static final String IDEM_SERVICE_MODE_PARAM = "--mode";
    private static final String REMOTE = "remote";
    private static final String LOG_LEVEL_PARAM = "--log-level";
    private static final String ORG_ID_PARAM = "--org-id";
    private static final String WORKER_GROUP_ID_PARAM = "--worker-group-id";
    private static final String ENV_LEMANS_KEY = "LEMANS_KEY";
    private static final String ENV_LEMANS_BASE_URL = "LEMANS_BASE_URL";

    final AtomicBoolean isRunning = new AtomicBoolean(false);
    WorkerProperties workerProperties;
    WorkerRegistrationDetails workerRegistrationDetails;
    String grpcHost;
    Integer grpcPort;
    CountDownLatch latch;
    Process process;

    public IacWorkerBootStrapper(WorkerProperties workerProperties, GrpcServerBootStrapper grpcServerRunner, WorkerRegistrationDetails workerRegistrationDetails) {
        this.workerProperties = workerProperties;
        this.workerRegistrationDetails = workerRegistrationDetails;
        this.grpcHost = "localhost";
        this.grpcPort = grpcServerRunner.getServerProperties().getServerPort();
    }

    @Override
    public void start() {
        if (isRunning()) {
            return;
        }
        log.info("Starting iac  worker {}/{} ....", workerProperties.getWorkerGroupId(), workerProperties.getWorkerId());
        log.info("grpc server {}:{}", this.grpcHost, this.grpcPort);

        try {
            latch = new CountDownLatch(1);

            /*
             * /usr/bin/python3 "$IDEMD_HOME"/idemd/run.py
             * --system-acct-key "$SYSTEM_ACCT_FILE_KEY" --system-acct-file "$SYSTEM_ACCT_FILE_PATH"
             * --task-manager-host "${TASK_MANAGER_HOST}" --idem-service-host "${IDEM_SERVICE_HOST}"
             */
            List<String> idemCommands = new ArrayList<>();
            String python = StringUtils.isBlank(System.getenv(PYTHON_VENV_PATH)) ? "python" : System.getenv(PYTHON_VENV_PATH);

            idemCommands.add(python);
            idemCommands.add(String.format("%s%s", System.getenv(WORKER_HOME), WORKER_STARTUP_SCRIPT));

            idemCommands.add(SYSTEM_ACCT_FILE_PARAM);
            idemCommands.add(System.getenv(ROUTER_FILE_ENV_NAME));

            idemCommands.add(SYSTEM_ACCT_KEY_PARAM);
            idemCommands.add(System.getenv(ROUTER_KEY_ENV_NAME));

            idemCommands.add(TASK_MANAGER_HOST_PARAM);
            idemCommands.add(grpcHost);
            idemCommands.add(TASK_MANAGER_PORT_PARAM);
            idemCommands.add(grpcPort.toString());
            idemCommands.add(IDEM_SERVICE_HOST_PARAM);
            idemCommands.add(grpcHost);
            idemCommands.add(IDEM_SERVICE_PORT_PARAM);
            idemCommands.add(grpcPort.toString());
            idemCommands.add(IDEM_SERVICE_MODE_PARAM);
            idemCommands.add(REMOTE);
            idemCommands.add(ORG_ID_PARAM);
            idemCommands.add(workerProperties.getOrgId());
            idemCommands.add(WORKER_GROUP_ID_PARAM);
            idemCommands.add(workerProperties.getWorkerGroupId());
            idemCommands.add(LOG_LEVEL_PARAM );
            idemCommands.add("info");
            log.info("Worker is started with commands {}", idemCommands);
            process = execute(idemCommands.toArray(String[]::new));
            if (process.isAlive()) {
                isRunning.set(true);
                startDaemonAwaitThread();
                log.info("idemd started on pid {}", process.pid());
            } else {
                isRunning.set(false);
                log.error("idemd did not start exit code {}", process.exitValue());
            }
        } catch (IOException e) {
            log.error("Error occurred while starting the idemd server %s", e);
            throw new RuntimeException(e);
        }

    }

    @Override
    public void stop() {
        log.info("Shutting down worker.");
        Optional.ofNullable(process).ifPresent(p -> {
            p.destroy();
            try {
                p.waitFor();
            } catch (InterruptedException e) {
                log.error("Error occurred while stopping worker %s", e);

            } finally {
                latch.countDown();
            }
        });
    }

    @Override
    public boolean isRunning() {
        return isRunning.get();
    }

    private void startDaemonAwaitThread() {
        Thread awaitThread = new Thread(() -> {
            try {
                latch.await();
            } catch (InterruptedException e) {
                log.error("worker startup awaiter interrupted.", e);
            } finally {
                isRunning.set(false);
            }
        });
        awaitThread.setName("idemd-server-awaiter");
        awaitThread.setDaemon(false);
        awaitThread.start();
    }

    @Override
    public int getPhase() {
        return workerProperties.getStartUpPhase();
    }

    private Process execute(String... commands) throws IOException {
        ProcessBuilder processBuilder = new ProcessBuilder();
        List<String> list = Arrays.asList(commands);
        String command = String.join(" ", list);
        processBuilder.command("bash", "-c", command);
        processBuilder.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        processBuilder.redirectError(ProcessBuilder.Redirect.INHERIT);
        Map<String, String> environment = processBuilder.environment();
        environment.putAll(System.getenv());
        environment.put(ENV_LEMANS_BASE_URL, workerRegistrationDetails.getLemansServiceUri());
        environment.put(ENV_LEMANS_KEY, workerRegistrationDetails.getKey());
        Process process = processBuilder.start();

        if (process.isAlive()) {
            process.onExit().whenComplete((process1, throwable) -> log.info("Process with pid: {} completed with {}",
                    process1.pid(), process1.exitValue()));
        }
        return process;
    }
}
