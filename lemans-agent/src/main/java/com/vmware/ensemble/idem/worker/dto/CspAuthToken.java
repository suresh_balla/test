/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.lang.Nullable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CspAuthToken extends CspExpriyToken {

    @JsonProperty(value = "access_token")
    private String accessToken;

    @Nullable
    @JsonProperty(value = "refresh_token")
    private String refreshToken;

    private String scope;

    @JsonProperty(value = "token_type")
    private String type;

    @Nullable
    @JsonProperty(value = "id_token")
    private String idToken;

}
