/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.dto;

import lombok.Data;

@Data
public class WorkerRegistrationDetails {
    private String orgId;
    private String key;
    private String lemansServiceUri;
}
