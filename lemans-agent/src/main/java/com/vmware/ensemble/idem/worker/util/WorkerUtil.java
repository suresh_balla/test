/*
 * Copyright (c) 2023 VMware, Inc. All Rights Reserved.
 */

package com.vmware.ensemble.idem.worker.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.core.io.Resource;
import org.springframework.util.FileCopyUtils;

import com.vmware.ensemble.idem.model.IAC_PROVIDER;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WorkerUtil {

    private static final ObjectMapper mapper = new ObjectMapper();

    public static IAC_PROVIDER getIACProvider(String iacType) {

        if (IAC_PROVIDER.TERRAFORM.name().equals(iacType)) {
            return IAC_PROVIDER.TERRAFORM;
        } else if (IAC_PROVIDER.IDEM.name().equals(iacType)) {
            return IAC_PROVIDER.IDEM;
        } else {
            return null;
        }
    }

    public static JsonNode fetchElementFromGraphqlResponse(JsonNode resultNode, String path) {

        String[] pathElements = path.split("\\.");
        JsonNode currentNode = resultNode;

        for (String pathElement : pathElements) {
            currentNode = currentNode.get(pathElement);

            if (currentNode == null) {
                log.info("Path not found: '{}' while parsing graphql response", path);
                return null;
            }
        }
        return currentNode;
    }

    public static String objectToString(Object object) throws JsonProcessingException {

        if (ObjectUtils.isEmpty(object)) {
            return null;
        }
        return mapper.writeValueAsString(object);
    }

    public static JsonNode toJsonNode(Object object) {

        if (ObjectUtils.isEmpty(object)) {
            return null;
        }
        return mapper.valueToTree(object);
    }

    public static <T> T convertJsonNodeToEntity(JsonNode jsonNode, Class<T> clazz) throws JsonProcessingException {

        if (ObjectUtils.isEmpty(jsonNode)) {
            return null;
        }
        return mapper.treeToValue(jsonNode, clazz);
    }

    public static String resourceAsString(Resource resource) throws IOException {

        try (Reader reader = new InputStreamReader(resource.getInputStream(), Charset.defaultCharset())) {
            return FileCopyUtils.copyToString(reader);
        }
    }


    public static <T> T convertBytesToEntity(byte[] src, Class<T> clazz) throws IOException {

        if (ObjectUtils.isEmpty(src)) {
            return null;
        }
        return mapper.readValue(src, clazz);
    }

    public static void writeEntityToFile(File file, Object object) throws IOException {

        mapper.writeValue(file, object);
    }

}
