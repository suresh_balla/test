#!/bin/bash -i

# Fail on errors
set -e

# Be receptive to core dumps
ulimit -c unlimited

# Allow high connection count per process (raise file descriptor limit)
ulimit -n 65536

echo "Idem worker"

#run setup_security.sh to create environment based on FIPS_MODE
export FIPS_MODE
"$IDEMD_HOME"/setup_security.sh

validate_ebs_client_file_env_vars() {
  if [ -z "$IDEM_AUTH_CLIENT_ID" ] || [ -z "$IDEM_AUTH_CLIENT_SECRET" ];
  then
    echo "Missing IDEM_AUTH_CLIENT_ID or IDEM_AUTH_CLIENT_ID env var."
    exit 1
  fi
  if [ -z "$CSP_URL" ] || [ -z "$EBS_URL" ];
  then
    echo "Missing CSP_URL or EBS_URL env var."
    exit 1
  fi
}

if [ "$BACKEND_CONFIG_FILE" == "" ]
then
  echo "current run is not a helm deployment and BACKEND_CONFIG_FILE variable not set. skipping the encryption of backend.yaml"
else
  #get acct backend config file and encrypt it
  OUTPUT_FERNET_FILE="/tmp/backend.yaml.fernet"
  echo "current working directory:$(pwd)"
  echo "backend.yaml location ""$BACKEND_CONFIG_FILE"
  echo "backend.yaml.fernet location "$OUTPUT_FERNET_FILE
  acct_key=$(acct encrypt "$BACKEND_CONFIG_FILE" --output-file=$OUTPUT_FERNET_FILE)

  export BACKEND_ACCT_KEY=$acct_key
  export BACKEND_ACCT_FILE=$OUTPUT_FERNET_FILE
fi
KAFKA_CLIENT_SECRET_FERNET_FILE="/tmp/kafkaClient.yaml.fernet"
echo "TASK_MANAGER_HOST: ${TASK_MANAGER_HOST}"
echo "IDEM_SERVICE_HOST: ${IDEM_SERVICE_HOST}"
# idem is installed in the "system" environment. So there is no need to activate any venv.

export ENABLE_GRPC_AUTH="true"

if [ "$IDEM_DOC_MODE" == "true" ]
then
  echo "Running in doc mode"
  python "$IDEMD_HOME"/idemd/run.py --idem-service-host "${IDEM_SERVICE_HOST}" --log-fmt-console="%(asctime)s.%(msecs)06d [%(filename)s:%(lineno)s - %(funcName)2s()] [%(levelname)s] [%(processName)s %(threadName)s] %(message)s" --log-level=info --log-datefmt="%Y-%m-%d %H:%M:%S" "$@"
else
  echo "Running in worker mode"
  if [ "$USE_RABBITMQ" == "true" ]
  then
    export SYSTEM_ACCT_FILE_PATH="/tmp/rmqClient.yaml.fernet"
    export SYSTEM_ACCT_FILE_KEY=$(idem encrypt "$RMQ_CLIENT_SECRET_FILE" --output-file=$SYSTEM_ACCT_FILE_PATH )
  elif [ "$USE_EBS" == "true" ]
  then
    if [ -z "$EBS_CLIENT_SECRET_FILE" ];
    then
      echo "No EBS client secret file provided. Using default EBS file template."
      EBS_CLIENT_SECRET_FILE="/run/config/ebsClient.yaml"
      validate_ebs_client_file_env_vars

      ebs_tmp_file=$(mktemp /tmp/ebs-tmp.XXXXXX)
      envsubst < "$EBS_CLIENT_SECRET_FILE" > "$ebs_tmp_file"
      mv "$ebs_tmp_file" "$EBS_CLIENT_SECRET_FILE"
    else
      echo "Using provided EBS client file: $EBS_CLIENT_SECRET_FILE"
    fi
    echo "Using EBS secret file"
    export SYSTEM_ACCT_FILE_PATH="/tmp/ebsClient.yaml.fernet"
    export SYSTEM_ACCT_FILE_KEY=$(idem encrypt "$EBS_CLIENT_SECRET_FILE" --output-file=$SYSTEM_ACCT_FILE_PATH )
  else
    echo "Using Kafka secret file"
    export SYSTEM_ACCT_FILE_PATH="/tmp/kafkaClient.yaml.fernet"
    export SYSTEM_ACCT_FILE_KEY=$(idem encrypt "$KAFKA_CLIENT_SECRET_FILE" --output-file=$SYSTEM_ACCT_FILE_PATH )
  fi

  # Adding logs due to VRAE-39079
  echo "Kafka secret file path is $KAFKA_CLIENT_SECRET_FILE"
  echo "Generated System account key is $SYSTEM_ACCT_FILE_KEY"
  python "$IDEMD_HOME"/idemd/run.py --system-acct-key="$SYSTEM_ACCT_FILE_KEY" --system-acct-file "$SYSTEM_ACCT_FILE_PATH" --task-manager-host "${TASK_MANAGER_HOST}" --idem-service-host "${IDEM_SERVICE_HOST}" --log-fmt-console="%(asctime)s.%(msecs)06d [%(filename)s:%(lineno)s - %(funcName)2s()] [%(levelname)s] [%(process)d %(processName)s %(threadName)s] %(message)s" --log-level=info --log-datefmt="%Y-%m-%d %H:%M:%S" "$@"
fi