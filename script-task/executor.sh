#!/bin/bash
# Copyright (c) 2024 Broadcom. All Rights Reserved.

#set -x
#env

WORKER_SCRIPT_PATH=$(dirname "$0")
BASE_PATH="$HOME/.bcworker"

. "${WORKER_SCRIPT_PATH}/library.sh"

function exit_with_error() {
    local exit_code="$1"
    shift
    echo "$2" >&2
    exit "${exit_code}"
}

function escape_json() {
    base64 -w0
}

function make_directory() {
    mkdir -p "$1" or || exit_with_error 1 "Failed to create directory: $1"
}

function remove_directory() {
    rm -rf "$1" or || exit_with_error 1 "Failed to delete directory: $1"
}

function main() {
    local script_path="${SCRIPT_DIRECTORY}/$1"
    
    if [ ! -f "${script_path}" ]
    then
        exit_with_error 1 "No such script exists: $1"
    fi
    shift

    # Working directory is the location where data is stored (E.g. git repo)
    local working_directory="${BASE_PATH}/${ORG_ID}/${PROJECT_ID}"
    # Task directory is the location where data specific to given task is stored (E.g. sbom generation)
    local task_directory="${working_directory}/${TASK_ID}"
    local output_file="${task_directory}/out.log"
    local error_file="${task_directory}/error.log"
    local json_file="${task_directory}/output.json"

    export WORKING_DIRECTORY="${working_directory}"
    export TASK_DIRECTORY="${task_directory}"
    export LIBRARY_DIRECTORY="${WORKER_SCRIPT_PATH}"

    print_debug "Task directory is ${task_directory}"
    make_directory "${task_directory}"

    "${script_path}" "$@" >"${output_file}" 2>"${error_file}"
    local exit_code="$?"
    local output=$(cat "${output_file}" | escape_json)
    local error=$(cat "${error_file}" | escape_json)

    cat >"${json_file}" <<EOF
{
    "tags": {
        "type": "script-result"
    },
    "message": {
        "exit_code": ${exit_code},
        "stdout": "$output",
        "stderr": "$error"
    },
    "run_name": "${REQUESTOR}::${ORG_ID}::${PROJECT_ID}::${TASK_ID}"
}
EOF

    #cat "${json_file}"

    upload 'idem-events' "${json_file}"
    #remove_directory "${task_directory}"
}

main "$@"
