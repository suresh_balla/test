from kafka import KafkaProducer
from kafka.errors import KafkaError
import sys
import os
import json
import logging


def publish_event_to_kafka_topic(topic, json_data):

    future = producer.send(topic, json_data)
    try:
        record_metadata = future.get(timeout=kafka_producer_timeout)
        logging.info(f"Successfully published message to kafka topic: {record_metadata.topic}")
    except KafkaError as err:
        logging.error(f"Exception occurred while producing kafka event: {err}")
        sys.exit(1)


kafka_bootstrap_server = os.getenv("BOOTSTRAP_SERVER", "localhost:9092")
kafka_producer_timeout = int(os.getenv("KAFKA_PRODUCER_TIMEOUT", 10))

producer = KafkaProducer(bootstrap_servers=[kafka_bootstrap_server],
                         value_serializer=lambda m: json.dumps(m).encode('utf-8'), retries=5)

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

try:

    # File name provided as an input
    if len(sys.argv) >= 3:
        logging.info(f"Reading content from file with name: {sys.argv[2]}")
        kafka_topic = sys.argv[1]
        input_file = sys.argv[2]
        with open(input_file) as f:
            data = json.load(f)
            publish_event_to_kafka_topic(kafka_topic, data)

    # file content provided as input from stdin
    else:
        logging.info(f"Reading input from stdin!!")
        kafka_topic = sys.argv[1]
        data = json.load(sys.stdin)
        publish_event_to_kafka_topic(kafka_topic, data)

except Exception as exc:
    logging.error(f"Exception observed while reading input: {exc}")
    sys.exit(1)

