#!/bin/bash

MAX_RETRIES=3

function print_error() {
    echo Error: "$@" 1>&2
}

function print_debug() {
    echo Debug: "$@" 1>&2
}

function exec_with_retry() {
    local count=0

    while [ $count -lt $MAX_RETRIES ]
    do
        scrubbed_command="$@"
        [ -z "$LEMANS_KEY" ] || scrubbed_command=$(echo "$@" | sed -e "s/$LEMANS_KEY/****/g")
        print_debug "Executing $scrubbed_command"
        "$@" && return 0

        (( count++ ))
        print_debug "Retry count: $count"
        sleep $count
    done

    print_error "Failed to execute command: $@"
    kill $KILL_ME_PID
    exit 1
}

function upload_to_lemans() {
    print_debug "Uploading to lemans stream: $1"
    local stream="$1"
    local input_file="$2"

    exec_with_retry curl -s -X POST "${LEMANS_BASE_URL}/le-mans/v1/streams/${stream}" \
        -H 'Content-Type: application/json' \
        -H "Authorization: Bearer ${LEMANS_KEY}" \
        -H "custom-key-header: ${WORKER_NAME}" \
        --data-binary "@${input_file}"
    print_debug "Uploaded ${input_file} to ${stream}"
}

function upload_to_kafka() {
    print_debug "Uploading to kafka topic: $1 file: $2"
    local topic="$1"
    local input_file="$2"

    # Execute Python script
    python ${LIBRARY_DIRECTORY}/kafka-producer.py ${topic} ${input_file}

    print_debug "Uploaded ${input_file} to ${topic}"
}

function upload() {
    local stream="$1"
    local file="$2"

    if [ "${IS_REMOTE_WORKER}" = 'true' ]
    then
        upload_to_lemans "${stream}" "${file}"
    else
        upload_to_kafka "${stream}" "${file}"
    fi
}
