#!/bin/bash

# Determine the operating system type and architecture
os_type=$(uname -s)
architecture=$(uname -m)

# Specify the desired Terraform version
terraform_version="1.5.0"

# Define the download URL based on the operating system type and architecture
case "$os_type" in
    Linux*)
        if [ "$architecture" == "x86_64" ]; then
            download_url="https://releases.hashicorp.com/terraform/${terraform_version}/terraform_${terraform_version}_linux_amd64.zip"
        elif [ "$architecture" == "aarch64" ]; then
            download_url="https://releases.hashicorp.com/terraform/${terraform_version}/terraform_${terraform_version}_linux_arm64.zip"
        else
            echo "Unsupported architecture: $architecture"
            exit 1
        fi
        ;;
    Darwin*)
        if [ "$architecture" == "x86_64" ]; then
            download_url="https://releases.hashicorp.com/terraform/${terraform_version}/terraform_${terraform_version}_darwin_amd64.zip"
        elif [ "$architecture" == "arm64" ]; then
            download_url="https://releases.hashicorp.com/terraform/${terraform_version}/terraform_${terraform_version}_darwin_arm64.zip"
        else
            echo "Unsupported architecture: $architecture"
            exit 1
        fi
        ;;
    CYGWIN*|MINGW*)
        if [ "$architecture" == "x86_64" ]; then
            download_url="https://releases.hashicorp.com/terraform/${terraform_version}/terraform_${terraform_version}_windows_amd64.zip"
        elif [ "$architecture" == "i686" ]; then
            download_url="https://releases.hashicorp.com/terraform/${terraform_version}/terraform_${terraform_version}_windows_386.zip"
        else
            echo "Unsupported architecture: $architecture"
            exit 1
        fi
        ;;
    *)
        echo "Unsupported operating system: $os_type"
        exit 1
        ;;
esac

# Destination directory for Terraform binary
destination_dir="$HOME/.local/bin"

# Download and extract Terraform
echo "Downloading Terraform version ${terraform_version}..."
curl -LO "${download_url}"
unzip "terraform_${terraform_version}_*.zip"

# Create the destination directory if it doesn't exist
mkdir -p "${destination_dir}"

# Move Terraform to the destination directory
echo "Moving Terraform to ${destination_dir}..."
mv terraform "${destination_dir}/"

# Add the bin directory to PATH if not already present
if [[ ":$PATH:" != *":${destination_dir}:"* ]]; then
  echo "Adding ${destination_dir} to PATH..."
  echo "export PATH=\"${destination_dir}:\$PATH\"" >> ~/.bashrc
  source ~/.bashrc
fi

echo "export PATH=\"$destination_dir:\$PATH\"" >> "$HOME/.bashrc"

# Reload the bashrc file
source "$HOME/.bashrc"

# Check if terraform is installed
terraform version
