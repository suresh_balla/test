import logging
import os
import signal
import subprocess
import time

from idemd.idemd.constants.constants import LOG_FORMATTER
from idemd.idemd.proc import (
    get_terminate_file_path,
    get_grace_timeout_interval_in_seconds,
)
from idemd.tool.run_space_dir_processor import process_run_space_dir


def create_terminate_file():
    terminate_file_path_local = get_terminate_file_path()
    with open(terminate_file_path_local, "w"):
        pass
    logging.info(f"File {terminate_file_path_local} created Successfully")


def get_running_idem_processes():
    # Case-insensitive search for python processes
    active_python_processes_cli_output = subprocess.check_output(
        "ps -ef | grep -i python | awk '{print $2 \" \" $9}'",
        shell=True,
        universal_newlines=True,
    )
    # Remove the trailing newline character from the active_python_processes_cli_output
    active_python_processes_cli_output = active_python_processes_cli_output[
        : len(active_python_processes_cli_output) - 1
    ]
    #  Split the modified active_python_processes_cli_output string into a list of strings,
    #  with each string representing a single line of output.
    active_python_processes_cli_output = active_python_processes_cli_output.split("\n")

    # Identify all running Python processes whose command includes the substring "idem state",
    # and store the process IDs of these Python processes in a list
    pids = []
    for process in active_python_processes_cli_output:
        process_parsed = list(map(str, process.split()))
        pid = int(process_parsed[0])
        script = process_parsed[1]
        if "/bin/idem" in script or "/usr/bin/idem" in script:
            pids.append(pid)

    return pids


# Wait for active idem processes to be terminated. If the timeout period is reached before all idem processes are
# terminated, the function returns with a bool status and the list of active processes.
def check_for_idem_processes():
    # Consider the timeout to be one minute less than the actual termination timeout
    # This gives us some window to do more cleanup before receiving a SIGKILL
    timeout = time.time() + get_grace_timeout_interval_in_seconds() - 60
    while True:
        active_idem_processes = get_running_idem_processes()
        if len(active_idem_processes) == 0:
            logging.info(
                f"Expected number of processes found. Proceeding for termination"
            )
            break
        elif time.time() > timeout:  # Check if the timeout has occurred
            logging.info(
                f"There are still {len(active_idem_processes)} python processes active. But timeout occurred"
            )
            break
        else:
            logging.info(
                f"There are still {len(active_idem_processes)} python processes active. Sleeping for 5 seconds"
            )
            time.sleep(5)

    return len(active_idem_processes) == 0, active_idem_processes


if __name__ == "__main__":
    # Set up logging configuration
    logging.basicConfig(
        level=logging.DEBUG,
        format=LOG_FORMATTER,
    )
    create_terminate_file()
    terminate_file_path = get_terminate_file_path()
    is_done, active_processes = check_for_idem_processes()

    if not is_done:
        for process in active_processes:
            logging.warning(f"Force killing idem task with PID {process}")
            os.kill(process, signal.SIGKILL)
    else:
        logging.info("No processes to force terminate")

    # After killing the remaining processes, send updates to idem service by processing the leftover
    # run space directories
    process_run_space_dir()
