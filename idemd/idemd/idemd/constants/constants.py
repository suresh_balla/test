"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json
import os
from os import environ
from pathlib import Path
from typing import Final

HOME = "HOME"
IDEMD = ".idemd"
RUN_NAME_DELIMITER = "::"
IDEMD_HOME = "IDEMD_HOME"
GLOBAL_TASK_TIMEOUT = 10800  # This is in seconds
TASK_CLAIM_TIMEOUT = 120  # This is in seconds
ENABLE_GRPC_AUTH_PROP = "ENABLE_GRPC_AUTH"

# Note: Final keyword works from python 3.8+
EQUALS_OPERATOR: Final = "="

# refer for more details https://github.com/grpc/grpc-proto/blob/master/grpc/service_config/service_config.proto
GRPC_IDEM_SERVICE_CONFIG = json.dumps(
    {
        "methodConfig": [
            {
                "name": [
                    {
                        "service": "com.vmware.ensemble.idem.service.IdemService",
                        "method": "taskUpdate",
                    },
                    {
                        "service": "com.vmware.ensemble.idem.service.IdemService",
                        "method": "downloadBundle",
                    },
                    {
                        "service": "com.vmware.ensemble.idem.service.IdemService",
                        "method": "downloadOldState",
                    },
                    {
                        "service": "com.vmware.ensemble.idem.service.IdemService",
                        "method": "updateIdemDocs",
                    },
                ],
                "retryPolicy": {
                    "maxAttempts": 5,
                    "initialBackoff": "2s",
                    "maxBackoff": "120s",
                    "backoffMultiplier": 2,
                    "retryableStatusCodes": ["UNAVAILABLE"],
                },
            }
        ]
    }
)

GRPC_TASK_MANAGER_SERVICE_CONFIG = json.dumps(
    {
        "methodConfig": [
            {
                "name": [
                    {
                        "service": "com.vmware.ensemble.idem.manager.service.TaskManagerService",
                        "method": "pendingTasks",
                    },
                    {
                        "service": "com.vmware.ensemble.idem.manager.service.TaskManagerService",
                        "method": "claimTask",
                    },
                ],
                "retryPolicy": {
                    "maxAttempts": 5,
                    "initialBackoff": "2s",
                    "maxBackoff": "120s",
                    "backoffMultiplier": 2,
                    "retryableStatusCodes": ["UNAVAILABLE"],
                },
            }
        ]
    }
)
GRPC_ENABLE_RETRIES = int(os.getenv("ENV_GRPC_ENABLE_RETRIES", default=1))
GRPC_KEEPALIVE_TIME_MS = int(os.getenv("ENV_GRPC_KEEPALIVE_TIME_MS", default=60000))
GRPC_KEEPALIVE_TIMEOUT_MS = int(
    os.getenv("ENV_GRPC_KEEPALIVE_TIMEOUT_MS", default=10000)
)
GRPC_KEEPALIVE_PERMIT_WITHOUT_CALLS = (
    True
    if os.getenv("ENV_GRPC_KEEPALIVE_PERMIT_WITHOUT_CALLS", default="True") == "True"
    else False
)

GRPC_MAX_MSG_SIZE = 5242880

# Calculated Constants
WORKER_INFO_DIR = (
    os.path.join(os.getenv(IDEMD_HOME), "worker_info")
    if environ.get(IDEMD_HOME) is not None
    else None
)
WORKER_INFO_FILE_PATH = (
    Path(WORKER_INFO_DIR).joinpath("identity.json")
    if WORKER_INFO_DIR is not None
    else None
)

# Credential related constants
ACCOUNT_PROFILES = "accountProfiles"
CLOUD_ACCOUNT_ID = "cloud_acct_id"
CLOUD_ACCOUNT_DOCUMENT_SELF_LINK = "cloud_account_document_self_link"
GUARDRAILS = "guardrails"
ENVIRONMENTS = "environments"
VAULT = "vault"
SUPPORTED_VRA_ACCT_KEYS = [CLOUD_ACCOUNT_ID, CLOUD_ACCOUNT_DOCUMENT_SELF_LINK]
SUPPORTED_ACCT_KEYS = [
    ENVIRONMENTS,
    GUARDRAILS,
    CLOUD_ACCOUNT_ID,
    CLOUD_ACCOUNT_DOCUMENT_SELF_LINK,
]

# CSP Constants
CSP_AUTH_TOKEN_URI = "{csp_base_url}/csp/gateway/am/api/auth/authorize"
BEARER_TOKEN = "Bearer {access_token}"
CSP_TOKEN = "csp_token"

AWS_PROVIDER = "aws"
AZURE_PROVIDER = "azure"

LOG_FORMATTER = (
    "%(asctime)s.%(msecs)06d [%(filename)s:%(lineno)s - %(funcName)2s()] [%(levelname)s] "
    "[%(process)d %(processName)s %(threadName)s] %(message)s"
)

DEFAULT_GIT_AUTH_TYPE = "personal_token"
GIT_AUTH_TYPE_PROP = "auth_type"

IDEM_DOC_DEFAULT_SUPPORTED_FUNCTIONS = (
    "states.aws, states.azure, states.gcp, states.avilb, exec.aws, exec.azure, "
    "exec.gcp, exec.avilb"
)
DOC_DELIMITER = ", "
IDEM_DOC_SUPPORTED_FUNCTIONS = "IDEM_DOC_SUPPORTED_FUNCTIONS"
