import grpc

from idemd.idemd.csp.constants import CLAIM_NAME_AZP
from idemd.idemd.csp.csp_token_validator import CSPTokenValidator
from idemd.idemd.validate.white_list import ALLOWED_SERVICES


class CSPAuthValidationInterceptor(grpc.ServerInterceptor):
    """
    Interceptor for CSP token-based authentication and authorization validation in gRPC server.
    """

    def intercept_service(self, continuation, handler_call_details):
        """
        Intercepts the gRPC service invocation and performs CSP token validation.

        Args:
            continuation (callable): Continuation of the RPC handling process.
            handler_call_details (grpc.HandlerCallDetails): Details of the RPC call.

        Returns:
            grpc.RpcMethodHandler: The method handler for the intercepted RPC call.

        Raises:
            grpc.RpcMethodHandler: An error handler with the appropriate status and message.

        """
        metadata = dict(handler_call_details.invocation_metadata)

        if "authorization" not in metadata:
            # Headers are empty, fail the request
            error_status = grpc.StatusCode.INVALID_ARGUMENT
            error_message = "Missing 'Authorization' header"
            return grpc.unary_unary_rpc_method_handler(
                self._create_error_handler(error_status, error_message)
            )

        token = metadata.get("authorization")
        validator = CSPTokenValidator()
        payload = validator.decode_token(token)
        response = validator.validate_token(payload)

        if response.is_successful:
            if validator.is_client_credential() and self.check_white_list(validator):
                return continuation(handler_call_details)
            else:
                error_status = grpc.StatusCode.PERMISSION_DENIED
                error_message = (
                    "Invalid CSP token. Service token for white-listed clients required"
                )
                return grpc.unary_unary_rpc_method_handler(
                    self._create_error_handler(error_status, error_message)
                )
        else:
            error_status = grpc.StatusCode.UNAUTHENTICATED
            error_message = "Invalid CSP token. Decode failed"
            return grpc.unary_unary_rpc_method_handler(
                self._create_error_handler(error_status, error_message)
            )

    @staticmethod
    def _create_error_handler(status, message):
        """
        Creates an error handler function that aborts the gRPC context with the specified status and message.

        Args:
            status (grpc.StatusCode): The gRPC status code.
            message (str): The error message.

        Returns:
            callable: The error handler function.

        """

        def error_handler(request, context):
            context.abort(status, message)

        return error_handler

    @staticmethod
    def check_white_list(validator):
        """
        Checks if the service in the CSP token is present in the allowed services whitelist.

        Args:
            validator (CSPTokenValidator): The CSP token validator object.

        Returns:
            bool: True if the service is whitelisted, False otherwise.

        """
        allowed_services = ALLOWED_SERVICES.split(",")
        return (
            validator.has_claim(CLAIM_NAME_AZP)
            and validator.decoded[CLAIM_NAME_AZP] in allowed_services
        )
