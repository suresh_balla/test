"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""
from idemd.idemd.validate.utils.command_utils import run_subprocess


# Execute `terraform-config-inspect --json` command on the provided path
def run_config_inspect(path):
    tf_config_inspect_cmd = [
        "terraform-config-inspect",
        "--json",
    ]
    return run_subprocess(tf_config_inspect_cmd, path)
