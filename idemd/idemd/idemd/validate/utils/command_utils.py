"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import os
import subprocess


# to execute validate command with a custom env
def run_subprocess_with_env(command, custom_env, folder_path):
    return subprocess.run(
        command,
        env=custom_env,
        capture_output=True,
        cwd=folder_path,
        text=True,
    )


# copy the original environment to add custom env variables
def run_subprocess(command, folder_path):
    environ_copy = dict(os.environ)
    return run_subprocess_with_env(command, environ_copy, folder_path)
