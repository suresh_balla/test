"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""
from idemd.idemd.validate.utils.command_utils import run_subprocess_with_env


# Execute `idem validate` command
def run_idem_validate(
    custom_env, folder_path, file_name, random_id, cache_path, idem_log_file_path
):
    idem_validate_command = [
        "idem",
        "validate",
        file_name,
        "--run-name",
        random_id,
        "--output",
        "json",
        "--cache-dir",
        cache_path,
        "--log-file",
        idem_log_file_path,
    ]

    return run_subprocess_with_env(idem_validate_command, custom_env, folder_path)
