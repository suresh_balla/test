"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""
import base64
import json
import os
import sys
from pathlib import Path

from idemgrpc import messages_pb2 as messages_pb2

from idemd.idemd.constants.constants import (
    IDEMD_HOME,
    DEFAULT_GIT_AUTH_TYPE,
    GIT_AUTH_TYPE_PROP,
)
from idemd.idemd.grpc_clients import idem_service_client_from_url
from idemd.idemd.run_space import remove_space_dir
from idemd.idemd.utils.git_credential_loader import GitCredentialsLoader
from idemd.idemd.utils.git_utils import clone_or_update_repo, resolve_git_context
from idemd.idemd.utils.iac_log_utils import get_default_logger
from idemd.idemd.validate.utils.idem_utils import run_idem_validate

logger = get_default_logger()


def clean_workspace(folder_path):
    if folder_path is not None:
        logger.info(f"Cleaning up workspace {folder_path}")
        remove_space_dir(None, Path(folder_path))


def create_git_workspace(task):
    git_context, git_meta = None, None
    try:
        git_context, git_meta = resolve_git_context(task)
    except RuntimeError as e:
        logger.error(
            f"Error resolving Git context for task id {task.taskIdentity.taskId}. Error {str(e)}"
        )

    if git_context is None:
        raise RuntimeError("Only Git is supported.")

    # Create a folder with the taskIdentity.taskId as the folder name
    folder_path = os.path.join(os.getenv(IDEMD_HOME), task.taskIdentity.taskId)
    os.makedirs(folder_path, exist_ok=True)

    if git_meta is not None and {} != git_meta:
        auth_type = git_meta.get(GIT_AUTH_TYPE_PROP)
    else:
        auth_type = DEFAULT_GIT_AUTH_TYPE

    credentials = GitCredentialsLoader.load_git_credentials_from_environment(
        auth_type=auth_type
    )

    git_context.credentials = credentials
    git_context.clone_path = folder_path

    try:
        git_context = clone_or_update_repo(git_context)
    except Exception as exc:
        clean_workspace(folder_path)
        raise exc

    return folder_path, git_context


def get_entry_point_from_task(validate_task):
    try:
        execution_context = json.loads(validate_task.executionContext)
    except json.decoder.JSONDecodeError as err:
        message = f"Error decoding execution_context for task {validate_task.taskIdentity.taskId}: {str(err)}"
        logger.error(message)
        raise RuntimeError(message)

    sls_entry_point = None

    if (
        execution_context["gitExecutionContext"]
        and len(execution_context["gitExecutionContext"]) != 0
    ):
        git_execution_context = execution_context["gitExecutionContext"]
        sls_entry_point = git_execution_context["slsSourceEntryPoint"]

    return sls_entry_point


def create_and_get_cache_at_path(folder_path):
    cache_path = folder_path + "/" + "idem_cache"
    os.makedirs(cache_path, exist_ok=True)
    return cache_path


def get_custom_env_from_task(validate_task):
    environ_copy = dict(os.environ)

    for key, value in validate_task.environments.items():
        environ_copy[key] = value

    return environ_copy


def get_execution_meta_from_git(task, git_context):
    git_meta = {"iacGitInfo": {}}
    git_meta["iacGitInfo"].update(task.iacGitInfo)

    commit_id = task.iacGitInfo.get("commitId", None)
    if commit_id is None:
        git_meta["iacGitInfo"].update({"commitId": git_context.commit_id})

    branch = task.iacGitInfo.get("branch", None)
    if branch is None:
        git_meta["iacGitInfo"].update({"branch": git_context.branch_name})

    return json.dumps(git_meta)


def get_local_idem_service_stub():
    idem_service_host = "localhost"
    idem_service_port = os.getenv("GRPC_PROXY_SERVER_PORT", 4026)
    return idem_service_client_from_url(f"{idem_service_host}:{idem_service_port}")


def start_validation(validate_task):
    folder_path = None
    git_context = None
    try:
        logger.info(
            f"Received validation request for {validate_task.taskIdentity.taskId}"
        )

        folder_path, git_context = create_git_workspace(validate_task)

        logger.info(
            f"Workspace created for validate request {validate_task.taskIdentity.taskId}"
        )

        sls_entry_point = get_entry_point_from_task(validate_task)
        cache_path = create_and_get_cache_at_path(folder_path)
        custom_env = get_custom_env_from_task(validate_task)

        idem_log_file_name = "idem.log"
        idem_log_file_path = folder_path + "/" + idem_log_file_name

        logger.info(f"Running validate at {folder_path}")

        result = run_idem_validate(
            custom_env,
            folder_path,
            sls_entry_point,
            validate_task.taskIdentity.taskId,
            cache_path,
            idem_log_file_path,
        )

        send_update_validate_task_request(validate_task, result, git_context, None)
    except Exception as err:
        send_update_validate_task_request(validate_task, None, git_context, err)
        logger.error(
            f"Validate request {validate_task.taskIdentity.taskId} failed with {str(err)}"
        )
    finally:
        clean_workspace(folder_path)


def send_update_validate_task_request(task, result, git_context, err):
    stub = get_local_idem_service_stub()

    execution_meta = get_execution_meta_from_git(task, git_context)

    request = messages_pb2.ValidateTaskUpdateRequest(
        taskIdentity=task.taskIdentity, executionMeta=execution_meta
    )
    if result is not None:
        if result.returncode == 0:
            logger.info(
                f"Task: {task.taskIdentity.taskId}, Validate status: COMPLETED with return code: "
                f"{result.returncode}."
            )
            request.status = "COMPLETED"
            request.response = result.stdout
            request.errorMessage = ""
        else:
            logger.info(
                f"Task: {task.taskIdentity.taskId}, Validate status: FAILED with return code: "
                f"{result.returncode}"
            )
            request.status = "FAILED"
            request.response = ""
            request.errorMessage = f"Validate failed with {result.stdout}"
    else:
        logger.info(
            f"Task: {task.taskIdentity.taskId}, Validate status: FAILED with error: {err}."
        )
        request.status = "FAILED"
        request.errorMessage = f"Validate failed with {err}"

    response = stub.updateValidateTask(request)

    if response.isUpdated:
        logger.info(f"Task {task.taskIdentity.taskId} was updated successfully.")
    else:
        logger.error(f"Task {task.taskIdentity.taskId} update failed.")


if __name__ == "__main__":
    validate_input_encoded = sys.argv[1]
    serialized_bytes = base64.b64decode(validate_input_encoded)
    validate_input = messages_pb2.ValidateInput.FromString(serialized_bytes)
    start_validation(validate_input)
