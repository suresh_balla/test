"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""
import json
import multiprocessing
import os
import signal
import threading
import time
import uuid
import zipfile
from pathlib import Path

import grpc
from idemgrpc import messages_pb2 as messages
from idemgrpc import worker_pb2_grpc as terraform_pb2_grpc

from idemd.idemd.constants.constants import (
    IDEMD_HOME,
    GRPC_KEEPALIVE_TIME_MS,
    GRPC_KEEPALIVE_TIMEOUT_MS,
    GRPC_KEEPALIVE_PERMIT_WITHOUT_CALLS,
)
from idemd.idemd.run_space import remove_space_dir
from idemd.idemd.utils.iac_log_utils import get_default_logger
from idemd.idemd.utils.worker_threadpool_executor import WorkerThreadPoolExecutor
from idemd.idemd.validate.csp_auth_validation_interceptor import (
    CSPAuthValidationInterceptor,
)
from idemd.idemd.validate.utils.idem_utils import run_idem_validate
from idemd.idemd.validate.utils.terraform_utils import run_config_inspect

logger = get_default_logger()

# Global variable to track if the server is shutting down
is_shutting_down = False
NUM_VALIDATE_WORKERS = min(multiprocessing.cpu_count(), 10)

# Counter to track active tasks
active_tasks = 0
active_tasks_lock = threading.Lock()

server = None


class TerraformConfigInspectNotFoundError(Exception):
    pass


# Function to increment the active task counter
def increment_active_tasks():
    global active_tasks
    with active_tasks_lock:
        active_tasks += 1


# Function to decrement the active task counter
def decrement_active_tasks():
    global active_tasks
    with active_tasks_lock:
        active_tasks -= 1


# Check if the ThreadPool is busy
def is_threadpool_busy():
    with active_tasks_lock:
        return active_tasks > 0


class IacValidationServicer(terraform_pb2_grpc.IacValidationServicer):
    def validateIacContent(self, request_iterator, context):
        # Generate a random ID
        random_id = str(uuid.uuid4())
        logger.info(
            f"Received a validate request. Assigning it random identifier - {random_id}"
        )

        content_chunks = []
        file_name = None
        file_type = None
        iac_type = None
        meta = None

        logger.info(
            f"Reconstructing the content from the chunks for validated request {random_id}"
        )
        for request in request_iterator:
            content_chunks.append(request.content.content)
            if file_type is None:
                file_type = request.content.fileType
            if file_name is None:
                file_name = request.content.fileName
            if iac_type is None:
                iac_type = request.iacProvider
            if meta is None:
                meta = request.meta

        combined_content = b"".join(content_chunks)

        # Create a folder with the random ID as the folder name
        folder_path = os.path.join(os.getenv(IDEMD_HOME), random_id)
        os.makedirs(folder_path)

        file_save_path = folder_path + "/" + file_name

        if file_name.endswith(".zip"):
            logger.info(f"Saving zip file to {folder_path}")
            with open(file_save_path, "wb") as file:
                file.write(combined_content)

            with zipfile.ZipFile(file_save_path, "r") as zip_ref:
                zip_ref.extractall(folder_path)

        else:
            # Save the file locally
            logger.info(f"Saving file to {folder_path}")
            with open(file_save_path, "wb") as file:
                file.write(combined_content)
        try:
            # Idem Validate
            if iac_type == messages.IAC_PROVIDER.IDEM:
                custom_env = (
                    os.environ.copy()
                )  # copy the original environment to add custom env variables

                entrypoint = None

                if meta is not None and meta != "":
                    meta_json = json.loads(meta)

                    if "ENVIRONMENTS" in meta_json:
                        environments = meta_json["ENVIRONMENTS"]
                        for key, value in environments.items():
                            custom_env[key] = value
                            logger.info(
                                f"Custom Environment Variable are key -> {key},"
                                f" value -> {value}"
                            )
                    if "entrypoint" in meta_json:
                        entrypoint = meta_json["entrypoint"]

                cache_path = folder_path + "/" + "idem_cache"
                os.makedirs(cache_path)

                idem_log_file_name = "idem.log"
                idem_log_file_path = folder_path + "/" + idem_log_file_name
                with open(idem_log_file_path, "wb"):
                    pass

                # zip file support
                if file_name.endswith(".zip"):
                    if entrypoint is not None:
                        sls_file_name = meta_json["entrypoint"]
                        sls_file_name = sls_file_name.replace(".sls", "")
                        response = run_iac_content_validation_idem(
                            custom_env,
                            folder_path,
                            sls_file_name,
                            random_id,
                            cache_path,
                            idem_log_file_path,
                        )

                    else:
                        logger.info("Entrypoint for zip file not provided")

                else:
                    file_name = file_name.replace(".sls", "")
                    response = run_iac_content_validation_idem(
                        custom_env,
                        folder_path,
                        file_name,
                        random_id,
                        "idem_cache",
                        idem_log_file_name,
                    )

            elif iac_type == messages.IAC_PROVIDER.TERRAFORM:
                response = run_iac_content_validation(folder_path)

            else:
                logger.info("Invalid IAC_TYPE received")

        finally:
            remove_space_dir(None, Path(folder_path))

        return response


def handle_shutdown(signum, frame):
    global is_shutting_down, server
    logger.info("Received shutdown signal, gracefully stopping the server.")
    is_shutting_down = True

    # Check if the ThreadPool is busy before shutting down
    while is_threadpool_busy():
        logger.info("Thread pool is busy. Waiting for tasks to complete.")
        time.sleep(10)
    else:
        logger.info("Thread pool is idle. Shutting down the server.")
        if server is not None:
            server.stop(grace=5)


def set_go_environment(go_path):
    # Check if GOPATH is already set
    if os.environ.get("GOPATH") is None:
        # Set the GOPATH environment variable
        os.environ["GOPATH"] = go_path

    # Add the Go binary path to the system's PATH variable if not already present
    go_bin_path = os.path.join(go_path, "bin")
    if go_bin_path not in os.environ.get("PATH", "").split(os.pathsep):
        os.environ["PATH"] = f"{go_bin_path}{os.pathsep}{os.environ['PATH']}"


def run_iac_content_validation_idem(
    custom_env, folder_path, file_name, random_id, cache_path, idem_log_file_path
):
    response = messages.IacValidationResponse()

    try:
        logger.info(f"Running validate at {folder_path}")
        result = run_idem_validate(
            custom_env,
            folder_path,
            file_name,
            random_id,
            cache_path,
            idem_log_file_path,
        )

        response = get_response(result)

    except Exception as e:
        # Exception occurred during execution, return error message
        response.data = ""
        response.success = False
        response.error = f"Validate at {folder_path} failed with {str(e)}"

    logger.info(f"returning response {threading.current_thread().name}")
    return response


def get_response(result):
    response = messages.IacValidationResponse()

    if result.returncode == 0:
        response.data = result.stdout
        response.success = True
        response.error = ""

    else:
        response.data = {}
        response.success = False
        response.error = f"Validate failed with {result.stderr}"

    return response


def run_iac_content_validation(path):
    response = messages.IacValidationResponse()

    try:
        logger.info(f"Running validate at {path}")
        result = run_config_inspect(path)

        response = get_response(result)

    except Exception as e:
        # Exception occurred during execution, return error message
        response.data = ""
        response.success = False
        response.error = f"Validate at {path} failed with {str(e)}"

    logger.info(f"returning response {threading.current_thread().name}")
    return response


def verify_terraform_config_inspect_on_path():
    try:
        response = run_config_inspect(".")
        return response.returncode == 0
    except Exception:
        logger.error(
            "Error occurred when validating the infra requirements for config inspect"
        )
        return False


def serve(host="localhost", port="4040"):
    if (
        os.getenv("ENABLE_TF_CONFIG_INSPECT") is not None
        and os.getenv("ENABLE_TF_CONFIG_INSPECT").lower() == "true"
    ):
        if not verify_terraform_config_inspect_on_path():
            raise EnvironmentError("Terraform Config Inspect not found")

    global server

    executor = WorkerThreadPoolExecutor(
        max_workers=NUM_VALIDATE_WORKERS,
        thread_name_prefix="validate_thread",
        initializer=WorkerThreadPoolExecutor.initializer,
        initargs=(),
    )

    options = [
        ("grpc.keepalive_time_ms", GRPC_KEEPALIVE_TIME_MS),
        ("grpc.keepalive_timeout_ms", GRPC_KEEPALIVE_TIMEOUT_MS),
        ("grpc.keepalive_permit_without_calls", GRPC_KEEPALIVE_PERMIT_WITHOUT_CALLS),
    ]

    server = grpc.server(
        thread_pool=executor,
        interceptors=[CSPAuthValidationInterceptor()],
        options=options,
    )

    # Add servicer to server
    terraform_pb2_grpc.add_IacValidationServicer_to_server(
        IacValidationServicer(), server
    )

    # Configure server options
    server.add_insecure_port(f"[::]:{int(port)}")

    # Start the server
    server.start()
    logger.info(f"Worker started listening for validate requests on port {port}")

    # Register signal handler for graceful shutdown
    signal.signal(signal.SIGINT, handle_shutdown)
    signal.signal(signal.SIGTERM, handle_shutdown)

    try:
        # Wait for termination signal
        server.wait_for_termination()
    except KeyboardInterrupt:
        handle_shutdown(signal.SIGINT, None)
