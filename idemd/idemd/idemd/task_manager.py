"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import os
import time
from pathlib import Path
from time import sleep

import grpc
from idemgrpc import messages_pb2 as messages_pb2

from idemd.idemd.constants.constants import TASK_CLAIM_TIMEOUT
from idemd.idemd.idem_execution_utils import execute_idem_task_helper
from idemd.idemd.idem_service_utils import send_task_update_for_failure
from idemd.idemd.proc import get_grace_timeout_interval_in_seconds
from idemd.idemd.tasks.script.execution_utils import execute_script_task_helper
from idemd.idemd.terraform.tf_execution_utils import execute_terraform_task_helper


def get_system_memory_utilization(hub):
    try:
        memory_usage = hub.memory_node.controller.usage_in_bytes
        memory_limit = hub.memory_node.controller.limit_in_bytes
        return (memory_usage / memory_limit) * 100
    except Exception as e:
        file_path1 = "/sys/fs/cgroup/memory.current"
        with open(file_path1, "r") as file1:
            memory_usage = int(file1.read())

        file_path2 = "/sys/fs/cgroup/memory.max"
        with open(file_path2, "r") as file2:
            memory_limit = int(file2.read())
        hub.log.info(f"Error observed while using cgroup v1 {e}")
        hub.log.info(f"memory_usage is {memory_usage}")
        hub.log.info(f"memory_limit is {memory_limit}")
        return (memory_usage / memory_limit) * 100


def send_to_claim(hub, task_notification):
    hub.log.info(
        f"Received a notification for task availability. "
        f"Worker {hub.idemd.worker.workerId} sending claim request"
    )
    response = None
    try:
        if task_notification.taskAvailable:
            response = hub.idemd.task_manager.claim_task()
    except Exception as e:
        hub.log.error(e)

    # For a successful claim, try running the task in idem runtime
    if response and response.claimed:
        task_id = response.taskSummary.taskIdentity.taskId
        try:
            ret, msg = hub.idemd.task_manager.execute_task_helper(response.taskSummary)
            hub.log.info(f"idem worker ran task {task_id} with return code {ret}")
            if ret != 0:
                hub.log.error(
                    f"A non zero exit code occurred for task with id {task_id}. With Error: {msg}"
                )
                if ret != -1:
                    raise Exception(msg)
        except Exception as err:
            # If an exception occurred
            hub.log.error(f"Error occurred when running task with ID {task_id} : {err}")
            send_task_update_for_failure(hub, response.taskSummary, err)
        return task_id
    else:
        hub.log.info(
            f"Worker {hub.idemd.worker.workerId} couldn't claim. Proceeding with next available task"
        )
        return None


def claim_task(hub):
    hub.log.info("Pre processing started before claiming task")
    stub = hub.idemd.task_manager_stub
    worker = hub.idemd.worker
    request = messages_pb2.ClaimTaskRequest()
    if worker.identity.orgId:
        request.workerIdentity.identity.orgId = worker.identity.orgId

    if worker.identity.projectId:
        request.workerIdentity.identity.projectId = worker.identity.projectId

    request.workerIdentity.workerType = worker.workerType
    request.workerIdentity.workerId = worker.workerId
    request.workerIdentity.iacProvider = worker.iacProvider

    if worker.workerGroupId:
        request.workerIdentity.workerGroupId = worker.workerGroupId
        hub.log.info(
            f"Sending claim task request from worker ID {worker.workerId},"
            f" worker group id {worker.workerGroupId} and orgId {worker.identity.orgId}"
        )
    else:
        hub.log.info(f"Sending claim task request from worker ID {worker.workerId}")

    try:
        task_claim_timeout = int(os.getenv("TASK_CLAIM_TIMEOUT", TASK_CLAIM_TIMEOUT))
        response = stub.claimTask(request, timeout=task_claim_timeout)
        return response
    except Exception as exc:
        hub.log.warning(
            f"Timeout observed while worker {worker.workerId} is trying to claim a task: {exc}"
        )
        return None


def execute_task_helper(hub, task):
    if messages_pb2.IAC_PROVIDER.TERRAFORM == task.taskIdentity.iacProvider:
        hub.log.info(f"Running TERRAFORM task {task.taskIdentity.taskId}")
        return execute_terraform_task_helper(hub, task)
    elif messages_pb2.IAC_PROVIDER.SCRIPT == task.taskIdentity.iacProvider:
        hub.log.info(f"Running SCRIPT task {task.taskIdentity.taskId}")
        return execute_script_task_helper(hub, task)
    else:
        hub.log.info(f"Running IDEM task {task.taskIdentity.taskId}")
        return execute_idem_task_helper(hub, task)


def wait_on_pending_tasks(hub):
    worker = hub.idemd.worker
    # enabled connection retry by default
    disable_connection_retry = os.getenv("DISABLE_CONNECTION_RETRY", "false")

    while not hub.idemd.worker_terminate:
        liveness_heartbeat()
        # use the latest stub from hub as it will have the latest channel details
        task_manager_stub = hub.idemd.task_manager_stub
        hub.log.info(f"Worker {worker.workerId} waiting on pending tasks")
        try:
            # https://github.com/grpc/grpc/blob/master/doc/wait-for-ready.md
            for task_notification in task_manager_stub.pendingTasks(
                worker, wait_for_ready=True, timeout=hub.idemd.channel_refresh_interval
            ):
                liveness_heartbeat()
                wait_or_proceed_to_claim_task(hub)
                update_worker_terminate(hub)
                if not hub.idemd.worker_terminate:
                    hub.idemd.thread_pool.submit(send_to_claim, hub, task_notification)
                else:
                    break

        except grpc.RpcError as exc:
            # TODO: introduce error code specific logging
            hub.log.warning(f"Error observed in the grpc channel: {exc}")
            if disable_connection_retry.lower() == "false":
                update_worker_terminate(hub)
                if not hub.idemd.worker_terminate:
                    verify_or_reconnect_server_channel(hub, worker)
            else:
                raise exc

    hub.log.info(
        f"Worker {worker.workerId} is about to be terminated. "
        f"Process {hub.idemd.pid} cannot claim anymore tasks"
    )

    # This is to give some settling time for all the ongoing tasks to reach a terminal state
    timeout = get_grace_timeout_interval_in_seconds()
    wait_for_terminate(timeout=timeout, interval=30)


def wait_for_terminate(timeout, interval):
    """
    Waits for a certain timeout and calls the given function at frequent intervals until the timeout is reached.
    :param timeout: The time to wait before giving up (in seconds).
    :param interval: The time interval at which to call the liveness (in seconds).
    """
    start_time = time.time()
    end_time = start_time + timeout
    while time.time() < end_time:
        liveness_heartbeat()
        time.sleep(interval)


def liveness_heartbeat():
    liveness_file = os.environ.get("LIVENESS_FILE", "live")
    Path(liveness_file).touch()


def wait_or_proceed_to_claim_task(hub):
    # Flag to enable/disable memory utilization check
    disable_memory_check = os.getenv("DISABLE_MEMORY_CHECK")
    if disable_memory_check is None or disable_memory_check.lower() == "false":
        memory_threshold = os.getenv("MEMORY_THRESHOLD", 70)
        memory_throttle_delay = os.getenv("MEMORY_THROTTLE_DELAY", 5)
        memory_usage = get_system_memory_utilization(hub)

        while memory_usage > memory_threshold:
            hub.log.info(
                f"Sleeping for {memory_throttle_delay} seconds as Memory usage: {memory_usage}% "
                f" exceeds the threshold!!"
            )
            sleep(memory_throttle_delay)
            liveness_heartbeat()
            memory_usage = get_system_memory_utilization(hub)


def update_worker_terminate(hub):
    # Update hub.idemd.worker_terminate based on external file flag
    if not hub.idemd.worker_terminate:
        if os.path.exists(hub.idemd.termination_file_path):
            hub.idemd.worker_terminate = True


def verify_or_reconnect_server_channel(hub, worker):
    if hub.idemd.task_manager_channel is not None:
        try:
            hub.log.info(
                f"Trying to verify connection with server for worker {worker.workerId}"
            )
            grpc.channel_ready_future(hub.idemd.task_manager_channel).result(timeout=60)
            hub.log.info(f"grpc channel is active for worker {worker.workerId}")
        except grpc.FutureTimeoutError:
            hub.log.info(
                f"grpc channel is not active. Establishing the connection again for worker"
                f" {worker.workerId}"
            )
            # Close the existing channel
            hub.idemd.task_manager_channel.close()
            hub.idemd.grpc_clients.init_task_manager_client()
    else:
        # Added log only for debugging purpose
        hub.log.warning(f"Grpc channel is not set for worker {worker.workerId}")
