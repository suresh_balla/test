"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

# Create a new run space

import base64
import json
import os
from json import JSONDecodeError
from pathlib import Path
from zipfile import ZipFile

import yaml
from idemgrpc import messages_pb2
from yaml.loader import SafeLoader

from idemd.idemd.constants.constants import (
    RUN_NAME_DELIMITER,
    HOME,
    IDEMD,
    IDEMD_HOME,
    CSP_AUTH_TOKEN_URI,
    DEFAULT_GIT_AUTH_TYPE,
    GIT_AUTH_TYPE_PROP,
)
from idemd.idemd.utils.git_credential_loader import (
    GitCredentialsLoader,
)
from idemd.idemd.utils.git_utils import GitCloneContext, clone_or_update_repo
from idemd.idemd.utils.iac_log_utils import get_default_logger

logger = get_default_logger()


def generate_run_id(hub, requester, org_id, project_id, task_id):
    return RUN_NAME_DELIMITER.join([requester, org_id, project_id, task_id])


def split_run_id(run_id):
    # setting the maxsplit parameter to 4, will return a list with 4 elements!
    result = {}
    if is_not_blank(run_id):
        temp = run_id.split(RUN_NAME_DELIMITER, 4)
        if 4 == len(temp):
            result["requester"] = temp[0]
            result["org_id"] = temp[1]
            result["project_id"] = temp[2]
            result["task_id"] = temp[3]

    return result


def get_space_dir(hub, run_id):
    return os.path.join(os.getenv(HOME), IDEMD, run_id)


def write_to_file(hub, acct_file_path, acct_file_content):
    f = open(acct_file_path, "wb")
    hub.log.debug(f"Final acct file content {acct_file_content}")
    f.write(acct_file_content)
    f.close()


def create_or_get_run_space_dir(hub, task):
    task_id = task.taskIdentity.taskId
    org_id = task.taskIdentity.identity.orgId
    project_id = task.taskIdentity.identity.projectId
    requester = task.requester
    run_id = generate_run_id(hub, requester, org_id, project_id, task_id)

    space_dir = get_space_dir(hub, run_id)

    hub.log.info(f"Creating directory for run ID {run_id}")
    # This line creates the directory with the specified path if it doesn't exist
    # The `parents=True` argument allows creating parent directories if they don't exist
    # The `exist_ok=True` argument ensures that the function doesn't raise an error if the directory already exists
    Path(space_dir).mkdir(parents=True, exist_ok=True)
    return run_id, space_dir


def resolve_git_context(task):
    if task is not None and task.iacGitInfo is not None and {} != task.iacGitInfo:
        try:
            source_url = task.iacGitInfo.get("repositoryURL", None)
            source_branch = task.iacGitInfo.get("branch", None)
            source_commit_id = task.iacGitInfo.get("commitId", None)

            source_meta = task.iacGitInfo.get("meta", None)

            # Create a GitContext object
            context = GitCloneContext(
                repo_url=source_url,
                branch_name=source_branch,
                commit_id=source_commit_id,
                clone_path=None,
                credentials=None,
            )

            return context, source_meta
        except Exception as e:
            err = f"An error occurred when resolving source for task {task.taskIdentity.taskId} {str(e)}"
            logger.error(err)
            raise RuntimeError(err)
    else:
        return None, None


def clone_bundle_from_git(hub, task):
    context, meta = resolve_git_context(task)
    if context is not None:
        auth_type = DEFAULT_GIT_AUTH_TYPE
        if meta is not None and {} != meta:
            auth_type = meta.get(GIT_AUTH_TYPE_PROP)

        # This is temporary
        credentials = GitCredentialsLoader.load_git_credentials_from_environment(
            auth_type=auth_type
        )

        context.credentials = credentials
        run_id, space_dir = create_or_get_run_space_dir(hub, task)
        context.clone_path = space_dir
        context = clone_or_update_repo(context)
        return context


def download_and_extract_bundle(hub, task):
    pass


def create(hub, task):
    run_id, space_dir = create_or_get_run_space_dir(hub, task)
    task_execution_context = {"input_type": None, "execution_context": None}
    git_execution_context = {
        "slsEntryPoint": None,
        "paramsEntryPoint": None,
        "slsSources": None,
        "paramSources": None,
    }
    file_execution_context = {"path": None, "entrypoint": None}
    if task.executionContext:
        execution_context = json.loads(task.executionContext)
        if execution_context is not None and execution_context != {}:
            if execution_context.get("gitExecutionContext", None) is not None:
                git_execution_context_entrypoints = execution_context.get(
                    "gitExecutionContext", None
                )
                hub.log.info(f"Cloning git repo for run ID {run_id}")
                context = clone_bundle_from_git(hub, task)
                task_execution_context["input_type"] = "GIT"
                git_execution_context["slsEntryPoint"] = Path(
                    space_dir
                    + "/"
                    + git_execution_context_entrypoints.get("slsSourceEntryPoint")
                )
                if (
                    git_execution_context_entrypoints.get("slsParamsEntryPoint")
                    is not None
                ):
                    git_execution_context["paramsEntryPoint"] = Path(
                        space_dir
                        + "/"
                        + git_execution_context_entrypoints.get("slsParamsEntryPoint")
                    )
                git_execution_context["slsSources"] = space_dir
                git_execution_context["paramSources"] = space_dir
                task_execution_context["execution_context"] = git_execution_context
            else:
                file_execution_context_entrypoint = execution_context.get(
                    "fileExecutionContext", None
                )
                hub.log.info(f"Downloading bundle for run ID {run_id}")
                path, file_type = download_bundle(hub, task, run_id)
                task_execution_context["input_type"] = "FILE"
                file_execution_context["path"] = path
                file_execution_context[
                    "entrypoint"
                ] = file_execution_context_entrypoint.get("entrypoint", None)
                task_execution_context["execution_context"] = file_execution_context
        else:
            hub.log.info(f"Downloading bundle for run ID {run_id}")
            path, file_type = download_bundle(hub, task, run_id)
            if file_type == "application/zip":
                meta = task.meta
                if task.meta:
                    meta = json.loads(task.meta)
                file_execution_context["path"] = path
                file_execution_context["entrypoint"] = meta.get("entrypoint", None)
                task_execution_context["input_type"] = "FILE"
                task_execution_context["execution_context"] = file_execution_context
            else:
                task_execution_context["input_type"] = "FILE"
                file_execution_context["path"] = path
                task_execution_context["execution_context"] = file_execution_context
    else:
        hub.log.info(f"Downloading bundle for run ID {run_id}")
        path, file_type = download_bundle(hub, task, run_id)
        if file_type == "application/zip":
            meta = task.meta
            if task.meta:
                meta = json.loads(task.meta)
            file_execution_context["path"] = path
            file_execution_context["entrypoint"] = meta.get("entrypoint", None)
            task_execution_context["input_type"] = "FILE"
            task_execution_context["execution_context"] = file_execution_context
        else:
            task_execution_context["input_type"] = "FILE"
            file_execution_context["path"] = path
            task_execution_context["execution_context"] = file_execution_context
    if task.taskIdentity.enforcedStateId:
        hub.log.info(
            f"Trying to get old state for enforced state id {task.taskIdentity.enforcedStateId}"
        )
        download_old_state(hub, task, run_id)
    return task_execution_context


def download_bundle(hub, task, run_id):
    stub = hub.idemd.idem_stub
    bundles = stub.downloadBundle(task.taskIdentity)
    space_dir = get_space_dir(hub, run_id)

    is_file_type_checked = False
    bundle_path = None
    file_type = None
    for bundle in bundles:
        if not is_file_type_checked:
            file_type = bundle.contentType
            bundle_path = Path(space_dir).joinpath(resolve_file_name(task, file_type))
            file = open(bundle_path, "wb")
            is_file_type_checked = True
        file.write(bundle.data)
    file.close()
    hub.log.info(f"Downloaded bundle for run ID {run_id}")
    return bundle_path, file_type


def generate_config_file(hub, processed_run_config, space_dir):
    config_file = Path(space_dir).joinpath("run_config.yaml")
    config_data = {"idem": processed_run_config}
    with open(config_file, "w") as f:
        yaml.dump(config_data, f, sort_keys=False, default_flow_style=False)
    return config_file


def download_old_state(hub, task, run_id):
    stub = hub.idemd.idem_stub
    bundles = stub.downloadOldState(task.taskIdentity)
    space_dir = get_space_dir(hub, run_id)
    merged_json = {}
    is_file_type_checked = False
    old_state = None

    hub.log.info(
        f"Checking for every old states from {task.taskIdentity.iacProvider} from IDEM service."
    )
    if messages_pb2.IAC_PROVIDER.TERRAFORM == task.taskIdentity.iacProvider:
        resource_arr = []
        old_state = Path(space_dir).joinpath("customEnforcedState.tfstate")
        old_state.parent.mkdir(parents=True, exist_ok=True)
        for bundle in bundles:
            if bundle.contentType == "application/json":
                if bundle.data:
                    resource_arr.append(json.loads(bundle.data.decode()))
            elif bundle.contentType == "application/exec-meta":
                if bundle.data is not None:
                    bundle_str = bundle.data.decode()
                    try:
                        bundle_dict = json.loads(bundle_str)
                        if "version" in bundle_dict:
                            merged_json["version"] = int(bundle_dict["version"])
                        if "terraform_version" in bundle_dict:
                            merged_json["terraform_version"] = str(
                                bundle_dict["terraform_version"]
                            )
                        if "format_version" in bundle_dict:
                            merged_json["format_version"] = str(
                                bundle_dict["format_version"]
                            )
                        if "serial" in bundle_dict:
                            merged_json["serial"] = int(bundle_dict["serial"])
                        if "lineage" in bundle_dict:
                            merged_json["lineage"] = str(bundle_dict["lineage"])
                        if bundle_dict.get("outputs"):
                            if bundle_dict["outputs"] == "{}":
                                merged_json["outputs"] = {}
                            else:
                                merged_json["outputs"] = dict(bundle_dict["outputs"])
                        if bundle_dict.get("check_results"):
                            if bundle_dict["check_results"] == "None":
                                bundle_dict["check_results"] = None
                            else:
                                merged_json["check_results"] = dict(
                                    bundle_dict["check_results"]
                                )

                        hub.log.info(
                            f"execution meta retrieved for task {task.taskIdentity.taskId}"
                        )
                    except JSONDecodeError:
                        hub.log.error(
                            f"Error occurred in decoding application execution meta for task {task.taskIdentity.taskId}"
                        )
        if resource_arr:
            merged_json["resources"] = resource_arr

        if old_state and merged_json:
            with open(old_state, "w") as f:
                sss = json.dumps(
                    merged_json, separators=(",", ":"), sort_keys=True, indent=2
                )
                f.write(sss)
            hub.log.info(f"Downloaded old state for run ID {run_id}")
    else:
        for bundle in bundles:
            if not is_file_type_checked:
                old_state = Path(space_dir).joinpath("cache").joinpath("old_state.json")
                old_state.parent.mkdir(parents=True, exist_ok=True)
                is_file_type_checked = True

            bundle_json_data = json.loads(bundle.data or "{}")
            if bundle_json_data:
                for key, value in bundle_json_data.items():
                    if isinstance(value, (str, bytes, bytearray)):
                        bundle_json_data[key] = json.loads(value or "{}")
                    if key:
                        merged_json[key] = bundle_json_data[key]

        if old_state:
            file = open(old_state, "wb")
            file.write(bytes(json.dumps(merged_json), "utf-8"))
            file.close()
        hub.log.info(f"Downloaded old state for run ID {run_id}")


def resolve_file_name(task, file_type):
    if file_type is not None and file_type == "application/zip":
        file_name = "sls_source.zip"
    elif file_type is not None and file_type == "application/json":
        file_name = "old_state.json"
    else:
        if messages_pb2.IAC_PROVIDER.TERRAFORM == task.taskIdentity.iacProvider:
            file_name = "tf_source.tf"
        else:
            file_name = "sls_source.sls"
    return file_name


def read_client_credentials(self):
    space_dir = os.path.join(os.getenv(IDEMD_HOME), "idemd/idemd/idemd")
    properties_file = Path(space_dir).joinpath("application.yaml").as_posix()
    with open(properties_file) as f:
        data = yaml.load(f, Loader=SafeLoader)
        sm = data.get("hub", {}).get("sm", {}).get("enabled", False)
        hub_graph_url = data.get("hub", {}).get("graphUrl", None)
        if not sm:
            url = data["security"]["csp"]["url"]
            url = CSP_AUTH_TOKEN_URI.format(csp_base_url=url)

            client_id = data["security"]["csp"]["auth"]["client"]["id"]
            secret = data["security"]["csp"]["auth"]["client"]["secret"]

            return {"csp": True, "auth": get_csp_details_basic_auth(url, client_id, secret), "hub_graph_url": hub_graph_url}
        else:
            return {"sm": True, "auth": data["hub"]["sm"]["auth"], "hub_graph_url": hub_graph_url}


def get_csp_details_basic_auth(url, client_id, secret):
    usr_pass = client_id + ":" + secret
    usr_pass = usr_pass.encode("ascii")

    b64_val = base64.b64encode(usr_pass)
    b64_val = b64_val.decode("ascii")

    csp_details = {"url": url, "b64_creds": b64_val}

    return csp_details


# Method to remove run space created for storing idem task related params which is used by idem
# runtime for task processing recursively calls directory and removes all constituent files
def remove_space_dir(hub, path):
    for child in path.glob("*"):
        if child.is_file():
            child.unlink()
        else:
            remove_space_dir(hub, child)
    path.rmdir()


def is_not_blank(string):
    return bool(string and string.strip())


# Method expects the zip file to contain template files and sub-directories at the root level itself
# It does not expect zip file to hold the actual content under extra level of sub-directory.
def extract_bundle(bundle_name, space_dir):
    with ZipFile(bundle_name, "r") as zObject:
        # Unzip the bundle and extract the content directly under space_dir
        zObject.extractall(space_dir)
