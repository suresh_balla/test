"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import os

from idemd.idemd.constants.constants import HOME, IDEMD, GLOBAL_TASK_TIMEOUT


async def start(
    hub,
    system_acct_file: str,
    system_acct_key: str,
):
    # Initialize the broker queue for evbus
    hub.log.info("Initialize the broker queue for evbus")
    await hub.evbus.broker.init()

    # Collect ingress profiles from acct
    hub.log.info("Collect ingress profiles from acct")
    ingress_profiles = await hub.evbus.acct.profiles(
        acct_file=system_acct_file,
        acct_key=system_acct_key,
    )

    hub.evbus.ingress_profiles = ingress_profiles

    # Start the listener in its own task
    hub.log.info("Start the listener in its own task")
    listener = hub.pop.Loop.create_task(hub.evbus.init.start(ingress_profiles))
    try:
        # Wait for the event bus to be started
        hub.log.info("Wait for the event bus to be started")
        await hub.evbus.init.join()

        # Start the gRPC clients for both idem service and task manager
        # Make sure to retrieve the idem_service client first as it updates the client_credentials in Hub
        hub.idemd.grpc_clients.init_clients()

        # Set the global timeout for idem tasks. Default timeout is 10800 seconds (3hrs)
        hub.idemd.global_task_timeout = float(
            os.getenv("GLOBAL_TASK_TIMEOUT", GLOBAL_TASK_TIMEOUT)
        )
        hub.log.info(
            f"Global task timeout set to {hub.idemd.global_task_timeout} seconds"
        )

        # Set default channel refresh interval to 43200 seconds
        hub.idemd.channel_refresh_interval = int(
            os.getenv("CHANNEL_REFRESH_INTERVAL", 43200)
        )

        hub.idemd.termination_file_path = get_terminate_file_path()
        hub.idemd.worker_terminate = False
        hub.idemd.pid = os.getpid()

        # Poll for tasks
        hub.log.info("Polling for tasks")

        # Check for concurrent execution flag
        task = hub.pop.Loop.create_task(hub.idemd.task_manager.wait_on_pending_tasks())
        # Stay in the infinite loop until interrupt
        await task
    except Exception as e:
        hub.log.error(e, exc_info=True)
    finally:
        hub.log.info("Shutting down worker")
        # Stop all the coroutines
        await hub.idemd.proc.stop()

        # Stop the event bus
        await hub.evbus.init.stop()
        await listener


async def join(hub):
    await hub.evbus.init.join()
    hub.log.info("Event bus started")


async def stop(hub):
    hub.log.info("Shutting down worker")


def get_terminate_file_path():
    return os.path.join(os.getenv(HOME), IDEMD, "terminate.txt")


def get_grace_timeout_interval_in_seconds():
    return int(os.getenv("GRACE_TIMEOUT_IN_SECONDS", 360))
