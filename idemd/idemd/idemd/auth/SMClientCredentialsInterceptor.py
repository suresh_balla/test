"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json
import sys
import jwt
import uuid
from datetime import datetime, timedelta
from typing import Any, Callable

import grpc
import requests as requests
from grpc_interceptor import ClientCallDetails, ClientInterceptor


class SMClientCredentialsInterceptor(ClientInterceptor):
    def __init__(self, client_creds):
        self.client_creds = client_creds

    def intercept(
        self,
        method: Callable,
        request_or_iterator: Any,
        call_details: grpc.ClientCallDetails,
    ):
        access_token = get_token(self.client_creds)
        new_details = ClientCallDetails(
            call_details.method,
            call_details.timeout,
            [("authorization", access_token)],
            call_details.credentials,
            call_details.wait_for_ready,
            call_details.compression,
        )

        return method(request_or_iterator, new_details)


def get_client_credentials_token(client_creds):
    with open(client_creds["service-token-config"]["privateKeyFilePath"], 'r') as file:
        private_key = file.read()
    payload = {
        "sub": client_creds["service-token-config"]["azp"],
        "exp": datetime.utcnow() + client_creds["clock-skew-in-mins"] * timedelta(seconds=1000),
        "org_id": client_creds["default-org-id"],
        "context_name": client_creds["default-org-id"],
        "azp": client_creds["service-token-config"]["azp"],
        "authorization_details": [],
        "perms": [],
        "jti": f"{uuid.uuid4()}",
        "iss": client_creds["jwk-set"]["internal-issuers"],
        "iat": datetime.utcnow()
    }
    jwt_instance = jwt.PyJWT()
    token = jwt_instance.encode(payload, private_key, algorithm='RS256')
    return token


expiration_time = None
auth_token = None

safety_duration_in_secs = 10


def get_token(client_creds):
    global expiration_time
    global auth_token
    try:
        now = datetime.now()
        if not expiration_time or now - expiration_time >= timedelta(seconds=0):
            auth_token = get_client_credentials_token(client_creds)
            # reduce expiration duration time by safety_duration_in_secs
            expires_in = client_creds["clock-skew-in-mins"] * 1000 - safety_duration_in_secs
            expiration_time = now + timedelta(seconds=expires_in)
    except Exception as e:
        print("Failed to get access token from SM. Exception is " + str(e))

    return auth_token
