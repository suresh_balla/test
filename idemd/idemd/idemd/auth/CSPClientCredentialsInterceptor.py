"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json
import sys
from datetime import datetime, timedelta
from typing import Any, Callable

import grpc
import requests as requests
from grpc_interceptor import ClientCallDetails, ClientInterceptor


class CSPClientCredentialsInterceptor(ClientInterceptor):
    def __init__(self, client_creds):
        self.client_creds = client_creds

    def intercept(
        self,
        method: Callable,
        request_or_iterator: Any,
        call_details: grpc.ClientCallDetails,
    ):
        access_token = get_token(self.client_creds)
        new_details = ClientCallDetails(
            call_details.method,
            call_details.timeout,
            [("authorization", access_token)],
            call_details.credentials,
            call_details.wait_for_ready,
            call_details.compression,
        )

        return method(request_or_iterator, new_details)


def get_client_credentials_token(client_creds):
    url = client_creds["url"]
    b64_val = client_creds["b64_creds"]

    payload = "grant_type=client_credentials"
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Basic %s" % b64_val,
    }

    try:
        response = requests.post(url, headers=headers, data=payload)
        token_dict = json.loads(response.content.decode("utf-8"))
    except:
        print(
            "get token from CSP is not successful . Exception is %s", sys.exc_info()[0]
        )
        token_dict = None
    return token_dict


expiration_time = None
auth_token = None

safety_duration_in_secs = 10


def get_token(client_creds):
    global expiration_time
    global auth_token
    try:
        now = datetime.now()
        if not expiration_time or now - expiration_time >= timedelta(seconds=0):
            token_dict = get_client_credentials_token(client_creds)
            auth_token = token_dict.get("access_token")
            # reduce expiration duration time by safety_duration_in_secs
            expires_in = int(token_dict.get("expires_in")) - safety_duration_in_secs
            expiration_time = now + timedelta(seconds=expires_in)
    except Exception as e:
        print("Failed to get access token from csp. Exception is " + str(e))

    return auth_token
