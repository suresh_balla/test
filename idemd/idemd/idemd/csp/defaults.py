"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

DEFAULT_ISSUER = "https://gaz-preview.csp-vidm-prod.com"
DEFAULT_OPEN_ID_CONFIG_URL = (
    "https://console-stg.cloud.vmware.com/.well-known/openid-configuration"
)
DEFAULT_HEADERS = {"Accept": "application/json"}
