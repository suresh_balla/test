"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import os
from datetime import datetime

import jwt
import requests
from cachetools import TTLCache
from cryptography.hazmat.backends.openssl.backend import backend
from jwt.exceptions import InvalidTokenError, InvalidSignatureError, DecodeError

from idemd.idemd.csp.constants import (
    CLAIM_NAME_USERNAME,
    CLAIM_NAME_ACCT,
    CLAIM_NAME_AZP,
    CLAIM_NAME_ISSUER,
    OPEN_ID_CONFIG_URL_PROP,
    TOKEN_ISSUER,
    CLAIM_NAME_IAT,
)
from idemd.idemd.csp.defaults import (
    DEFAULT_OPEN_ID_CONFIG_URL,
    DEFAULT_ISSUER,
    DEFAULT_HEADERS,
)
from idemd.idemd.utils.iac_log_utils import get_default_logger

logger = get_default_logger()

# Create an open id cache, that can only contain one entry and expires every 24 hours. To prevent reuse of the
# cache, setting the maxsize to 1 => Only one key, which is open ID URL can be present in the cache.
# Caching OpenID config is in line with the best practices shared here
# --  https://confluence.eng.vmware.com/display/CSPSDD/OpenID+Standards+-+Well+Known+End+Points
open_id_config_cache = TTLCache(maxsize=1, ttl=86400)


class TokenValidationResponse:
    """
    This class provides functionality to decode and validate tokens using
     OpenID configuration and JWKS (JSON Web Key Set).
    """

    def __init__(self):
        self._decoded_token = None
        self._is_successful = False

    @property
    def decoded_token(self):
        return self._decoded_token

    @decoded_token.setter
    def decoded_token(self, decoded_token):
        self._decoded_token = decoded_token

    @property
    def is_successful(self):
        return self._is_successful

    @is_successful.setter
    def is_successful(self, is_successful):
        self._is_successful = is_successful


class CSPTokenValidator:
    def __init__(
        self,
    ):
        self._decoded = None
        self._open_id_url = os.getenv(
            OPEN_ID_CONFIG_URL_PROP, DEFAULT_OPEN_ID_CONFIG_URL
        )
        self.current_time = int(datetime.now().timestamp())
        self._meta = {}

    @property
    def decoded(self):
        return self._decoded

    @decoded.setter
    def decoded(self, value):
        self._decoded = value

    @property
    def open_id_url(self):
        return self._open_id_url

    @open_id_url.setter
    def open_id_url(self, value):
        self._open_id_url = value

    def get_meta_string(self):
        return ", ".join(f"{key}={value}" for key, value in self._meta.items())

    def decode_token(self, _token):
        """
        Decodes the provided token using the OpenID configuration and JWKS.

        Parameters:
            _token (str): The token to be decoded.

        Returns: dict or None: If the token is successfully decoded and validated, returns a dictionary representing
        the decoded token. If the token is invalid or cannot be decoded, returns None.

        Raises:
            None

        Notes:
            This method retrieves the OpenID configuration, including the issuer and JWKS URL.
            It uses the JWKS URL to obtain the signing key for the token.
            The token is then decoded using the RS256 algorithm and the obtained key.
            If the decoding process is successful, the decoded token dictionary is returned.
            If the token is invalid or an error occurs during decoding, None is returned.
        """
        open_id_config = self.get_open_id_config()

        if open_id_config is None:
            logger.error(
                f"Could not retrieve open ID configuration from {self.open_id_url}"
            )
            return None

        _issuer = open_id_config.get("issuer")
        if _issuer is None:
            logger.error("issuer is none")
            return None

        jwks_url = open_id_config.get("jwks_uri", None)

        if jwks_url is None:
            logger.error("jwks_uri is none")
            return None

        try:
            jwks_client = jwt.PyJWKClient(jwks_url)
            key = jwks_client.get_signing_key_from_jwt(_token)

            if hasattr(backend, "_fips_enabled"):
                if backend._fips_enabled:
                    logger.info("The backend used by JWT library is FIPS-enabled")
                else:
                    logger.info("The backend used by JWT library is not FIPS-enabled")

            self._decoded = jwt.decode(_token, algorithms=["RS256"], key=key.key)
            return self._decoded
        except InvalidSignatureError:
            logger.error("Signature verification for the token failed")
            return None
        except DecodeError:
            logger.error("Error occurred when decoding the token")
            return None
        except InvalidTokenError:
            logger.error("Token is invalid")
            return None

    def validate_token(self, decoded_token):
        """
        Validates the provided decoded token for expiration, issuer, and other validations.

        Parameters:
            decoded_token (dict): The decoded token to be validated.

        Returns: TokenValidationResponse: An instance of the TokenValidationResponse class containing information
        about the validation result.

        Raises:
            None
        """
        response = TokenValidationResponse()
        self.decoded = decoded_token
        try:
            if self.decoded is not None:
                if not self.validate_claims():
                    return response

                if self.is_user_token():
                    logger.info("User token")
                else:
                    logger.info("Client token")
                response.decoded_token = self.decoded
                response.is_successful = True

        except InvalidTokenError:
            logger.error("Token is invalid")

        return response

    def get_open_id_config(self):
        logger.info("Retrieving Open ID well known configuration")
        headers = DEFAULT_HEADERS
        open_id_config = None

        if self.open_id_url in open_id_config_cache:
            # If the configuration is present in the cache, return it.
            logger.info("Open ID configuration was retrieved from the cache.")
            open_id_config = open_id_config_cache[self.open_id_url]
        else:
            try:
                response = requests.get(self.open_id_url, headers=headers)
                response.raise_for_status()
                open_id_config = response.json()
                open_id_config_cache[self.open_id_url] = open_id_config
                logger.info(
                    "Open ID configuration was retrieved from the server and cached."
                )
            except Exception as err:
                logger.error(
                    f"Failure in retrieving the Open ID configuration. {str(err)}"
                )

        return open_id_config

    def is_user_token(self):
        # If "acct" or "username" claim exist, it is a user token; otherwise, it is a client token.
        return self.has_claim(CLAIM_NAME_ACCT) or self.has_claim(CLAIM_NAME_USERNAME)

    def has_claim(self, claim):
        return claim in self.decoded

    def is_client_credential(self):
        return not self.is_user_token()

    def get_client_id(self):
        if self.is_client_credential():
            return self.decoded[CLAIM_NAME_AZP]
        return None

    def validate_iat(self):
        if CLAIM_NAME_IAT not in self.decoded:
            logger.warning("Missing 'iat' claim")
            return False

        # Check the token issuance time against the current time
        token_issued_time = self.decoded[CLAIM_NAME_IAT]

        # Check if 'iat' claim is a valid numeric value
        if not isinstance(token_issued_time, int):
            logger.warning("Invalid 'iat' claim value")
            return False

        # Compare the token issuance time with the current time
        if self.current_time - token_issued_time < 0:
            logger.warning("Invalid 'iat' claim value. Issued at a future time")
            return False

        return True

    def validate_issuer(self):
        if CLAIM_NAME_ISSUER not in self.decoded:
            logger.warning("Missing issuer in the token")
            return False

        # This call will retrieve the data from the cache only. So it should be okay to call the method again
        open_id_config = self.get_open_id_config()

        expected_issuer = open_id_config.get("issuer")

        issuer = self.decoded[CLAIM_NAME_ISSUER]

        # As per https://confluence.eng.vmware.com/display/CSPSDD/Token+Validation+with+CSP+Issuer+URL,
        # GAZ issuers will be deprecated soon. The ISSUER must be compared with the issuer from the well known
        # configuration.
        if issuer == expected_issuer:
            return True
        # Only as a fallback, use the GAZ issuers
        expected_issuer = os.getenv(TOKEN_ISSUER, DEFAULT_ISSUER)
        if issuer != os.getenv(TOKEN_ISSUER, DEFAULT_ISSUER):
            logger.warning(
                f"Issuer mismatch. Token contains {issuer}. It should be {expected_issuer}"
            )
            return False
        return True

    def validate_expiration(self):
        if "exp" not in self.decoded:
            logger.warning("Token is missing expiration time")
            return False

        if self.decoded["exp"] < self.current_time:
            logger.warning("Token expired")
            return False

        return True

    def validate_claims(self):
        # Check expiration
        # Check issuer
        # Check if 'iat' claim exists
        return (
            self.validate_expiration()
            and self.validate_iat()
            and self.validate_issuer()
        )
