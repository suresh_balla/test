"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json

from idemgrpc import messages_pb2 as messages_pb2

from idemd.idemd.terraform.entity_id.entity_id_generator import get_entity_id


def send_task_update_for_failure(hub, task, error):
    try:
        task_update_request = prepare_task_update_request(task, error, "FAILED")
        response = hub.idemd.idem_stub.taskUpdate(task_update_request)
        if response.isUpdated:
            hub.log.info(
                f"Task with id {task.taskIdentity.taskId} is updated with status {task_update_request.status} "
                f"because of the error -- {str(error)}"
            )
        else:
            hub.log.error(
                f"Error updating status for task with id {task.taskIdentity.taskId}. "
                f"Error - {response.message}"
            )
    except Exception as err:
        hub.log.error(
            f"Error updating status for task with id {task.taskIdentity.taskId}. Error - {err}"
        )


def send_task_update_with_resources(hub, task, resources, meta, return_code, error):
    if return_code != 0 and error:
        task_update_request = prepare_task_update_request(task, error, "FAILED")
    else:
        task_update_request = prepare_task_update_request(task, None, "COMPLETED")
    task_update_request.taskIdentity.iacProvider = hub.idemd.worker.iacProvider
    task_update_request.executionMeta.extend(meta)
    for resource in resources:
        resource_obj = messages_pb2.ResourceState()
        resource_obj.stateId = resource["name"]
        resource_obj.name = resource["name"]
        resource_obj.eventType = messages_pb2.ResourceState.EventType.CREATED
        resource_obj.state = json.dumps(resource)
        if hub.idemd.worker.iacProvider == messages_pb2.IAC_PROVIDER.TERRAFORM:
            entity_id = get_entity_id(hub, resource, task)
            if entity_id:
                resource_obj.entityIds.append(entity_id)
        task_update_request.resourceState.append(resource_obj)

    return send_task_update(hub, task, task_update_request)


def send_task_update(hub, task, task_update_request):
    ret = 0
    exc = None
    try:
        response = hub.idemd.idem_stub.taskUpdate(task_update_request)
        if response.isUpdated:
            hub.log.info(
                f"Task with id {task.taskIdentity.taskId} is updated with status {task_update_request.status}"
            )
        else:
            exc = f"Task with id {task.taskIdentity.taskId} not updated. Cause - {response.message}"
            hub.log.error(exc)
            ret = 1
    except Exception as err:
        exc = f"Error updating status for task with id {task.taskIdentity.taskId}. Error - {err}"
        hub.log.error(exc)
        ret = 1
    return ret, exc


def prepare_task_update_request(task, error, status):
    task_update_request = messages_pb2.TaskUpdateRequest()
    task_update_request.status = status
    task_update_request.taskIdentity.taskId = task.taskIdentity.taskId
    task_update_request.taskIdentity.identity.orgId = task.taskIdentity.identity.orgId
    task_update_request.taskIdentity.identity.projectId = (
        task.taskIdentity.identity.projectId
    )
    task_update_request.errorMessage = str(error)
    return task_update_request
