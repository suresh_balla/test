"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import asyncio
import subprocess
import traceback

import yaml

from idemd.idemd.acct_utils import has_acct_details
from idemd.idemd.constants.constants import *
from idemd.idemd.idem_service_utils import send_task_update_for_failure


def execute_idem_task_helper(hub, task):
    hub.log.info(f"Task {task.taskIdentity.taskId} is being executed now ")
    worker = hub.idemd.worker
    task_execution_context = hub.idemd.run_space.create(task)
    if task_execution_context.get("input_type") == "GIT":
        path = task_execution_context["execution_context"]["slsEntryPoint"]
    else:
        path = task_execution_context["execution_context"]["path"]
    hub.log.info(
        f"Run space {path} created successfully for Task {task.taskIdentity.taskId}"
    )
    task_id = task.taskIdentity.taskId
    org_id = task.taskIdentity.identity.orgId
    project_id = task.taskIdentity.identity.projectId
    project_type = task.taskIdentity.identity.projectType
    task_mode = task.taskMode
    requester = task.requester
    run_id = hub.idemd.run_space.generate_run_id(requester, org_id, project_id, task_id)
    space_dir = hub.idemd.run_space.get_space_dir(run_id)
    acct_file = Path(space_dir).joinpath("acct_file.fernet").as_posix()
    cache_dir = Path(space_dir).joinpath("cache").as_posix()
    cred_params = task.credentialParameters
    acct_params = json.loads(cred_params)

    sls_params = task.slsParameters
    sls_params_yaml_file_path = None
    if sls_params:
        sls_params_json = json.loads(sls_params)
        sls_params_yaml_file_path = (
            Path(space_dir).joinpath("sls_params.sls").as_posix()
        )
        with open(sls_params_yaml_file_path, "w") as f:
            yaml.dump(sls_params_json, f, sort_keys=False, default_flow_style=False)

    sls_file = path.as_posix()

    cred_store_args = []

    # System file will have Kafka details for SaaS worker and account profiles for remote worker
    decrypted_system_file_content = {}
    # vRA Backend file details
    decrypted_backend_file_content = {}
    # Account file passed in the Task Request
    decrypted_acct_file_content = {}

    # This is always present in the case of a SaaS worker
    merged_acct_key = hub.OPT.idemd.system_acct_key

    if hub.OPT.idemd.system_acct_file:
        hub.log.info(
            f"Processing system account details for task {task.taskIdentity.taskId}"
        )
        system_acct_file = open(hub.OPT.idemd.system_acct_file, "r")
        if merged_acct_key:
            system_file = system_acct_file.read()
            decrypted_system_file_content = asyncio.run(
                hub.crypto.init.decrypt(
                    data=system_file.encode(), key=merged_acct_key, plugin="fernet"
                )
            )
        else:
            with system_acct_file as stream:
                try:
                    decrypted_system_file_content = yaml.safe_load(stream)
                except yaml.YAMLError as exc:
                    hub.log.error(str(exc))

        # Setting the key to "task_id" will ensure that all the events produced for a task, will land into
        # the same partition and as such, the ordering of events is maintained
        # https://confluence.eng.vmware.com/display/CAEng/Ordering+messages+with+multiple+partitions
        kafka_profiles = decrypted_system_file_content.get("kafka")
        if kafka_profiles is not None:
            for key, value in kafka_profiles.items():
                value["key"] = task_id
                hub.log.debug(
                    f"Updated key as task id {task_id} for kafka profile {key}"
                )
        system_acct_file.close()

    if has_acct_details(acct_params):
        hub.log.info(
            f"Processing backend account profile details for task {task.taskIdentity.taskId}"
        )
        if os.getenv("BACKEND_ACCT_KEY") is not None:
            backend_acct_key = os.getenv("BACKEND_ACCT_KEY")
            backend_acct_file = os.getenv("BACKEND_ACCT_FILE")
            system_acct_file = open(backend_acct_file, "r")
            encrypted_backend_file = system_acct_file.read()

            decrypted_backend_file_content = asyncio.run(
                hub.crypto.init.decrypt(
                    data=encrypted_backend_file.encode(),
                    key=backend_acct_key,
                    plugin="fernet",
                )
            )
            merged_acct_key = backend_acct_key
            # org_id is needed by cloud-creds-plugin for creating org specific csp token based
            # on oauth creds defined in backend.yaml
            acct_params["org_id"] = org_id
            # project_id is needed by cloud-creds-plugin to invoke fetch account profile which is project scoped.
            acct_params["project_id"] = project_id
            acct_params["project_type"] = project_type
            acct_params["task_mode"] = task_mode
            vra_plugin_args = ["--extras", json.dumps(acct_params)]
            cred_store_args.extend(vra_plugin_args)

    if "--acct-profile" in acct_params and acct_params["--acct-profile"] is not None:
        # system acct_file contains the profiles. Hence, no merging is required.
        hub.log.info(f"Processing ACCT_PROFILE for task {task.taskIdentity.taskId}")
        cred_plugin_args = ["--acct-profile", acct_params["--acct-profile"]]
        cred_store_args.extend(cred_plugin_args)

    if "--acct-file" in acct_params and acct_params["--acct-file"] is not None:
        hub.log.info(
            f"Processing ACCT_FILE AND ACCT_KEY for Task {task.taskIdentity.taskId}"
        )
        decrypted_acct_file_content = asyncio.run(
            hub.crypto.init.decrypt(
                data=acct_params["--acct-file"].encode(),
                key=acct_params["--acct-key"],
                plugin="fernet",
            )
        )
        merged_acct_key = acct_params["--acct-key"]

    system_dict = {**decrypted_system_file_content, **decrypted_backend_file_content}
    final_merged_acct_file = dict(decrypted_acct_file_content, **system_dict)

    hub.log.info(f"Generating final account file for task {task.taskIdentity.taskId}")
    hub.log.debug(f"Final file content {final_merged_acct_file}")
    hub.log.debug(
        f"key used for encryption for task {task.taskIdentity.taskId} is {merged_acct_key}"
    )

    encrypted_final_merged_acct_file = asyncio.run(
        hub.crypto.init.encrypt(
            data=final_merged_acct_file, key=merged_acct_key, plugin="fernet"
        )
    )

    hub.log.info(
        f"Writing the final account file for task  {task.taskIdentity.taskId} to the sandbox directory"
    )
    hub.idemd.run_space.write_to_file(acct_file, encrypted_final_merged_acct_file)

    return hub.idemd.idem_execution_utils.execute_idem_task_as_process(
        task,
        sls_file,
        acct_file,
        merged_acct_key,
        cache_dir,
        run_id,
        cred_store_args,
        space_dir,
        sls_params_yaml_file_path,
        task_execution_context,
    )

    # TODO: Revisit the flow that uses a method call
    # else:
    #     return asyncio.run(
    #         hub.idemd.idem_execution_utils.execute_task(
    #             task, run_id, acct_file, acct_params["--acct-key"], space_dir, cache_dir
    #         )
    #     )


def run_idem_sub_process(hub, args_idem, space_dir, task, custom_env, proc_timeout):
    message, proc = None, None
    try:
        proc = subprocess.Popen(
            args=args_idem,
            env=custom_env,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        try:
            stdout, stderr = proc.communicate(timeout=proc_timeout)
            hub.log.info(
                f" Process ID {proc.pid} for task ID {task.taskIdentity.taskId}"
            )
            if stdout:
                message = stdout.decode()
                hub.log.info(f"[stdout]\n{message}")
            if stderr:
                message = stderr.decode()
                hub.log.error(f"[stderr]\n{message}")
            return_code = proc.returncode
        except subprocess.TimeoutExpired:
            hub.log.error(
                f"Timeout occurred for task"
                f" {task.taskIdentity.taskId}. Killing the process with PID "
                f"{proc.pid} and will proceed with idem service update"
            )
            try:
                # On timeout error, explicitly kill the process
                proc.kill()
            except OSError:
                # Ignore 'no such process' error
                pass
            # exit code for timeout
            return_code = 124
            message = f"Timeout occurred for task {task.taskIdentity.taskId}"
        except Exception as exc:
            # Any exception other than the TimeoutError will be caught here. This can happen when the process
            # itself fails with some Exception before timeout.
            # await proc.communicate()
            hub.log.error(
                f"Exception Occurred. Task {task.taskIdentity.taskId} failed with: {exc}"
            )
            message = repr(exc)
            return_code = 1
    except Exception as err:
        hub.log.error(
            f"Error occurred when spawning a new process for task {task.taskIdentity.taskId} - {str(err)}"
        )
        return_code = 1
        message = repr(err)
    finally:
        hub.idemd.run_space.remove_space_dir(Path(space_dir))
        hub.log.info(
            f"Run space {space_dir} removed successfully for Task {task.taskIdentity.taskId}"
        )
        if message:
            message = message[-1000:]
    return return_code, message


def process_run_config_passed(hub, task, run_config):
    try:
        processed_run_config = dict(run_config)
        return processed_run_config
    except Exception as err:
        hub.log.error(
            "Error processing run_config passed by user, terminating idem task execution"
        )
        send_task_update_for_failure(hub, task, err)


def execute_idem_task_as_process(
    hub,
    task,
    sls_source,
    acct_file,
    acct_key,
    cache_dir,
    run_id,
    cred_store_args,
    space_dir,
    sls_params,
    task_execution_context,
):
    hub.log.info(f"Executing task {task.taskIdentity.taskId} as a process")
    file_path = sls_source

    idem = os.getenv("IDEM_PATH", "idem")

    args_idem = [idem, "state"]

    custom_env = (
        os.environ.copy()
    )  # copy the original environment to add custom env variables
    hub.log.debug(
        f"Original Environment Variables for taskID {task.taskIdentity.taskId} are {custom_env}"
    )
    environments = task.environments
    run_config = task.runConfig

    if task_execution_context["input_type"] == "GIT":
        sls_entrypoint = str(
            task_execution_context["execution_context"]["slsEntryPoint"]
        )
        args_idem.extend([sls_entrypoint])

        sls_params_entrypoint_path = task_execution_context["execution_context"][
            "paramsEntryPoint"
        ]

        if sls_params or sls_params_entrypoint_path:
            args_idem.extend(["--params"])
            if sls_params:
                args_idem.extend([sls_params])
            if sls_params_entrypoint_path:
                sls_params_entrypoint = str(sls_params_entrypoint_path)
                args_idem.extend([sls_params_entrypoint])

        sls_sources = task_execution_context["execution_context"]["slsSources"]
        if sls_sources:
            args_idem.extend(["--sls-sources", sls_sources])

        params_sources = task_execution_context["execution_context"]["paramSources"]
        if params_sources:
            args_idem.extend(["--param-sources", params_sources])
    else:
        if task_execution_context["execution_context"]["entrypoint"] is not None:
            file_path = "file://" + sls_source
            entrypoint = task_execution_context["execution_context"]["entrypoint"]
            args_idem.extend([entrypoint, "--sls-sources"])
            args_idem.extend([file_path])
        else:
            args_idem.extend([file_path])
        if sls_params is not None:
            args_idem.extend(["--params", sls_params])

    # Sometimes the fernet key is generated with two hyphens('--') and the shell is treating it as an
    # argument when it is really not. So instead of "--acct-key key" it will be now  "--acct-key=key"
    acct_key_arg = EQUALS_OPERATOR.join(["--acct-key", acct_key])

    # Keep 'basic' as the default value
    reconciler = "basic"
    # To override the reconciler value for troubleshooting
    disable_reconciler = os.getenv("DISABLE_RECONCILER")
    if disable_reconciler is not None and disable_reconciler.lower() == "true":
        reconciler = "none"

    idem_logging_format = (
        "%(asctime)s.%(msecs)06d [%(filename)s:%(lineno)s - %(funcName)2s()] "
        "[%(levelname)s] [%(process)d %(processName)s %(threadName)s] [task_id {}] [idem] %(message)s".format(
            task.taskIdentity.taskId
        )
    )

    idem_log_level = os.getenv("IDEM_LOG_LEVEL", "info")

    args_options = [
        "--reconciler",
        reconciler,
        "--run-name",
        run_id,
        "--cache-dir",
        cache_dir,
        acct_key_arg,
        "--acct-file",
        acct_file,
        "--log-level",
        idem_log_level,
        "--log-fmt-console",
        idem_logging_format,
        "--log-plugin",
        "null",
        "--error-callback-ref=tool.error_handler.publish_error_callback_handler",
    ]

    use_serial_runtime = os.getenv("USE_SERIAL_RUNTIME")
    if use_serial_runtime is not None and use_serial_runtime.lower() == "true":
        # by default, the runtime is 'parallel'
        args_options.extend(["--runtime", "serial"])

    no_progress_bar = os.getenv("NO_PROGRESS_BAR")
    if no_progress_bar is not None and no_progress_bar.lower() == "true":
        # by default, it shows progress bar. For the purpose of this service progress bar is not important
        args_options.extend(["--no-progress-bar"])

    if task.taskIdentity.enforcedStateId:
        args_options.extend(["--esm-plugin", "worker_esm"])
        args_options.extend(["--upgrade-esm"])

    parallel_execution_batch_size = os.getenv("IDEM_PARALLEL_EXECUTION_BATCH_SIZE")
    if parallel_execution_batch_size is not None:
        # Idem's default execution runtime is parallel
        args_options.extend(["--batch-size", parallel_execution_batch_size])

    if task.taskMode and task.taskMode == "TEST":
        hub.log.info(f"Running task {task.taskIdentity.taskId} in TEST mode")
        args_options.extend(["--test"])

    if environments:
        for environmentsEntry in environments:
            custom_env[environmentsEntry.key] = environmentsEntry.value
            hub.log.debug(
                f"Custom Environment Variable for taskID {task.taskIdentity.taskId} are key -> {environmentsEntry.key},"
                f" value -> {environmentsEntry.value}"
            )

    if run_config:
        processed_run_config = process_run_config_passed(hub, task, run_config)
        if processed_run_config:
            config_file_path = hub.idemd.run_space.generate_config_file(
                processed_run_config, space_dir
            )
            hub.log.debug(
                f"idem cli command has been enhanced with --config {config_file_path}"
            )
            args_options.extend(["--config", config_file_path])

    args_idem = [*args_idem, *args_options]
    args_idem.extend(cred_store_args)

    proc_timeout = (
        task.expiresInSeconds
        if task.expiresInSeconds is not None and task.expiresInSeconds > 0
        else hub.idemd.global_task_timeout
    )

    hub.log.info(
        f"Executing the idem command {args_idem} for task {task.taskIdentity.taskId}"
    )

    return run_idem_sub_process(
        hub, args_idem, space_dir, task, custom_env, proc_timeout
    )


async def execute_task(
    hub,
    run_id: str,
    acct_file: str,
    acct_key: str,
    space_dir: str,
    cache_dir: str,
):
    sls = ["sls_source"]
    src = hub.idem.sls_source.init.get_refs(sources=[f"file://{space_dir}"], refs=sls)
    apply_kwargs = dict(
        name=run_id,
        sls_sources=src["sls_sources"],
        sls=src["sls"],
        render="jinja|yaml|replacements",
        runtime="serial",
        subs=["states"],
        cache_dir=cache_dir,
        test=False,
        acct_key=acct_key,
        acct_file=acct_file,
    )
    # TO DO Need to add esm_plugin="worker_esm", if applicable in future
    context_manager = hub.idem.managed.context(
        run_name=run_id,
        cache_dir=cache_dir,
        acct_key=acct_key,
        acct_file=acct_file,
        serial_plugin="msgpack",
    )
    try:
        async with context_manager as state:
            apply_kwargs["managed_state"] = state
            await hub.idem.state.apply(**apply_kwargs)

            if hub.idem.RUNS[run_id]["errors"]:
                # pop is required to remove the run from memory
                run_data = hub.idem.RUNS.pop(run_id)
                return run_data["errors"]

            # Reconciliation loop
            asyncio.run(
                hub.reconcile.init.run(
                    name=run_id,
                    plugin="basic",
                    pending_plugin="default",
                    apply_kwargs=apply_kwargs,
                )
            )
    except Exception as e:
        display = hub.output.nested.display(str(e))
        hub.log.debug(display)
        hub.log.debug(traceback.format_exc())
        # Return a non-zero error code
        return 1, repr(e)

    run_data = hub.idem.RUNS.pop(run_id)
    return run_data["running"], None
