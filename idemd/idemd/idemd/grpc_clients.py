"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import grpc
from idemgrpc import idemService_pb2_grpc as idem_grpc
from idemgrpc import taskManagerService_pb2_grpc as task_manager_grpc

from idemd.idemd.auth.CSPClientCredentialsInterceptor import (
    CSPClientCredentialsInterceptor,
)
from idemd.idemd.auth.SMClientCredentialsInterceptor import (
    SMClientCredentialsInterceptor,
)
from idemd.idemd.constants.constants import *
from idemd.idemd.run_space import read_client_credentials

client_creds = {}


def init_idem_service_client(hub):
    idem_service_url = (
        f"{hub.OPT.idemd.idem_service_host}:{hub.OPT.idemd.idem_service_port}"
    )
    hub.log.info(f"worker connecting to {idem_service_url}")
    # bind the channel and stub to co-located grpc server
    channel = _get_idem_service_channel_with_props(idem_service_url)

    if os.environ.get("ENABLE_GRPC_AUTH", "false") == "true":
        hub.log.info(f"ENABLE_GRPC_AUTH is true")
        creds = hub.idemd.run_space.read_client_credentials()
        hub.idemd.client_creds = creds["auth"]
        if creds.get("csp"):
            interceptors = [CSPClientCredentialsInterceptor(hub.idemd.client_creds)]
        else:
            interceptors = [SMClientCredentialsInterceptor(hub.idemd.client_creds)]
        channel = grpc.intercept_channel(channel, *interceptors)
    else:
        hub.log.info(f"ENABLE_GRPC_AUTH is false or None")

    stub = idem_grpc.IdemServiceStub(channel)
    hub.idemd.idem_channel = channel
    hub.idemd.idem_stub = stub


def init_task_manager_client(hub):
    task_manager_url = (
        f"{hub.OPT.idemd.task_manager_host}:{hub.OPT.idemd.task_manager_port}"
    )
    hub.log.info(f"idem worker connecting to {task_manager_url}")
    # bind the channel and stub to co-located grpc server
    channel = _get_task_manager_channel_with_props(task_manager_url)
    creds = {}
    if os.environ.get("ENABLE_GRPC_AUTH", "false") == "true":
        hub.log.info(f"ENABLE_GRPC_AUTH is true")
        creds = hub.idemd.run_space.read_client_credentials()
        if hub.idemd.client_creds is None:
            hub.idemd.client_creds = creds["auth"]
        if creds.get("sm"):
            interceptors = [SMClientCredentialsInterceptor(hub.idemd.client_creds)]
        else:
            interceptors = [CSPClientCredentialsInterceptor(hub.idemd.client_creds)]

        channel = grpc.intercept_channel(channel, *interceptors)
    else:
        hub.log.info(f"ENABLE_GRPC_AUTH is false or None")

    stub = task_manager_grpc.TaskManagerServiceStub(channel)
    hub.idemd.task_manager_channel = channel
    hub.idemd.task_manager_stub = stub


def idem_service_client_from_url(url):
    # bind the channel and stub to co-located grpc server
    channel = _get_idem_service_channel_with_props(url)

    if os.environ.get("ENABLE_GRPC_AUTH", "false") == "true":
        client_credentials = read_client_credentials(None)
        interceptors = [CSPClientCredentialsInterceptor(client_credentials)]
        channel = grpc.intercept_channel(channel, *interceptors)

    return idem_grpc.IdemServiceStub(channel)


def _get_idem_service_channel_with_props(url):
    return grpc.insecure_channel(
        url,
        options=[
            ("grpc.service_config", GRPC_IDEM_SERVICE_CONFIG),
            ("grpc.enable_retries", GRPC_ENABLE_RETRIES),
            ("grpc.keepalive_time_ms", GRPC_KEEPALIVE_TIME_MS),
            ("grpc.keepalive_timeout_ms", GRPC_KEEPALIVE_TIMEOUT_MS),
            (
                "grpc.keepalive_permit_without_calls",
                GRPC_KEEPALIVE_PERMIT_WITHOUT_CALLS,
            ),
            # Added the max message size for gRPC to accommodate larger payloads
            # by picking its value from environment variable. Can be reviewed and adjusted as necessary in the future.
            (
                "grpc.max_receive_message_length",
                GRPC_MAX_MSG_SIZE,
            ),  # 5MB
            (
                "grpc.max_send_message_length",
                GRPC_MAX_MSG_SIZE,
            ),  # 5MB
        ],
    )


def _get_task_manager_channel_with_props(url):
    return grpc.insecure_channel(
        url,
        options=[
            ("grpc.service_config", GRPC_TASK_MANAGER_SERVICE_CONFIG),
            ("grpc.enable_retries", GRPC_ENABLE_RETRIES),
            ("grpc.keepalive_time_ms", GRPC_KEEPALIVE_TIME_MS),
            ("grpc.keepalive_timeout_ms", GRPC_KEEPALIVE_TIMEOUT_MS),
            (
                "grpc.keepalive_permit_without_calls",
                GRPC_KEEPALIVE_PERMIT_WITHOUT_CALLS,
            ),
        ],
    )


def init_clients(hub):
    hub.log.info("Start the gRPC clients for both idem service and task manager")
    init_idem_service_client(hub)
    init_task_manager_client(hub)
