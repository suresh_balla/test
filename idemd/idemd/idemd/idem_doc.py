"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""
import json
import os
import subprocess

from idemgrpc import messages_pb2 as messages

from idemd.idemd.constants.constants import (
    IDEM_DOC_DEFAULT_SUPPORTED_FUNCTIONS,
    IDEM_DOC_SUPPORTED_FUNCTIONS,
    DOC_DELIMITER,
)

# 'states.aws, states.azure, states.gcp, exec.aws, exec.azure, exec.gcp'
SUPPORTED_FUNCTIONS = os.getenv(
    IDEM_DOC_SUPPORTED_FUNCTIONS, IDEM_DOC_DEFAULT_SUPPORTED_FUNCTIONS
)

# To be on the safer side, setting the chunk size to a smaller value
DOC_DATA_CHUNK_SIZE = 32


def generate_idem_doc_stream(hub):
    global SUPPORTED_FUNCTIONS
    SUPPORTED_FUNCTIONS = SUPPORTED_FUNCTIONS.split(DOC_DELIMITER)
    for fun in SUPPORTED_FUNCTIONS:
        try:
            hub.log.info(f"Generating doc for {fun}")
            p = subprocess.run(
                ["idem", "doc", fun, "--output", "json"],
                stdout=subprocess.PIPE,
                text=True,
            )

            doc_data = []
            if p.stderr is None:
                json_doc = json.loads(p.stdout)
                for item in json_doc.items():
                    response = messages.IdemDoc()
                    response.schema = json.dumps(item, indent=None)
                    doc_data.append(response)

                # Generator expression to create a stream of messages.IdemDocUpdateRequest objects,
                # where each object represents a chunk of data to be sent in a gRPC stream.
                request_stream = (
                    messages.IdemDocUpdateRequest(
                        function=fun, docs=[doc for doc in chunk]
                    )
                    for chunk in chunk_data(doc_data, DOC_DATA_CHUNK_SIZE)
                )

                try:
                    # Use the gRPC stream to send requests
                    idem_service_response = hub.idemd.idem_stub.updateIdemDocsStream(
                        request_stream
                    )
                    if not idem_service_response.isUpdated:
                        hub.log.info(
                            f"Failed to update docs for {len(doc_data)} functions in idem service for "
                            f"{fun}. Reason: {idem_service_response.message}"
                        )
                    else:
                        hub.log.info(
                            f"Successfully updated {len(doc_data)} functions "
                            f"for {fun} in idem service"
                        )
                except Exception as e:
                    hub.log.error(f"Error during gRPC call: {e}")
            else:
                hub.log.error(p.stderr)
        except Exception as err:
            hub.log.error(f"Error Generating doc for {fun} - {err}")


def chunk_data(data, chunk_size):
    """
    Yield successive chunks from a given iterable.

    Parameters:
    - data: An iterable containing the data to be chunked.
    - chunk_size: An integer representing the desired size of each chunk.

    Yields:
    - A list containing successive chunks of data, each with a size of `chunk_size`.

    Example:
    ```
    data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    chunked_data = list(chunk_data(data, 3))
    # Result: [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10]]
    ```
    """
    current_chunk = []
    for item in data:
        current_chunk.append(item)
        if len(current_chunk) == chunk_size:
            yield current_chunk
            current_chunk = []
    if current_chunk:
        yield current_chunk
