"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

from idemd.idemd.constants.constants import (
    ACCOUNT_PROFILES,
    SUPPORTED_ACCT_KEYS,
    SUPPORTED_VRA_ACCT_KEYS,
)


def has_acct_details(acct_params):
    if ACCOUNT_PROFILES in acct_params and len(acct_params[ACCOUNT_PROFILES]) != 0:
        return True
    for key in SUPPORTED_ACCT_KEYS:
        if key in acct_params and acct_params[key] is not None:
            return True
    return False


def has_vra_acct_details(acct_params):
    for key in SUPPORTED_VRA_ACCT_KEYS:
        if key in acct_params and acct_params[key] is not None:
            return True
    return False


def has_acct_profile_details(acct_params):
    if ACCOUNT_PROFILES in acct_params and len(acct_params[ACCOUNT_PROFILES]) != 0:
        return True
