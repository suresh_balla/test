"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

from cgroupspy import trees


def load_memory_node(hub):
    cgroup_tree = trees.Tree()
    memory_node = cgroup_tree.get_node_by_path("/memory/")
    hub.memory_node = memory_node
    hub.log.info(f" memory usage found  {hub.memory_node.controller.usage_in_bytes}")
    hub.log.info(f" memory limit found  {hub.memory_node.controller.limit_in_bytes}")
