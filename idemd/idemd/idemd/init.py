"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json
import os
import uuid

from idemgrpc import messages_pb2

from idemd.idemd.constants.constants import WORKER_INFO_FILE_PATH
from idemd.idemd.validate.validate_v1 import serve
from idemd.tool.run_space_dir_processor import process_run_space_dir


def __init__(hub):
    # Remember not to start your app in the __init__ function
    # This function should just be used to set up the plugin subsystem
    # The run.py is where your app should usually start
    for dyne in ["evbus", "idem", "loop"]:
        hub.pop.sub.add(dyne_name=dyne)


def cli(hub):
    # Your app's options can now be found under hub.OPT.idemd
    kwargs = dict(hub.OPT.idemd)

    # Initialize the asyncio event loop
    hub.pop.loop.create()

    # Initialise worker identity
    worker = messages_pb2.WorkerIdentity()
    worker.identity.orgId = hub.OPT.idemd.org_id
    worker.identity.projectId = hub.OPT.idemd.project_id
    worker.workerGroupId = hub.OPT.idemd.worker_group_id
    operating_mode = kwargs.get("mode")

    hub.idemd.thread_pool = hub.loop.thread_pool.executor()

    if operating_mode == "DIRECT":
        worker.workerType = messages_pb2.WorkerIdentity.WorkerType.DIRECT
    else:
        worker.workerType = messages_pb2.WorkerIdentity.WorkerType.REMOTE

    try:
        worker_id = retrieve_existing_worker_identity(hub)
    except Exception as err:
        hub.log.error(f"Error occurred while retrieving worker id - {err}")
        worker_id = None
    if worker_id is None:
        worker.workerId = str(uuid.uuid4())
        hub.log.info(
            f"worker identity not found. Assigning a new identity {worker.workerId}"
        )
        try:
            save_worker_identity(hub, worker.workerId)
        except Exception as ex:
            hub.log.error(f"Error occurred while saving worker id - {ex}")
    else:
        worker.workerId = worker_id
        hub.log.info(f"worker identity already exists {worker.workerId}")
        # Since the worker id exists, most probably the worker restarted. In that case, we should clean up
        # existing run spaces.
        try:
            process_run_space_dir()
        except Exception as run_exc:
            hub.log.error(
                f"Couldn't process run space during worker restart. Error {run_exc}"
            )

    disable_memory_check = os.getenv("DISABLE_MEMORY_CHECK")
    if disable_memory_check is None or disable_memory_check.lower() == "false":
        try:
            hub.idemd.cgroups_utils.load_memory_node()
        except Exception as err:
            hub.log.error(f" Could not load cgroup memory - {err}")

    hub.log.info(f"Worker {worker.workerId} operating in {operating_mode} mode")
    worker.iacProvider = get_iac_provider()
    hub.idemd.worker = worker

    hub.pop.Loop.run_until_complete(
        hub.idemd.proc.start(
            system_acct_file=hub.OPT.idemd.system_acct_file,
            system_acct_key=hub.OPT.idemd.system_acct_key,
        )
    )


# Method to generate Idem docs for function references on the hub
# Will be invoked only when IDEM_DOC_MODE environment variable is set.
def doc(hub):
    # Initialize idem service client
    hub.idemd.grpc_clients.init_idem_service_client()
    hub.idemd.idem_doc.generate_idem_doc_stream()
    return


def retrieve_existing_worker_identity(hub):
    if WORKER_INFO_FILE_PATH is not None:
        hub.log.info("Retrieving worker identity")
        if WORKER_INFO_FILE_PATH.is_file():
            with open(WORKER_INFO_FILE_PATH, "r") as f:
                data = json.load(f)
            return data.get("id")
        else:
            return None
    else:
        return None


def save_worker_identity(hub, worker_id):
    hub.log.info("Saving new worker identity ")
    if WORKER_INFO_FILE_PATH is not None:
        data = {}
        if WORKER_INFO_FILE_PATH.is_file():
            with open(WORKER_INFO_FILE_PATH, "r") as f:
                data = json.load(f)
        worker_id_dict = {"id": worker_id}
        with open(WORKER_INFO_FILE_PATH, "w+") as f:
            var = {**worker_id_dict, **data}
            f.write(json.dumps(var))
    else:
        hub.log.info(
            f"Directory {WORKER_INFO_FILE_PATH} does not exist to save/retrieve worker identity"
        )


def get_iac_provider():
    default_iac_provider = messages_pb2.IAC_PROVIDER.Name(
        messages_pb2.IAC_PROVIDER.IDEM
    )
    iac_provider = os.getenv("IAC_PROVIDER")

    if iac_provider is not None:
        try:
            messages_pb2.IAC_PROVIDER.Value(iac_provider)
            return iac_provider
        except ValueError as v_err:
            raise RuntimeError(f"Invalid provider type {iac_provider}. Error {v_err}")
    else:
        return default_iac_provider


# Method to start worker in validate mode
def validate():
    worker_host = os.getenv("WORKER_HOST")
    worker_port = os.getenv("WORKER_PORT")
    serve(worker_host, worker_port)
