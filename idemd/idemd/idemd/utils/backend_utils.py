"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json
import os

import yaml

from idemd.idemd.acct_utils import has_acct_details


def resolve_backends(task):
    """
    Resolves the backends for a given task.
    """

    cred_params = task.credentialParameters
    acct_params = json.loads(cred_params)
    if has_acct_details(acct_params) and os.getenv("BACKEND_CONFIG_FILE") is not None:
        with open(os.getenv("BACKEND_CONFIG_FILE"), "r") as system_acct_file:
            file = system_acct_file.read()

            # Load the YAML string as a dict
            backends = yaml.safe_load(file)

        if backends is not None and backends != {}:
            return backends["acct-backends"]
    else:
        return {}

    return {}
