"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import concurrent.futures
import threading
from threading import current_thread

from idemd.idemd.utils.iac_log_utils import get_default_logger

logger = get_default_logger()


class WorkerThreadPoolExecutor(concurrent.futures.ThreadPoolExecutor):
    """
    WorkerThreadsExecutor extends the ThreadPoolExecutor class from the concurrent.futures module.
    It provides additional functionality for initializing worker threads and logging thread completion.
    """

    def __init__(
        self, max_workers=None, thread_name_prefix="", initializer=None, initargs=()
    ):
        super().__init__(max_workers, thread_name_prefix)
        self._initializer = initializer
        self._initargs = initargs

    @staticmethod
    def done_callback(future):
        """
        Callback function to be executed when a task is completed.

        """
        name = current_thread().name
        try:
            result = future.result() if future.result() is not None else ""
            logger.info(f"Thread {name} has finished execution for task {result}")
        except Exception as e:
            logger.warning(
                f"An exception occurred in the call back of thread {name}: {str(e)}. But it still finished "
                f"execution"
            )

    @staticmethod
    def initializer():
        """
        Function for initializing the worker thread.
        """
        name = current_thread().name
        logger.info(f"Initializing thread {name}")

    def submit(self, fn, *args, **kwargs):
        """
        Submits a callable to be executed by the thread pool.

        If an initializer function is provided and the current thread is not the main thread,
        the initializer will be executed before submitting the task.

        """
        if (
            self._initializer is not None
            and current_thread() is not threading.main_thread()
        ):
            # Execute initializer before submitting the task
            self._initializer(*self._initargs)
        future = super().submit(fn, *args, **kwargs)
        future.add_done_callback(self.done_callback)
        return future
