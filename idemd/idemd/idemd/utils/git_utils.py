"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""
import os

from git import Repo

from idemd.idemd.utils.iac_log_utils import get_default_logger

logger = get_default_logger()


class GitCloneContext:
    def __init__(self, repo_url, clone_path, credentials, branch_name, commit_id):
        self._repo_url = repo_url
        self._clone_path = clone_path
        self._credentials = credentials
        self._branch_name = branch_name
        self._commit_id = commit_id

    @property
    def repo_url(self):
        return self._repo_url

    @repo_url.setter
    def repo_url(self, value):
        self._repo_url = value

    @property
    def clone_path(self):
        return self._clone_path

    @clone_path.setter
    def clone_path(self, value):
        self._clone_path = value

    @property
    def credentials(self):
        return self._credentials

    @credentials.setter
    def credentials(self, value):
        self._credentials = value

    @property
    def branch_name(self):
        return self._branch_name

    @branch_name.setter
    def branch_name(self, value):
        self._branch_name = value

    @property
    def commit_id(self):
        return self._commit_id

    @commit_id.setter
    def commit_id(self, value):
        self._commit_id = value


def clone_repo(context):
    repo = None
    try:
        logger.info(f"Cloning {context.repo_url} to {context.clone_path} STARTED")
        if context.credentials.username and context.credentials.password:
            repo = Repo.clone_from(
                context.repo_url.replace(
                    "https://",
                    f"https://{context.credentials.username}:{context.credentials.password}@",
                ),
                context.clone_path,
            )
            logger.info(
                f"Cloning {context.repo_url} to {context.clone_path} using username/password COMPLETED"
            )
        elif context.credentials.ssh_key_path:
            repo = Repo.clone_from(
                context.repo_url,
                context.clone_path,
                env={"GIT_SSH_COMMAND": f"ssh -i {context.credentials.ssh_key_path}"},
            )
            logger.info(
                f"Cloning {context.repo_url} to {context.clone_path} using ssh COMPLETED"
            )
        elif context.credentials.personal_token:
            repo = Repo.clone_from(
                context.repo_url.replace(
                    "https://",
                    f"https://oauth2:{context.credentials.personal_token}@",
                ),
                context.clone_path,
            )
            logger.info(
                f"Cloning {context.repo_url} to {context.clone_path} using personal_token COMPLETED"
            )
        elif context.credentials.project_token:
            # https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html
            repo = Repo.clone_from(
                context.repo_url.replace(
                    "https://",
                    f"https://worker:{context.credentials.project_token}@",
                ),
                context.clone_path,
            )
            logger.info(
                f"Cloning {context.repo_url} to {context.clone_path} using project token COMPLETED"
            )
    except Exception as e:
        logger.info(
            f"Cloning {context.repo_url} to {context.clone_path} FAILED. Error {str(e)}"
        )
        raise e

    if repo is not None:
        try:
            if os.path.exists(context.clone_path):
                if context.branch_name:
                    logger.info(
                        f"Switching to {context.branch_name} branch for the git repo {context.repo_url}"
                    )
                    origin = repo.remotes.origin
                    origin.fetch()
                    repo.git.reset("--hard", "HEAD")
                    repo.git.checkout(
                        "-B", context.branch_name, f"origin/{context.branch_name}"
                    )
                else:
                    context.branch_name = repo.active_branch.name
        except Exception as e:
            logger.error(f"Error: {e}")
            raise e

        # Check if the commit ID exists in request
        if os.path.exists(context.clone_path):
            if context.commit_id:
                try:
                    logger.info(
                        f"Checking out {context.commit_id} from the git repo {context.repo_url}"
                    )
                    repo.git.checkout(context.commit_id)
                except Exception as e:
                    msg = f"Commit ID not found in current branch {context.branch_name}"
                    logger.error(msg)
                    raise e
            else:
                context.commit_id = repo.head.object.hexsha


def clone_or_update_repo(git_context):
    clone_repo(git_context)
    return git_context


def resolve_git_context(task):
    if task is not None and task.iacGitInfo is not None and {} != task.iacGitInfo:
        try:
            source_url = task.iacGitInfo.get("repositoryURL", None)
            source_branch = task.iacGitInfo.get("branch", None)
            source_commit_id = task.iacGitInfo.get("commitId", None)

            source_meta = task.iacGitInfo.get("meta", None)

            # Create a GitContext object
            context = GitCloneContext(
                repo_url=source_url,
                branch_name=source_branch,
                commit_id=source_commit_id,
                clone_path=None,
                credentials=None,
            )

            return context, source_meta
        except Exception as e:
            err = f"An error occurred when resolving source for task {task.taskIdentity.taskId} {str(e)}"
            logger.error(err)
            raise RuntimeError(err)
    else:
        return None, None
