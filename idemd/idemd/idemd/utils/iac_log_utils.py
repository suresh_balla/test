"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import logging

from idemd.idemd.constants.constants import LOG_FORMATTER

logger_dict = {}


def get_named_logger(name):
    """
    Returns a logger object with the specified name.
    """
    if name not in logger_dict:
        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)
        logger.propagate = False

        console_handler = logging.StreamHandler()
        console_handler.setLevel(logging.DEBUG)

        formatter = logging.Formatter(LOG_FORMATTER)
        console_handler.setFormatter(formatter)

        logger.addHandler(console_handler)

        logger_dict[name] = logger

    return logger_dict[name]


def get_default_logger():
    return get_named_logger(name="default")
