"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import os
from urllib.parse import urlparse

from idemd.idemd.utils.manifest_helper import ManifestFactory
from idemd.idemd.utils.vault_helper import retrieve_secrets


class InvalidCredentialsError(Exception):
    """Exception raised for invalid credentials."""

    pass


class InvalidAuthorizationTypeError(Exception):
    """Exception raised for invalid auth_type."""

    pass


class GitAuthCredentials:
    """Class representing credentials for authentication."""

    def __init__(
        self,
        username=None,
        password=None,
        ssh_key_path=None,
        personal_token=None,
        project_token=None,
    ):
        self._username = username
        self._password = password
        self._ssh_key_path = ssh_key_path
        self._personal_token = personal_token
        self._project_token = project_token

    @property
    def username(self):
        return self._username

    @username.setter
    def username(self, value):
        self._username = value

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, value):
        self._password = value

    @property
    def ssh_key_path(self):
        return self._ssh_key_path

    @ssh_key_path.setter
    def ssh_key_path(self, value):
        self._ssh_key_path = value

    @property
    def project_token(self):
        return self._project_token

    @project_token.setter
    def project_token(self, value):
        self._project_token = value

    @property
    def personal_token(self):
        return self._personal_token

    @personal_token.setter
    def personal_token(self, value):
        self._personal_token = value


class GitCredentialsLoader:
    """Class for loading git credential data."""

    @staticmethod
    def load_git_credentials_from_manifest(auth_type=None):
        """
        Load Git credentials from the manifest.

        Args: auth_type (str): Type of authentication to load (e.g., 'personal_token', 'project_token',
        'username_password', 'ssh').

        Returns:
            GitAuthCredentials: Loaded Git credentials.

        Raises: InvalidCredentialsError: If the authentication type is unrecognized or Git credentials are not found
        in the manifest.
        """
        manifest = ManifestFactory.get_instance()
        response = manifest.get("git", {})
        credentials = GitCredentialsLoader.prepare_credentials(response, auth_type)

        return credentials

    @staticmethod
    def load_git_credentials_from_vault(git_url, auth_type):
        """
        Load Git credentials from the Vault.
        """
        parsed_url = urlparse(git_url)
        path = parsed_url.hostname

        vault_address = os.getenv("VAULT_URL")

        manifest = ManifestFactory.get_instance()

        if vault_address is None:
            vault_address = manifest.get("VAULT_URL")

        vault_token = os.getenv("VAULT_TOKEN")
        if vault_token is None:
            vault_token = manifest.get("VAULT_TOKEN")

        response = retrieve_secrets(vault_address, vault_token, path)

        credentials = GitCredentialsLoader.prepare_credentials(response, auth_type)

        return credentials

    @staticmethod
    def load_git_credentials_from_environment(auth_type=None):
        """
        Load Git credentials from the OS environment.
        """

        response = {
            "project_token": os.getenv("GIT_PROJECT_TOKEN", None),
            "personal_token": os.getenv("GIT_PERSONAL_TOKEN", None),
            "username": os.getenv("GIT_USERNAME", None),
            "password": os.getenv("GIT_PASSWORD", None),
        }

        credentials = GitCredentialsLoader.prepare_credentials(response, auth_type)

        return credentials

    @staticmethod
    def prepare_credentials(response, auth_type):
        credentials = GitAuthCredentials()

        if auth_type is None:
            raise InvalidAuthorizationTypeError("auth_type should be provided")

        if response is not None and {} != response:
            if "personal_token" == auth_type:
                credentials.personal_token = response.get("personal_token")
            elif "project_token" == auth_type:
                credentials.project_token = response.get("project_token")
            elif "username_password" == auth_type:
                credentials.username = response.get("username")
                credentials.password = response.get("password")
            elif "ssh" == auth_type:
                credentials.ssh_key_path = response.get("ssh_key_path")
            else:
                raise InvalidAuthorizationTypeError(
                    f"Unrecognized auth type {auth_type}"
                )
        else:
            raise InvalidCredentialsError(f"Git Credentials not found in manifest")

        return credentials
