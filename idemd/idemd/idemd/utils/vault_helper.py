"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import hvac
from hvac.exceptions import VaultError, InvalidPath, Forbidden

from idemd.idemd.utils.iac_log_utils import get_default_logger

logger = get_default_logger()


def retrieve_secrets(vault_address, vault_token, path):
    # Connect to Vault
    client = hvac.Client(url=vault_address, token=vault_token)

    try:
        response = client.secrets.kv.v2.read_secret_version(
            path=path, raise_on_deleted_version=True
        )

        if response.get("data") and response["data"].get("data"):
            ret = response["data"]["data"]
            logger.info(f"Retrieving Credentials at vault path {path}")
            return ret
        else:
            logger.error("No credentials found in Vault.")

    except InvalidPath:
        logger.error(f"Invalid secret path {path}")

    except Forbidden:
        logger.error("Access to Vault denied.")

    except VaultError:
        logger.error("Error accessing Vault.")

    return {}
