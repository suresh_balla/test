"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import os
import threading

import yaml

from idemd.idemd.utils.iac_log_utils import get_default_logger

logger = get_default_logger()


class ManifestFactory:
    __manifest = None
    __lock = threading.Lock()

    @staticmethod
    def get_instance():
        if ManifestFactory.__manifest is None:
            with ManifestFactory.__lock:
                if ManifestFactory.__manifest is None:
                    ManifestFactory()
        return ManifestFactory.__manifest

    def __init__(self):
        if ManifestFactory.__manifest is not None:
            raise Exception(
                "ManifestFactory class is a singleton, use get_instance() to access the instance."
            )
        else:
            ManifestFactory.__manifest = self.load_manifest()

    @staticmethod
    def load_manifest():
        """
        Load the manifest data from a YAML file.

        Returns:
            dict: The loaded manifest data.
        """
        #  TODO: Manifest can be encrypted and loaded. So we may have to decrypt it here
        manifest_file_path = os.getenv("MANIFEST_MOUNT_PATH")
        manifest_data = {}

        try:
            with open(manifest_file_path, "r") as file:
                manifest_data = yaml.safe_load(file)
        except Exception as e:
            logger.error("Error while reading manifest file: %s", str(e))

        return manifest_data
