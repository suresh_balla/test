"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json

from idemd.idemd.acct_utils import has_vra_acct_details
from idemd.idemd.constants.constants import GUARDRAILS, VAULT
from idemd.idemd.terraform.credential_providers.credential_providers import (
    CredentialProvider,
)
from idemd.idemd.terraform.credential_providers.guardrails_environments_provider import (
    GuardrailsEnvironmentsCredentialContext,
)
from idemd.idemd.terraform.credential_providers.no_op_credential_context import (
    NoOpCredentialContext,
)
from idemd.idemd.terraform.credential_providers.vault_credential_provider import (
    VaultCredentialContext,
)
from idemd.idemd.terraform.credential_providers.vra_credentials_provider import (
    VRACredentialContext,
)
from idemd.idemd.utils.backend_utils import resolve_backends
from idemd.idemd.utils.iac_log_utils import get_default_logger

logger = get_default_logger()


def resolve_credential_type(task):
    err_msg = (
        f"Invalid credential payload specified for task {task.taskIdentity.taskId}"
    )

    try:
        cred_params = json.loads(task.credentialParameters)
    except json.JSONDecodeError:
        logger.warning(
            f"Couldn't load credential parameter as a a dictionary. Task {task.taskIdentity.taskId}"
        )
        raise ValueError(err_msg)

    if "skipCheck" in cred_params and cred_params["skipCheck"].lower() == "true":
        return CredentialProvider.NO_OP.name

    if GUARDRAILS in cred_params:
        return CredentialProvider.ENVIRONMENTS.name

    if VAULT in cred_params:
        return CredentialProvider.VAULT.name

    if has_vra_acct_details(cred_params):
        return CredentialProvider.VRA.name

    raise ValueError(err_msg)


class CredentialContextFactory:
    @staticmethod
    def get_context_with_type(_type, task, backends):
        credential_mapping = {
            CredentialProvider.VRA.name: lambda: VRACredentialContext(task, backends),
            CredentialProvider.ENVIRONMENTS.name: lambda: GuardrailsEnvironmentsCredentialContext(
                task, backends
            ),
            CredentialProvider.NO_OP.name: lambda: NoOpCredentialContext(None),
            CredentialProvider.VAULT.name: lambda: VaultCredentialContext(task),
        }

        _type = _type or resolve_credential_type(task)
        context = credential_mapping.get(_type)
        if context:
            return context()
        else:
            raise ValueError("Invalid credential mode")

    @staticmethod
    def get_context(task):
        backends = resolve_backends(task)
        return CredentialContextFactory.get_context_with_type(None, task, backends)
