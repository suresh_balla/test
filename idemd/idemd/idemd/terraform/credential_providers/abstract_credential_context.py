"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

from abc import ABC, abstractmethod


class AbstractCredentialContext(ABC):
    def __init__(self, task):
        self.task = task

    def get_task(self):
        return self.task

    def set_task(self, task):
        self.task = task

    @abstractmethod
    def get_creds_as_map(self):
        pass
