"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json
import os

from idemd.idemd.constants.constants import VAULT
from idemd.idemd.terraform.credential_providers.abstract_credential_context import (
    AbstractCredentialContext,
)
from idemd.idemd.utils.iac_log_utils import get_default_logger
from idemd.idemd.utils.vault_helper import retrieve_secrets

logger = get_default_logger()


class VaultCredentialContext(AbstractCredentialContext):
    def __init__(self, task):
        super().__init__(task)
        self.vault_url = os.getenv("VAULT_URL")
        self.vault_token = os.getenv("VAULT_TOKEN")

    def get_creds_as_map(self):
        logger.info(
            f"Retrieving credentials from Vault for task {self.task.taskIdentity.taskId}"
        )

        cred_params = self.task.credentialParameters
        acct_params = json.loads(cred_params)

        if VAULT in acct_params:
            vault = acct_params[VAULT]
        else:
            return {}

        # TODO: This needs to be documented clearly on how these secrets are expected to be configured
        provider = vault.get("provider", {}).lower()
        if provider is None:
            logger.error("Expected key 'provider' is not found for the credentials")
            return {}

        profile = vault.get("profile", {})
        if profile is None:
            logger.error("Expected key 'profile' is not found for the credentials")
            return {}

        secret_path = f"{provider}/{profile}"
        response = retrieve_secrets(self.vault_url, self.vault_token, secret_path)
        return response
