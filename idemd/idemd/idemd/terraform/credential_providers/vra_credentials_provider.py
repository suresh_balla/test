"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json
from urllib.error import HTTPError

import requests

from idemd.idemd.acct_utils import has_vra_acct_details
from idemd.idemd.constants.constants import (
    CLOUD_ACCOUNT_DOCUMENT_SELF_LINK,
    CLOUD_ACCOUNT_ID,
    CSP_AUTH_TOKEN_URI,
    BEARER_TOKEN,
    AWS_PROVIDER,
    AZURE_PROVIDER,
)
from idemd.idemd.run_space import get_csp_details_basic_auth
from idemd.idemd.terraform.credential_providers.credential_context import (
    BackendCredentialContext,
)
from idemd.idemd.terraform.credential_providers.credential_providers import (
    CredentialProvider,
)
from idemd.idemd.terraform.csp_utils import get_csp_token
from idemd.idemd.utils.iac_log_utils import get_default_logger

VRA_MGMT_ENDPOINT_URI = (
    "{provisioning_base_url}/provisioning/mgmt/endpoints?expandCredentials=true"
)
logger = get_default_logger()


class VRACredentialContext(BackendCredentialContext):
    def get_creds_as_map(self):
        org_id = self.task.taskIdentity.identity.orgId

        if org_id is None:
            raise ValueError(f"{org_id} is not a valid VRA org id.")

        if "vra" not in self.backends:
            raise EnvironmentError(
                "vRA is not set properly in the backend. please check it again"
            )
        else:
            try:
                vra_env = self.backends["environment"]["default"]
            except KeyError:
                raise EnvironmentError(
                    "Environments is present but default is missing. please check it again"
                )

        # 1. Retrieve the token from the client creds
        csp_url = CSP_AUTH_TOKEN_URI.format(csp_base_url=vra_env["csp_base_url"])
        client_creds = get_csp_details_basic_auth(
            csp_url, vra_env["client_id"], vra_env["client_secret"]
        )
        csp_token = get_csp_token(org_id, client_creds, CredentialProvider.VRA.name)

        # 2. Use the token to make a call to vRA
        url = VRA_MGMT_ENDPOINT_URI.format(
            provisioning_base_url=self.backends["vra"]["default"]["base_url"]
        )
        ret = {}

        cred_params = self.task.credentialParameters
        acct_params = json.loads(cred_params)

        if has_vra_acct_details(acct_params):
            document_self_link = acct_params.get(CLOUD_ACCOUNT_DOCUMENT_SELF_LINK)
            account_id = acct_params.get(CLOUD_ACCOUNT_ID)
        else:
            raise RuntimeError(
                "Auth credentials pre-requisites are not satisfied in the request."
            )

        if account_id is None and document_self_link is None:
            logger.info(
                "VRA account Id and document_self_link are not available in account config, Skipping vra backend."
            )
            return ret

        if document_self_link is not None:
            account_filter = "documentSelfLink eq " + f"'{document_self_link}'"
        else:
            account_filter = "id eq " + account_id

        if account_filter is not None:
            url = url + "&$filter=(" + account_filter + ")"

        headers = {
            "content-type": "application/json; charset=utf-8",
            "Authorization": BEARER_TOKEN.format(access_token=csp_token),
            "Accept": "application/json",
        }
        logger.debug("Calling API " f"{url}")
        response = requests.get(url, headers=headers)
        response_json = response.json()
        if response.status_code == 200:
            endpoints = response_json
        else:
            logger.error("Error occurred " + response_json["ret"])
            raise HTTPError(
                url, response_json["status"], response_json["comment"], None, None
            )

        # 3. get the secrets from endpoints
        total_count = endpoints["totalCount"]

        if total_count >= 1:
            document_links = endpoints["documentLinks"][0]
            document = endpoints["documents"][document_links]
            if AWS_PROVIDER == document["endpointType"]:
                ret["AWS_ACCESS_KEY_ID"] = document["authCredentials"]["privateKeyId"]
                ret["AWS_SECRET_ACCESS_KEY"] = document["authCredentials"]["privateKey"]
            elif AZURE_PROVIDER == document["endpointType"]:
                ret["ARM_CLIENT_ID"] = document["authCredentials"]["privateKeyId"]
                ret["ARM_CLIENT_SECRET"] = document["authCredentials"]["privateKey"]
                ret["ARM_SUBSCRIPTION_ID"] = document["authCredentials"]["userLink"]
                ret["ARM_TENANT_ID"] = document["authCredentials"]["customProperties"][
                    "azureTenantId"
                ]

            else:
                raise NotImplementedError(
                    "Cloud Account for Provider:{} not supported!".format(
                        document["endpointType"]
                    )
                )
        else:
            if document_self_link is not None:
                raise RuntimeError(
                    "No cloud account found in VRA with document_self_link:{}".format(
                        document_self_link
                    )
                )
            else:
                raise RuntimeError(
                    "No cloud account found in VRA with account_id:{}".format(
                        account_id
                    )
                )

        logger.info("Successfully fetched Credentials from VRA.")
        return ret
