"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

from idemd.idemd.terraform.credential_providers.credential_context import (
    AbstractCredentialContext,
)
from idemd.idemd.utils.iac_log_utils import get_default_logger

logger = get_default_logger()


class NoOpCredentialContext(AbstractCredentialContext):
    def get_creds_as_map(self):
        return {}
