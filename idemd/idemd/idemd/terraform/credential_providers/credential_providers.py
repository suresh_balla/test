"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

from enum import Enum


class CredentialProvider(Enum):
    VRA = 1
    ENVIRONMENTS = 2
    NO_OP = 3  # Credentials are not required for the operation
    VAULT = 4
