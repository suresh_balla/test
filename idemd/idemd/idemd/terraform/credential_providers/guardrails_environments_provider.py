"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""
import json

import requests

from idemd.idemd.constants.constants import (
    CSP_AUTH_TOKEN_URI,
    BEARER_TOKEN,
    AWS_PROVIDER,
    AZURE_PROVIDER,
)
from idemd.idemd.run_space import get_csp_details_basic_auth
from idemd.idemd.terraform.credential_providers.credential_context import (
    BackendCredentialContext,
)
from idemd.idemd.terraform.credential_providers.credential_providers import (
    CredentialProvider,
)
from idemd.idemd.terraform.csp_utils import get_csp_token
from idemd.idemd.utils.iac_log_utils import get_default_logger

RESOURCE_SERVICE_URI = "{base_url}/guardrails-resource/graphql"
ENVIRONMENTS = "environments"

logger = get_default_logger()

QUERY_STRING = """query (
                        $ENVIRONMENTS :[GuardrailsCredsMetaInput!]!
                        $PROJECT_ID: String!
                        $MODE: CredsAccessModeEnum
                        ){
                      guardrailsEnvironmentQueries {
                        _queryGuardrailsCreds(
                          filter : {
                            meta: $ENVIRONMENTS
                            projectId: $PROJECT_ID
                          }
                        ) {
                            environmentId
                            projectId
                            environmentType
                            provider
                            credentialData (mode: $MODE) 
                            {
                                credsAvailable,
                                creds
                            }
                      	}
                     }
                    }
                    """


class GuardrailsEnvironmentsCredentialContext(BackendCredentialContext):
    def get_creds_as_map(self):
        task_id = self.task.taskIdentity.taskId

        logger.info(f"In GuardrailsEnvironmentsCredentialContext for task id {task_id}")

        if "environment" not in self.backends:
            raise EnvironmentError(
                "Environments is not set properly in the backend. please check it again"
            )
        else:
            try:
                guardrails_env = self.backends["environment"]["default"]
            except KeyError:
                raise EnvironmentError(
                    "Environments is present but default is missing. please check it again"
                )

        ret = {}

        org_id = self.task.taskIdentity.identity.orgId
        project_id = self.task.taskIdentity.identity.projectId
        project_type = self.task.taskIdentity.identity.projectType
        task_mode = self.task.taskMode

        cred_params = self.task.credentialParameters
        acct_params = json.loads(cred_params)

        if "guardrails" in acct_params:
            guardrails = acct_params["guardrails"]
            is_tuple = type(guardrails) is tuple
            if "meta" in guardrails and not is_tuple:
                if project_id is None:
                    raise ValueError(
                        "environment_config.project_id is not a valid project_id."
                    )

                environments = guardrails["meta"]
                csp_url = CSP_AUTH_TOKEN_URI.format(
                    csp_base_url=guardrails_env["csp_base_url"]
                )

                client_creds = get_csp_details_basic_auth(
                    csp_url,
                    guardrails_env["client_id"],
                    guardrails_env["client_secret"],
                )

                csp_token = get_csp_token(
                    org_id, client_creds, CredentialProvider.ENVIRONMENTS.name
                )

                base_url = RESOURCE_SERVICE_URI.format(
                    base_url=guardrails_env["base_url"]
                )

                for environment in environments:
                    logger.debug("Environment found.")
                    provider = (
                        environment["provider"] if "provider" in environment else None
                    )
                    if provider is None:
                        raise ValueError("provider is not valid.")

                    if provider == "dlp":
                        return {}

                ret = process_environment(
                    environments,
                    project_id,
                    project_type,
                    task_mode,
                    csp_token,
                    base_url,
                )

        return ret


def process_environment(
    environments, project_id, project_type, mode, csp_token, base_url
):
    for environment in environments:
        logger.info(
            "Query environment for environment id {}".format(
                environment.get("environmentId")
            )
        )

    creds = {}

    variables = {"ENVIRONMENTS": environments, "PROJECT_ID": project_id, "MODE": mode}

    payload = json.dumps({"query": QUERY_STRING, "variables": variables})

    headers = {
        "Authorization": BEARER_TOKEN.format(access_token=csp_token),
        "Content-Type": "application/json",
    }

    if project_type:
        headers["X-aria-project-type"] = project_type

    response = requests.request("POST", base_url, headers=headers, data=payload)
    response = response.json()
    creds_data = response["data"]["guardrailsEnvironmentQueries"][
        "_queryGuardrailsCreds"
    ]

    if creds_data is not None and len(creds_data) > 0:
        for cred_details in creds_data:
            if "credentialData" in cred_details:
                if cred_details["credentialData"]["creds"]:
                    creds = cred_details["credentialData"]["creds"][0]
                else:
                    raise RuntimeError(
                        "Credentials not found for provider {}".format(
                            cred_details["provider"]
                        )
                    )
    else:
        raise RuntimeError("Credentials not found")

    provider_creds = populate_provider_specific_creds(creds)

    return provider_creds


def populate_provider_specific_creds(creds):
    provider = None
    creds_map = {}
    if "provider_name" in creds:
        provider = creds.get("provider_name")

    if AWS_PROVIDER == provider:
        creds_map["AWS_ACCESS_KEY_ID"] = creds.get("aws_access_key_id")
        creds_map["AWS_SECRET_ACCESS_KEY"] = creds.get("aws_secret_access_key")
        creds_map["AWS_SESSION_TOKEN"] = creds.get("aws_session_token")
        creds_map["AWS_REGION"] = creds.get("region_name")
    elif AZURE_PROVIDER == provider:
        creds_map["ARM_SUBSCRIPTION_ID"] = creds.get("subscription_id")
        creds_map["ARM_CLIENT_ID"] = creds.get("client_id")
        creds_map["ARM_CLIENT_SECRET"] = creds.get("secret")
        creds_map["ARM_TENANT_ID"] = creds.get("tenant")
    else:
        raise NotImplementedError(
            f"Only AWS and Azure provider are supported as of now. {provider} is not supported"
        )

    return creds_map
