"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

from idemd.idemd.terraform.credential_providers.abstract_credential_context import (
    AbstractCredentialContext,
)
from idemd.idemd.utils.iac_log_utils import get_default_logger

logger = get_default_logger()


class BackendCredentialContext(AbstractCredentialContext):
    def get_creds_as_map(self):
        pass

    def __init__(self, task, backends):
        super().__init__(task)
        self.backends = backends

    def get_backends(self):
        return self.backends

    def set_backends(self, backends):
        self.backends = backends
