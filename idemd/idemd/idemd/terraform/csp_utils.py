"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json
import sys
import threading
import time

import requests as requests
from cachetools import TTLCache

from idemd.idemd.utils.iac_log_utils import get_default_logger

logger = get_default_logger()
safety_duration_in_secs = 10
# Define a TTLCache to store the CSP tokens and their expiration times
# In general, CSP tokens have 1799 seconds to expire. Setting it to 1800 to clear the cache faster
token_cache = TTLCache(ttl=1800, maxsize=1000)
# Define a lock to synchronize access to the cache
cache_lock = threading.Lock()


def get_client_credentials_token_with_org(org_id, client_creds):
    url = client_creds["url"]
    b64_val = client_creds["b64_creds"]

    payload = "grant_type=client_credentials"

    if org_id is not None:
        payload = f"{payload}&orgId={org_id}"

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Basic %s" % b64_val,
    }

    try:
        response = requests.post(url, headers=headers, data=payload)
        token_dict = json.loads(response.content.decode("utf-8"))
    except:
        logger.error(
            "get token from CSP is not successful . Exception is %s", sys.exc_info()[0]
        )
        token_dict = None
    return token_dict


def get_org_less_client_credentials_token(client_creds):
    return get_client_credentials_token_with_org(None, client_creds)


def get_csp_token(org, client_creds, creds_type):
    # Acquire the lock before accessing the cache
    cache_lock.acquire()
    if org is None:
        key = f"{'no_org'}-{creds_type}"
    else:
        key = f"{org}-{creds_type}"

    try:
        # Check if the token for this org is in the cache and is not expired
        if key in token_cache:
            token, expiration = token_cache[key]
            if expiration > time.time():
                logger.info(f"Retrieving existing CSP token for key {key} from cache.")
                return token

        # If the token is not in the cache or is expired, retrieve a new token and update the cache
        logger.info(f"Old CSP token expired for org {org}. Retrieving new CSP token")
        token, expiration = _retrieve_csp_access_token(org, client_creds)
        token_cache[key] = (token, expiration)
        return token
    finally:
        # Release the lock after accessing the cache
        cache_lock.release()


def _retrieve_csp_access_token(org_id, client_creds):
    now = time.time()
    try:
        if org_id is not None:
            token_dict = get_client_credentials_token_with_org(org_id, client_creds)
        else:
            token_dict = get_org_less_client_credentials_token(client_creds)
        csp_access_token = token_dict.get("access_token")
        expires_in = int(token_dict.get("expires_in")) - safety_duration_in_secs
        token_expiration_time = now + expires_in
    except Exception as e:
        message = f"Failed to get access token from csp for org {org_id}. Exception is {str(e)}"
        logger.info(message)
        raise RuntimeError(message)

    return csp_access_token, token_expiration_time
