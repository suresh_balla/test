"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""


class InvalidArnException(Exception):
    pass


class Arn(object):
    """
    This represents the ARN object of AWS
    Reference - https://docs.aws.amazon.com/IAM/latest/UserGuide/reference-arns.html
    Format:
        arn:partition:service:region:account-id:resource-id
        arn:partition:service:region:account-id:resource-type/resource-id
        arn:partition:service:region:account-id:resource-type:resource-id
    """

    def __init__(self, partition, service, region, account_id, resource_type, resource):
        self.partition = partition
        self.service = service
        self.region = region
        self.account_id = account_id
        self.resource_type = resource_type
        self.resource = resource


def validate_arn(arn):
    """
    Validates the ARN string
    :param arn: ARN string
    :return: boolean
    """
    return arn.startswith("arn:")


def parse_arn(arn):
    """
    Parses ARN string and returns the ARN Object

    :param arn: ARN string
    :return: ARN object of AWS
    """

    if not validate_arn(arn):
        raise InvalidArnException(f"Invalid arn string {arn}")

    # Reference - https://docs.aws.amazon.com/IAM/latest/UserGuide/reference-arns.html
    elements = arn.split(":", 5)
    service = elements[2]
    resource = elements[5]

    if service in ["apigateway", "execute-api", "s3", "sns"]:
        resource_type = None
    else:
        resource_type, resource = parse_resource(resource)

    return Arn(
        partition=elements[1],
        service=service,
        region=None if elements[3] == "" else elements[3],
        account_id=None if elements[4] == "" else elements[4],
        resource_type=resource_type,
        resource=resource,
    )


def parse_resource(resource):
    """
    Extracts the resource type and resource ID from the last part of an ARN.

    :param resource: last part of the ARN
    :return: resource_type, resource ID
    """

    first_separator_index = -1
    for index, char in enumerate(resource):
        if char in (":", "/"):
            first_separator_index = index
            break

    if first_separator_index != -1:
        resource_type = resource[:first_separator_index]
        resource = resource[first_separator_index + 1 :]
    else:
        resource_type = None

    return resource_type, resource
