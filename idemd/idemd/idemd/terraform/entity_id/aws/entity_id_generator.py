"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json
import os

from idemd.idemd.terraform.entity_id.arn_parser import parse_arn


def get_region(attributes, creds):
    """
    Returns the region based on the provided attributes and credentials.

    If the resource contains an ARN, the region is extracted from it.
    If no ARN is found, the region is obtained from the attributes directly.
    If no region is specified in the attributes, it falls back to the region specified in the credentials.
    If no region is specified in the credentials or any of the above cases, the default region is 'global'.
    """
    arn = attributes.get("arn")
    if arn:
        arn = parse_arn(arn)
        if arn.region is not None:
            return arn.region
    region = attributes.get("region")
    if region:
        return region
    if creds[0].get("overrides") and creds[0].get("overrides").get("region"):
        return creds[0]["overrides"]["region"]
    return "global"


def build_string(service_list, attributes):
    """
    Builds a string by concatenating nested attributes based on the provided service list.

    Args:
        service_list (list): A list of nested attribute names.
        attributes (dict): Attribute values of terraform resource.

    Returns:
        str: The concatenated string of nested attributes, separated by dots.

    If any nested attribute in the service list is None, the function returns None.
    Only non-None attributes are included in the final string.
    The nested attributes are concatenated using dots as separators.
    """

    nested_attributes = [attributes.get(item) for item in service_list[2:]]
    if None in nested_attributes:
        return None
    filtered_attributes = [attr for attr in nested_attributes if attr is not None]
    result = ".".join(filtered_attributes)
    return result


def get_id(resource_type, service_list, attributes, origin):
    """
    Returns the ID for the given resource based on the provided parameters.

    Args:
        resource_type (str): The type of terraform resource.
        service_list (list): A list of nested attribute names.
        attributes (dict): Attribute values of terraform resource.
        origin (str): The origin or region value.

    Returns:
        str: The ID of the resource.

    If the service list has at least three elements, the ID is built by concatenating nested attributes using dots.
    By default, the ID obtained from the attributes is returned.
    Sometimes, the ID could be an ARN (Amazon Resource Name), so we extract the ID by splitting the ARN and retrieving the last component.
    For other combinations of cases, Additional logic may be required to handle and determine the appropriate ID.
    """

    if len(service_list) >= 3:
        id = build_string(service_list, attributes)
        if id:
            return id

    id = attributes.get("id")
    if id:
        id = id.split(":")[-1]

    if resource_type == "aws_s3_bucket_policy":
        return f"{id}.bucket.resourcePolicy"
    elif resource_type in [
        "aws_config_configuration_recorder",
        "aws_config_configuration_recorder_status",
        "aws_guardduty_detector",
    ]:
        return origin
    elif resource_type == "aws_organizations_policy_attachment":
        target_id = attributes.get("target_id")
        if target_id and target_id.startswith("ou-"):
            service_list[1] = "OrganizationalUnit"
            return target_id
        else:
            return attributes.get("policy_id")
    elif resource_type == "aws_wafv2_web_acl_association":
        web_acl_arn = attributes.get("web_acl_arn")
        if web_acl_arn:
            return web_acl_arn.split(":")[-1]

    return id


def get_aws_entity_id(hub, resource, task):
    """
    Generates the entity ID for an AWS resource based on the provided parameters.

    Args:
        resource : Terraform AWS resource.
        task: The task object, which holds the environment ID and region information.

    Returns:
        dict: The entity ID as a dictionary.

    Loads the supported Terraform AWS resource types from a JSON file.
    Retrieves the environment ID, resource type, instances, and attributes from the resource dictionary.
    Checks if the resource type is supported.
    Constructs the ID field of entity ID for the resource using the get_id() function.
    Constructs and returns the entity ID dictionary.

    Note: Error messages are printed to the console if any required keys or conditions are missing.

    """
    creds = json.loads(task.credentialParameters)
    meta = creds["guardrails"]["meta"]

    with open(
        os.path.realpath(
            os.path.join(
                os.path.dirname(__file__), "supported_terraform_aws_resources.json"
            )
        ),
        "r",
    ) as file:
        supported_types = json.load(file)

    environment_id = meta[0]["environmentId"]
    resource_type = resource.get("type")
    if not resource_type:
        hub.log.debug("Error: 'type' key is missing in the resource.")
        return None

    instances = resource.get("instances")
    if instances is None or len(instances) == 0:
        hub.log.debug("Error: 'instances' key is missing or empty.")
        return None

    attributes = instances[0].get("attributes")
    if attributes is None:
        hub.log.debug(
            "Error: 'attributes' key is missing in the 'instances' dictionary."
        )
        return None

    service_list = supported_types.get(resource_type)
    if not service_list:
        hub.log.debug(
            "Error: Entity ID generation not supported for type" + resource_type
        )
        return None

    global_region_list = [
        "aws_iam_account_password_policy",
        "aws_ce_anomaly_monitor",
        "aws_ce_anomaly_subscription",
        "aws_ce_cost_category",
        "aws_iam_policy",
        "aws_organizations_organizational_unit",
        "aws_organizations_organization",
        "aws_organizations_policy",
        "aws_organizations_policy_attachment",
        "aws_organizations_account",
        "aws_wafv2_web_acl_association",
        "aws_iam_policy",
        "aws_iam_role",
        "aws_iam_role_policy",
        "aws_iam_role_policy_attachment",
        "aws_iam_user",
        "aws_iam_user_policy",
        "aws_iam_service_linked_role",
        "aws_iam_group",
        "aws_iam_group_policy_attachment",
    ]
    if resource_type in global_region_list or (
        resource_type == "aws_wafv2_web_acl" and attributes.get("scope") == "CLOUDFRONT"
    ):
        origin = "global"
    else:
        origin = get_region(attributes, meta)

    id = get_id(resource_type, service_list, attributes, origin)

    entity_id = {
        "provider": "AWS",
        "service": service_list[0],
        "instance": environment_id,
        "origin": origin,
        "type": service_list[1],
        "id": id,
    }
    return entity_id
