"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import google

from idemd.idemd.terraform.entity_id.aws.entity_id_generator import get_aws_entity_id
from idemd.idemd.terraform.entity_id.azure.entity_id_generator import (
    get_azure_entity_id,
)


def get_entity_id(hub, resource, task):
    resource_type = resource.get("type")
    if not resource_type:
        hub.log.debug("Entity Id generation not supported")
        return None
    provider = resource_type.split("_")[0]
    if resource.get("mode") != "managed":
        hub.log.debug(
            f"Entity Id generation not supported for the mode : {resource.get('mode')}"
        )
        return None

    if provider == "azurerm":
        entity_id = get_azure_entity_id(hub, resource)
    elif provider == "aws":
        entity_id = get_aws_entity_id(hub, resource, task)
    else:
        hub.log.debug("Entity ID generation is not supported for the given provider.")
        return None
    if not entity_id:
        return None
    eid = google.protobuf.struct_pb2.Struct()
    eid["provider"] = entity_id["provider"]
    eid["service"] = entity_id["service"]
    eid["instance"] = entity_id["instance"]
    if entity_id.get("origin"):
        eid["origin"] = entity_id["origin"]
    eid["type"] = entity_id["type"]
    eid["id"] = entity_id["id"]
    return eid
