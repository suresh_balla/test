"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json
import os
import re


def get_azure_entity_id(hub, tf_resource):
    """
    Constructs the entity ID based on the given Terraform Azure resource.

    Args:
        hub: The hub object.
        tf_resource: The Terraform resource object.

    Returns:
        dict: The entity ID as a dictionary.
    """
    tf_resource_type = tf_resource.get("type")

    with open(
        os.path.realpath(
            os.path.join(
                os.path.dirname(__file__), "supported_terraform_azure_resources.json"
            )
        ),
        "r",
    ) as file:
        supported_types = json.load(file)

    if tf_resource_type not in supported_types:
        hub.log.debug("Entity ID not supported for resource: " + tf_resource_type)
        return None

    try:
        instances = tf_resource.get("instances")
        if instances is None or len(instances) == 0:
            hub.log.debug("Error: 'instances' key is missing or empty.")
            return None

        attributes = instances[0].get("attributes")
        if attributes is None:
            hub.log.debug(
                "Error: 'attributes' key is missing in the 'instances' dictionary."
            )
            return
        resource_id = attributes.get("id")
        if not resource_id:
            hub.log.debug(
                "Error: 'resource_id' is missing in the 'attributes' dictionary."
            )
            return

        to_type = lambda v: v[0].upper() + v[1:-1]
        resource = parse_resource_id(resource_id)
        origin = None
        if "resource_namespace" in resource:
            service = resource["resource_namespace"].replace("Microsoft.", "")
            resource_type = (
                to_type(resource["resource_type"])
                if resource["resource_type"] != "publicIPAddresses"
                else "PublicIPAddress"
            )
            resource_id = resource["resource_name"]
            if "resource_group" in resource:
                origin = resource["resource_group"]
        elif "resource_group" in resource:
            service = "ResourceManager"
            resource_type = "ResourceGroup"
            resource_id = resource["resource_group"]
        else:
            service = "Subscription"
            resource_type = "Model"
            resource_id = resource["subscription"]
        entity_id = {
            "provider": "Azure",
            "service": service,
            "instance": resource["subscription"],
            "type": resource_type,
            "id": resource_id.lower(),
        }
        if origin is not None:
            entity_id["origin"] = origin.lower()
        return entity_id

    except KeyError:
        hub.log.debug("Entity ID not supported for resource: " + tf_resource_type)
        return None


_ARMID_RE = re.compile(
    "(?i)/subscriptions/(?P<subscription>[^/]*)(/resourceGroups/(?P<resource_group>[^/]*))?"
    "(/providers/(?P<namespace>[^/]*)/(?P<type>[^/]*)/(?P<name>[^/]*)(?P<children>.*))?"
)

_CHILDREN_RE = re.compile(
    "(?i)(/providers/(?P<child_namespace>[^/]*))?/"
    "(?P<child_type>[^/]*)/(?P<child_name>[^/]*)"
)


def parse_resource_id(rid):
    """Parses a resource_id into its various parts.
    Returns a dictionary with a single key-value pair, 'name': rid, if invalid resource id.
    :param rid: The resource id being parsed
    :type rid: str
    :returns: A dictionary with following key/value pairs (if found):
        - subscription:            Subscription id
        - resource_group:          Name of resource group
        - namespace:               Namespace for the resource provider (i.e. Microsoft.Compute)
        - type:                    Type of the root resource (i.e. virtualMachines)
        - name:                    Name of the root resource
        - child_namespace_{level}: Namespace for the child resoure of that level
        - child_type_{level}:      Type of the child resource of that level
        - child_name_{level}:      Name of the child resource of that level
        - last_child_num:          Level of the last child
        - resource_parent:         Computed parent in the following pattern: providers/{namespace}\
        /{parent}/{type}/{name}
        - resource_namespace:      Same as namespace. Note that this may be different than the \
        target resource's namespace.
        - resource_type:           Type of the target resource (not the parent)
        - resource_name:           Name of the target resource (not the parent)
    :rtype: dict[str,str]
    """
    if not rid:
        return {}
    match = _ARMID_RE.match(rid)
    if match:
        result = match.groupdict()
        children = _CHILDREN_RE.finditer(result["children"] or "")
        count = None
        for count, child in enumerate(children):
            result.update(
                {
                    key + "_%d" % (count + 1): group
                    for key, group in child.groupdict().items()
                }
            )
        result["last_child_num"] = count + 1 if isinstance(count, int) else None
        result = _populate_alternate_kwargs(result)
    else:
        result = dict(name=rid)
    return {key: value for key, value in result.items() if value is not None}


def _populate_alternate_kwargs(kwargs):
    """Translates the parsed arguments into a format used by generic ARM commands
    such as the resource and lock commands.
    """

    resource_namespace = kwargs["namespace"]
    resource_type = (
        kwargs.get("child_type_{}".format(kwargs["last_child_num"])) or kwargs["type"]
    )
    resource_name = (
        kwargs.get("child_name_{}".format(kwargs["last_child_num"])) or kwargs["name"]
    )

    _get_parents_from_parts(kwargs)
    kwargs["resource_namespace"] = resource_namespace
    kwargs["resource_type"] = resource_type
    kwargs["resource_name"] = resource_name
    return kwargs


def _get_parents_from_parts(kwargs):
    """Get the parents given all the children parameters."""
    parent_builder = []
    if kwargs["last_child_num"] is not None:
        parent_builder.append("{type}/{name}/".format(**kwargs))
        for index in range(1, kwargs["last_child_num"]):
            child_namespace = kwargs.get("child_namespace_{}".format(index))
            if child_namespace is not None:
                parent_builder.append("providers/{}/".format(child_namespace))
            kwargs["child_parent_{}".format(index)] = "".join(parent_builder)
            parent_builder.append(
                "{{child_type_{0}}}/{{child_name_{0}}}/".format(index).format(**kwargs)
            )
        child_namespace = kwargs.get(
            "child_namespace_{}".format(kwargs["last_child_num"])
        )
        if child_namespace is not None:
            parent_builder.append("providers/{}/".format(child_namespace))
        kwargs["child_parent_{}".format(kwargs["last_child_num"])] = "".join(
            parent_builder
        )
    kwargs["resource_parent"] = "".join(parent_builder) if kwargs["name"] else None
    return
