"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import asyncio
import json
import os
from pathlib import Path

from idemgrpc import messages_pb2 as messages

from idemd.idemd.constants.constants import DEFAULT_GIT_AUTH_TYPE, GIT_AUTH_TYPE_PROP
from idemd.idemd.idem_service_utils import (
    send_task_update,
    send_task_update_with_resources,
    prepare_task_update_request,
)
from idemd.idemd.run_space import (
    resolve_file_name,
    extract_bundle,
    create_or_get_run_space_dir,
)
from idemd.idemd.terraform.credential_providers.credential_context_factory import (
    CredentialContextFactory,
)
from idemd.idemd.utils.git_credential_loader import GitCredentialsLoader
from idemd.idemd.utils.git_utils import (
    GitCloneContext,
    clone_or_update_repo,
    resolve_git_context,
)
from idemd.idemd.utils.iac_log_utils import get_default_logger

logger = get_default_logger()


def create_terraform_workspace(hub, task):
    """
    Utility function to create a workspace for running Terraform tasks.

    Return Value:
        tuple: A tuple containing information about the created workspace.
            - If the Git use case is applicable, it returns a tuple `(space_dir, git_context)` where `tf_root_dir` is
            the terraform workspace path in the cloned Git repository.
            - If the Git use case is not applicable (non-Git use case), it returns a tuple `(path, file_type)`
             where `path` is the directory path of the created workspace and `file_type` indicates the type of Terraform
             bundle used.

    """
    context, meta = resolve_git_context(task)
    run_id, space_dir = create_or_get_run_space_dir(hub, task)

    # This creates and downloads the Terraform bundle
    # Git use case
    if context is not None:
        auth_type = DEFAULT_GIT_AUTH_TYPE
        if meta is not None and {} != meta:
            auth_type = meta.get(GIT_AUTH_TYPE_PROP)

        credentials = GitCredentialsLoader.load_git_credentials_from_vault(
            git_url=context.repo_url, auth_type=auth_type
        )

        context.credentials = credentials
        context.clone_path = space_dir

        context = clone_or_update_repo(context)

        execution_context = json.loads(task.exeutionContext)
        source_base_dir = None

        if (
            execution_context["gitExecutionContext"]
            and len(execution_context["gitExecutionContext"]) != 0
        ):
            git_execution_context = execution_context["gitExecutionContext"]
            source_base_dir = git_execution_context["baseDir"]

        tf_root_dir = space_dir
        if source_base_dir is not None:
            tf_root_dir = tf_root_dir + source_base_dir

        return tf_root_dir, context
    else:
        path, file_type = hub.idemd.run_space.create(task)

        # Unzip and extract Terraform bundle
        extract_terraform_bundle(task, path, file_type)
        logger.info(
            f"Run space {path} created successfully for Task {task.taskIdentity.taskId}"
        )

        return space_dir, file_type


def execute_terraform_task_helper(hub, task):
    space_dir = None
    try:
        logger.info(
            f"Task {task.taskIdentity.taskId} is claimed as it being executed now"
        )

        # Update the caller with RUNNING status
        task_update_request = prepare_task_update_request(task, None, "RUNNING")
        send_task_update(hub, task, task_update_request)

        # Create work space from task
        space_dir, _type = create_terraform_workspace(hub, task)

        try:
            context = CredentialContextFactory.get_context(task)
        except ValueError as ve:
            logger.info(f"Invalid credential mode. {str(ve)}")
            raise ve

        creds_map = context.get_creds_as_map()

        return asyncio.run(
            execute_terraform_task_as_process(hub, task, space_dir, creds_map, _type)
        )
    finally:
        if space_dir is not None:
            logger.info(f"deleted directory")


async def execute_terraform_task_as_process(
    hub, task, space_dir, creds_map, workspace_context
):
    params = task.slsParameters
    meta = task.meta

    # Define the Terraform commands to execute
    terraform_init_command = ["terraform", "init"]
    terraform_apply_command = [
        "terraform",
        "apply",
        "-auto-approve",
        "-refresh=false",
        "-state-out=terraform.tfstate",
        "-input=false",
    ]
    terraform_apply_command = get_command_for_task(
        terraform_apply_command, task, meta, params, space_dir
    )

    terraform_pull_command = ["terraform", "state", "pull"]

    terraform_plan_command = [
        "terraform",
        "plan",
        "-out=tfPlan",
        "-input=false",
    ]
    terraform_plan_command = get_command_for_task(
        terraform_plan_command, task, meta, params, space_dir
    )

    terraform_plan_show = ["terraform", "show", "-json", "tfPlan"]

    # Execute `terraform init` asynchronously in the specified directory
    logger.info(f"Running terraform init for task {task.taskIdentity.taskId}")
    _, init_err, return_code = await run_command(terraform_init_command, space_dir)

    if init_err and return_code != 0:
        return 1, f"Error occurred when initializing terraform run space {space_dir}"

    custom_env = os.environ.copy()
    if creds_map:
        for creds in creds_map.items():
            custom_env[creds[0]] = creds[1]

    if task.taskMode and task.taskMode == "TEST":
        # Execute `terraform plan`
        logger.info(
            f"Running terraform plan: {terraform_plan_command} for task {task.taskIdentity.taskId} in TEST mode"
        )
        _, plan_err, return_code = await run_command_with_env(
            terraform_plan_command, space_dir, custom_env
        )

        if plan_err and return_code != 0:
            return (
                1,
                f"Error occurred when running `terraform plan` from run space {space_dir} Error is {plan_err}",
            )

        show_output, show_err, return_code = await run_command(
            terraform_plan_show, space_dir
        )

        if show_err and return_code != 0:
            return (
                1,
                f"Error occurred when running `terraform show on plan output` from run space {space_dir} Error is {show_err}",
            )

        try:
            show_output_dict = json.loads(show_output[0])
        except json.decoder.JSONDecodeError:
            show_output_dict = json.loads(show_output)

        resources, meta = get_resources_and_meta(show_output_dict, workspace_context)
        res = send_task_update_with_resources(
            hub, task, resources, meta, return_code, plan_err
        )

    else:
        # Execute `terraform apply` asynchronously in the specified directory
        logger.info(
            f"Running terraform apply: {terraform_apply_command} for task {task.taskIdentity.taskId}"
        )
        _, apply_err, apply_return_code = await run_command_with_env(
            terraform_apply_command, space_dir, custom_env
        )

        # Execute `terraform pull` asynchronously in the specified directory even if apply fails still
        # we do a `terraform pull` and send the task update as even if one resource is failed we want to process other
        # resource executions
        logger.info(f"Running terraform pull for task {task.taskIdentity.taskId}")
        show_output, show_err, return_code = await run_command(
            terraform_pull_command, space_dir
        )

        if show_err and return_code != 0:
            return (
                1,
                f"Error occurred when running terraform pull, when retrieving the state from run space {space_dir}. "
                f"Error is {show_err}",
            )

        try:
            show_output_dict = json.loads(show_output[0])
        except json.decoder.JSONDecodeError:
            show_output_dict = json.loads(show_output)

        resources, meta = get_resources_and_meta(show_output_dict, workspace_context)
        res = send_task_update_with_resources(
            hub, task, resources, meta, apply_return_code, apply_err
        )

        if apply_err and apply_return_code != 0:
            return (
                -1,
                f"Error occurred when applying the terraform state in run space {space_dir}. Error is {apply_err}",
            )

    return res


def get_resources_and_meta(output_dict, workspace_context):
    meta = []
    resources = {}
    for key, value in output_dict.items():
        if key in ["resources", "resource_changes"]:
            resources = value
        else:
            a = messages.KeyValuePair()
            a.key = str(key)
            a.value = str(value)
            meta.append(a)

    if isinstance(workspace_context, GitCloneContext):
        commit = messages.KeyValuePair()
        commit.key = "commit_id"
        commit.value = workspace_context.commit_id
        meta.append(commit)

        branch = messages.KeyValuePair()
        branch.key = "branch"
        branch.value = workspace_context.branch_name
        meta.append(branch)

    return resources, meta


async def run_command(command, cwd):
    """
    Run a Terraform command asynchronously.
    """
    process = await asyncio.create_subprocess_exec(
        *command,
        cwd=cwd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )

    stdout, stderr = await process.communicate()

    return stdout.decode(), stderr.decode(), process.returncode


async def run_command_with_env(command, cwd, env):
    """
    Run a Terraform command asynchronously with a custom environment.
    """
    process = await asyncio.create_subprocess_exec(
        *command,
        cwd=cwd,
        env=env,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )

    stdout, stderr = await process.communicate()

    return stdout.decode(), stderr.decode(), process.returncode


def extract_terraform_bundle(task, bundle_path, file_type):
    if file_type is not None and file_type == "application/zip":
        space_dir = bundle_path.parent.absolute()
        bundle_name = Path(space_dir).joinpath(resolve_file_name(task, file_type))
        extract_bundle(bundle_name, space_dir)
        logger.info(
            f"Bundle extracted for task_id: {task.taskIdentity.taskId} at path: {space_dir}"
        )


def get_params(terraform_command, params, taskid):
    logger.info(
        f"get_params() function call to extract the variable values for task: {taskid}"
    )
    if len(params) != 0:
        json_params = json.loads(params)
        for key, value in json_params.items():
            variable = "-var=" + key + "=" + value
            terraform_command.append(variable)

    return terraform_command


def get_variable_files(terraform_command, meta, taskid):
    logger.info(
        f"get_variable_files() function call to extract the names of variable files for task: {taskid}"
    )
    if len(meta) != 0:
        json_meta = json.loads(meta)
        for key, value in json_meta.items():
            if key == "varFiles":
                for file in value:
                    var_file = "-var-file=" + file
                    terraform_command.append(var_file)

    return terraform_command


def get_state_file(terraform_command, task, meta, space_dir):
    logger.info(
        f"get_state_files() function call for taskid {task.taskIdentity.taskId}"
    )
    if os.path.isfile(Path(space_dir).joinpath("customEnforcedState.tfstate")):
        terraform_command.append("-state=customEnforcedState.tfstate")
    elif len(meta) != 0:
        json_meta = json.loads(meta)
        for key, value in json_meta.items():
            if key == "stateFile":
                file = "-state=" + value
                terraform_command.append(file)

    return terraform_command


def get_command_for_task(terraform_command, task, meta, params, space_dir):
    terraform_command = get_state_file(terraform_command, task, meta, space_dir)
    terraform_command = get_params(terraform_command, params, task.taskIdentity.taskId)
    terraform_command = get_variable_files(
        terraform_command, meta, task.taskIdentity.taskId
    )

    return terraform_command
