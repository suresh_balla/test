"""
Copyright (c) 2024 VMware, Inc. All Rights Reserved.
"""

import json
import os
import subprocess

import jwt
import uuid
from datetime import datetime, timedelta
import requests as requests
from urllib.error import HTTPError

#management endpoint properties
PERMISSION_COLLECTION = "COLLECTION"
API_KEY_PROPERTY = "CredentialAPIKey"

expiration_time = None
sm_auth_token = None
BEARER_TOKEN = "Bearer {access_token}"

safety_duration_in_secs = 10

from idemgrpc import messages_pb2

from idemd.idemd.run_space import read_client_credentials

QUERY_ENDPOINT_CREDS_GRAPHQL = ""
with open(os.path.realpath(os.path.join(os.path.dirname(__file__), "queryGitEndpointCreds.graphql")), "r") as f:
    QUERY_ENDPOINT_CREDS_GRAPHQL = f.read()


def execute_script_task_helper(hub, task):
    if not task.executionContext:
        err = "Task has no executionContext"
        hub.log.error(err)
        return 1, err

    task_id = task.taskIdentity.taskId
    org_id = task.taskIdentity.identity.orgId
    project_id = task.taskIdentity.identity.projectId
    project_type = task.taskIdentity.identity.projectType
    executor_path = os.getenv("SCRIPT_TASK_DIRECTORY") + '/executor.sh'

    execution_context = json.loads(task.executionContext)
    cmd = execution_context.get("cmd")

    env = {
        "REQUESTOR": task.requester,
        "TASK_ID": task_id,
        "ORG_ID": org_id,
        "PROJECT_ID": project_id,
    }
    env = {**env, **os.environ.copy()}

    entity_id = execution_context.get("entityId")
    if entity_id is not None or entity_id:
        env["ENTITY_ID"] = entity_id

    endpoint_id = execution_context.get("endpointId")
    if endpoint_id is not None or endpoint_id:
        env["ENDPOINT_ID"] = endpoint_id

    if hub.idemd.worker.workerType == messages_pb2.WorkerIdentity.WorkerType.REMOTE:
        env["IS_REMOTE_WORKER"] = "true"
    else:
        env["IS_REMOTE_WORKER"] = "false"

    if env["IS_REMOTE_WORKER"] == "false":
        git_token = get_git_token(hub, task, org_id, endpoint_id)
        if git_token is not None:
            env['GIT_PERSONAL_TOKEN'] = git_token
            # Hack need to be fixed once we have concrete logic to get endpoint url from management endpoint matching with repo path
            env['GIT_TYPE'] = execution_context.get("params", {}).get("-et", None)
            execution_context.get("params", {}).pop("-et", None)
            env['GIT_ENDPOINT'] = execution_context.get("params", {}).get("-eu", None)
            execution_context.get("params", {}).pop("-eu", None)


    args = [executor_path, cmd]

    args_in = execution_context.get("params")
    if args_in and isinstance(args_in, dict):
        for k, v in args_in.items():
            args.extend([k, v])

    env_in = execution_context.get("environment")
    if env_in and isinstance(env_in, dict):
        env = {**env, **env_in}

    proc_timeout = (
        task.expiresInSeconds
        if task.expiresInSeconds is not None and task.expiresInSeconds > 0
        else hub.idemd.global_task_timeout
    )

    message, proc = None, None
    try:
        hub.log.debug(f"About to execute script task for org: {org_id}, project {project_id}, task: {task_id}")
        proc = subprocess.Popen(
            args=args,
            env=env,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        try:
            stdout, stderr = proc.communicate(timeout=proc_timeout)
            hub.log.info(
                f" Process ID {proc.pid} for task ID {task.taskIdentity.taskId}"
            )
            if stdout:
                message = stdout.decode()
                hub.log.info(f"[stdout]\n{message}")
            if stderr:
                message = stderr.decode()
                hub.log.error(f"[stderr]\n{message}")
            return_code = proc.returncode
        except subprocess.TimeoutExpired:
            hub.log.error(
                f"Timeout occurred for task"
                f" {task.taskIdentity.taskId}. Killing the process with PID "
                f"{proc.pid} and will proceed with idem service update"
            )
            try:
                # On timeout error, explicitly kill the process
                proc.kill()
            except OSError:
                # Ignore 'no such process' error
                pass
            # exit code for timeout
            return_code = 124
            message = f"Timeout occurred for task {task.taskIdentity.taskId}"
        except Exception as exc:
            # Any exception other than the TimeoutError will be caught here. This can happen when the process
            # itself fails with some Exception before timeout.
            # await proc.communicate()
            hub.log.error(
                f"Exception Occurred. Task {task.taskIdentity.taskId} failed with: {exc}"
            )
            message = repr(exc)
            return_code = 1
    except Exception as err:
        hub.log.error(
            f"Error occurred when spawning a new process for task {task.taskIdentity.taskId} - {str(err)}"
        )
        return_code = 1
        message = repr(err)

    return return_code, message


def get_git_token(hub, task, org_id, endpoint_id):
    creds = read_client_credentials(hub)
    token = get_access_token(hub, task, creds, org_id)

    hub_graph_url = creds["hub_graph_url"]+"/hub-private/graphql"
    hub.log.info(f"Query Git endpoint credential ")
    variables = {
        "MANAGMENT_ENDPOINT_IDs": [endpoint_id],
        "PERMISSION_TYPE": PERMISSION_COLLECTION,
        "PROPERTY_NAME": [API_KEY_PROPERTY]
    }
    hub.log.info(f"Invoking url: {hub_graph_url} with variables: {variables}")

    graphql_response = invoke_graphql(hub, token, QUERY_ENDPOINT_CREDS_GRAPHQL, variables, hub_graph_url)
    error_response = graphql_response.get("errors")
    if error_response is not None and len(graphql_response["errors"]) > 0:
        hub.log.error(f"mutation_error_response {error_response}")
        comment = graphql_response["errors"][0]['message']
        return {'result': False, 'comment': comment, 'ret': None}

    hub.log.info("graphql_response: ",
                 graphql_response["data"]["managementEndpointQuery"]["_queryManagementEndpointCredentials"])
    endpoint = graphql_response["data"]["managementEndpointQuery"]["_queryManagementEndpointCredentials"]

    creds_properties = None
    if endpoint is not None:
        hub.log.info(f"Got existing managementEndpoint")
        mgmt_cred = \
            endpoint[0][
                'managementEndpointCredentials']
        if mgmt_cred is not None:
            creds_properties = \
                mgmt_cred[0]['properties']
    if creds_properties is not None:
        return get_property_by_name(creds_properties, API_KEY_PROPERTY)
    else:
        hub.log.info(f"Didn't find creds for managementEndpoint {endpoint_id}")
        return None


def get_property_by_name(creds_prop, prop_name):
    for prop in creds_prop:
        if prop['name'] == prop_name:
            return prop['value']
    return None

# get token csp/sm
def get_access_token(hub, task, creds, org_id):
    hub.idemd.client_creds = creds["auth"]
    if creds.get("csp"):
        return get_csp_token(hub, hub.idemd.client_creds, org_id)
    else:
        return get_sm_token(hub.idemd.client_creds)


def get_csp_token(hub, client_creds, orgId):
    url = client_creds["url"]
    b64_val = client_creds["b64_creds"]

    payload = "grant_type=client_credentials&orgId={org_id}"
    payload = payload.format(org_id=orgId)
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Basic %s" % b64_val,
    }

    response = requests.post(url, headers=headers, data=payload)
    if response.status_code == 200:
        token_dict = json.loads(response.content.decode("utf-8"))
        return token_dict['access_token']
    else:
        hub.log.error(f"Failed to get access_token status_code {response.status_code} for org_id {orgId} ")
        raise HTTPError(url, response.status_code, response.reason, None, None)


def get_sm_token(client_creds):
    global expiration_time
    global sm_auth_token
    try:
        now = datetime.now()
        if not expiration_time or now - expiration_time >= timedelta(seconds=0):
            sm_auth_token = get_client_credentials_token(client_creds)
            # reduce expiration duration time by safety_duration_in_secs
            expires_in = client_creds["clock-skew-in-mins"] * timedelta(seconds=1000) - safety_duration_in_secs
            expiration_time = now + timedelta(seconds=expires_in)
    except Exception as e:
        print("Failed to get access token from SM. Exception is " + str(e))

    return sm_auth_token


def invoke_graphql(hub, access_token, query_string, variables, graphql_url, retry=0, required_csp_token=None):
    url = graphql_url
    json_query = {"query": query_string, "variables": variables}
    payload = json.dumps(json_query)
    headers = {
        "content-type": "application/json; charset=utf-8",
        "Authorization": BEARER_TOKEN.format(access_token=access_token),
        "Accept": "application/json",
    }
    result = requests.post(url, headers=headers, data=payload)
    # result = await hub.exec.request.json.post(ctx, url=f"{url}", headers=headers, params=None,
    #                                           data=payload)
    if result.status_code == 200:
        return json.loads(result.content.decode("utf-8"))
    else:
        hub.log.error(f"Failed to query git endpoint creds  status_code {result.status_code}")
        raise HTTPError(url, result.status_code, result.reason, None, None)


def get_client_credentials_token(client_creds):
    with open(client_creds["service-token-config"]["privateKeyFilePath"], 'r') as file:
        private_key = file.read()
    payload = {
        "sub": client_creds["service-token-config"]["azp"],
        "exp": datetime.utcnow() + client_creds["clock-skew-in-mins"] * timedelta(seconds=1000),
        "org_id": client_creds["default-org-id"],
        "context_name": client_creds["default-org-id"],
        "azp": client_creds["service-token-config"]["azp"],
        "authorization_details": [],
        "perms": [],
        "jti": f"{uuid.uuid4()}",
        "iss": client_creds["jwk-set"]["internal-issuers"],
        "iat": datetime.utcnow()
    }
    jwt_instance = jwt.JWT()
    token = jwt_instance.encode(payload, private_key, algorithm='RS256')
    return token


# This is just test function will be removed once code stablizes
def tester():
    os.environ['SCRIPT_DIRECTORY'] = '/home/ncd/vmware/njakhar/bellevue-ci/idem-service-worker/scripts'
    os.environ['SCRIPT_TASK_DIRECTORY'] = '/home/ncd/vmware/njakhar/bellevue-ci/idem-service-worker/script-task'
    os.environ['GIT_TYPE'] = 'gitlab'
    os.environ['GIT_ENDPOINT'] = 'https://gitlab.eng.vmware.com'
    os.environ['GIT_PERSONAL_TOKEN'] = 's6i7SMT7RLDc4DfP1D5x'
    os.environ['LEMANS_KEY'] = 'acc75ldO6msl6vsNIBifYzZutDWQ8uZh'
    os.environ['LEMANS_BASE_URL'] = 'https://data.be.symphony-dev.com'
    os.environ['WORKER_NAME'] = str(uuid.uuid4())

    repo_collection_context = '{ "cmd": "rw-repo-collection-task" }'
    sbom_generation_context = '{ "cmd": "rw-repo-sbom-task", "params": { "-p": "cmbu/guardrails-service", "-b": "master", "-c": "473ce8d71bda4e5ed18779f5522cde222ffeb216" } }'
    sbom_generation_context_latest = '{ "cmd": "rw-repo-sbom-task", "params": { "-p": "cmbu/guardrails-service", "-b": "master" } }'

    execute_script_task_helper(type("", (dict,), {
            'log': type("", (dict,), {'info': print, 'debug': print, 'error': print}),
            'idemd': type("", (dict,), {'worker': type("", (dict,), {'workerType': 1})})
        }), type(
            "",
            (dict,), {
                'expiresInSeconds': 10000,
                'executionContext': sbom_generation_context_latest,
                'requester': 'ASSESSMENT',
                'taskIdentity': type(
                    "",
                    (dict,), {
                        'taskId': str(uuid.uuid4()),
                        'identity': type(
             "",
                            (dict,), {
                                'orgId': str(uuid.uuid4()),
                                'projectId': 'default',
                                'projectType': 'dummy',
                            },
                        ),
                    },
                ),
            }
        ),
    )

def tester_environment():
    os.environ['SCRIPT_DIRECTORY'] = '/Users/javarun/ws/gitlab/idem-service-worker/scripts'
    os.environ['SCRIPT_TASK_DIRECTORY'] = '/Users/javarun/ws/gitlab/idem-service-worker/script-task'
    os.environ["IDEMD_HOME"] = '/Users/javarun/ws/gitlab/idem-service-worker'
    os.environ['GIT_TYPE'] = 'gitlab'
    os.environ['GIT_ENDPOINT'] = 'https://gitlab.eng.vmware.com'
    os.environ['GIT_PERSONAL_TOKEN'] = 's6i7SMT7RLDc4DfP1D5x'
    os.environ['LEMANS_KEY'] = 'acc75ldO6msl6vsNIBifYzZutDWQ8uZh'
    os.environ['LEMANS_BASE_URL'] = 'https://data.be.symphony-dev.com'
    os.environ['WORKER_NAME'] = str(uuid.uuid4())

    repo_collection_context = '{ "cmd": "rw-repo-collection-task" }'
    sbom_generation_context = '{ "cmd": "rw-repo-sbom-task", "params": { "-p": "cmbu/guardrails-service", "-b": "master", "-c": "473ce8d71bda4e5ed18779f5522cde222ffeb216" } }'
    sbom_generation_context_latest = '{"endpointId":"12463cd6-ade8-4c28-b646-f4e07c5e72ee", "cmd": "rw-repo-sbom-task", "params": { "-p": "cmbu/guardrails-service", "-b": "master" } }'

    execute_script_task_helper(type("", (dict,), {
        'log': type("", (dict,), {'info': print, 'debug': print, 'error': print}),
        'idemd': type("", (dict,), {'worker': type("", (dict,), {'workerType': 0})})
        # 'idemd': type("", (dict,), {})
    }), type(
        "",
        (dict,), {
            'expiresInSeconds': 10000,
            'executionContext': sbom_generation_context_latest,
            'requester': 'ASSESSMENT',
            'taskIdentity': type(
                "",
                (dict,), {
                    'taskId': str(uuid.uuid4()),
                    'identity': type(
                        "",
                        (dict,), {
                            'orgId': 'ee04bfae-a665-4f20-a5b9-d8b043180252',
                            'projectId': 'default',
                            'projectType': 'dummy',
                        },
                    ),
                },
            ),
        }
    ),
)

#tester_environment()

#tester()
