import os

from idemd.idemd.utils.worker_threadpool_executor import WorkerThreadPoolExecutor


def policy(hub):
    return hub.loop[hub.loop.auto.detect()].policy()


def executor(hub):
    max_workers = hub.OPT.idemd.max_workers
    try:
        max_workers = int(os.getenv("MAX_WORKERS", hub.OPT.idemd.max_workers))
    except Exception as e:
        hub.log.error(
            f"Exception occurred when retrieving configured max workers. Error - {e}"
        )

    hub.log.info(f"No of workers configured is {max_workers}")

    return WorkerThreadPoolExecutor(
        max_workers=max_workers,
        thread_name_prefix="worker_thread",
        initializer=WorkerThreadPoolExecutor.initializer,
        initargs=(),
    )


def get(hub):
    return hub.loop[hub.loop.auto.detect()].get()
