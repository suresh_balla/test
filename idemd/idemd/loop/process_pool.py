import concurrent.futures


def policy(hub):
    return hub.loop[hub.loop.auto.detect()].policy()


def executor(hub):
    return concurrent.futures.ProcessPoolExecutor(
        max_workers=hub.OPT.idemd.max_workers,
        initializer=hub.loop.process_pool.initializer,
        initargs=(),
    )


def get(hub):
    return hub.loop[hub.loop.auto.detect()].get()


def initializer(hub):
    """
    This function is called when a new Pool is created
    """
    hub.log.debug("Created a new process pool")
