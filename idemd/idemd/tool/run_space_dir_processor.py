import heapq
import logging
import os
import time
from pathlib import Path

from idemgrpc import messages_pb2 as messages_pb2

from idemd.conf import CONFIG
from idemd.idemd.constants.constants import (
    HOME,
    IDEMD,
    GLOBAL_TASK_TIMEOUT,
    ENABLE_GRPC_AUTH_PROP,
)
from idemd.idemd.grpc_clients import idem_service_client_from_url
from idemd.idemd.run_space import split_run_id


def process_run_space_dir():
    # Load configuration data
    conf = dict(list(CONFIG.items()))
    global_task_timeout = float(os.getenv("GLOBAL_TASK_TIMEOUT", GLOBAL_TASK_TIMEOUT))

    # We will use max_workers property to find out the max number of tasks that can be active at a time
    if "MAX_WORKERS" in os.environ:
        max_workers = os.getenv("MAX_WORKERS")
    else:
        max_workers = get_default_from_conf(conf, "max_workers")

    run_space_parent_dir = os.path.join(os.getenv(HOME), IDEMD)

    dir_list = get_recently_modified_dirs(run_space_parent_dir, max_workers)

    pending_tasks = []
    current_time = time.time()

    for directory in dir_list:
        abs_path = os.path.join(run_space_parent_dir, directory)
        if os.path.isdir(abs_path):
            abs_path_dir = Path(abs_path)
            try:
                last_modified_time = max(
                    [
                        abs_path_dir.joinpath(root).joinpath(file).stat().st_mtime
                        for root, _, files in os.walk(abs_path_dir)
                        for file in files
                    ]
                )
            except ValueError as exc:
                logging.error(
                    f"Error occurred when finding the last modified time for {abs_path_dir} - {exc}"
                )
                last_modified_time = 0
            # Any task should have been timed out after global_task_timeout.
            # Any run space with modified time older that global_task_timeout is not considered for update
            time_since_last_modification = current_time - last_modified_time
            if time_since_last_modification <= global_task_timeout:
                run_space_details = split_run_id(directory)
                if bool(run_space_details):
                    pending_tasks.append(run_space_details)
            else:
                logging.info(
                    f" Directory {directory} is older than expected. Do not process it"
                )
    logging.info(pending_tasks)

    if "IDEM_SERVICE_HOST" in os.environ:
        idem_service_host = os.getenv("IDEM_SERVICE_HOST")
    else:
        idem_service_host = get_default_from_conf(conf, "idem_service_host")

    try:
        idem_service_port = int(os.getenv("IDEM_SERVICE_PORT"))
    except ValueError:
        idem_service_port = int(get_default_from_conf(conf, "idem_service_port"))
    except TypeError:
        idem_service_port = int(get_default_from_conf(conf, "idem_service_port"))

    idem_service_url = f"{idem_service_host}:{idem_service_port}"

    logging.info(f"Idem service Url {idem_service_url}")

    grpc_auth_reset = False
    if (
        ENABLE_GRPC_AUTH_PROP not in os.environ
        or os.environ.get(ENABLE_GRPC_AUTH_PROP) == "false"
    ):
        os.environ[ENABLE_GRPC_AUTH_PROP] = "true"
        grpc_auth_reset = True

    stub = idem_service_client_from_url(idem_service_url)

    for task in pending_tasks:
        task_update_request = messages_pb2.TaskUpdateRequest()
        task_update_request.status = "FAILED"
        task_update_request.taskIdentity.taskId = task["task_id"]
        task_update_request.taskIdentity.identity.orgId = task["org_id"]
        task_update_request.taskIdentity.identity.projectId = task["project_id"]
        task_update_request.errorMessage = (
            "Forcing the task to fail as the worker is going be terminated"
        )

        response = stub.taskUpdate(task_update_request)
        if response.isUpdated:
            logging.info(
                f"Updated task with ID {task_update_request.taskIdentity.taskId} "
                f"with status {task_update_request.status}"
            )
        else:
            logging.error(
                f"Could not update task with ID {task_update_request.taskIdentity.taskId} "
                f"with status {task_update_request.status}"
            )

    if grpc_auth_reset:
        os.environ.pop(ENABLE_GRPC_AUTH_PROP)


def get_default_from_conf(conf, key):
    result = None
    try:
        result = conf[key]["default"]
    except KeyError:
        logging.error(f"Key {key} is not found in configuration")

    return result


# Function to get the last modified time of a file or directory
def get_last_modified_time(path):
    return os.path.getmtime(path)


# Function to get the top #max_workers directories with the most recent modifications
def get_recently_modified_dirs(root_dir, max_workers):
    recent_dirs = []
    for dirname in os.listdir(root_dir):
        dir_full_path = os.path.join(root_dir, dirname)
        if os.path.isdir(dir_full_path):
            # Get the last modified time of the directory
            last_modified_time = get_last_modified_time(dir_full_path)
            # Add the directory and its last modified time to the heap
            heapq.heappush(recent_dirs, (last_modified_time, dir_full_path))
    # Get the top 10 directories with the most recent modifications
    return [dir_path for _, dir_path in heapq.nlargest(max_workers, recent_dirs)]
