"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json


async def publish_error_callback_handler(hub, error_data: dict):
    """
    A default idem callback handler for publish event failure which pushes an error event back to the queue

    .. code-block:: json

        {
            "tags": {
                "plugin": "<plugin-name>",
                <original-event-tags>
            },
            "publish_event_error_message": "<error-message-from-publish-failure>",
            "run_name": "<run-name>"
        }

    """
    if error_data:
        original_body = json.loads(error_data["body"].decode())
        new_tags = {
            "ref": "idem.event.publish_error_callback_handler",
            "plugin": error_data.get("plugin", None),
        }
        if original_body:
            if original_body.get("publish_event_error_message"):
                # If this event failed to publish error event, return it.
                # The error would be logged already with evbus
                return
            if error_data["profile"] == "idem-run":  # idem run has a known
                # messagelargesize exception hence handling it by blanking the actual message
                original_message = original_body.get("message", {})
                # Override original message which is causing largesizeerror by replacing with blank
                message_override = {
                    "changes": "",
                    "new_state": "",
                    "result": "false",
                    "comment": str(error_data["exception"]),
                }
                original_message = original_message | message_override
                original_tags = original_body.get("tags", {})
                new_tags = original_tags | new_tags
                new_body = {"tags": new_tags, "message": original_message}
                body = (
                    original_body
                    | new_body
                    | {"publish_event_error_message": str(error_data["exception"])}
                )
            else:
                run_name = original_body.get("run_name", None)
                body = dict(
                    tags=new_tags,
                    run_name=run_name,
                    publish_event_error_message=str(error_data["exception"]),
                )
            await hub.evbus.broker.put(
                profile=error_data["profile"],
                body=body,
            )
