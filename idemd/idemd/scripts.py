"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import os

import pop.hub

from idemd.idemd.init import validate
from idemd.idemd.proc import get_terminate_file_path


def start_with_hub():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idemd")
    hub.pop.config.load(["idemd", "acct", "idem", "evbus"], cli="idemd")
    terminate_file_path_local = get_terminate_file_path()
    if os.path.isfile(terminate_file_path_local):
        os.remove(terminate_file_path_local)
    if (
        os.getenv("IDEM_DOC_MODE") is not None
        and os.getenv("IDEM_DOC_MODE").lower() == "true"
    ):
        hub["idemd"].init.doc()
    else:
        hub["idemd"].init.cli()


def start():
    if (
        os.getenv("VALIDATE_MODE") is not None
        and os.getenv("VALIDATE_MODE").lower() == "true"
    ):
        validate()
    else:
        raise ValueError("Invalid configuration")
