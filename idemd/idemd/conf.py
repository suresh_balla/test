CONFIG = {
    "config": {
        "default": None,
        "help": "Load extra options from a configuration file onto hub.OPT.idemd",
    },
    "task_manager_host": {
        "default": "127.0.0.1",
        "help": "host address for task manager",
    },
    "task_manager_port": {
        "default": "4025",
        "help": "host port for task manager",
    },
    "idem_service_host": {
        "default": "127.0.0.1",
        "help": "host address for idem service",
    },
    "idem_service_port": {
        "default": "4030",
        "help": "host port for idem service",
    },
    "org_id": {
        "default": "",
        "help": "org_id when remote",
    },
    "project_id": {
        "default": "",
        "help": "project_id when remote",
    },
    "enforced_state_id": {
        "default": "",
        "help": "existing enforced_state_id if applicable",
    },
    "worker_id": {
        "default": "",
        "help": "id to use for the worker",
    },
    "worker_group_id": {
        "default": "",
        "help": "id of worker group assigned",
    },
    "mode": {
        "default": "DIRECT",
        "help": "operating mode of worker",
    },
    "max_workers": {
        "default": 10,
        "help": "max number of worker thread/processes to spin off",
    },
    "system_acct_key": {
        "default": None,
        "help": "key used to encrypt the ingress queue credentials",
    },
    "system_acct_file": {
        "default": None,
        "help": "encrypted ingress queue credentials file path",
    },
}

# Include keys from the CONFIG dictionary that you want to expose on the cli
# The values for these keys are a dictionaries that will be passed as kwargs to argparse.ArgumentParser.add_option
CLI_CONFIG = {
    "config": {"options": ["-c"]},
    "task_manager_host": {},
    "task_manager_port": {},
    "idem_service_host": {},
    "idem_service_port": {},
    "system_acct_key": {},
    "system_acct_file": {},
    "mode": {},
    "max_workers": {},
    "worker_id": {},
    "worker_group_id": {},
    "org_id": {},
    "serialize_plugin": {
        "source": "evbus",
    },
}

# These are the namespaces that your project extends
# The hub will extend these keys with the modules listed in the values
DYNE = {"idemd": ["idemd"], "loop": ["loop"], "tool": ["tool"]}
