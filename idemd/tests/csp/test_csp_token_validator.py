import unittest
from datetime import datetime
from unittest.mock import MagicMock, patch

import jwt

from idemd.idemd.csp.csp_token_validator import CSPTokenValidator


class CSPTokenValidatorTests(unittest.TestCase):
    def setUp(self):
        self.validator = CSPTokenValidator()
        self.validator.current_time = int(datetime.now().timestamp())

    @patch("idemd.idemd.csp.csp_token_validator.CSPTokenValidator.get_open_id_config")
    def test_decode_token_valid(self, get_open_id_config_mock):
        token = "valid_token"
        expected_decoded = {"sub": "user123", "exp": datetime.now().timestamp() + 3600}

        # Mock the get_signing_key_from_jwt method
        signing_key_mock = MagicMock()
        signing_key_mock.key = "key"
        jwks_client_mock = MagicMock()
        jwks_client_mock.return_value.get_signing_key_from_jwt.return_value = (
            signing_key_mock
        )

        # Mock the jwt.decode method
        jwt_decode_mock = MagicMock(return_value=expected_decoded)

        # Set the desired response for get_open_id_config
        get_open_id_config_mock.return_value = {
            "jwks_uri": "mock_jwks_uri",
            "issuer": "mock_issuer",
        }

        # Patch the methods with the mocks
        with patch("jwt.PyJWKClient", jwks_client_mock):
            with patch("jwt.decode", jwt_decode_mock):
                decoded = self.validator.decode_token(token)

        # Assertions
        self.assertEqual(decoded, expected_decoded)
        jwks_client_mock.assert_called_once_with("mock_jwks_uri")
        jwt_decode_mock.assert_called_once_with(
            token, algorithms=["RS256"], key=signing_key_mock.key
        )
        jwks_client_mock.return_value.get_signing_key_from_jwt.assert_called_once_with(
            token
        )

    @patch("idemd.idemd.csp.csp_token_validator.CSPTokenValidator.get_open_id_config")
    def test_decode_token_invalid(self, get_open_id_config_mock):
        token = "invalid_token"

        # Mock the get_signing_key_from_jwt method
        jwks_client_mock = MagicMock()
        jwks_client_mock.return_value.get_signing_key_from_jwt.side_effect = (
            jwt.InvalidSignatureError
        )

        # Mock the jwt.decode method
        jwt_decode_mock = MagicMock(return_value=None)

        # Set the desired response for get_open_id_config
        get_open_id_config_mock.return_value = {
            "jwks_uri": "mock_jwks_uri",
            "issuer": "mock_issuer",
        }

        # Patch the methods with the mocks
        with patch("jwt.PyJWKClient", jwks_client_mock):
            with patch("jwt.decode", jwt_decode_mock):
                decoded = self.validator.decode_token(token)

        # Assertions
        self.assertEqual(decoded, None)
        jwks_client_mock.assert_called_once_with("mock_jwks_uri")
        jwks_client_mock.return_value.get_signing_key_from_jwt.assert_called_once_with(
            token
        )

    def test_validate_token_expired(self):
        var = {
            "exp": datetime.now().timestamp() - 3600,
            "iat": datetime.now().timestamp() - 4800,
        }

        response = self.validator.validate_token(var)

        self.assertFalse(response.is_successful)
        self.assertIsNone(response.decoded_token)

    def test_validate_user_token(self):
        var = {
            "iss": "https://gaz-preview.csp-vidm-prod.com",
            "exp": int(datetime.now().timestamp()) + 3600,
            "iat": int(datetime.now().timestamp()) - 4800,
            "jti": "309e7af0-0dc0-48fd-b69f-24e5e4e5cf80",
            "acct": "test@vmware.com",
            "username": "test",
        }

        response = self.validator.validate_token(var)

        self.assertTrue(response.is_successful)
        self.assertIsNotNone(response.decoded_token)

        self.validator.decoded = var
        self.assertTrue(self.validator.is_user_token())

    def test_validate_client_token(self):
        var = {
            "iss": "https://gaz-preview.csp-vidm-prod.com",
            "exp": int(datetime.now().timestamp()) + 3600,
            "iat": int(datetime.now().timestamp()) - 4800,
        }

        response = self.validator.validate_token(var)

        self.assertTrue(response.is_successful)
        self.assertIsNotNone(response.decoded_token)

        self.validator.decoded = var
        self.assertTrue(self.validator.is_client_credential())

    def test_validate_future_issued_at(self):
        var = {
            "iss": "https://gaz-preview.csp-vidm-prod.com",
            "exp": datetime.now().timestamp() + 3600,
            "iat": datetime.now().timestamp() + 4800,
        }

        self.validator.decoded = var
        self.assertFalse(self.validator.validate_iat())

    def test_validate_null_issued_at(self):
        var = {
            "iss": "https://gaz-preview.csp-vidm-prod.com",
            "exp": datetime.now().timestamp() + 3600,
        }

        self.validator.decoded = var
        self.assertFalse(self.validator.validate_iat())


if __name__ == "__main__":
    unittest.main()
