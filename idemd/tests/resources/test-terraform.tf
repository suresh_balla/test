resource "local_file" "test_file_1" {
	filename = "test1.txt"
	content = "Testing terraform with file1"
}

resource "local_file" "test_file_2" {
    filename = "test2.txt"
    content = "Testing terraform with file2"
}