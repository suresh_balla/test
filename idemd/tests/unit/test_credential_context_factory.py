"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json

from idemgrpc import messages_pb2 as messages

from idemd.idemd.terraform.credential_providers.credential_context_factory import (
    CredentialContextFactory,
)
from idemd.idemd.terraform.credential_providers.guardrails_environments_provider import (
    GuardrailsEnvironmentsCredentialContext,
)
from idemd.idemd.terraform.credential_providers.no_op_credential_context import (
    NoOpCredentialContext,
)
from idemd.idemd.terraform.credential_providers.vra_credentials_provider import (
    VRACredentialContext,
)


def test_get_context_guardrails():
    task = messages.TaskSummary()

    creds = {
        "guardrails": {
            "meta": [
                {
                    "environmentId": "63da663f-0df9-4fe5-b832-4486ea2c0ff0",
                    "profile": "default",
                    "provider": "AZURE",
                }
            ]
        }
    }

    task.credentialParameters = json.dumps(creds)

    task_identity = messages.TaskIdentity()
    task_identity.enforcedStateId = "test"
    task_identity.taskId = "test"

    identity = messages.Identity()
    identity.orgId = "orgId"
    identity.projectId = "projectId"

    task.taskIdentity.CopyFrom(task_identity)

    backends = {}

    context = CredentialContextFactory.get_context(task)
    assert GuardrailsEnvironmentsCredentialContext.__name__ in str(type(context))


def test_get_context_no_creds():
    task = messages.TaskSummary()

    creds = {
        "skipCheck": "true",
    }

    task.credentialParameters = json.dumps(creds)

    task_identity = messages.TaskIdentity()
    task_identity.enforcedStateId = "test"
    task_identity.taskId = "test"

    identity = messages.Identity()
    identity.orgId = "orgId"
    identity.projectId = "projectId"

    task.taskIdentity.CopyFrom(task_identity)

    context = CredentialContextFactory.get_context(task)
    assert NoOpCredentialContext.__name__ in str(type(context))


def test_get_context_vra():
    task = messages.TaskSummary()

    creds = {"cloud_acct_id": "test"}

    task.credentialParameters = json.dumps(creds)

    task_identity = messages.TaskIdentity()
    task_identity.enforcedStateId = "test"
    task_identity.taskId = "test"

    identity = messages.Identity()
    identity.orgId = "orgId"
    identity.projectId = "projectId"

    task.taskIdentity.CopyFrom(task_identity)

    backends = {}

    context = CredentialContextFactory.get_context(task)
    assert VRACredentialContext.__name__ in str(type(context))
