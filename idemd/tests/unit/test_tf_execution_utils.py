"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json
import shutil
import unittest
import uuid
from contextlib import contextmanager
from pathlib import Path

import grpc
from idemgrpc import idemService_pb2_grpc
from idemgrpc import messages_pb2 as messages

from idemd.idemd.run_space import remove_space_dir
from idemd.idemd.terraform.tf_execution_utils import execute_terraform_task_as_process
from idemd.idemd.terraform.tf_execution_utils import (
    get_state_file,
    get_variable_files,
    get_params,
    get_command_for_task,
)
from idemd.idemd.utils.worker_threadpool_executor import WorkerThreadPoolExecutor


@contextmanager
def start_idem_service_server_get_stub(cls):
    server = grpc.server(WorkerThreadPoolExecutor(max_workers=10))
    idemService_pb2_grpc.add_IdemServiceServicer_to_server(cls(), server)
    port = server.add_insecure_port("localhost:4025")
    server.start()

    try:
        with grpc.insecure_channel("localhost:%d" % port) as channel:
            yield idemService_pb2_grpc.IdemServiceStub(channel)
    finally:
        server.stop(None)


class IdemServiceGrpc(idemService_pb2_grpc.IdemServiceServicer):
    def taskUpdate(self, request, context):
        res = messages.TaskUpdateResponse()
        res.isUpdated = True
        res.message = "Update task successful"
        return res


@unittest.skip("Terraform execution")
async def test_execute_terraform_task_as_process(hub, mock_hub):
    source_file = Path("tests/resources/test-terraform.tf")
    space_dir = f"tests/test-run-{str(uuid.uuid4())}"
    Path(space_dir).mkdir(parents=True, exist_ok=True)
    destination_file = Path(f"{space_dir}/test-terraform.tf")

    # Copying the tf file into the running directory
    shutil.copy2(source_file, destination_file)

    task = messages.TaskSummary()
    task_identity = messages.TaskIdentity()
    task_identity.taskId = "1234"
    task.taskIdentity.CopyFrom(task_identity)
    worker = messages.WorkerIdentity()
    mock_hub.idemd.worker = worker
    with start_idem_service_server_get_stub(IdemServiceGrpc) as stub:
        mock_hub.idemd.idem_stub = stub
        res = await execute_terraform_task_as_process(
            mock_hub, task, space_dir, None, None
        )
    assert res
    file_path1 = Path(f"{space_dir}/test1.txt")
    file_path2 = Path(f"{space_dir}/test2.txt")

    with open(file_path1, "r") as file1, open(file_path2, "r") as file2:
        content1 = file1.read()
        content2 = file2.read()

    assert content1 == "Testing terraform with file1"
    assert content2 == "Testing terraform with file2"

    remove_space_dir(hub, Path(space_dir))


def test_get_state_file(hub):
    terraform_command = [
        "terraform",
        "apply",
    ]
    task = messages.TaskSummary()
    task_identity = messages.TaskIdentity()
    task_identity.taskId = "test"
    task_identity.iacProvider = "TERRAFORM"

    sls_params = {"location": "eastus", "name": "test-rg"}
    task.slsParameters = json.dumps(sls_params)

    meta = {"varFiles": ["test1.tfvars", "test2.tfvars"], "stateFile": "state.tfstate"}
    task.meta = json.dumps(meta)
    task.taskIdentity.CopyFrom(task_identity)
    space_dir = "/path/to/space_dir"
    new_terraform_command = get_state_file(
        terraform_command, task, json.dumps(meta), space_dir
    )

    expected_command = [
        "terraform",
        "apply",
        "-state=state.tfstate",
    ]

    assert len(new_terraform_command) == 3
    assert new_terraform_command == expected_command, "Command content does not match"


def test_get_var_file(hub):
    terraform_command = [
        "terraform",
        "apply",
    ]
    task = messages.TaskSummary()
    task_identity = messages.TaskIdentity()
    task_identity.taskId = "test"
    task_identity.iacProvider = "TERRAFORM"

    sls_params = {"location": "eastus", "name": "test-rg"}
    task.slsParameters = json.dumps(sls_params)

    meta = {"varFiles": ["test1.tfvars", "test2.tfvars"], "stateFile": "state.tfstate"}
    task.meta = json.dumps(meta)
    task.taskIdentity.CopyFrom(task_identity)

    new_terraform_command = get_variable_files(
        terraform_command, json.dumps(meta), task.taskIdentity.taskId
    )

    expected_command = [
        "terraform",
        "apply",
        "-var-file=test1.tfvars",
        "-var-file=test2.tfvars",
    ]

    assert len(new_terraform_command) == 4
    assert new_terraform_command == expected_command, "Command content does not match"


def test_get_param(hub):
    terraform_command = [
        "terraform",
        "apply",
    ]
    task = messages.TaskSummary()
    task_identity = messages.TaskIdentity()
    task_identity.taskId = "test"
    task_identity.iacProvider = "TERRAFORM"

    sls_params = {"location": "eastus", "name": "test-rg"}
    task.slsParameters = json.dumps(sls_params)

    meta = {"varFiles": ["test1.tfvars", "test2.tfvars"], "stateFile": "state.tfstate"}
    task.meta = json.dumps(meta)

    task.taskIdentity.CopyFrom(task_identity)

    new_terraform_command = get_params(
        terraform_command, json.dumps(sls_params), task.taskIdentity.taskId
    )

    expected_command = [
        "terraform",
        "apply",
        "-var=location=eastus",
        "-var=name=test-rg",
    ]

    assert len(new_terraform_command) == 4
    assert new_terraform_command == expected_command, "Command content does not match"


def test_get_command_for_task(hub):
    terraform_command = [
        "terraform",
        "apply",
    ]
    task = messages.TaskSummary()
    task_identity = messages.TaskIdentity()
    task_identity.taskId = "test"
    task_identity.iacProvider = "TERRAFORM"

    sls_params = {"location": "eastus", "name": "test-rg"}
    task.slsParameters = json.dumps(sls_params)

    meta = {"varFiles": ["test1.tfvars", "test2.tfvars"], "stateFile": "state.tfstate"}
    task.meta = json.dumps(meta)

    task.taskIdentity.CopyFrom(task_identity)
    space_dir = "/path/to/space_dir"

    new_terraform_command = get_command_for_task(
        terraform_command,
        task,
        json.dumps(meta),
        json.dumps(sls_params),
        space_dir,
    )

    expected_command = [
        "terraform",
        "apply",
        "-state=state.tfstate",
        "-var=location=eastus",
        "-var=name=test-rg",
        "-var-file=test1.tfvars",
        "-var-file=test2.tfvars",
    ]

    assert len(new_terraform_command) == 7
    assert new_terraform_command == expected_command, "Command content does not match"
