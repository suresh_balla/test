import google
from idemgrpc import messages_pb2 as messages_pb2

from idemd.idemd.terraform.entity_id.entity_id_generator import get_entity_id


def test_get_entity_id_generator_with_aws_resource(hub):
    aws_resource = {
        "mode": "managed",
        "type": "aws_ebs_volume",
        "name": "example_volume",
        "provider": 'provider["registry.terraform.io/hashicorp/aws"]',
        "instances": [
            {
                "schema_version": 0,
                "attributes": {
                    "arn": "arn:aws:ec2:us-east-1::volume/vol-11b12664",
                    "availability_zone": "",
                    "encrypted": False,
                    "final_snapshot": False,
                    "id": "vol-11b12664",
                    "iops": 0,
                    "kms_key_id": "",
                    "outpost_arn": "",
                    "size": 40,
                    "snapshot_id": "",
                    "tags": {"Name": "HelloWorld"},
                    "tags_all": {"Name": "HelloWorld"},
                    "throughput": 0,
                    "type": "gp2",
                },
                "sensitive_attributes": [],
                "private": "eyJlMmJmYjczMC1lY2FhLTExZTYtOGY4OC0zNDM2M2JjN2M0YzAiOnsiY3JlYXRlIjozMDAwMDAwMDAwMDAsImRlbGV0ZSI6MzAwMDAwMDAwMDAwLCJ1cGRhdGUiOjMwMDAwMDAwMDAwMH19",
            }
        ],
    }
    task = messages_pb2.TaskSummary()
    task_identity = messages_pb2.TaskIdentity()
    task.taskIdentity.CopyFrom(task_identity)
    task.taskIdentity.taskId = "b66ef857-ce62-4d31-8a74-6b890315711f"
    task.taskIdentity.iacProvider = "TERRAFORM"
    task.credentialParameters = '{"guardrails":{"meta":[{"profile":"default","provider":"AWS","overrides":{"region":"us-east-1"},"environmentId":"555555555555"}]},"cloud_acct_org_id":"66666666-ffff-4444-bbbb-000000000000"}'
    task.taskMode = "ENFORCEMENT"
    expected_entity_id = google.protobuf.struct_pb2.Struct()
    expected_entity_id["provider"] = "AWS"
    expected_entity_id["service"] = "EC2"
    expected_entity_id["instance"] = "555555555555"
    expected_entity_id["origin"] = "us-east-1"
    expected_entity_id["type"] = "Volume"
    expected_entity_id["id"] = "vol-11b12664"
    entity_id = get_entity_id(hub, aws_resource, task)
    assert entity_id is not None
    assert entity_id == expected_entity_id


def test_get_entity_id_generator_with_azure_resource(hub):
    azure_resource = {
        "mode": "managed",
        "type": "azurerm_resource_group",
        "name": "example",
        "provider": 'provider["registry.terraform.io/hashicorp/azurerm"]',
        "instances": [
            {
                "schema_version": 0,
                "attributes": {
                    "id": "/subscriptions/33333333-8888-4444-bbbb-aaaaaaaaaaaa/resourceGroups/tf-rg-test",
                    "location": "westeurope",
                    "name": "tf-rg-test",
                    "tags": {},
                },
                "sensitive_attributes": [],
                "private": "eyJlMmJmYjczMC1lY2FhLTExZTYtOGY4OC0zNDM2M2JjN2M0YzAiOnsiY3JlYXRlIjo1NDAwMDAwMDAwMDAwLCJkZWxldGUiOjU0MDAwMDAwMDAwMDAsInJlYWQiOjMwMDAwMDAwMDAwMCwidXBkYXRlIjo1NDAwMDAwMDAwMDAwfX0=",
            }
        ],
    }

    task = messages_pb2.TaskSummary()
    task.taskIdentity.taskId = "b66ef857-ce62-4d31-8a74-6b890315711f"
    task.taskIdentity.iacProvider = "TERRAFORM"
    task.credentialParameters = '{"guardrails":{"meta":[{"profile":"default","provider":"AZURE","overrides":{"region":"East US"},"environmentId":"33333333-8888-4444-bbbb-aaaaaaaaaaaa"}]},"cloud_acct_org_id":"66666666-ffff-4444-bbbb-999999999999"}'
    task.taskMode = "ENFORCEMENT"
    expected_entity_id = google.protobuf.struct_pb2.Struct()
    expected_entity_id["provider"] = "Azure"
    expected_entity_id["service"] = "ResourceManager"
    expected_entity_id["instance"] = "33333333-8888-4444-bbbb-aaaaaaaaaaaa"
    expected_entity_id["type"] = "ResourceGroup"
    expected_entity_id["id"] = "tf-rg-test"
    entity_id = get_entity_id(hub, azure_resource, task)
    assert entity_id is not None
    assert entity_id == expected_entity_id


def test_get_entity_id_generator_returns_none(hub):
    resource = {
        "mode": "data",
        "type": "azurerm_subscription",
        "name": "primary",
        "provider": 'provider["registry.terraform.io/hashicorp/azurerm"]',
        "instances": [
            {
                "schema_version": 0,
                "attributes": {
                    "display_name": "example-subscription",
                    "id": "/subscriptions/33333333-9999-7777-8888-000000000000",
                    "state": "Enabled",
                    "subscription_id": "33333333-9999-7777-8888-000000000000",
                    "tenant_id": "66666666-8888-9999-8888-111111111111",
                },
                "sensitive_attributes": [],
            }
        ],
    }
    task = messages_pb2.TaskSummary()
    task.taskIdentity.taskId = "b66ef857-ce62-4d31-8a74-6b890315711f"
    task.taskIdentity.iacProvider = "TERRAFORM"
    task.taskMode = "ENFORCEMENT"
    entity_id = get_entity_id(hub, resource, task)
    # Expects None because only mode 'managed' is supported
    assert entity_id is None
