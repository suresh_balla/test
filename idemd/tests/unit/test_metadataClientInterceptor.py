"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

from unittest import mock

import idemd.idemd.auth.CSPClientCredentialsInterceptor
from idemd.idemd.auth import (
    CSPClientCredentialsInterceptor as metadata_client_interceptor,
)


def mocked_requests_post(*args, **kwargs):
    class MockResponse:
        def __init__(self):
            self.status_code = 200
            self.content = (
                b'{"access_token": "client_cred_token", "expires_in": "2899"}'
            )

        def json(self):
            return self.json_data

    return MockResponse()


def mocked_json_loads(*args, **kwargs):
    return {"access_token": "client_cred_token", "expires_in": "2899"}


@mock.patch("requests.post", side_effect=mocked_requests_post)
@mock.patch(
    "json.loads",
    return_value={"access_token": "client_cred_token", "expires_in": "28799"},
)
def test_get_client_credentials_token(mock_request, mock_json, mock_hub):
    mock_hub.idemd.client_creds = {
        "url": "https://console-stg.cloud.vmware.com/csp/gateway/am/api/auth/authorize",
        "b64_creds": "cHJvdmlzaW9uaW5nOm5xZVhETpYcNyMzWVpOQlE=",
    }

    token_as_dict = metadata_client_interceptor.get_client_credentials_token(
        mock_hub.idemd.client_creds
    )
    assert token_as_dict
    assert token_as_dict["access_token"] == "client_cred_token"


def test_get_token(mock_hub):
    idemd.idemd.auth.CSPClientCredentialsInterceptor.get_client_credentials_token = (
        mock.Mock(
            name="get credentials token",
            return_value={"access_token": "client_cred_token", "expires_in": "28799"},
        )
    )
    auth_token = metadata_client_interceptor.get_token(mock_hub)
    assert auth_token == "client_cred_token"
