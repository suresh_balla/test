"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import os
from unittest import mock

import pytest

from idemd.idemd import grpc_clients as clients


@pytest.mark.parametrize("enable_grpc_auth", ["true", "false"])
def test_client(mock_hub, hub, enable_grpc_auth):
    """
    test idem service client functionality
    """
    mock_hub.OPT.idemd.idem_service_host = "localhost"
    mock_hub.OPT.idem.idem_service_port = 4030
    mock_hub.idemd.client_creds = mock.MagicMock()
    os.environ.setdefault("ENABLE_GRPC_AUTH", enable_grpc_auth)
    clients.init_idem_service_client(mock_hub)
    assert mock_hub.idemd.idem_stub
    assert mock_hub.idemd.idem_channel


@pytest.mark.parametrize("enable_grpc_auth", ["true", "false"])
def test_task_manager_client(mock_hub, hub, enable_grpc_auth):
    """
    test idem service client functionality
    """
    mock_hub.OPT.idemd.task_manager_host = "localhost"
    mock_hub.OPT.idem.task_manager_port = 4025
    mock_hub.idemd.client_creds = mock.MagicMock()
    os.environ.setdefault("ENABLE_GRPC_AUTH", enable_grpc_auth)
    clients.init_task_manager_client(mock_hub)
    assert mock_hub.idemd.task_manager_channel
    assert mock_hub.idemd.task_manager_stub
