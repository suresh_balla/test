"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import os
from concurrent import futures
from contextlib import contextmanager

import grpc
from idemgrpc import idemService_pb2_grpc
from idemgrpc import messages_pb2 as messages
from idemgrpc import taskManagerService_pb2_grpc

from idemd.idemd import task_manager as task_manager
from idemd.idemd.acct_utils import has_acct_details
from idemd.idemd.idem_execution_utils import execute_idem_task_as_process, execute_task


@contextmanager
def start_task_manager_server_get_stub(cls):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    taskManagerService_pb2_grpc.add_TaskManagerServiceServicer_to_server(cls(), server)
    port = server.add_insecure_port("localhost:4025")
    server.start()

    try:
        with grpc.insecure_channel("localhost:%d" % port) as channel:
            yield taskManagerService_pb2_grpc.TaskManagerServiceStub(channel)
    finally:
        server.stop(None)


class TaskManagerGrpc(taskManagerService_pb2_grpc.TaskManagerServiceServicer):
    def pendingTasks(self, request, context):
        res = messages.TaskSummary()
        return iter([res])

    def claimTask(self, request, context):
        res = messages.ClaimTaskResponse()
        res.claimed = True
        return res


def test_execute_task_as_process(mock_hub, tmpdir):
    task = messages.TaskSummary()
    task.expiresInSeconds = 10800
    task_identity = messages.TaskIdentity()
    task_identity.enforcedStateId = "test"
    task_identity.taskId = "test"

    sls_source = os.path.join(tmpdir, "sls_source.sls")
    acct_key = "kx2Wfdy0N9p_tPdekTN6HCpPf7TchBpM1m1BlP8kxAc="
    acct_file = "gAAAAABidNL6VfKWPWiKp0ncGYztc"
    cache_dir = tmpdir
    run_id = "12::34::56"
    meta = None
    cred_store_args = []
    space_dir = os.path.join(str(tmpdir), "123456")
    sls_params = None
    task_execution_context = {
        "input_type": "FILE",
        "execution_context": {"path": sls_source, "entrypoint": None},
    }

    execute_idem_task_as_process(
        mock_hub,
        task,
        sls_source,
        acct_file,
        acct_key,
        cache_dir,
        run_id,
        cred_store_args,
        space_dir,
        sls_params,
        task_execution_context,
    )


def test_send_task_update_for_failure(mock_hub):
    task = messages.TaskSummary()
    task.expiresInSeconds = 10800
    task_identity = messages.TaskIdentity()
    task_identity.enforcedStateId = "test"
    task_identity.taskId = "test"

    identity = messages.Identity()
    identity.orgId = "orgId"
    identity.projectId = "projectId"

    task.taskIdentity.CopyFrom(task_identity)
    error = "Invalid Input"

    with start_idem_service_server_get_stub(IdemServiceGrpc) as stub:
        mock_hub.idemd.idem_stub = stub
        task_manager.send_task_update_for_failure(mock_hub, task, error)


@contextmanager
def start_idem_service_server_get_stub(cls):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    idemService_pb2_grpc.add_IdemServiceServicer_to_server(cls(), server)
    port = server.add_insecure_port("localhost:4025")
    server.start()

    try:
        with grpc.insecure_channel("localhost:%d" % port) as channel:
            yield idemService_pb2_grpc.IdemServiceStub(channel)
    finally:
        server.stop(None)


class IdemServiceGrpc(idemService_pb2_grpc.IdemServiceServicer):
    def taskUpdate(self, request, context):
        res = messages.TaskUpdateResponse()
        res.isUpdated = True

        # task_identity = messages.TaskIdentity()
        # task_identity.enforcedStateId = "test"
        # task_identity.taskId = "test"
        #
        # res.taskIdentity = task_identity
        res.message = "update Failed"
        return res


async def test_execute_task(mock_hub, tmpdir):
    run_id = "12::34::56"
    acct_key = "kx2Wfdy0N9p_tPdekTN6HCpPf7TchBpM1m1BlP8kxAc="
    acct_file = "gAAAAABidNL6VfKWPWiKp0ncGYztc"
    cache_dir = tmpdir
    space_dir = os.path.join(str(tmpdir), "123456")

    await execute_task(mock_hub, run_id, acct_file, acct_key, space_dir, cache_dir)


def test_has_acct_details(mock_hub):
    acct_params = {
        "guardrails": {
            "meta": [{"environmentId": "537788492150", "provider": "aws"}],
            "project_id": "66d7d09d-7431-4d9d-a53a-ee403c48c4ea",
        }
    }
    assert has_acct_details(acct_params) is True
