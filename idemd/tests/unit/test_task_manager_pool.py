"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import json
import os
from concurrent import futures
from contextlib import contextmanager
from pathlib import Path
from unittest import mock
from unittest.mock import patch

import grpc
from cryptography import fernet as fnet
from idemgrpc import idemService_pb2_grpc
from idemgrpc import messages_pb2 as messages
from idemgrpc import taskManagerService_pb2_grpc

from idemd.idemd import run_space as rs
from idemd.idemd import task_manager as task_manager, idem_execution_utils
from idemd.idemd.grpc_clients import init_task_manager_client
from idemd.idemd.idem_execution_utils import execute_idem_task_helper


def test_wait_on_pending_tasks(mock_hub, hub):
    """
    test generate_run_id method functionality
    """
    mock_hub.OPT.idemd.task_manager_host = "localhost"
    mock_hub.OPT.idem.task_manager_port = "4025"
    mock_hub.idemd.thread_pool = mock.MagicMock()
    worker = messages.WorkerIdentity()
    mock_hub.idemd.worker = worker
    worker.workerType = messages.WorkerIdentity.WorkerType.DIRECT
    mock_hub.memory_node = mock.MagicMock()
    mock_hub.memory_node.controller = mock.MagicMock()
    mock_hub.idemd.client_creds = mock.MagicMock()
    mock_hub.idemd.worker_terminate = False
    mock_hub.idemd.termination_file_path = mock.MagicMock()
    mock_hub.idemd.pid = mock.MagicMock()

    mock_hub.idemd.channel_refresh_interval = 10

    with patch("cgroupspy.interfaces.IntegerFile", autospec=True) as mock1:
        mock_hub.memory_node.controller.usage_in_bytes = mock1
    with patch("cgroupspy.interfaces.IntegerFile", autospec=True) as mock2:
        mock_hub.memory_node.controller.usage_in_bytes = mock2
    mock_hub.memory_node.controller.usage_in_bytes = 1024 * 100 * 100
    mock_hub.memory_node.controller.limit_in_bytes = 2048 * 100 * 100

    # populate client
    mock_hub.idemd.client_creds = mock.MagicMock()
    init_task_manager_client(mock_hub)

    with start_task_manager_server_get_stub(TaskManagerGrpc) as stub:
        mock_hub.idemd.task_manager_stub = stub
        task_manager.wait_on_pending_tasks(mock_hub)


def test_update_worker_terminate(mock_hub):
    mock_hub.idemd.termination_file_path = mock.MagicMock()
    mock_hub.idemd.worker_terminate = False
    with patch("os.path.exists") as exists_mock:
        exists_mock.return_value = False
        task_manager.update_worker_terminate(mock_hub)
        assert mock_hub.idemd.worker_terminate is False, (
            "Expected terminate flag to remain "
            "False when termination file does not exist"
        )

        exists_mock.return_value = True
        task_manager.update_worker_terminate(mock_hub)
        assert mock_hub.idemd.worker_terminate is True, (
            "Expected terminate flag to be set " "to True when termination file exists"
        )


def test_claim_task(mock_hub, hub):
    """
    test generate_run_id method functionality
    """
    task = messages.TaskSummary()
    task_identity = messages.TaskIdentity()
    task_identity.enforcedStateId = "test"
    task_identity.taskId = "test"

    identity = messages.Identity()
    identity.orgId = "orgId"
    identity.projectId = "projectId"

    task.taskIdentity.CopyFrom(task_identity)

    worker = messages.WorkerIdentity()

    worker.workerType = 0
    worker.workerId = "worker"
    worker.identity.CopyFrom(identity)

    # populate client
    mock_hub.idemd.client_creds = mock.MagicMock()
    init_task_manager_client(mock_hub)

    with start_task_manager_server_get_stub(TaskManagerGrpc) as stub:
        mock_hub.idemd.task_manager_stub = stub
        mock_hub.idemd.worker = worker
        ret = task_manager.claim_task(mock_hub)
        assert ret != -1


@contextmanager
def start_task_manager_server_get_stub(cls):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    taskManagerService_pb2_grpc.add_TaskManagerServiceServicer_to_server(cls(), server)
    port = server.add_insecure_port("localhost:4025")
    server.start()

    try:
        with grpc.insecure_channel("localhost:%d" % port) as channel:
            yield taskManagerService_pb2_grpc.TaskManagerServiceStub(channel)
    finally:
        server.stop(None)


class TaskManagerGrpc(taskManagerService_pb2_grpc.TaskManagerServiceServicer):
    def pendingTasks(self, request, context):
        res = messages.TaskSummary()
        return iter([res])

    def claimTask(self, request, context):
        res = messages.ClaimTaskResponse()
        res.claimed = True
        return res


def test_execute_task_helper(mock_hub, tmpdir):
    task = messages.TaskSummary()
    task_identity = messages.TaskIdentity()
    task_identity.enforcedStateId = "test"
    task_identity.taskId = "test"

    taskSummary = {
        "cloud_acct_region": "us-east-1",
        "cloud_acct_id": "dfe1f20-016d-4821-b2fd-54cbf555b654",
        "cloud_acct_org_id": "b373cda4-ae0f-4d5a-9eca-f30bd30c9cd",
    }
    task.credentialParameters = json.dumps(taskSummary)

    identity = messages.Identity()
    identity.orgId = "orgId"
    identity.projectId = "projectId"

    task.taskIdentity.CopyFrom(task_identity)

    worker = messages.WorkerIdentity()

    worker.workerType = 0
    worker.workerId = "worker"
    worker.identity.CopyFrom(identity)

    task_execution_context = {
        "input_type": "FILE",
        "execution_context": {
            "path": Path(os.path.join(str(tmpdir), "abc.sls")),
            "entrypoint": None,
        },
    }

    rs.create = mock.Mock(name="create_task")
    mock_hub.idemd.run_space.create = mock.Mock(
        name="create",
        return_value=task_execution_context,
    )
    rs.generate_run_id = mock.Mock(
        name="generate_run_id", return_value="req::12::34::56"
    )
    space_dir = os.path.join(str(tmpdir), "123456")
    rs.get_space_dir = mock.Mock(name="get_space_dir", return_value=space_dir)
    rs.download_bundle = mock.Mock(
        name="download_bundle",
        return_value=(Path(os.path.join(str(tmpdir), "abc.sls")), "application/xml"),
    )
    mock_hub.OPT.idemd.system_acct_file = os.path.join(str(tmpdir), "sys_file.yaml")
    Path(mock_hub.OPT.idemd.system_acct_file).touch()

    mock_hub.idemd.worker = worker
    fnet.encrypt = mock.Mock("encrypt")

    idem_execution_utils.execute_idem_task_as_process = mock.Mock(
        name="execute_task_as_process"
    )
    ret = execute_idem_task_helper(mock_hub, task)
    assert ret != -1


@contextmanager
def start_idem_service_server_get_stub(cls):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    idemService_pb2_grpc.add_IdemServiceServicer_to_server(cls(), server)
    port = server.add_insecure_port("localhost:4025")
    server.start()

    try:
        with grpc.insecure_channel("localhost:%d" % port) as channel:
            yield idemService_pb2_grpc.IdemServiceStub(channel)
    finally:
        server.stop(None)


class IdemServiceGrpc(idemService_pb2_grpc.IdemServiceServicer):
    def taskUpdate(self, request, context):
        res = messages.TaskUpdateResponse()
        res.isUpdated = True

        # task_identity = messages.TaskIdentity()
        # task_identity.enforcedStateId = "test"
        # task_identity.taskId = "test"
        #
        # res.taskIdentity = task_identity
        res.message = "update Failed"
        return res
