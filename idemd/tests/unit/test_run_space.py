"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import os
from concurrent import futures
from contextlib import contextmanager
from threading import Thread
from unittest.mock import patch, mock_open

import grpc
import pytest
from idemgrpc import idemService_pb2_grpc
from idemgrpc import messages_pb2 as messages

from idemd.idemd import run_space as rs


def test_generate_run_id(mock_hub, hub):
    """
    test generate_run_id method functionality
    """
    org_id = "org"
    task_id = "task"
    project_id = "project"
    requester = "requester"

    ret = rs.generate_run_id(mock_hub, requester, org_id, project_id, task_id)
    assert ret

    assert "requester::org::project::task" == ret


def test_get_space_dir(mock_hub, hub):
    """
    test get_space_dir method functionality
    """
    run_id = "requester:org::proj::task"

    ret = rs.get_space_dir(mock_hub, run_id)
    assert ret


@pytest.mark.parametrize(
    "file_type",
    [
        ("application/zip", "sls_source.zip"),
        ("application/json", "old_state.json"),
        ("application/xml", "sls_source.sls"),
    ],
)
def test_resolve_file_name(file_type):
    """
    test resolve_file_name method functionality
    """
    task = messages.TaskSummary()
    task_identity = messages.TaskIdentity()
    task_identity.taskId = "test"
    task_identity.iacProvider = messages.IAC_PROVIDER.IDEM

    identity = messages.Identity()
    identity.orgId = "orgId"
    identity.projectId = "projectId"
    task.taskIdentity.CopyFrom(task_identity)

    worker = messages.WorkerIdentity()

    worker.workerType = 0
    worker.workerId = "worker"
    worker.identity.CopyFrom(identity)

    ret = rs.resolve_file_name(task, file_type[0])
    assert ret == file_type[1]


@contextmanager
def start_idem_server_get_stub(cls):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    idemService_pb2_grpc.add_IdemServiceServicer_to_server(cls(), server)
    port = server.add_insecure_port("localhost:4030")
    server.start()

    try:
        with grpc.insecure_channel("localhost:%d" % port) as channel:
            yield idemService_pb2_grpc.IdemServiceStub(channel)
    finally:
        server.stop(None)


class IdemServiceGrpc(idemService_pb2_grpc.IdemServiceServicer):
    def downloadBundle(self, request, context):
        res = messages.BundleChunk()
        return iter([res])

    def downloadOldState(self, request, context):
        res = messages.BundleChunk()
        return iter([res])


class IdemServiceGrpcWithThreads(idemService_pb2_grpc.IdemServiceServicer):
    def downloadBundle(self, request, context):
        res = messages.BundleChunk()
        if request.taskId is not None and request.taskId == "test0":
            res.contentType = "application/zip"

        return iter([res])


def test_download_bundle(mock_hub, tmpdir):
    with start_idem_server_get_stub(IdemServiceGrpcWithThreads) as stub:
        mock_hub.idemd.idem_stub = stub

        num_threads = 2
        threads = [None] * num_threads

        os.environ["HOME"] = str(tmpdir)

        with patch("builtins.open", mock_open()) as m:
            for i in range(len(threads)):
                task = messages.TaskSummary()
                task_identity = messages.TaskIdentity()
                task_identity.enforcedStateId = "test" + str(i)
                task_identity.taskId = "test" + str(i)

                task.taskIdentity.CopyFrom(task_identity)
                run_id = "requester::org::project::task" + str(i)
                threads[i] = ThreadWithReturnValue(
                    target=rs.download_bundle,
                    args=(
                        mock_hub,
                        task,
                        run_id,
                    ),
                )
                threads[i].start()

            for i in range(len(threads)):
                var = threads[i].join()
                assert "task" + str(i) in str(var[0])
                filetype = ""
                if i == 0:
                    filetype = "application/zip"
                assert var[1] == filetype


class ThreadWithReturnValue(Thread):
    def __init__(
        self, group=None, target=None, name=None, args=(), kwargs={}, Verbose=None
    ):
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None

    def run(self):
        if self._target is not None:
            self._return = self._target(*self._args, **self._kwargs)

    def join(self, *args):
        Thread.join(self, *args)
        return self._return


def test_create(mock_hub, hub):
    """
    test create method functionality
    """

    task = messages.TaskSummary()
    task_identity = messages.TaskIdentity()
    task_identity.enforcedStateId = "test"
    task_identity.taskId = "test"

    identity = messages.Identity()
    identity.orgId = "orgId"
    identity.projectId = "projectId"

    task.taskIdentity.CopyFrom(task_identity)

    with start_idem_server_get_stub(IdemServiceGrpc) as stub:
        mock_hub.idemd.idem_stub = stub
        rs.create(mock_hub, task=task)


def test_read_client_credentials(mock_hub, tmpdir):
    appConfig = """
  security:
    csp:
      url: https://console-stg.cloud.vmware.com
      auth:
        redirect:
          uri: http://localhost
        client: 
          id: <PLACEHOLDER_CLIENT_ID>
          secret: <SECRET>
  hub:
    sm:
      enabled: false"""

    config_folder = os.path.join(str(tmpdir), "idemd/idemd/idemd")
    if not os.path.exists(config_folder):
        os.makedirs(config_folder)
    filepath = os.path.join(config_folder, "application.yaml")
    f = open(filepath, "w")
    print(appConfig)
    f.write(appConfig)
    f.close()

    os.environ["IDEMD_HOME"] = str(tmpdir)
    csp_details = rs.read_client_credentials(mock_hub)
    assert csp_details
    assert csp_details["auth"]["url"]
    assert csp_details["auth"]["b64_creds"]


def test_write_to_file(mock_hub, tmpdir):
    content = b"sls_for_vpc"
    filepath = os.path.join(tmpdir, "sls_source.sls")
    rs.write_to_file(mock_hub, filepath, content)
    file = open(filepath, "rb")
    assert file.read() == content

def test_read_client_credentials_for_sm(mock_hub, tmpdir):
    appConfig = """
security:
  csp:
    url: https://console-stg.cloud.vmware.com
    auth:
      redirect:
        uri: http://localhost
      client: 
        id: <PLACEHOLDER_CLIENT_ID>
        secret: <SECRET>
hub:
  sm:
    enabled: true
    auth:
      default-org-id: test-org_id
      jwk-set:
        internal-issuers:
          - https://internal-issuer/any/path
      service-token-config:
        privateKeyFilePath: "/src/test/resources/sm-auth/private_key.pem"
        publicKeyFilePath: "/src/test/resources/sm-auth/public_key.pem"
        tokenExpireInSec: 3600
        azp: "ensemble-service"
        issuer: https://internal-issuer/any/path
        """

    config_folder = os.path.join(str(tmpdir), "idemd/idemd/idemd")
    if not os.path.exists(config_folder):
        os.makedirs(config_folder)
    filepath = os.path.join(config_folder, "application.yaml")
    f = open(filepath, "w")
    f.write(appConfig)
    f.close()

    os.environ["IDEMD_HOME"] = str(tmpdir)
    sm_details = rs.read_client_credentials(mock_hub)
    assert sm_details
    assert sm_details["auth"]["default-org-id"]
    assert sm_details["auth"]["service-token-config"]
