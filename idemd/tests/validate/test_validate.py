"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import os
import unittest
from unittest.mock import patch, MagicMock

from idemgrpc import messages_pb2

from idemd.idemd.validate.validate_v1 import (
    IacValidationServicer,
    handle_shutdown,
    verify_terraform_config_inspect_on_path,
)


class IacValidationTestCase(unittest.TestCase):
    mock_request = None

    @classmethod
    def setUpClass(cls):
        cls.mock_request = MagicMock()
        cls.mock_request.content.content = b"Some content"
        cls.mock_request.content.fileType = "txt"
        cls.mock_request.content.fileName = "test.txt"
        cls.default_iac_provider = messages_pb2.IAC_PROVIDER.TERRAFORM
        cls.mock_request.meta = '{"meta": {"entrypoint": "test"}}'
        os.environ["IDEMD_HOME"] = "."

    @patch("idemd.idemd.validate.validate_v1.run_config_inspect")
    def test_run_iac_content_validation_success(self, mock_run_config_inspect):
        mock_run_config_inspect.return_value.returncode = 0
        mock_run_config_inspect.return_value.stdout = '{"key": "value"}'

        servicer = IacValidationServicer()
        self.mock_request.iacProvider = self.default_iac_provider
        response = servicer.validateIacContent([self.mock_request], MagicMock())

        self.assertTrue(response.success)
        self.assertEqual(response.data, '{"key": "value"}')
        self.assertEqual(response.error, "")

    @patch("idemd.idemd.validate.validate_v1.run_idem_validate")
    def test_idem_validation_success(self, mock_run_idem_validate):
        mock_run_idem_validate.return_value.returncode = 0
        mock_run_idem_validate.return_value.stdout = '{"key": "value"}'

        servicer = IacValidationServicer()
        self.mock_request.iacProvider = messages_pb2.IAC_PROVIDER.IDEM
        response = servicer.validateIacContent([self.mock_request], MagicMock())

        self.assertTrue(response.success)
        self.assertEqual(response.data, '{"key": "value"}')
        self.assertEqual(response.error, "")

    @patch("idemd.idemd.validate.validate_v1.run_config_inspect")
    def test_run_iac_content_validation_failure(self, mock_run_config_inspect):
        mock_run_config_inspect.return_value.returncode = 1
        mock_run_config_inspect.return_value.stderr = "Error executing command"

        servicer = IacValidationServicer()
        self.mock_request.iacProvider = self.default_iac_provider
        response = servicer.validateIacContent([self.mock_request], MagicMock())

        self.assertFalse(response.success)
        self.assertEqual(response.data, "")

    @patch(
        "idemd.idemd.validate.validate_v1.run_config_inspect",
        side_effect=Exception("Execution error"),
    )
    def test_run_iac_content_validation_exception(self, mock_run_config_inspect):
        servicer = IacValidationServicer()
        self.mock_request.iacProvider = self.default_iac_provider
        response = servicer.validateIacContent([self.mock_request], MagicMock())

        self.assertFalse(response.success)
        self.assertEqual(response.data, "")

    @patch("subprocess.run")
    def test_verify_terraform_config_inspect_on_path_found(self, mock_run):
        mock_run.return_value.returncode = 0
        result = verify_terraform_config_inspect_on_path()

        self.assertTrue(result)

    @patch("subprocess.run", side_effect=Exception("Execution error"))
    def test_verify_terraform_config_inspect_on_path_exception(self, mock_run):
        result = verify_terraform_config_inspect_on_path()

        self.assertFalse(result)

    @patch("subprocess.run")
    def test_verify_terraform_config_inspect_on_path_not_found(self, mock_run):
        mock_run.return_value.returncode = 1
        result = verify_terraform_config_inspect_on_path()

        self.assertFalse(result)

    @patch(
        "idemd.idemd.validate.validate_v1.server",
        side_effect=lambda *args, **kwargs: MagicMock(),
    )
    def test_handle_shutdown(self, mock_server):
        handle_shutdown(2, None)
        mock_server.stop.assert_called_once_with(grace=5)

    @patch(
        "idemd.idemd.validate.validate_v1.server",
        side_effect=lambda *args, **kwargs: MagicMock(),
    )
    def test_handle_shutdown_threadpool_busy(self, mock_server):
        mock_is_threadpool_busy = MagicMock()
        mock_is_threadpool_busy.side_effect = [True, False]
        with patch(
            "idemd.idemd.validate.validate_v1.is_threadpool_busy",
            mock_is_threadpool_busy,
        ):
            handle_shutdown(2, None)
        mock_server.stop.assert_called_once_with(grace=5)

    @patch(
        "idemd.idemd.validate.validate_v1.server",
        side_effect=lambda *args, **kwargs: MagicMock(),
    )
    def test_handle_shutdown_threadpool_idle(self, mock_server):
        mock_is_threadpool_busy = MagicMock()
        mock_is_threadpool_busy.return_value = False
        with patch(
            "idemd.idemd.validate.validate_v1.is_threadpool_busy",
            mock_is_threadpool_busy,
        ):
            handle_shutdown(2, None)
        mock_server.stop.assert_called_once_with(grace=5)


if __name__ == "__main__":
    unittest.main()
