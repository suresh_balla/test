import json
import os
import unittest
import uuid
from unittest.mock import patch, Mock, call

from idemgrpc import messages_pb2 as messages

from idemd.idemd.validate.validate_v2 import (
    start_validation,
    create_git_workspace,
    clean_workspace,
)


@patch("idemd.idemd.validate.validate_v2.logger.info")
@patch("idemd.idemd.validate.validate_v2.get_local_idem_service_stub")
@patch("idemd.idemd.validate.validate_v2.run_idem_validate")
@patch("idemd.idemd.utils.git_utils.clone_repo")
def test_validate_success(
    mock_clone_repo, mock_run_idem_validate, mock_update_validate, logger_info
):
    task = messages.ValidateInput()

    task_identity = messages.TaskIdentity()
    task_identity.taskId = "test".join(str(uuid.uuid4()))
    task.taskIdentity.CopyFrom(task_identity)
    task.iacGitInfo["repositoryURL"] = "git@gitlab.test-repo.git"
    task.iacGitInfo["branch"] = "master"
    task.iacGitInfo["commitId"] = "f780f0cd18eebaf19274a2a2ed1c947085a81773"

    executionContext = {"gitExecutionContext": {"slsSourceEntryPoint": "main.sls"}}
    task.executionContext = json.dumps(executionContext)

    os.environ["IDEMD_HOME"] = os.getcwd()
    os.environ["GIT_PERSONAL_TOKEN"] = "rfS_jML3bus8skDUh5pom"

    mock_clone_repo.return_value = {}
    mock_run_idem_validate.return_value.returncode = 0
    mock_run_idem_validate.return_value.stdout = '{"key": "value"}'
    mock_update_validate.return_value = mock_stub = Mock()
    mock_stub.updateValidateTask.return_value.isUpdated = True

    start_validation(task)

    mock_clone_repo.assert_called_once()
    mock_run_idem_validate.assert_called_once()
    mock_stub.updateValidateTask.assert_called_once()
    expected_log_message = f"Task: {task.taskIdentity.taskId}, Validate status: COMPLETED with return code: 0."
    expected_calls = [call(expected_log_message)]
    logger_info.assert_has_calls(expected_calls, any_order=True)


@patch("idemd.idemd.validate.validate_v2.logger.error")
@patch("idemd.idemd.validate.validate_v2.get_local_idem_service_stub")
@patch("idemd.idemd.validate.validate_v2.run_idem_validate")
@patch("idemd.idemd.utils.git_utils.clone_repo")
def test_validate_update_failure(
    mock_clone_repo, mock_run_idem_validate, mock_update_validate, logger_error
):
    task = messages.ValidateInput()

    task_identity = messages.TaskIdentity()
    task_identity.taskId = "test".join(str(uuid.uuid4()))
    task.taskIdentity.CopyFrom(task_identity)
    task.iacGitInfo["repositoryURL"] = "git@gitlab.test-repo.git"
    task.iacGitInfo["branch"] = "master"
    task.iacGitInfo["commitId"] = "f780f0cd18eebaf19274a2a2ed1c947085a81773"

    executionContext = {"gitExecutionContext": {"slsSourceEntryPoint": "main.sls"}}
    task.executionContext = json.dumps(executionContext)

    os.environ["IDEMD_HOME"] = os.getcwd()
    os.environ["GIT_PERSONAL_TOKEN"] = "rfS_jML3bus8skDUh5pom"

    mock_clone_repo.return_value = {}
    mock_run_idem_validate.return_value.returncode = 0
    mock_run_idem_validate.return_value.stdout = '{"key": "value"}'
    mock_update_validate.return_value = mock_stub = Mock()
    mock_stub.updateValidateTask.return_value.isUpdated = False

    start_validation(task)

    mock_clone_repo.assert_called_once()
    mock_run_idem_validate.assert_called_once()
    mock_stub.updateValidateTask.assert_called_once()
    logger_error.assert_called_with(f"Task {task.taskIdentity.taskId} update failed.")


@patch("idemd.idemd.validate.validate_v2.logger.info")
@patch("idemd.idemd.validate.validate_v2.get_local_idem_service_stub")
@patch("idemd.idemd.validate.validate_v2.run_idem_validate")
@patch("idemd.idemd.utils.git_utils.clone_repo")
def test_validate_failure(
    mock_clone_repo, mock_run_idem_validate, mock_update_validate, logger_info
):
    task = messages.ValidateInput()

    task_identity = messages.TaskIdentity()
    task_identity.taskId = "test".join(str(uuid.uuid4()))
    task.taskIdentity.CopyFrom(task_identity)

    task.iacGitInfo["repositoryURL"] = "git@gitlab.test-repo.git"
    task.iacGitInfo["branch"] = "master"
    task.iacGitInfo["commitId"] = "f780f0cd18eebaf19274a2a2ed1c947085a81773"

    executionContext = {"gitExecutionContext": {"slsSourceEntryPoint": "main.sls"}}
    task.executionContext = json.dumps(executionContext)

    os.environ["IDEMD_HOME"] = os.getcwd()

    mock_clone_repo.return_value = {}
    mock_run_idem_validate.return_value.returncode = 1
    mock_run_idem_validate.return_value.stdout = "Validate Failed"
    mock_update_validate.return_value = mock_stub = Mock()
    mock_stub.updateValidateTask.return_value.isUpdated = True

    start_validation(task)

    mock_clone_repo.assert_called_once()
    mock_run_idem_validate.assert_called_once()
    mock_stub.updateValidateTask.assert_called_once()
    expected_log_message = (
        f"Task: {task.taskIdentity.taskId}, Validate status: FAILED with return code: 1"
    )
    expected_calls = [call(expected_log_message)]
    logger_info.assert_has_calls(expected_calls, any_order=True)


@patch("idemd.idemd.utils.git_utils.clone_repo")
def test_create_git_workspace(mock_clone_repo):
    task = messages.ValidateInput()

    task_identity = messages.TaskIdentity()
    task_identity.taskId = "test".join(str(uuid.uuid4()))
    task.taskIdentity.CopyFrom(task_identity)

    task.iacGitInfo["repositoryURL"] = "git@gitlab.test-repo.git"
    task.iacGitInfo["branch"] = "master"
    task.iacGitInfo["commitId"] = "f780f0cd18eebaf19274a2a2ed1c947085a81773"

    executionContext = {"gitExecutionContext": {"slsSourceEntryPoint": "main.sls"}}
    task.executionContext = json.dumps(executionContext)

    os.environ["IDEMD_HOME"] = os.getcwd()
    mock_clone_repo.return_value = {}

    folder_path, git_context = create_git_workspace(task)
    assert git_context.repo_url == "git@gitlab.test-repo.git"
    assert git_context.branch_name == "master"
    assert git_context.commit_id == "f780f0cd18eebaf19274a2a2ed1c947085a81773"
    expected_path = os.path.join(os.getcwd(), task.taskIdentity.taskId)
    assert folder_path == expected_path
    clean_workspace(folder_path)


def test_validate_create_git_workspace_without_git_credentials():
    task = messages.ValidateInput()

    task_identity = messages.TaskIdentity()
    task_identity.taskId = "test".join(str(uuid.uuid4()))
    task.taskIdentity.CopyFrom(task_identity)

    with unittest.TestCase().assertRaises(Exception) as context:
        folder_path, git_context = create_git_workspace(task)

    unittest.TestCase().assertEqual(str(context.exception), "Only Git is supported.")
