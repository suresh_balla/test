"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import unittest
from concurrent import futures

from idemd.idemd.utils.worker_threadpool_executor import WorkerThreadPoolExecutor


def dummy_task(arg):
    return arg


class TestWorkerThreadPoolExecutor(unittest.TestCase):
    def test_submit_without_initializer(self):
        executor = WorkerThreadPoolExecutor()
        future = executor.submit(dummy_task, "no_init")
        result = future.result()
        self.assertEqual(result, "no_init")
        self.assertIsInstance(future, futures.Future)

    def test_submit_with_initializer(self):
        executor = WorkerThreadPoolExecutor(initializer=self.dummy_initializer)
        future = executor.submit(dummy_task, "init")
        result = future.result()
        self.assertEqual(result, "init")
        self.assertIsInstance(future, futures.Future)

    def dummy_initializer(self):
        pass


if __name__ == "__main__":
    unittest.main()
