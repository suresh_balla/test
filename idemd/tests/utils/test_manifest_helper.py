"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import unittest
from unittest.mock import patch

from idemd.idemd.utils.manifest_helper import ManifestFactory


class ManifestFactoryTest(unittest.TestCase):
    @patch("builtins.open")
    @patch.dict("os.environ", {"MANIFEST_MOUNT_PATH": "/path/to/manifest.yaml"})
    def test_load_manifest_success(self, mock_open):
        # Mock the file content
        mock_file_content = """
        key1: value1
        key2: value2
        """

        mock_open.return_value.__enter__.return_value = mock_file_content

        # Call the load_manifest method
        manifest = ManifestFactory.load_manifest()

        # Assert the loaded manifest
        expected_manifest = {"key1": "value1", "key2": "value2"}
        self.assertEqual(manifest, expected_manifest)

    @patch("builtins.open")
    @patch.dict("os.environ", {"MANIFEST_MOUNT_PATH": "/path/to/manifest.yaml"})
    def test_get_instance_singleton(self, mock_open):
        # Mock the file content
        mock_file_content = """
        key1: value1
        key2: value2
        """

        mock_open.return_value.__enter__.return_value = mock_file_content

        # Call get_instance() multiple times
        instance1 = ManifestFactory.get_instance()
        instance2 = ManifestFactory.get_instance()

        # Assert the instances are the same
        self.assertIs(instance1, instance2)


if __name__ == "__main__":
    unittest.main()
