"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import os
import unittest.mock as mock

from idemd.idemd.utils.backend_utils import resolve_backends


def test_resolve_backends_invalid_acct_details():
    task = mock.Mock()
    task.credentialParameters = '{"param1": "value1"}'

    os.environ["BACKEND_CONFIG_FILE"] = "backend.yaml"

    backends = resolve_backends(task)

    assert backends == {}


def test_resolve_backends():
    task = mock.Mock()
    task.credentialParameters = '{"accountProfiles": "value1"}'
    output_file = "backend.yaml"
    os.environ["BACKEND_CONFIG_FILE"] = output_file

    import yaml

    data = {"acct-backends": {"default": {"test": "test"}}}

    # Writing the data to a YAML file

    with open(output_file, "w") as file:
        yaml.dump(data, file)

    backends = resolve_backends(task)

    assert {} != backends
    assert "default" in backends.keys()

    # Deleting the file
    os.remove(output_file)
