"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import unittest
from unittest.mock import patch

from idemd.idemd.utils.git_credential_loader import GitAuthCredentials
from idemd.idemd.utils.git_utils import (
    clone_repo,
    GitCloneContext,
    clone_or_update_repo,
)


class TestCloneOrUpdateRepo(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.context = GitCloneContext(
            "https://github.com/example/repo.git",
            "/path/to/repo",
            GitAuthCredentials(),
            None,
            None,
        )

    def tearDown(self) -> None:
        self.context.credentials.ssh_key_path = None
        self.context.credentials.username = None
        self.context.credentials.password = None
        self.context.credentials.personal_token = None
        self.context.credentials.project_token = None

    @patch("os.path.exists")
    @patch("idemd.idemd.utils.git_utils.clone_repo")
    def test_clone_or_update_repo_new_repo(self, mock_clone_repo, mock_path_exists):
        mock_path_exists.return_value = False
        mock_clone_repo.return_value = {}

        updated_context = clone_or_update_repo(self.context)

        mock_clone_repo.assert_called_once_with(updated_context)
        self.tearDown()

    @patch("git.repo.base.Repo.clone_from")
    def test_clone_repo_with_username_password(self, mock_clone_from):
        self.context.credentials.username = "username"
        self.context.credentials.password = "password"

        clone_repo(self.context)

        mock_clone_from.assert_called_once_with(
            f"https://username:password@{self.context.repo_url.replace('https://', '')}",
            self.context.clone_path,
        )
        self.tearDown()

    @patch("git.repo.base.Repo.clone_from")
    def test_clone_repo_with_ssh_key(self, mock_clone_from):
        self.context.credentials.ssh_key_path = "/path/to/ssh_key"

        clone_repo(self.context)

        mock_clone_from.assert_called_once_with(
            self.context.repo_url,
            self.context.clone_path,
            env={"GIT_SSH_COMMAND": f"ssh -i {self.context.credentials.ssh_key_path}"},
        )
        self.tearDown()

    @patch("git.repo.base.Repo.clone_from")
    def test_clone_repo_with_personal_token(self, mock_clone_from):
        self.context.credentials.personal_token = "personal_token"

        clone_repo(self.context)

        mock_clone_from.assert_called_once_with(
            f"https://oauth2:{self.context.credentials.personal_token}@{self.context.repo_url.replace('https://', '')}",
            self.context.clone_path,
        )
        self.tearDown()

    @patch("git.repo.base.Repo.clone_from")
    def test_clone_repo_with_project_token(self, mock_clone_from):
        self.context.credentials.project_token = "project_token"

        clone_repo(self.context)

        mock_clone_from.assert_called_once_with(
            f"https://worker:{self.context.credentials.project_token}@{self.context.repo_url.replace('https://', '')}",
            self.context.clone_path,
        )
        self.tearDown()


if __name__ == "__main__":
    unittest.main()
