"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

from unittest.mock import patch

from hvac.exceptions import VaultError, InvalidPath, Forbidden

from idemd.idemd.utils.vault_helper import retrieve_secrets


@patch("hvac.Client")
def test_retrieve_secrets_valid_path(client_mock):
    client_mock.return_value.secrets.kv.v2.read_secret_version.return_value = {
        "data": {"data": {"username": "testuser", "password": "testpassword"}}
    }

    # Call the method being tested
    result = retrieve_secrets("http://vault_address", "vault_token", "valid_path")

    # Assert the expected result
    assert result == {"username": "testuser", "password": "testpassword"}


@patch("hvac.Client")
def test_retrieve_secrets_invalid_path(client_mock):
    client_mock.return_value.secrets.kv.v2.read_secret_version.side_effect = InvalidPath

    result = retrieve_secrets("http://vault_address", "vault_token", "invalid_path")

    assert result == {}


@patch("hvac.Client")
def test_retrieve_secrets_forbidden(client_mock):
    client_mock.return_value.secrets.kv.v2.read_secret_version.side_effect = Forbidden

    result = retrieve_secrets("http://vault_address", "vault_token", "path")

    assert result == {}


@patch("hvac.Client")
def test_retrieve_secrets_vault_error(client_mock):
    client_mock.return_value.secrets.kv.v2.read_secret_version.side_effect = VaultError

    result = retrieve_secrets("http://vault_address", "vault_token", "path")

    assert result == {}
