"""
Copyright (c) 2023 VMware, Inc. All Rights Reserved.
"""

import os

from idemd.scripts import start, start_with_hub

if __name__ == "__main__":
    if os.getenv("SKIP_HUB") is not None and os.getenv("SKIP_HUB").lower() == "true":
        start()
    else:
        start_with_hub()
