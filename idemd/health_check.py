import os
import sys

import grpc

from idemd.idemd.utils.iac_log_utils import get_default_logger

logger = get_default_logger()

SERVICE_CALL_TIMEOUT_IN_SECONDS = 3


def check_service_health():
    worker_host = os.getenv("WORKER_HOST", "localhost")
    worker_port = os.getenv("WORKER_PORT", "4040")

    # Optional: Set a timeout for the connection attempt
    grpc_channel_options = [
        (
            "grpc.connect_timeout_ms",
            1000 * SERVICE_CALL_TIMEOUT_IN_SECONDS,
        )  # Set the timeout in milliseconds
    ]
    channel = grpc.insecure_channel(
        f"localhost:{int(worker_port)}", options=grpc_channel_options
    )

    try:
        grpc.channel_ready_future(channel).result(
            timeout=SERVICE_CALL_TIMEOUT_IN_SECONDS
        )
        logger.info("Connection established. Service is active.")
        return 0
    except grpc.FutureTimeoutError:
        logger.error("Timeout occurred. Service is not reachable.")
        return 1
    except Exception as e:
        logger.warning("An error occurred while connecting:", str(e))
        return 1


if __name__ == "__main__":
    result = check_service_health()
    sys.exit(result)
