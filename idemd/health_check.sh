#!/bin/bash

set -e

CHANNEL_REFRESH_INTERVAL="${CHANNEL_REFRESH_INTERVAL:-"43200"}"
LIVENESS_FILE="${LIVENESS_FILE:-"live"}"

if [[ ! -f "$LIVENESS_FILE" ]]; then
    echo "Liveness file $LIVENESS_FILE does not exist!"
    exit 1
fi

# Threshold = 1.5 refresh interval, accounting for the fact there are no floating point operations in bash
liveness_age_threshold_sec="$((15 * $CHANNEL_REFRESH_INTERVAL / 10))"

liveness_file_age_sec="$(( $(date +%s) - $(stat --format=%Y "$LIVENESS_FILE") ))"
echo "Liveness file $LIVENESS_FILE is $liveness_file_age_sec seconds old"

result=0
if (( $liveness_file_age_sec >= $liveness_age_threshold_sec )); then
    echo "Liveness file $LIVENESS_FILE is older than $liveness_age_threshold_sec seconds"
    result=1
fi

echo "Return code of health check is ${result}"
exit $result
