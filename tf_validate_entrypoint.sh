#!/bin/bash -i

# Fail on errors
set -e

# Be receptive to core dumps
ulimit -c unlimited

# Allow high connection count per process (raise file descriptor limit)
ulimit -n 65536

echo "Terraform Validate worker"

python "$IDEMD_HOME"/idemd/run.py --log-fmt-console="%(asctime)s.%(msecs)06d [%(filename)s:%(lineno)s - %(funcName)2s()] [%(levelname)s] [%(processName)s %(threadName)s] %(message)s" --log-level=info --log-datefmt="%Y-%m-%d %H:%M:%S" "$@"