#!/bin/bash

# Copyright (c) 2024 Broadcom. All Rights Reserved.

#set -x

MAX_MERGES=5

MY_PATH=$(dirname "$0")

. "${MY_PATH}/git-common.sh"

function jsonizer() {
    local filter=''
    if [ ! -z "${REPOSITORY_EXCLUDE}" ]
    then
        filter="select($2|test(\"${REPOSITORY_EXCLUDE}\")|not) | "
    fi
    if [ ! -z "${REPOSITORY_INCLUDE}" ]
    then
        filter="${filter}select($2|test(\"${REPOSITORY_INCLUDE}\")) | "
    fi

    jq "{ \"repos\": [ .[] | ${filter} {\"id\": $1?, \"path\": $2?, \"skip\": $3, \"url\": $4?, \"ssh_url\": $5?, \"default_branch\": $6?, \"worker_group_id\": \"${WORKER_GROUP_ID}\", \"endpoint_url\": \"${REPOSITORY_ENDPOINT}\", \"endpoint_type\": \"${REPOSITORY_ENDPOINT_TYPE}\", \"meta\": { \"language\": $7 } } ] }"
}

function merger() {
    jq -s add
}

function url_encode() {
    echo "$1" | jq -Rr @uri
}

function process_recursively() {
    local next="$1"
    local first=1
    local getpage="$2"
    local getnext="$3"
    local getjson="$4"
    local getcommit="$5"
    local contents=""
    local page_number=1
    local merge_count=0
    local temp_dir=$(mktemp -d --suffix -git-collector)

    while [ $first -eq 1 -o -n "$next" ]
    do
        first=0
        if [ $merge_count -eq $MAX_MERGES ]
        then
            echo "$contents" | process_contents $page_number $temp_dir
            contents=""
            merge_count=0
            (( page_number++ ))
        fi

        print_debug "Getting page: $next"
        local repos=$(exec_with_retry $getpage $next | $getjson)

        local commits='[]'
        local oifs="$IFS"
        local line
        while read line
        do
            if [ -z "$line" ]
            then
                continue
            fi

            local id skip branch
            IFS=':' read id path skip branch <<< "$line"

            if [ "$skip" = "true" -o -z "$branch" ]
            then
                print_debug "Skipping collection of commit details for $path"
            else
                commits=$( ( echo "$commits" ; exec_with_retry $getcommit "$id" "$branch" ) | merger )
            fi
        done <<< $(echo $repos | jq -r '.repos[] | (.id|tostring) + ":" + (.path|tostring) + ":"  + (.skip|tostring) + ":" + ."default_branch"')
        IFS="$oifs"
        
        repos=$( ( echo "$repos"; echo "$commits" ) | jq --slurp '{ "repos": [ .[0].repos, .[1] ] | transpose | map(add) }' )
        
        contents=$( ( echo "$repos" ; echo "$contents" ) | jq --slurp '{ "repos": ( .[0].repos + .[1].repos ) }' )
        next=$(exec_with_retry $getnext $next)
        (( merge_count++ ))
    done

    echo "$contents" | process_contents $page_number $temp_dir
    echo "Output Directory: $temp_dir"
}

function github_getpage() {
    print_debug "Getting github repo list for page: $1"
    local topic=''
    if [ ! -z "${REPOSITORY_TOPICS}" ]
    then
        topic="--topic ${REPOSITORY_TOPICS}"
    fi

    gh repo list ${topic} -L 10000 --json nameWithOwner,url,sshUrl,defaultBranchRef,languages $1
}

function github_getnext() {
    eval echo "\$GH_$1"
}

function github_jsonizer() {
    print_debug "Jsonizing github repo list"
    jsonizer \
        .nameWithOwner \
        .nameWithOwner \
        false \
        .url \
        .sshUrl \
        .defaultBranchRef.name \
        '.languages | ( if (reduce .[] as $item (false; . or $item.node.name == "Java") ) == true then "java" else "unknown" end )'
}

function github_commit() {
    print_debug "Getting github commit for repo: $1 and branch: $2"
    # Return null in case there is no default branch
    if [ -z "$2" ]
    then
        print_debug "No default branch for $1"
        echo '[ { "commit_id" : null, "committed_timestamp": null } ]'
        return
    fi

    gh api "/repos/$1/commits?sha=$2&per_page=1" | jq '[ .[] | { "commit_id": .sha, "committed_timestamp": .commit.committer.date, "committed_by": .commit.author.name } ]'
}

function process_github() {
    # Documentation: https://cli.github.com/manual/gh
    export GH_HOST=$(echo "$REPOSITORY_ENDPOINT" | sed -e 's/[^/]*\/\/\([^@]*@\)\?\([^:/]*\).*/\2/')
    export GH_TOKEN="$REPOSITORY_ENDPOINT_TOKEN"

    print_debug "Processing github repos for host $GH_HOST"

    local prev=""
    while read org
    do
        eval GH_$prev=$org
        prev=$org
    done <<< $(gh org list | grep -v "Showing [0-9]* of [0-9]* organizations")
    
    process_recursively "$REPOSITORY_ENDPOINT_PAGE" github_getpage github_getnext github_jsonizer github_commit
}

function gitlab_curler() {
    print_debug "Running command: $1 --request GET --header PRIVATE-TOKEN: ${REPOSITORY_ENDPOINT_TOKEN} -s ${REPOSITORY_ENDPOINT}/api/v4/$2"
    $1 --request GET \
        --header "PRIVATE-TOKEN: ${REPOSITORY_ENDPOINT_TOKEN}" \
        -s "${REPOSITORY_ENDPOINT}/api/v4/$2"
}

function gitlab_curl_projects() {
    local topic=''
    if [ ! -z "${REPOSITORY_TOPICS}" ]
    then
        topic="&topic=$(url_encode ${REPOSITORY_TOPICS})"
    fi

    gitlab_curler "$1" "projects?simple=false&membership=true&page=$2${topic}"
}

function gitlab_getpage() {
    gitlab_curl_projects curl $1
    #local resuts=$(gitlab_curler curl $1 | gitlab_jsonizer)
}

function gitlab_getnext() {
    gitlab_curl_projects "curl -I" $1 | grep X-Next-Page | sed -e 's/\r$//'| sed -e 's/.*X-Next-Page: \([0-9]*\)\s*/\1/'
}

function gitlab_jsonizer() {
    jsonizer .id .path_with_namespace .empty_repo .http_url_to_repo .ssh_url_to_repo .default_branch '"new"'
}

function gitlab_commit() {
    if [ -z "$2" ]
    then
        print_debug "No default branch for $1"
        echo '[ { "commit_id" : null, "committed_timestamp": null } ]'
        return
    fi

    local ref=$(url_encode "$2")
    local commits languages

    languages=$(gitlab_curler curl "projects/$1/languages")
    print_debug "Languages for project $1 : ${languages}."

    commits=$(gitlab_curler curl "projects/$1/repository/commits?ref_name=${ref}")
    print_debug "Commits for project $1 : ${commits}."

    message=$(echo "$commits" | jq -r .message)

    if echo $commits | jq -r '.message' | grep -q "Forbidden"; then
        echo '[ { "commit_id" : null, "committed_timestamp": null } ]'
    elif echo $commits | jq -r '.message' | grep -q "Internal Server"; then
        echo '[ { "commit_id" : null, "committed_timestamp": null } ]'
    else
        echo "$languages" "$commits" | jq --slurp '[ { "commit_id": .[1] | .[0].id, "committed_timestamp": .[1] | .[0].committed_date, "committed_by": .[1] | .[0].author_name, "meta": { "language": (if .[0] | .Java then "java" else "unknown" end ) } } ]'
    fi
}

function process_gitlab() {
    # API documentation: https://docs.gitlab.com/ee/api/projects.html
    print_debug "Processing gitlab repos for host ${REPOSITORY_ENDPOINT}"
    process_recursively "$REPOSITORY_ENDPOINT_PAGE" gitlab_getpage gitlab_getnext gitlab_jsonizer gitlab_commit
}

function usage() {
    cat << EOF
    Usage: "$0" [ -r endpoint -t type -k token ]
    Options:
        -r endpoint     the git service endpoint
        -t type         the git service type. Possible values: github, gitlab
        -k token        the git access token to be used
        -p topics       topics to be included
        -i regex        repo paths to include
        -x regex        repo paths to exclude
EOF

    exit $1
}

function args() {
    local what=""
    while [ $# -gt 0 ]
    do
        if [ -n "$what" ]
        then
            case $what in
                r) REPOSITORY_ENDPOINT="$1" ;;
                t)
                    if [ "$1" != "gitlab" -a "$1" != "github" ]
                    then
                        print_error "Unsupported Repository Endpoint Type: $1"
                        usage 1
                    fi

                    REPOSITORY_ENDPOINT_TYPE="$1"
                    ;;
                k) REPOSITORY_ENDPOINT_TOKEN="$1" ;;
                i) REPOSITORY_INCLUDE="$1" ;;
                x) REPOSITORY_EXCLUDE="$1" ;;
                p) REPOSITORY_TOPICS="$1" ;;
            esac

            shift
            what=""
            continue
        fi

        case $1 in
            -r|--repository-endpoint)
                what=r
                shift
                ;;
            -t|--repository-endpoint-type)
                what=t
                shift
                ;;
            -k|--repository-endpoint-token)
                what=k
                shift
                ;;
            -i|--include)
                what=i
                shift
                ;;
            -x|--exclude)
                what=x
                shift
                ;;
            -p|--topics)
                what=p
                shift
                ;;
            -h|--help)
                usage 0
                ;;
            *)
                print_error "Unsupported parameter: $1"
                usage 1
                ;;
        esac
    done

    if [ -z "$REPOSITORY_ENDPOINT_TYPE" ]
    then
        print_error "Missing Repository Endpoint Type"
        usage 1
    elif [ -z "$REPOSITORY_ENDPOINT" ]
    then
        print_error "Missing Repository Endpoint"
        usage 1
    elif [ -z "$REPOSITORY_ENDPOINT_TOKEN" ]
    then
        print_error "Missing Repository Endpoint Token"
        usage 1
    fi
}

REPOSITORY_ENDPOINT_TYPE="$GIT_TYPE"
REPOSITORY_ENDPOINT="$GIT_ENDPOINT"
REPOSITORY_ENDPOINT_TOKEN="$GIT_PERSONAL_TOKEN"
REPOSITORY_ENDPOINT_PAGE="$GIT_PAGE"

args "$@"

case "$REPOSITORY_ENDPOINT_TYPE" in
    github)
        if [ -z "$REPOSITORY_ENDPOINT_PAGE" ]
        then
            REPOSITORY_ENDPOINT_PAGE=""
        fi
        process_github ;;
    gitlab)
        if [ -z "$REPOSITORY_ENDPOINT_PAGE" ]
        then
            REPOSITORY_ENDPOINT_PAGE="1"
        fi
        process_gitlab ;;
esac
