#!/bin/bash

# Copyright (c) 2024 Broadcom. All Rights Reserved.

#set -x

MAX_RETRIES=3
KILL_ME_PID=$$

. "${LIBRARY_DIRECTORY}/library.sh"

function print_output_file() {
    echo "Debug: $1" 1>&2
    echo "Output File: $1"
}

function process_contents() {
    print_debug "Processing merged page number: $1"
    local output="$2/$1.json"
    cat > "$output"

    print_output_file "${output}"
}

function prepare_for_upload() {
    local event_type="$1"
    local content=$(cat "$2" | gzip -c | base64 -w0)
    print_debug "Content for event_type: $event_type from file: $2 is zipped and encoded. Ready for upload."
    local output="$3"
    if [ -n "$ENTITY_ID" ]
    then
      RECORD_ENTITY_ID="\"entity_id\": \"${ENTITY_ID}\","
    fi

    if [ -n "$ENDPOINT_ID" ]
    then
      RECORD_ENDPOINT_ID="\"endpoint_id\": \"${ENDPOINT_ID}\","
    fi

    cat >"${output}" << EOF
{
  "event_type": "${event_type}",
  "task_id": "${TASK_ID}",
  "org_id": "${ORG_ID}",
  "project_id": "${PROJECT_ID}",
  "content_type": "application/gzip",
  ${RECORD_ENDPOINT_ID}
  ${RECORD_ENTITY_ID}
  "content": "${content}"
}
EOF
}

function upload_git_data() {
    upload git-events "$1"
}
