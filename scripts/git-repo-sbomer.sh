#!/bin/bash
#set -x
#Copyright (c) 2024 Broadcom, Inc. All Rights Reserved.

MY_PATH=$(dirname "$0")

. "${MY_PATH}/git-common.sh"

function git_clone() {
    local location="$1"
    local repo_name="$2"
    local git_url="$3"
    local branch="$4"

    if [ ! -d "$location/$repo_name/.git" ]
    then
        cd "$location"
        exec_with_retry git clone "${git_url}" --single-branch --branch "${branch}"
        cd -
    fi
}

function git_checkout() {
    cd "$1"
    if [ ! -z "$3" -a "$3" != "latest" ]
    then
        git checkout "$3"
    else
        REPOSITORY_COMMIT_ID=$(git log --pretty=oneline -n 1 | awk '{print $1}')
    fi
    cd -
}

function process_output() {
    local in_data="$1"
    local output_location="$2"
    local filename="$3"

    local sbom_data
    local repo_language
    if [ -z "${in_data}" ]
    then
        sbom_data="null"
        repo_language="unknown"
    else
        sbom_data=$(cat "${in_data}")
        repo_language="java"
    fi

    cat << EOF | process_contents "${filename}" "${output_location}"
{   
    "git_endpoint_url": "${REPOSITORY_ENDPOINT}",
    "git_path": "${REPO_PATH}",
    "branch": "${REPO_BRANCH}",
    "commit_id": "${REPOSITORY_COMMIT_ID}",
    "sbom_data": ${sbom_data},
    "meta": { "language": "${repo_language}" }
}
EOF
}

function git_assessment_type() {
    if [ -f "$1/build.gradle" ]
    then
        echo 'gradle'
    elif [ -f "$1/pom.xml" ]
    then
        echo 'mvn'
    else
        process_output "" "$2" "$3"
    fi
}

function usage() {
    cat <<EOF
    Usage: "$0" [ -r endpoint -t type -k token ] -p path -b branch [ -c commit-id ]
    Options:
        -r endpoint     the git service endpoint
        -t type         the git service type. Possible values: github, gitlab
        -k token        the git access token to be used
        -p path         the git path to be used
        -b branch       the git branch to be used
        -c commit-id    the git commit-id to be used
EOF

    exit $1
}

function args() {
    local what=""
    while [ $# -gt 0 ]; do
        if [ -n "$what" ]; then
            case $what in
            r) REPOSITORY_ENDPOINT="$1" ;;
            t)
                if [ "$1" != "gitlab" -a "$1" != "github" ]; then
                    print_error "Unsupported Repository Endpoint Type: $1"
                    usage 1
                fi

                REPOSITORY_ENDPOINT_TYPE="$1"
                ;;
            k) REPOSITORY_ENDPOINT_TOKEN="$1" ;;
            p) REPO_PATH="$1" ;;
            b) REPO_BRANCH="$1" ;;
            c) REPOSITORY_COMMIT_ID="$1" ;;
            esac

            shift
            what=""
            continue
        fi

        case $1 in
        -r | --repository-endpoint)
            what=r
            shift
            ;;
        -t | --repository-endpoint-type)
            what=t
            shift
            ;;
        -k | --repository-endpoint-token)
            what=k
            shift
            ;;
        -p | --repository-path)
            what=p
            shift
            ;;
        -b | --repository-branch)
            what=b
            shift
            ;;
        -c | --repository-commit-id)
            what=c
            shift
            ;;
        -h | --help)
            usage 0
            ;;
        *)
            print_error "Unsupported parameter: $1"
            usage 1
            ;;
        esac
    done

    if [ -z "$REPOSITORY_ENDPOINT_TYPE" ]; then
        print_error "Missing Repository Endpoint Type"
        usage 1
    elif [ -z "$REPOSITORY_ENDPOINT" ]; then
        print_error "Missing Repository Endpoint"
        usage 1
    elif [ -z "$REPOSITORY_ENDPOINT_TOKEN" ]; then
        print_error "Missing Repository Endpoint Token"
        usage 1
    fi
    if [ -z "$REPO_PATH" ]; then
        print_error "Missing Repository PATH"
        usage 1
    elif [ -z "$REPO_BRANCH" ]; then
        print_error "Missing Repository Endpoint Token"
        usage 1
    fi
}

function main() {
    local endpoint_host=$(echo "${GIT_ENDPOINT}" | sed -e 's%.*//\(.*\)%\1%')
    local git_directory="${WORKING_DIRECTORY}/git/${endpoint_host}"
    local asset_directory="${TASK_DIRECTORY}/asset"
    local repo_directory="${git_directory}/${REPO_PATH}"

    local url="https://oauth2:${REPOSITORY_ENDPOINT_TOKEN}@${endpoint_host}/${REPO_PATH}"

    mkdir -p "${repo_directory}" "${asset_directory}"

    local repo_name=$(basename "${REPO_PATH}" .git)
    local repo_clone_directory="${repo_directory}/${repo_name}"

    print_debug "Cloning Repository: ${repo_name} Branch: ${REPO_BRANCH} in directory: ${repo_clone_directory}."

    git_clone "${repo_directory}" "${repo_name}" "${url}" "${REPO_BRANCH}"
    git_checkout "${repo_clone_directory}" "${REPO_BRANCH}" "${REPOSITORY_COMMIT_ID}"

    local type=$(git_assessment_type "${repo_clone_directory}" "${asset_directory}" "${TASK_ID}")

    print_debug "Starting assessment for: ${repo_directory} with type: ${type}"
    "${MY_PATH}/cyclone-run-${type}-assessment.sh" -b "${repo_clone_directory}" -o "${asset_directory}" 1>&2
    local status=$?
    local assessment_file=$(ls -1 ${asset_directory}/assessment/*.json)

    if [ "$status" -ne 0 ] && [ "$type" = "gradle" ];
    then
      export JAVA_HOME=$JAVA_11_PATH
      export PATH=$JAVA_HOME/bin:$PATH
      print_debug "Using java version : $(java --version) for build"
      "${MY_PATH}/cyclone-run-${type}-assessment.sh" -b "${repo_clone_directory}" -o "${asset_directory}" 1>&2
      status=$?
      assessment_file=$(ls -1 ${asset_directory}/assessment/*.json)
    fi

    rm -rf "${repo_directory}"
    if [ ${status} -eq 0 -a -f "${assessment_file}" ]
    then
        print_debug "Processing assessment: ${assessment_file}"
        process_output "${assessment_file}" "${asset_directory}" "${TASK_ID}"
    else
        print_error "Failed to generate sbom"
        exit 1
    fi
}

REPOSITORY_ENDPOINT_TYPE="$GIT_TYPE"
REPOSITORY_ENDPOINT="$GIT_ENDPOINT"
REPOSITORY_ENDPOINT_TOKEN="$GIT_PERSONAL_TOKEN"

args "$@"
main
