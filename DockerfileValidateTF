FROM harbor-repo.vmware.com/dockerhub-proxy-cache/library/photon:4.0-20231028

RUN tdnf update -y  \
    && tdnf install -y gcc python3 python3-pip python3-devel build-essential linux-api-headers git \
    && tdnf clean all

ENV VALIDATE_MODE "true"
ENV SKIP_HUB "true"

# Define an environment variable to control Go installation
ENV ENABLE_TF_CONFIG_INSPECT "false"

# Conditionally install Go and tf-config-inspect based on the value of ENABLE_TF_CONFIG_INSPECT
RUN if [ "$ENABLE_TF_CONFIG_INSPECT" = "true" ]; then \
    # Install Go
    ENV GO_VERSION 1.20.5 \
    && curl -O https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz && \
    tar -xvf go${GO_VERSION}.linux-amd64.tar.gz && \
    mv go /usr/local && \
    rm go${GO_VERSION}.linux-amd64.tar.gz \
    \
    # Set Go environment variables
    && ENV GOROOT /usr/local/go \
    && ENV GOPATH /go \
    && ENV PATH $GOROOT/bin:$GOPATH/bin:$PATH \
    \
    # Create a workspace directory
    && mkdir -p $GOPATH/src $GOPATH/bin \
    \
    # Set the working directory
    && WORKDIR $GOPATH \
    \
    # Print Go version as a sanity check
    # Install terraform-config-inspect for validate use cases
    && go version && \
    go install github.com/hashicorp/terraform-config-inspect@v0.0.0-20230614215431-f32df32a01cd \
    && terraform-config-inspect --json; \
fi


ENV IDEMD_HOME=/opt/idemd PIP_CONFIG_FILE=$IDEMD_HOME/pip.conf

COPY setup_security.sh $IDEMD_HOME/setup_security.sh
COPY pip.conf $IDEMD_HOME/pip.conf
ADD idemd $IDEMD_HOME/idemd
ADD tf_validate_entrypoint.sh $IDEMD_HOME/tf_validate_entrypoint.sh

RUN python -m pip install --upgrade pip  \
    && python -m pip install setuptools==59.0.1 asyncpg==0.26.0 wheel==0.37.1 poetry==1.7.1 \
    && python -m pip install -r $IDEMD_HOME/idemd/requirements/idem-grpc.txt \
    && python -m pip install -r $IDEMD_HOME/idemd/requirements/idem-guardrails.txt \
    && chmod +x $IDEMD_HOME/tf_validate_entrypoint.sh  \
    && mkdir $IDEMD_HOME/worker_info \
    && chmod +x $IDEMD_HOME/worker_info 

# Always install the current project at the end

# poetry by default creates a virtual env. This will disable it
RUN cd $IDEMD_HOME/idemd  \
    && python -m poetry config virtualenvs.create false  \
    && python -m poetry install --no-root --no-dev  \
    && python -m pip install $IDEMD_HOME/idemd --no-dependencies

# Set the FIPS_MODE build argument
ARG FIPS_MODE

# Set the default value for FIPS_MODE as "disabled" if not provided
ENV FIPS_MODE=${FIPS_MODE:-disabled}

RUN chmod +x $IDEMD_HOME/setup_security.sh && $IDEMD_HOME/setup_security.sh

WORKDIR $IDEMD_HOME
ENTRYPOINT ["./tf_validate_entrypoint.sh"]