#!/bin/bash

#Based on the value of the FIPS_MODE  point to the correct version of cryptography library.
#default value of FIPS_MODE is disabled.
FIPS_MODE_DISABLED="disabled"
FIPS_MODE_ENABLED="enabled"
FIPS_MODE_STRICT="strict"
export NON_FIPS_DIR="/non-fips"
export FIPS_DIR="/fips"
export SYSTEM_CRYPTOGRAPHY_LOCATION=$(python -c "import site; print(site.getsitepackages()[0])")

if [ ! -n "$FIPS_MODE" ]
then
  export FIPS_MODE="$FIPS_MODE_DISABLED"
  echo "WARNING: FIPS_MODE environment variable is not set! The default value is set to $FIPS_MODE"
else
  export FIPS_MODE=${FIPS_MODE,,}
fi
echo "FIPS_MODE=$FIPS_MODE"

if ! [[ "$FIPS_MODE" =~ ^($FIPS_MODE_DISABLED|$FIPS_MODE_ENABLED|$FIPS_MODE_STRICT)$ ]]
then
  echo "ERROR: FIPS_MODE environment variable ($FIPS_MODE) is not set correctly! Use $FIPS_MODE_DISABLED/$FIPS_MODE_ENABLED/$FIPS_MODE_STRICT"
  exit 2
fi

if [[ "$FIPS_MODE" =~ ^($FIPS_MODE_ENABLED|$FIPS_MODE_STRICT)$ ]]
then
  echo "FIPS cryptography library location: $FIPS_DIR"
  rm -f $SYSTEM_CRYPTOGRAPHY_LOCATION/cryptography
  ln -s $FIPS_DIR/cryptography $SYSTEM_CRYPTOGRAPHY_LOCATION/cryptography
else
  echo "NON_FIPS cryptography library location: $NON_FIPS_DIR"
  # Disabling FIPS in NON_FIPS mode.
  # Refer https://confluence.eng.vmware.com/display/PHOT/Fips+Enablement+in+Photon
  echo -e "[alg_sect]\ndefault_properties = "-fips"\n" >>/etc/ssl/user.cnf
  rm -f $SYSTEM_CRYPTOGRAPHY_LOCATION/cryptography
  ln -s $NON_FIPS_DIR/cryptography $SYSTEM_CRYPTOGRAPHY_LOCATION/cryptography
fi

IS_FIPS_BACKEND_ENABLED=$(python -c "from cryptography.hazmat.backends.openssl import backend; print(backend._fips_enabled)")
echo "IS_FIPS_BACKEND_ENABLED=$IS_FIPS_BACKEND_ENABLED"