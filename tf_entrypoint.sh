#!/bin/bash -i

# Fail on errors
set -e

# Be receptive to core dumps
ulimit -c unlimited

# Allow high connection count per process (raise file descriptor limit)
ulimit -n 65536

echo "Terraform worker"

validate_ebs_client_file_env_vars() {
  if [ -z "$IDEM_AUTH_CLIENT_ID" ] || [ -z "$IDEM_AUTH_CLIENT_SECRET" ];
  then
    echo "Missing IDEM_AUTH_CLIENT_ID or IDEM_AUTH_CLIENT_ID env var."
    exit 1
  fi
  if [ -z "$CSP_URL" ] || [ -z "$EBS_URL" ];
  then
    echo "Missing CSP_URL or EBS_URL env var."
    exit 1
  fi
}

if [ "$BACKEND_CONFIG_FILE" == "" ]
then
  echo "current run is not a helm deployment and BACKEND_CONFIG_FILE variable not set. skipping the encryption of backend.yaml"
else
  echo "BACKEND_CONFIG_FILE is set to $BACKEND_CONFIG_FILE"
fi

echo "TASK_MANAGER_HOST: ${TASK_MANAGER_HOST}"
echo "IDEM_SERVICE_HOST: ${IDEM_SERVICE_HOST}"
export ENABLE_GRPC_AUTH="true"

python "$IDEMD_HOME"/idemd/run.py --task-manager-host "${TASK_MANAGER_HOST}" --idem-service-host "${IDEM_SERVICE_HOST}" --log-fmt-console="%(asctime)s.%(msecs)06d [%(filename)s:%(lineno)s - %(funcName)2s()] [%(levelname)s] [%(processName)s %(threadName)s] %(message)s" --log-level=info --log-datefmt="%Y-%m-%d %H:%M:%S" "$@"