# Idem Service Worker
* A worker is a container for running group of idem tasks. It contains the below components
    * Worker business logic layer for orchestration of task
      * Responsible for claiming tasks, creating run spaces and concurrent execution of tasks
      * Also updates idem service in case of failures not related to Idem runtime
      * CSP configuration, Message Broker config and any other configuration is available in this layer
    * Runtime Layer - Contains idem plugins – Core plugins, VMware private plugins and public cloud plugins
      * This is execution runtime for any idem state submission
* Has two operating modes SaaS, Remote

# Local Dev Environment Setup
https://confluence.eng.vmware.com/display/CAEng/Idem+Service+Local+Development+Setup

# Local Idem Runtime setup
* `sh dev-tools/create_local_env_without_worker.sh` - This will set up a local python virtual environment will all the plugins required for worker. This will be beneficial if an engineer wants to test any new functionality in a worker like environment
* `sh dev-tools/create_local_env_with_worker.sh` - Additionally, it will install worker business logic


More details about the plugins used in the worker are available here https://confluence.eng.vmware.com/display/CAEng/Plugins+in+Idem+Worker