#!/bin/bash

NON_FIPS_DIR="/non-fips"
FIPS_DIR="/fips"
SYSTEM_CRYPTOGRAPHY_LOCATION=$(python -c "import site; print(site.getsitepackages()[0])")

mkdir $NON_FIPS_DIR
mkdir $FIPS_DIR

#move default cryptography library to NON_FIPS directory
mv $SYSTEM_CRYPTOGRAPHY_LOCATION/cryptography $NON_FIPS_DIR/

#Remove default library from system and install FIPS certified cryptography library from vmware
tdnf remove -y python3-cryptography
tdnf install -y python3-cryptography
tdnf install -y openssl-fips-provider

#move fips certified cryptography library to FIPS directory
mv $SYSTEM_CRYPTOGRAPHY_LOCATION/cryptography $FIPS_DIR/

#by default point to the default cryptography library
ln -s $NON_FIPS_DIR/cryptography $SYSTEM_CRYPTOGRAPHY_LOCATION/cryptography