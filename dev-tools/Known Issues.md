### Issue#1. For Apple Silicon, installing cryptography fails with `#include <openssl/opensslv.h>` error
Fix - Before installing SSEAPE, add these two commands in the shell script
* `export LDFLAGS="-L/opt/homebrew/opt/openssl@1.1/lib"`
* `export CPPFLAGS="-I/opt/homebrew/opt/openssl@1.1/include"` 

SSEAPE is probably using an older version of cryptography, and we have to link with openssl@1.1 instead of openssl@3