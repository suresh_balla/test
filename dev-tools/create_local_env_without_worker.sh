#! /bin/bash

if [[ -z "${IDEMD_HOME}" ]]; then
  echo "IDEMD_HOME is not set. Please set it to the root directory of your local worker git repo."
  exit 1
else
  echo "Worker root directory is ${IDEMD_HOME}"
fi

if [[ -z "${1}" ]]; then
  venv_name=".test"
  echo "Default virtual environment is \"$venv_name\""
else
  venv_name="${1}"
  echo "Virtual environment provided is \"$venv_name\""
fi

# create virtual environment
echo "Creating virtual environment in ${IDEMD_HOME}"
python3 -m venv "$venv_name"

# activate virtual environment
echo "Activating virtual environment \"$venv_name\""
source "$venv_name"/bin/activate

# install modules
echo "Installing private idem plugins in virtual environment"
python3 -m pip install --upgrade pip
python3 -m pip install setuptools==59.0.1 asyncpg==0.26.0 wheel==0.37.1 poetry==1.7.1
python3 -m pip install -r "$IDEMD_HOME"/idemd/requirements/idem-grpc.txt
python3 -m pip install -r "$IDEMD_HOME"/idemd/requirements/idem-cloud-creds.txt
python3 -m pip install -r "$IDEMD_HOME"/idemd/requirements/idem-saas-esm.txt
python3 -m pip install -r "$IDEMD_HOME"/idemd/requirements/idem-secure-state.txt
python3 -m pip install -r "$IDEMD_HOME"/idemd/requirements/idem-ekstoken.txt
python3 -m pip install -r "$IDEMD_HOME"/idemd/requirements/idem-dlp.txt
python3 -m pip install -r "$IDEMD_HOME"/idemd/requirements/idem-guardrails.txt
python3 -m pip install -r "$IDEMD_HOME"/idemd/requirements/idem-tmc.txt
python3 -m pip install -r "$IDEMD_HOME"/idemd/requirements/idem-kubernetes.txt
#export LDFLAGS="-L/opt/homebrew/opt/openssl@1.1/lib"
#export CPPFLAGS="-I/opt/homebrew/opt/openssl@1.1/include"
curl https://build-artifactory.eng.vmware.com/artifactory/vrealizessc-pypi-local/SSEAPE/8.12.2.0/SSEAPE-8.12.2.0-py3-none-any.whl --output SSEAPE-8.12.2.0-py3-none-any.whl
python3 -m pip install SSEAPE-8.12.2.0-py3-none-any.whl
python3 -m pip install -r "$IDEMD_HOME"/idemd/requirements/idem-saltstack.txt
python3 -m pip install -r "$IDEMD_HOME"/idemd/requirements/idem-evbus-ebs.txt
python3 -m pip install -r "$IDEMD_HOME"/idemd/requirements/idem-fips.txt
python3 -m pip install -r "$IDEMD_HOME"/idemd/requirements/idem-eds.txt
python3 -m pip install -r "$IDEMD_HOME"/idemd/requirements/idem-vra-encryption.txt
python3 -m pip install -r "$IDEMD_HOME"/idemd/requirements/idem-evbus-router.txt

echo "Switching directory to install core plugins"
cd "$IDEMD_HOME"/idemd || exit
echo "$PWD"

echo "Installing core idem plugins"
python3 -m poetry config virtualenvs.create false
python3 -m poetry install --no-root --no-dev

# shellcheck disable=SC2140
echo "Activate virtual environment $venv_name using "\"source "$venv_name"/bin/activate""\"

echo "Optionally run 'pip freeze' to check if the plugins are installed correctly"