#! /bin/bash

. dev-tools/create_local_env_without_worker.sh

echo "Installing worker"
python3 -m pip install "$IDEMD_HOME"/idemd --no-dependencies

# shellcheck disable=SC2140
echo "Activate virtual environment $venv_name using "\"source "$venv_name"/bin/activate""\"

echo "Optionally run 'pip freeze' to check if the plugins are installed correctly"