{{ $isDatabaseUpgradeEnabled := include "isDatabaseUpgradeEnabled" . }}
{{- if ne $isDatabaseUpgradeEnabled "true" }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.validateTf.serviceName }}
  labels:
    app: {{ .Values.validateTf.serviceName }}
    product: {{ .Values.product }}
    environment: {{ .Values.environment }}
spec:
  replicas: {{ .Values.idemServiceWorkerValidateTf.replicas }}
  strategy:
    rollingUpdate:
      maxUnavailable: 40%
  revisionHistoryLimit: 2
  selector:
    matchLabels:
      app: "{{ .Values.validateTf.serviceName }}"
      product: {{ .Values.product }}
      environment: {{ .Values.environment }}
  template:
    metadata:
      labels:
        app: {{ .Values.validateTf.serviceName }}
        product: {{ .Values.product }}
        environment: {{ .Values.environment }}
    spec:
      terminationGracePeriodSeconds: {{ .Values.grace_timeout_in_seconds }}
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchExpressions:
                  - key: "app"
                    operator: In
                    values:
                      - "{{ .Values.validateTf.serviceName }}"
                  - key: "build"
                    operator: In
                    values:
                      - "{{ default .Values.global.defaultServiceImageVersion .Values.tag }}"
              namespaces: ["{{ .Release.Namespace }}"]
              topologyKey: kubernetes.io/hostname
      volumes:
      - name: heap-dump
        emptyDir: {}
      - name: application-config
        configMap:
          name: "{{ .Values.serviceName }}-application-config-{{ .Values.tag }}"

      - name: ssl-mount-path
        emptyDir: { }
      - name: health-storage
        emptyDir:
          # Keep the liveness file in a file to enable read-only root FS
          # Keep the volume in memory to avoid overhead from exessive IO on the host's disk
          medium: Memory
      containers:
        - name: {{ .Values.validateTf.serviceName }}
          image: "{{ include "active-registry" . }}/{{ .Values.image.repository }}-validate-tf:{{ .Values.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          livenessProbe:
            exec:
              command:
              - python
              - /opt/idemd/idemd/health_check.py
            initialDelaySeconds: 60
            periodSeconds: 60
          readinessProbe:
            exec:
              command:
              - python
              - /opt/idemd/idemd/health_check.py
            initialDelaySeconds: 60
            periodSeconds: 60
          volumeMounts:
          - mountPath: /var/log
            name: heap-dump
          - mountPath: /opt/idemd/idemd/idemd/idemd/
            name: application-config
            readOnly: false
          - mountPath: /health
            name: health-storage
            readOnly: false
          env:
            - name: APPLICATION_CONFIG_FILE
              value: /opt/idemd/idemd/idemd/idemd/application.yaml
            - name: WAVEFRONT_TAGS
              value: app:{{ .Values.validateTf.serviceName }},environment:{{ .Values.environment }},build:{{ .Values.tag }},namespace_name:{{ .Release.Namespace }}
            - name: WORKER_HOST
              value: {{ .Values.validateTf.serviceName }}.{{ .Release.Namespace }}.svc.cluster.local
            - name: WORKER_PORT
              value: "{{ .Values.validateTf.port }}"
            - name: OPEN_ID_CONFIG_URL
              value: "{{ .Values.validateTf.openIdConfigUrlProp }}"
            - name: TOKEN_ISSUER
              value: "{{ .Values.validateTf.tokenIssuer }}"
            
          ports:
            - containerPort: {{ .Values.validateTf.port }}
          resources:
            limits:
              cpu: {{ .Values.limits.cpu }}
              memory: {{ .Values.limits.memory }}
            requests:
              cpu: {{ .Values.requests.cpu }}
              memory: {{ .Values.requests.memory }}
      restartPolicy: Always
      imagePullSecrets:
        - name: registrysecret
{{- end }}