FROM harbor-repo.vmware.com/dockerhub-proxy-cache/library/photon:4.0-20231028

RUN tdnf update -y  \
    && tdnf install -y gcc python3 python3-pip python3-devel build-essential linux-api-headers wget unzip git \
    && tdnf clean all

ENV IDEMD_HOME=/opt/idemd PIP_CONFIG_FILE=$IDEMD_HOME/pip.conf

COPY setup_security.sh $IDEMD_HOME/setup_security.sh
COPY pip.conf $IDEMD_HOME/pip.conf
ADD idemd $IDEMD_HOME/idemd
ADD tf_entrypoint.sh $IDEMD_HOME/tf_entrypoint.sh

RUN python -m pip install --upgrade pip  \
    && python -m pip install setuptools==59.0.1 asyncpg==0.26.0 wheel==0.37.1 poetry==1.7.1 \
    && python -m pip install -r $IDEMD_HOME/idemd/requirements/idem-grpc.txt \
    && chmod +x $IDEMD_HOME/tf_entrypoint.sh  \
    && mkdir $IDEMD_HOME/worker_info \
    && chmod +x $IDEMD_HOME/worker_info

# Always install the current project at the end

# poetry by default creates a virtual env. This will disable it
RUN cd $IDEMD_HOME/idemd  \
    && python -m poetry config virtualenvs.create false  \
    && python -m poetry install --no-root --no-dev  \
    && python -m pip install $IDEMD_HOME/idemd --no-dependencies

RUN wget https://releases.hashicorp.com/terraform/1.5.0/terraform_1.5.0_linux_amd64.zip  \
    && unzip terraform_1.5.0_linux_amd64.zip && rm terraform_1.5.0_linux_amd64.zip  \
    && mv terraform /usr/bin/terraform

# Set the FIPS_MODE build argument
ARG FIPS_MODE

# Set the default value for FIPS_MODE as "disabled" if not provided
ENV FIPS_MODE=${FIPS_MODE:-disabled}

RUN chmod +x $IDEMD_HOME/setup_security.sh && $IDEMD_HOME/setup_security.sh

WORKDIR $IDEMD_HOME
ENTRYPOINT ["./tf_entrypoint.sh"]